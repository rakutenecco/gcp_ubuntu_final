import MeCab
import sys
import os
import shutil
import re
import collections
from datetime import datetime, timedelta
from collections import Counter

# today
today = datetime.strptime(datetime.strftime(datetime.now(), "%Y%m%d"), "%Y%m%d")

# command line argument
args = sys.argv
category = args[1]
arg_day = datetime.strptime(args[2], "%Y%m%d")
bef_day = str((arg_day - today).days)

exec_date_hyphen = (datetime.now() + timedelta(days=int(bef_day))).date()
exec_date = datetime.strftime(exec_date_hyphen, "%Y%m%d")
create_dir_mon = "src/mecab/FGS301-prtimes/{0}".format(exec_date[0:6])
create_dir_day = "src/mecab/FGS301-prtimes/{0}/{1}".format(exec_date[0:6], exec_date)

try:
    os.makedirs(create_dir_mon)
except FileExistsError:
    pass

try:
    os.makedirs(create_dir_day)
except FileExistsError:
    pass

source_file = "total-data/FGS301-prtimes/{0}/{1}/fgs301-prtimes-{2}.txt".format(exec_date[0:6], exec_date, category)
destination_file = "{0}/fgs301-prtimes-{1}.txt".format(create_dir_day, category)
noun_result_file = "{0}/fgs301-prtimes-{1}-noun-result.txt".format(create_dir_day, category)
verb_result_file = "{0}/fgs301-prtimes-{1}-verb-result.txt".format(create_dir_day, category)
adj_result_file = "{0}/fgs301-prtimes-{1}-adj-result.txt".format(create_dir_day, category)
etc_result_file = "{0}/fgs301-prtimes-{1}-etc-result.txt".format(create_dir_day, category)

# 記事本文のみ抽出したファイル作成
data_list = []
text_list = []
remove_small_list = []
removed_list = []

# 品詞ごとに分析結果を使用頻度が多かった順にファイル出力
def count_elem(class_list, result_file):
    result_list = []
    counter = Counter(class_list)
    for value, count in counter.most_common():
        result_list.append("{0},{1}".format(re.sub(r"[' ]", "", str(value))[1:-1],count))

    f = open(result_file, "w", encoding="utf-8")
    result_list = [str(i) for i in result_list]
    f.write("\n".join(result_list))
    f.close()

f = open(source_file, "r")
text = f.readlines()[3:]

#print(text)

for elem in text:
    data_list.append(elem.split("\t"))

# 記事本文だけ抜き出す
for i in range(len(data_list)):
    
    #text_list.append(re.sub("", "", (re.sub("https?://[\w/:%#\$&\?\(\)~\.=\+\-…]+", "", str(data_list[i][6])))))
    text_work = ""
    text_work = data_list[i][6].translate(str.maketrans({chr(0xFF01 + i): chr(0x21 + i) for i in range(94)}))
    #text_work = re.sub("(http|https)?://[\w/:%#\$&\?\(\)~\.=\+\-…]+", "", str(data_list[i][6]))
    text_work = re.sub("(http|https)?://[\w/:%#\$&\?\(\)~\.=\+\-…]+", "", str(text_work))
    text_work = re.sub("[0-9a-z_./?-]+(@|＠)([0-9a-z-]+\.)+[0-9a-z-]+", "", text_work)
    text_work = re.sub("[0-9a-zA-Z_./?-]+(@|＠)[0-9a-zA-Z_./?-]+", "", text_work)
    text_work = re.sub("[0-9]+-[0-9]+-[0-9]+", "", text_work)
    text_work = re.sub("[0-9]+-[0-9]+", "", text_work)
    text_work = re.sub("[0-9]{4}[\s]{0,1}年", "", text_work)
    text_work = re.sub("[0-9]{4}[\s]{0,1}年度", "", text_work)
    text_work = re.sub("[0-9]{1,2}[\s]{0,1}月", "", text_work)
    text_work = re.sub("[0-9]{1,2}[\s]{0,1}日", "", text_work)
    text_work = re.sub("[0-9]+[\s]{0,1}:[0-9]+[\s]{0,1}", "", text_work)
    text_work = re.sub("[0-9]+[\s]{0,1}－[\s]{0,1}[0-9]+[\s]{0,1}－[\s]{0,1}[0-9]+", "", text_work)
    text_work = re.sub("[0-9]+[\s]{0,1}－[\s]{0,1}[0-9]+[\s]{0,1}", "", text_work)
    text_work = re.sub("[-]{2,}", "", text_work)
    text_work = re.sub("[\s]{2,}", "", text_work)
    text_work = re.sub("[☆]{2,}", "", text_work)
    text_work = re.sub("[★]{2,}", "", text_work)
    text_work = re.sub("[♪]{2,}", "", text_work)
    text_work = re.sub("[‣]{2,}", "", text_work)
    text_work = re.sub("[－]{2,}", "", text_work)
    text_work = re.sub("[_]{2,}", "", text_work)
    text_work = re.sub("[ー]{2,}", "", text_work)
    text_work = re.sub("[･]{2,}", "", text_work)
    text_work = re.sub("[=]{2,}", "", text_work)
    text_work = re.sub("[━]{2,}", "", text_work)
    text_work = re.sub("[〜]{2,}", "", text_work)
    text_work = re.sub("[～]{2,}", "", text_work)
    text_work = re.sub("[-]{2,}", "", text_work)
    text_work = re.sub("[–]{2,}", "", text_work)
    text_work = re.sub("[/]{2,}", "", text_work)
    text_work = re.sub("[?]{2,}", "", text_work)
    text_work = re.sub("[0-9]+[\s]{0,1}位", "", text_work)
    #text_list.append(re.sub(r"[①②③④⑤⑥⑦⑧⑨⑩◯◎￣!<>‥•~➩■●▲▼◆▽◇〇○♬◉↓→*▸▷\"\[\]〔〕「」｢｣〝〟″┓┛┏┗※『』〈〉()≪≫《》━–-．【】〒　 ♡❝❞“”×:;︶˘・〜―⇒︓]", "", text_work))
    text_list.append(re.sub(r"[．①②③④⑤⑥⑦⑧⑨⑩◯◎￣!<>‥•~➩■●▲▼◆▽◇〇○♬◉↓→*▸▷\"\[\]〔〕「」｢｣〝〟″┓┛┏┗※『』〈〉()≪≫《》━–-【】〒　 ♡❝❞“”×:;︶˘・〜―⇒︓]", "", text_work))

    #print(text_list)
# 半角記号の置換
#for i in range(len(text_list)):
#    removed_list.append(re.sub(re.compile("[■◆●▼『』▸★〜「」※ 　・×“”♡¥―【】…˘＜＞（）：:≪≫-／〒＋]","", text_list[i]))

#for i in range(len(remove_small_list)):
#    remove_big_list.append(re.sub(r'[︰-＠]', "", remove_small_list[i]))

# 本文のみ記事ごとに改行して出力
#print(text_list)
f = open(destination_file, "w", encoding="utf-8")
f.write("\n".join(text_list))
f.close()

# 分析結果格納用配列
analyze_list = []
counter_list = []
noun_list = []
verb_list = []
adj_list = []
etc_list = []

f =  open(destination_file,'r')
text = f.read()

#mecab = MeCab.Tagger("-Ochasen -u /usr/share/mecab/dic/ipadic/original.dic -d /var/app/total/src/mecab/mecab-ipadic-neologd")
mecab = MeCab.Tagger("-Ochasen -u /usr/share/mecab/dic/ipadic/original.dic")

analyze = mecab.parse(text)
analyze_list = analyze.split("\n")

# 単語と品詞の配列作成
for i in range(len(analyze_list)):
    try:
        if(analyze_list[i].split("\t")[3].startswith("名詞")):
            noun_list.append((analyze_list[i].split("\t")[0], analyze_list[i].split("\t")[3]))
        elif(analyze_list[i].split("\t")[3].startswith("動詞")):
            verb_list.append((analyze_list[i].split("\t")[0], analyze_list[i].split("\t")[3]))
        elif(analyze_list[i].split("\t")[3].startswith("形容詞")):
            adj_list.append((analyze_list[i].split("\t")[0], analyze_list[i].split("\t")[3]))
        else:
            etc_list.append((analyze_list[i].split("\t")[0], analyze_list[i].split("\t")[3]))
    except:
        None

count_elem(noun_list, noun_result_file)
count_elem(verb_list, verb_result_file)
count_elem(adj_list, adj_result_file)
count_elem(etc_list, etc_result_file)
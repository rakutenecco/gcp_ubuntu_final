import MeCab
import sys
import os
import shutil
import re
import collections
from datetime import datetime, timedelta
from collections import Counter

# today
today = datetime.strptime(datetime.strftime(datetime.now(), "%Y%m%d"), "%Y%m%d")

# command line argument
args = sys.argv
arg_day = datetime.strptime(args[1], "%Y%m%d")
bef_day = str((arg_day - today).days)

exec_date_hyphen = (datetime.now() + timedelta(days=int(bef_day))).date()
exec_date = datetime.strftime(exec_date_hyphen, "%Y%m%d")
create_dir_mon = "src/mecab/FGS301-prtimes/{0}".format(exec_date[0:6])
create_dir_day = "src/mecab/FGS301-prtimes/{0}/{1}".format(exec_date[0:6], exec_date)

try:
    os.makedirs(create_dir_mon)
except FileExistsError:
    pass

try:
    os.makedirs(create_dir_day)
except FileExistsError:
    pass

noun_result_file = "{0}/fgs301-prtimes-noun-result.txt".format(create_dir_day)
verb_result_file = "{0}/fgs301-prtimes-verb-result.txt".format(create_dir_day)
adj_result_file = "{0}/fgs301-prtimes-adj-result.txt".format(create_dir_day)
etc_result_file = "{0}/fgs301-prtimes-etc-result.txt".format(create_dir_day)

# 記事本文のみ抽出したファイル作成
text_list = []
remove_small_list = []
removed_list = []

# 品詞ごとに分析結果を使用頻度が多かった順にファイル出力
def count_elem(class_list, result_file):
    result_list = []
    counter = Counter(class_list)
    for value, count in counter.most_common():
        result_list.append("{0},{1}".format(re.sub(r"[' ]", "", str(value))[1:-1],count))

    f = open(result_file, "w", encoding="utf-8")
    result_list = [str(i) for i in result_list]
    f.write("\n".join(result_list))
    f.close()


# 分析結果格納用配列
analyze_list = []
counter_list = []
noun_list = []
verb_list = []
adj_list = []
etc_list = []

#f =  open(destination_file,'r')
#text = f.read()

#mecab = MeCab.Tagger("-Ochasen -u /usr/share/mecab/dic/ipadic/original.dic -d /var/app/total/src/mecab/mecab-ipadic-neologd")
mecab = MeCab.Tagger("-Ochasen -u /usr/share/mecab/dic/ipadic/original.dic")

#analyze = mecab.parse(text)
analyze = mecab.parse("龍角散、鬼滅の刃が分析できない。龍角散")
analyze_list = analyze.split("\n")

# 単語と品詞の配列作成
for i in range(len(analyze_list)):
    try:
        if(analyze_list[i].split("\t")[3].startswith("名詞")):
            noun_list.append((analyze_list[i].split("\t")[0], analyze_list[i].split("\t")[3]))
        elif(analyze_list[i].split("\t")[3].startswith("動詞")):
            verb_list.append((analyze_list[i].split("\t")[0], analyze_list[i].split("\t")[3]))
        elif(analyze_list[i].split("\t")[3].startswith("形容詞")):
            adj_list.append((analyze_list[i].split("\t")[0], analyze_list[i].split("\t")[3]))
        else:
            etc_list.append((analyze_list[i].split("\t")[0], analyze_list[i].split("\t")[3]))
    except:
        None

count_elem(noun_list, noun_result_file)
count_elem(verb_list, verb_result_file)
count_elem(adj_list, adj_result_file)
count_elem(etc_list, etc_result_file)
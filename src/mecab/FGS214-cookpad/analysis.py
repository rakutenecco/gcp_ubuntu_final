import MeCab
import sys
import pathlib
import os
import shutil
import re
import collections
from datetime import datetime, timedelta
from collections import Counter

# today
#today = datetime.strptime(datetime.strftime(datetime.now(), "%Y%m%d"), "%Y%m%d")

# command line argument
#args = sys.argv
#arg_day = datetime.strptime(args[1], "%Y%m%d")
#bef_day = str((arg_day - today).days)

#exec_date_hyphen = (datetime.now() + timedelta(days=int(bef_day))).date()
#exec_date = datetime.strftime(exec_date_hyphen, "%Y%m%d")
#create_dir_mon = "src/mecab/FGS214-cookpad/{0}".format(exec_date[0:6])
#create_dir_day = "src/mecab/FGS214-cookpad/{0}/{1}".format(exec_date[0:6], exec_date)


#source_file = "src/rakuten/FGS214-cookpad/{0}/{1}/fgs214-cookpad.txt".format(exec_date[0:6], exec_date)
source_file = "src/rakuten/FGS214-cookpad/download_data/fgs214-cookpad_download.txt"
#destination_file = "{0}/fgs214-cookpad.txt".format(create_dir_day)
#noun_result_file = "{0}/fgs214-cookpad-noun-result.txt".format(create_dir_day)
#verb_result_file = "{0}/fgs214-cookpad-verb-result.txt".format(create_dir_day)
#adj_result_file = "{0}/fgs214-cookpad-adj-result.txt".format(create_dir_day)
#etc_result_file = "{0}/fgs214-cookpad-etc-result.txt".format(create_dir_day)
destination_file = "fgs214-cookpad.txt"
noun_result_file = "fgs214-cookpad-noun-result.txt"
verb_result_file = "fgs214-cookpad-verb-result.txt"
adj_result_file = "fgs214-cookpad-adj-result.txt"
etc_result_file = "fgs214-cookpad-etc-result.txt"

# 記事本文のみ抽出したファイル作成
data_list = []
text_list = []
remove_small_list = []
removed_list = []

# 品詞ごとに分析結果を使用頻度が多かった順にファイル出力
def count_elem(class_list, result_file):
    result_list = []
    counter = Counter(class_list)
    for value, count in counter.most_common():
        result_list.append("{0},{1}".format(re.sub(r"[' ]", "", str(value))[1:-1],count))

    f = open(result_file, "w", encoding="utf-8")
    result_list = [str(i) for i in result_list]
    f.write("\n".join(result_list))
    f.close()

article_list = []
f = open(source_file, "r")
text = f.readlines()[2:]
for elem in text:
    # 実行日までのデータをリストにアペンドする処理を追加したい
    #if(elem[0:8] <= exec_date):
    #    data_list.append(elem.split(","))
    article_list.append(elem.split(","))

#print(list1)
date = ""
text = ""
article_sum_list =[]
flg = True

for elem in article_list:

    #print(elem[0])
    
    if(date == elem[0]):
        #text = text + elem[1].replace("\n","").replace("　","")
        text = text + elem[1].replace("　","")
        #text = text + elem[1]
    
    else:
        if(flg):
            flg = False
            date = elem[0]
            #text = text + elem[1].replace("\n","").replace("　","")
            text = text + elem[1].replace("　","")
            #text = text + elem[1]
            #print(text)

        else:
            article_sum_list.append([date, text])
            text = ""
            date = elem[0]
            text = text + elem[1]
    
article_sum_list.append([date, text])

#print(article_sum_list)


#text = f.readlines()[3:]
#for elem in text:
#    data_list.append(elem.split("\t"))

# 記事本文だけ抜き出す
'''
for i in range(len(data_list)):
    text_work = ""
    text_work = data_list[i][6].translate(str.maketrans({chr(0xFF01 + i): chr(0x21 + i) for i in range(94)}))
    text_work = re.sub("(http|https)?://[\w/:%#\$&\?\(\)~\.=\+\-…]+", "", str(text_work))
    text_work = re.sub("[0-9a-z_./?-]+(@|＠)([0-9a-z-]+\.)+[0-9a-z-]+", "", text_work)
    text_work = re.sub("[0-9a-zA-Z_./?-]+(@|＠)[0-9a-zA-Z_./?-]+", "", text_work)
    text_work = re.sub("[0-9]+-[0-9]+-[0-9]+", "", text_work)
    text_work = re.sub("[0-9]+-[0-9]+", "", text_work)
    text_work = re.sub("[0-9]{4}[\s]{0,1}年", "", text_work)
    text_work = re.sub("[0-9]{4}[\s]{0,1}年度", "", text_work)
    text_work = re.sub("[0-9]{1,2}[\s]{0,1}月", "", text_work)
    text_work = re.sub("[0-9]{1,2}[\s]{0,1}日", "", text_work)
    text_work = re.sub("[0-9]+[\s]{0,1}:[0-9]+[\s]{0,1}", "", text_work)
    text_work = re.sub("[0-9]+[\s]{0,1}－[\s]{0,1}[0-9]+[\s]{0,1}－[\s]{0,1}[0-9]+", "", text_work)
    text_work = re.sub("[0-9]+[\s]{0,1}－[\s]{0,1}[0-9]+[\s]{0,1}", "", text_work)
    text_work = re.sub("[-]{2,}", "", text_work)
    text_work = re.sub("[\s]{2,}", "", text_work)
    text_work = re.sub("[☆]{2,}", "", text_work)
    text_work = re.sub("[★]{2,}", "", text_work)
    text_work = re.sub("[♪]{2,}", "", text_work)
    text_work = re.sub("[‣]{2,}", "", text_work)
    text_work = re.sub("[－]{2,}", "", text_work)
    text_work = re.sub("[_]{2,}", "", text_work)
    text_work = re.sub("[ー]{2,}", "", text_work)
    text_work = re.sub("[･]{2,}", "", text_work)
    text_work = re.sub("[=]{2,}", "", text_work)
    text_work = re.sub("[━]{2,}", "", text_work)
    text_work = re.sub("[〜]{2,}", "", text_work)
    text_work = re.sub("[～]{2,}", "", text_work)
    text_work = re.sub("[-]{2,}", "", text_work)
    text_work = re.sub("[–]{2,}", "", text_work)
    text_work = re.sub("[?]{2,}", "", text_work)
    text_work = re.sub("[0-9]+[\s]{0,1}位", "", text_work)
    text_list.append(re.sub(r"[①②③④⑤⑥⑦⑧⑨⑩◯◎￣!<>‥•~➩■●▲▼◆▽◇〇○♬◉↓→*▸▷\"\[\]〔〕「」｢｣〝〟″┓┛┏┗※『』〈〉()≪≫《》【】/〒　 ♡❝❞“”×:;︶˘・〜―⇒︓]", "", text_work))
'''

#text_list = "鬼滅の刃"
for article in article_sum_list:

    create_dir_mon = "src/mecab/FGS214-cookpad/{0}".format(article[0][0:6])
    try:
        os.makedirs(create_dir_mon)
    except FileExistsError:
        pass
    
    create_dir_day = create_dir_mon + "/" + article[0]
    try:
        os.makedirs(create_dir_day)
    except FileExistsError:
        pass
    
    destination_dir = create_dir_day + "/" + destination_file
    
    # 半角記号の置換
    #for i in range(len(text_list)):
    #    removed_list.append(re.sub(re.compile("[■◆●▼『』▸★〜「」※ 　・×“”♡¥―【】…˘＜＞（）：:≪≫-／〒＋]","", text_list[i]))

    #for i in range(len(remove_small_list)):
    #    remove_big_list.append(re.sub(r'[︰-＠]', "", remove_small_list[i]))

    # 本文のみ記事ごとに改行して出力。

    #print(article[1])

    #os.makedirs('temp', exist_ok=True)
    empty_file = pathlib.Path(destination_dir)
    empty_file.touch()

    os.chmod(destination_dir, 0o755)

    f = open(destination_dir, "w", encoding="utf-8")
    #f.write("\n".join(text_list))
    f.write(article[1])
    f.close()

    # 分析結果格納用配列
    analyze_list = []
    counter_list = []
    noun_list = []
    verb_list = []
    adj_list = []
    etc_list = []

    f =  open(destination_dir,'r')
    text = f.read()

    #mecab = MeCab.Tagger("-Ochasen -u /usr/share/mecab/dic/ipadic/original.dic -d /var/app/total/src/mecab/mecab-ipadic-neologd")
    mecab = MeCab.Tagger("-Ochasen -u /usr/share/mecab/dic/ipadic/original.dic")

    analyze = mecab.parse(text)

    #if (destination_dir == 'src/mecab/FGS214-cookpad/201912/20191226/fgs214-cookpad.txt'):
    #    print(analyze)

    analyze_list = analyze.split("\n")

    # 単語と品詞の配列作成
    for i in range(len(analyze_list)):
        try:
            if(analyze_list[i].split("\t")[3].startswith("名詞")):
                noun_list.append((analyze_list[i].split("\t")[0], analyze_list[i].split("\t")[3]))
            elif(analyze_list[i].split("\t")[3].startswith("動詞")):
                verb_list.append((analyze_list[i].split("\t")[0], analyze_list[i].split("\t")[3]))
            elif(analyze_list[i].split("\t")[3].startswith("形容詞")):
                adj_list.append((analyze_list[i].split("\t")[0], analyze_list[i].split("\t")[3]))
            else:
                etc_list.append((analyze_list[i].split("\t")[0], analyze_list[i].split("\t")[3]))
        except:
            None

    count_elem(noun_list, create_dir_day + "/" + noun_result_file)
    count_elem(verb_list, create_dir_day + "/" + verb_result_file)
    count_elem(adj_list, create_dir_day + "/" + adj_result_file)
    count_elem(etc_list, create_dir_day + "/" + etc_result_file)
'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');
const jimp = require('jimp');
const moment = require('moment');
const shell = require('shelljs');

const conf = new(require('../conf.js'));
const log = new(require('../modules/log.js'));
const util = new(require('../modules/util.js'));

//
const today = moment().format('YYYYMMDD');

// parse args
const cli = commandArgs([
    { name: 'extension', alias: 'e', type: String }, // jpg, png
    { name: 'range', alias: 'R', type: Number }, // 30, 45
    { name: 'run', alias: 'r', type: Number },
    { name: 'input', alias: 'i', type: String },
    { name: 'output', alias: 'o', type: String }
]);

const extension = cli['extension'] || 'jpg';
const range = cli['range'] || 30;
const input = cli['input'] || `./${conf.dirPath_in_img}`;
const output = cli['output'] || `./${conf.dirPath_out}/${today}/img/`;


// init action
const init = function () {

    return new Promise((resolve, reject) => {
        co(function* () {
            log.info(log.base(__filename), 'start init');

            // reset dir output/test/img
            util.mkdir(`./${conf.dirPath_out_test}`);
            shell.exec(`rm -f -r ./${conf.dirPath_out_test}/img`);
            util.mkdir(`./${conf.dirPath_out_test}/img`);

            yield util.mkdir(conf.dirPath_out);
            yield util.mkdir(conf.dirPath_img);
            yield util.mkdir(conf.dirPath_img_input);
            resolve();
        });
    });
}

// get rotate images action
const getRotateImages = function () {

    return new Promise((resolve, reject) => {
        co(function* () {
            log.info(log.base(__filename), 'start getRotateImages');

            util.mkdir(`./${conf.dirPath_out}/${today}`);
            util.mkdir(`./${conf.dirPath_out}/${today}/img`);

            // check input is dir or file
            let dir = input;
            let splits = input.split('/');
            let file = splits[splits.length - 1];
            let isFile = false;

            if (file && file.match('.')) {
                isFile = true;
                let reg = new RegExp(file, 'g');
                dir = input.replace(reg, '').replace(/\/$/, '');
            }

            // read dir
            let filenames = fs.readdirSync(dir);
            for (let i = 0; i < filenames.length; i++) {

                if (isFile && filenames[i] !== file) continue;
                if (!filenames[i].match((new RegExp(extension)))) continue;

                for (let j = 0; j < 360; j = j + range) {
                    jimp.read(`${dir}/${filenames[i]}`, function (err, lenna) {
                        if (err) throw err;
                        lenna.background(0xFFFFFFFF)
                            .rotate(j)
                            .opaque()
                            .write(`${output}/${filenames[i]}`.replace(`.${extension}`, `_${j}.${extension}`));
                    });
                }
            }
            yield util.sleep(3000);
        });
    });
}

// get rotate images action sync
const getRotateImagesSync = function () {

    return new Promise((resolve, reject) => {
        co(function* () {
            log.info(log.base(__filename), 'start getRotateImages');

            util.mkdir(`./${conf.dirPath_out}/${today}`);
            util.mkdir(`./${conf.dirPath_out}/${today}/img`);

            // check input is dir or file
            let dir = input;
            let splits = input.split('/');
            let file = splits[splits.length - 1];
            let isFile = false;

            if (file && file.match('.')) {
                isFile = true;
                let reg = new RegExp(file, 'g');
                dir = input.replace(reg, '').replace(/\/$/, '');
            }

            // read dir
            let filenames = fs.readdirSync(dir);
            for (let i = 0; i < filenames.length; i++) {

                if (isFile && filenames[i] !== file) continue;
                if (!filenames[i].match((new RegExp(extension)))) continue;

                for (let j = 0; j < 360; j = j + range) {
                    jimp.read(`${dir}/${filenames[i]}`, function (err, lenna) {
                        if (err) throw err;
                        lenna.background(0xFFFFFFFF)
                            .rotate(j)
                            .opaque()
                            .write(`${output}/${filenames[i]}`.replace(`.${extension}`, `_${j}.${extension}`));
                    });
                    yield util.sleep(3000);
                }
            }
        });
    });
}


// test action
const test = function () {

    return new Promise((resolve, reject) => {
        co(function* () {
            log.info(log.base(__filename), 'start test');

            for (let j = 0; j < 360; j = j + range) {
                jimp.read(`./${conf.dirPath_img_sample}/sample01.jpg`, function (err, lenna) {
                    if (err) throw err;
                    lenna.opaque().background(0xFFFFFFFF)
                        .rotate(j)
                        .opaque()
                        .write(`./${conf.dirPath_out_test}/img/sample01_${j}.jpg`);
                });
                yield util.sleep(3000);
            }
        });
    });
}

// run action
const run = function () {

    co(function* () {

        switch (cli['run']) {
            case 2: // test action
                yield init();
                yield test();
                break;
            case 1: // usual action
            default:
                yield init();
                yield getRotateImages();
        }
    });
}

run();
'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');

const conf = new(require('../conf.js'));
const db = new(require('../modules/db.js'));
const util = new(require('../modules/util.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
]);

// init action
const init = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield db.init();
            yield db.init(`${conf.dirPath_db}/cron0.db`);
            yield db.init(`${conf.dirPath_db}/cron1.db`);
            yield db.init(`${conf.dirPath_db}/cron2.db`);
            yield db.init(`${conf.dirPath_db}/cron3.db`);
            yield db.init(`${conf.dirPath_db}/cron4.db`);
            yield db.init(`${conf.dirPath_db}/cron5.db`);
            yield db.init(`${conf.dirPath_db}/cron6.db`);
            yield db.init(`${conf.dirPath_db}/cron10.db`);
            yield db.init(`${conf.dirPath_db}/cron10_01.db`);
            yield db.init(`${conf.dirPath_db}/cron10_02.db`);
            yield db.init(`${conf.dirPath_db}/cron10_03.db`);
            yield db.init(`${conf.dirPath_db}/cron10_04.db`);
            yield db.init(`${conf.dirPath_db}/cron10_05.db`);
            yield db.init(`${conf.dirPath_db}/cron10_06.db`);
            yield db.init(`${conf.dirPath_db}/cron11.db`);
            yield db.init(`${conf.dirPath_db}/cron11_01.db`);
            yield db.init(`${conf.dirPath_db}/cron11_02.db`);
            yield db.init(`${conf.dirPath_db}/cron11_03.db`);
            yield db.init(`${conf.dirPath_db}/cron11_04.db`);
            yield db.init(`${conf.dirPath_db}/cron11_05.db`);
            yield db.init(`${conf.dirPath_db}/cron12.db`);
            yield db.init(`${conf.dirPath_db}/cron12_01.db`);
            yield db.init(`${conf.dirPath_db}/cron12_02.db`);
            yield db.init(`${conf.dirPath_db}/cron12_03.db`);
            yield db.init(`${conf.dirPath_db}/cron12_04.db`);
            yield db.init(`${conf.dirPath_db}/cron12_05.db`);
            yield db.init(`${conf.dirPath_db}/cron13.db`);
            yield db.init(`${conf.dirPath_db}/cron13_01.db`);
            yield db.init(`${conf.dirPath_db}/cron13_02.db`);
            yield db.init(`${conf.dirPath_db}/cron13_03.db`);
            yield util.mkdir(conf.dirPath_out);
            resolve();
        });
    });
}

// check database
const test = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield db.connect(conf.filePath_db);
            const type = `get`;
            const sql = `SELECT * FROM user_info`;
            const opt = {};
            let res = yield db.do(type, sql, opt);
            fs.writeFileSync(`./${conf.dirPath_out_test}/setDB.txt`, JSON.stringify(res));
            yield db.close();
            resolve();
        });
    });
}

// run action
const run = function () {

    co(function* () {

        switch (cli['run']) {
            case 2: // test action
                yield test();
                break;
            case 1: // usual action
            default:
                yield init();
        }
    });
}

run();
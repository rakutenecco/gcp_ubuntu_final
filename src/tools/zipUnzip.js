'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');

const conf = new(require('../conf.js'));
const log = new(require('../modules/log.js'));
const util = new(require('../modules/util.js'));
const zip = new(require('../modules/zip.js'));


// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number }
]);


// init action
const init = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            resolve();
        });
    });
}

// get rotate images action
const unzipFile = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            zip.init();
            zip.unzipFile(`${conf.dirPath_zip}/sample.txt.zip`, conf.dirPath_zip);
        });
    });
}

// get rotate images action
const unzipDir = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            zip.init();
            zip.unzipDir(conf.dirPath_zip, conf.dirPath_zip);
        });
    });
}


// run action
const run = function () {

    co(function* () {

        switch (cli['run']) {
            case 2: // test action

                break;
            case 1: // usual action
            default:
                yield unzipDir();
        }
    });
}

run();
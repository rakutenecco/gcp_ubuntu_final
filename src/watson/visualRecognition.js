'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');
const moment = require('moment');

// new modules
const conf = new(require('../conf.js'));
const db = new(require('../modules/db.js'));
const util = new(require('../modules/util.js'));
const watson = new(require('../modules/watson.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number }
]);


// init action
const init = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield watson.init();

            resolve();
        });
    });
}

// get classifier action by visual_recognition
const getClassifier = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let img_url = `${conf.dirPath_img}/sample/sample01.jpg`;
            let classifier_ids = 'animalxdirection_1734308103';
            let threshold = undefined;

            // call getClassifier
            let res = yield watson.getClassifier(img_url, classifier_ids, threshold);
            console.log(JSON.stringify(res, undefined, 2));

        });
    });
}

// get classifier action by visual_recognition
const createClassifier = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let param_name = 'animal-8directions';
            let param_zips = [
                { name: '0', path_zip: `${conf.dirPath_in_img}/0.zip` },
                { name: '45', path_zip: `${conf.dirPath_in_img}/45.zip` },
                { name: '90', path_zip: `${conf.dirPath_in_img}/90.zip` },
                { name: '135', path_zip: `${conf.dirPath_in_img}/135.zip` },
                { name: '180', path_zip: `${conf.dirPath_in_img}/180.zip` },
                { name: '225', path_zip: `${conf.dirPath_in_img}/225.zip` },
                { name: '270', path_zip: `${conf.dirPath_in_img}/270.zip` },
                { name: '315', path_zip: `${conf.dirPath_in_img}/315.zip` }
            ];

            // call createClassifier
            let res = yield watson.createClassifier(param_name, param_zips);
            console.log(JSON.stringify(res, null, 2));
        });
    });
}


// get list classifier action by visual_recognition
const listClassifier = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // call listClassifier
            let res = yield watson.listClassifier();
            console.log(JSON.stringify(res, null, 2));
        });
    });
}



// run action
const run = function () {

    co(function* () {

        switch (cli['run']) {

            //
            case 3: // list action
                yield init();
                yield listClassifier();
                break;
            case 2: // create action
                yield init();
                yield createClassifier();
                break;
            case 1: // usual action
            default:
                yield init();
                yield getClassifier();
                break;
        }
    });
}

run();
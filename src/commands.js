'use strict';

// modules
const co = require('co');
const commandArgs = require('command-line-args');
const shell = require('shelljs');

const conf = new(require('./conf.js'));
const log = new(require('./modules/log.js'));
const util = new(require('./modules/util.js'));

// parse args
const cli = commandArgs([
    { name: 'script', alias: 's', type: String },
]);


// run action
const run = function () {

    co(function* () {

        switch (cli['script']) {
            case 'mount':
                shell.exec(`sudo mount -t vboxsf share ${conf.dirPath_share_vb}`);
                break;
        }
    });
}

run();
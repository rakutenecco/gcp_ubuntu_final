'use strict';

const node_util = require('util');
const myconf = require('../myconf.json');
const ConfSuper = require('./modules/conf.js');

// Config 関数生成
let Config = function () {

    // inherit super
    ConfSuper.call(this);

    // total config for the system
    this.title = 'Config';

    set(this);
};

// set myconf to conf
let set = function (self) {

    for (let key in myconf) {
        if (myconf.hasOwnProperty(key)) {
            self[key] = myconf[key];
        }
    }
}

// inherit super
Config.prototype = Object.create(ConfSuper.prototype, {
    constructor: {
        value: Config,
        enumerable: false,
        writable: true,
        configurable: true
    }
});

module.exports = Config;
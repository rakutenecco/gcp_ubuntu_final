'use strict';

const shell = require('shelljs');
const cronJob = require('cron').CronJob;
const moment = require('moment');
const fs = require('fs');
const date = moment().format('YYYYMMDD');
const today = moment().format('YYYY-MM-DD hh:mm:ss');
const path_log = `../log/${date}.txt`;
let now = '';
const conf = new (require('/var/app/total/src/conf.js'));

/*
cronJob(`00 00 00 * * 1-5`)
Seconds: 0-59
Minutes: 0-59
Hours: 0-23
Day of Month: 1-31
Months: 0-11
Day of Week: 0-6
*/

//// 01 ////
// FGS258 twitter posts daily
//new cronJob(`00 12 00 * * 0-6`, function () {
new cronJob(`00 19 19 * * 2`, function () {
        
    //let targetday = '20190419';
    let filepath;
            
    filepath = `${conf.dirPath_share_vb}/targets/targets_food_02_01.txt`;
    if (fs.existsSync(filepath)) {
        shell.exec(`node src/rakuten/FGS258-twitter/shell.js -r 1 -n cron11_04 -f ${filepath} -d continue`);
        shell.exec(`node src/rakuten/FGS258-twitter/shell.js -r 1 -n cron11_04 -f ${filepath} -t 20190604`);
        shell.exec(`node src/rakuten/FGS258-twitter/shell.js -r 1 -n cron11_04 -f ${filepath} -t 20190605`);
    }

    // filepath = `${conf.dirPath_share_vb}/targets/targets_watch.txt`;
    // if (fs.existsSync(filepath)) {
    //     shell.exec(`node src/rakuten/FGS258-twitter/shell.js -r 3 -n cron11_01 -f ${filepath}`);
    //     //shell.exec(`node src/rakuten/FGS258-twitter/shell.js -r 3 -n cron11_01 -f ${filepath} -t ${targetday}`);
    // }

}, function () {
    // error
    console.log(`Error ${today} : FGS258 twitter posts scrape cron21_01`);
},
true, 'Asia/Tokyo');

console.log('cron21_01(cron11_04) setting');

'use strict';

const shell = require('shelljs');
const cronJob = require('cron').CronJob;
const moment = require('moment');
const fs = require('fs');
const conf = new (require('/var/app/total/src/conf.js'));

/*
cronJob(`00 00 00 * * 1-5`)
Seconds: 0-59
Minutes: 0-59
Hours: 0-23
Day of Month: 1-31
Months: 0-11
Day of Week: 0-6
*/

//// 01 ////
// FGS286 yahoo all weekly
new cronJob(`00 30 00 * * 0`, function () {
//new cronJob(`00 31 16 * * 5`, function () {
    let st = moment().format('YYYY/MM/DD HH:mm:ss');
    console.log(` yahoo cron13 start ${st}`);
    //let startdate = moment().add(-7, 'd').format('YYYYMMDD');
    // let startdate = '20190616';
    // let getdate = '20190620'
    // let filename;

    // filename = `targets_all_06_06.txt`;
    // //shell.exec(`node src/rakuten/FGS286-yahoo/shell.js -r 2 -n cron13_03 -f ${filename} -s ${startdate} -g ${getdate}`);

    // shell.exec(`node src/rakuten/FGS286-yahoo/shell.js -r 2 -n cron13_03 -f targets_all_06_06.txt -s ${startdate}`);
    // shell.exec(`node src/rakuten/FGS286-yahoo/shell.js -r 1 -n cron13_01 -f targets_all_06_04.txt -s ${startdate}`);
    // shell.exec(`node src/rakuten/FGS286-yahoo/shell.js -r 1 -n cron13_02 -f targets_all_06_05.txt -s ${startdate}`);
    // shell.exec(`node src/rakuten/FGS286-yahoo/shell.js -r 1 -n cron13_03 -f targets_all_06_06.txt -s ${startdate}`);

    //shell.exec(`node src/rakuten/FGS286-yahoo/shell.js -r 2 -n cron13_04 -f targets_all_06_04.txt -s 20190707`);
    let tdy = moment().format('YYYYMMDD');
    if( tdy == '20190811'){
        shell.exec(`node src/rakuten/FGS286-yahoo/shell.js -r 1 -n cron13_01 -f targets_all_06_04.txt -s 20190802`);
        shell.exec(`node src/rakuten/FGS286-yahoo/shell.js -r 1 -n cron13_02 -f targets_all_06_05.txt -s 20190806`);
        shell.exec(`node src/rakuten/FGS286-yahoo/shell.js -r 1 -n cron13_03 -f targets_all_06_06.txt -s 20190807`);
    }

    if( tdy == '20190818'){
        shell.exec(`node src/rakuten/FGS286-yahoo/shell.js -r 1 -n cron13_01 -f targets_all_06_04.txt -s 20190811`);
        shell.exec(`node src/rakuten/FGS286-yahoo/shell.js -r 1 -n cron13_02 -f targets_all_06_05.txt -s 20190811`);
        shell.exec(`node src/rakuten/FGS286-yahoo/shell.js -r 1 -n cron13_03 -f targets_all_06_06.txt -s 20190812`);
    }

    console.log(` yahoo cron13 end   ${st} - ${moment().format('YYYY/MM/DD HH:mm:ss')}`);

}, function () {
    // error
    console.log(`Error ${today} : FGS286 yahoo scrape cron13`);
},
true, 'Asia/Tokyo');

console.log('cron13 setting');

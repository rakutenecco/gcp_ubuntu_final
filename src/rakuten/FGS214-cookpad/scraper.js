'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');
const moment = require('moment');

// new modules
const conf = new(require('../../conf.js'));
const db = new(require('../../modules/db.js'));
const tsv = new(require('../../modules/tsv.js'));
const util = new(require('../../modules/util.js'));
const cheerio = new(require('../../modules/cheerio-httpcli.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
    { name: 'startday', alias: 's', type: String },
]);

const today = moment().format('YYYYMMDD');
const filePath_db = `${conf.dirPath_db}/cron0.db`;
const startday = cli['startday'];

// drop table for concern tables
const drop = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // drop table proxy_list
            yield db.connect(filePath_db);

            let sql = `DROP TABLE IF EXISTS FGS214_url;`;
            yield db.do(`run`, sql, {});

            sql = `DROP TABLE IF EXISTS FGS214_article;`;
            yield db.do(`run`, sql, {});
            yield db.close();

            resolve();
        });
    });
}


// set url list
const setList = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield cheerio.init(10000);
            // create table "proxy_list"
            yield db.connect(filePath_db);
            const sql = `CREATE TABLE IF NOT EXISTS FGS214_url ` +
                `(c_num TEXT primary key, c_getdate TEXT, c_URL TEXT, c_title TEXT, c_publish TEXT, c_category TEXT);`;
            //console.log('sql = ' + sql);
            yield db.do(`run`, sql, {});

            let up_date = today;
            let p = 1;
            let n = 0;
            let start_site = 'https://news.cookpad.com/articles';
            let site;

            //while (p <= 6) {
            while (up_date >= startday) {

                if (p == 1) {
                    site = start_site;
                } else {
                    site = start_site + '?page=' + p;
                }
                //console.log(site);
                //console.log(p);

                let $ = yield cheerio.get(site);
                //console.log($('body').html());
                if ($ && $('body').html().match(/article_list/i)) {
                    $('.article_list').find('li').each(function (i, e) {
                        co(function* () {

                            n = n + 1;
                            //console.log(n);

                            let url = 'https://news.cookpad.com' + $(e).find('a').attr('href');
                            //console.log(url);

                            let title = $(e).find('.article_title').eq(0).text().replace(/[\n\r\t\(\) ,]/g, '');
                            //console.log(title);

                            let publish = $(e).find('.published_date').eq(0).text();
                            //console.log(publish);
                            publish = publish.substr(0, 4) + publish.substr(5, 2) + publish.substr(8, 2)
                            //console.log(publish);
                            up_date = publish;
                            //console.log(up_date);

                            let rlabel = $(e).find('.label');
                            let category = '';
                            for (let j = 0; j < rlabel.length; j++) {
                                let str = rlabel.eq(j).text().replace(/[\n\r\t\(\) ,]/g, '');
                                if (category.length > 0) {
                                    category = category + ',' + str;
                                } else {
                                    category = str;
                                }
                            }
                            //console.log(category);

                            const sql = `INSERT OR REPLACE INTO FGS214_url ` +
                                `(c_num, c_getdate,c_URL,c_title,c_publish,c_category) VALUES (?, ?, ?, ?, ?, ?);`;
                            //console.log('sql = ' + sql);
                            let num = ('000' + n).slice(-4)
                            //console.log('num = ' + num);
                            let opt = [num, today, url, title, publish, category];
                            yield db.do(`run`, sql, opt);
                        });
                    });
                }
                p = p + 1;

            }

            yield db.close();

            resolve();
        });
    });
}

// init action
const init = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // create table "proxy_list"
            yield db.connect(filePath_db);
            const sql = `CREATE TABLE IF NOT EXISTS FGS214_article ` +
                `(c_num TEXT primary key, c_getdate TEXT, c_art_title TEXT, c_article TEXT);`;
            //console.log('sql = ' + sql);
            yield db.do(`run`, sql, {});

            resolve();
        });
    });
}

// get url list
const getList = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // create table "proxy_list"
            let sql = `SELECT * FROM FGS214_url`;
            let res = yield db.do(`all`, sql, {});

            resolve(res);
        });
    });
}
let arttl;
let artxt;
// scrape proxy data, and set db as proxy_list
const scrape = function (urls) {

    return new Promise((resolve, reject) => {
        co(function* () {

            for (let i = 0; i < urls.length; i++) {

                let number = urls[i].c_num;
                let getdate = urls[i].c_getdate;
                arttl = '';
                artxt = '';

                //scrape_article
                let next = yield scrape_article(urls[i].c_URL);
                while (next) {
                    next = yield scrape_article(next);
                }

                //insert
                if (arttl) {
                    const sql = `INSERT OR REPLACE INTO FGS214_article ` +
                        `(c_num,c_getdate,c_art_title,c_article) ` +
                        `VALUES (?, ?, ?, ?);`;
                    //console.log('sql = ' + sql);
                    let opt = [number, getdate, arttl, artxt];
                    yield db.do(`run`, sql, opt);
                }
            }

            resolve();
        });
    });
}
// scrape proxy data, and set db as proxy_list
const scrape_article = function (url) {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield cheerio.init(10000);
            let $ = yield cheerio.get(url);
            if ($ && $('body').html().match(/article_contents/i)) {

                let e = $('.article_contents');
                arttl = $(e).find('.article_title').text().replace(/[\n\r\t\(\) ,]/g, '');

                let article = $(e).find('.main_content').text().replace(/(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|\ud83c[\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|\ud83c[\ude32-\ude3a]|\ud83c[\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff])/g, '');
              
                let insta = $(e).find('.instagram-media').text();
                artxt = artxt + article.replace(insta, '').replace(/[\n\r\t\(\) ,]/g, '');

                if ($(e).html().match(/pagination_border/i)) {
                    let ptxt = $(e).find('.pagination_border').text().replace(/[\n\r\t\(\) ,]/g, '');
                    artxt = artxt.replace(ptxt, '');
                    let tmp = $(e).find('.pagination_border').find('li');
                    let p = tmp[tmp.length - 1];

                    if ($(p).find('a').attr('class') == 'next') {
                        let url2 = 'https://news.cookpad.com' + $(p).find('a').attr('href');
                        resolve(url2);
                    } else {
                        resolve();
                    }
                } else {
                    resolve();
                }
            }
        });
    });
}

// end action
const end = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield db.close();

            resolve();
        });
    });
}

// set out file to share folder
const setOutFile = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // get scraped data
            yield db.connect(filePath_db);
            let sql = `SELECT a.c_getdate, a.min_num, a.c_URL, a.c_title, a.c_publish, a.c_category, b.c_art_title, b.c_article ` +
                `FROM ( SELECT MAX(c_getdate) c_getdate, MIN(c_num) min_num, c_URL, MAX(c_title) c_title, c_publish, MAX(c_category) c_category FROM FGS214_url GROUP BY c_publish, c_URL ) a ,FGS214_article b ` +
                `WHERE a.c_getdate = b.c_getdate AND a.min_num = b.c_num ` +
                `AND a.c_publish >= '${startday}' AND a.c_publish < '${today}' ` +
                `ORDER BY a.min_num;`;
            //console.log('sql = ' + sql);
            let res = yield db.do(`all`, sql, {});

            // write header
            let filepath = `${conf.dirPath_share_vb}/FGS214-${today}.txt`;
            fs.appendFileSync(filepath, ['データ取得日', 'number', 'URL', 'タイトル', '更新日', 'カテゴリー', '記事内タイトル', '記事'].join('\t') + '\n', 'utf-8');
            fs.appendFileSync(filepath, ['c_getdate', 'c_num', 'c_URL', 'c_title', 'c_publish', 'c_category', 'c_art_title', 'c_article'].join('\t') + '\n', 'utf-8');
            fs.appendFileSync(filepath, ['VARCHAR(8)', 'VARCHAR(6)', 'VARCHAR(200)', 'VARCHAR(1000)', 'VARCHAR(8)', 'VARCHAR(1000)', 'VARCHAR(1000)', 'VARCHAR(20000)'].join('\t') + '\n', 'utf-8');

            // write body
            for (let i = 0; i < res.length; i++) {
                fs.appendFileSync(filepath, [res[i].c_getdate, res[i].min_num, res[i].c_URL, res[i].c_title, res[i].c_publish, res[i].c_category, res[i].c_art_title, res[i].c_article].join('\t') + '\n', 'utf-8');
            }

            yield db.close();
            resolve(res);
        });
    });
}



// run action
const run = function () {

    co(function* () {
        // 
        switch (cli['run']) {
            case 1: // drop table
                yield drop();
                break;
            case 2: // set url list
                yield setList();
                break;
            case 3: // scrape data
                yield init();
                const list = yield getList();
                yield scrape(list);
                yield end();
                break;
            case 4: // out data to share folder
                yield setOutFile();
                break;

            default:
                break;
        }
    });
}

run();
import sys
import math
import os
import glob
import numpy as np
import pandas as pd
import img2pdf
import PyPDF2
import matplotlib as mpl
import matplotlib.dates as mdates
import statsmodels.api as sm
from sklearn.linear_model import LinearRegression
from datetime import datetime, timedelta
from matplotlib import pyplot as plt
from matplotlib.font_manager import FontProperties
from matplotlib.backends.backend_pdf import PdfPages
from pathlib import Path
from PIL import Image

font_path = '/usr/share/fonts/truetype/fonts-japanese-gothic.ttf'
font_prop = FontProperties(fname=font_path)
mpl.rcParams['font.family'] = font_prop.get_name()

# today
today = datetime.strptime(datetime.strftime(datetime.now(), "%Y%m%d"), "%Y%m%d")

# command line argument
args = sys.argv
#category = args[1]
#arg_date = datetime.strptime(args[2], "%Y%m%d")
arg_date = datetime.strptime(args[1], "%Y%m%d")
bef_day = str((arg_date - today).days)

# 日付配列作成
exec_date_hyphen = (datetime.now() + timedelta(days=int(bef_day))).date()
exec_date = datetime.strftime(exec_date_hyphen, "%Y%m%d")
exec_year = exec_date[0:4]
exec_month = exec_date[0:6]

#画像保存先ディレクトリ作成
output_dir = "src/rakuten/FGS214-cookpad/cookpad_pdf_out_{0}/".format(exec_date)
os.makedirs(output_dir, exist_ok=True)

# 実行年の日付リスト作成
day_list = []
#first_date = datetime(int(exec_year), 1, 1, 0, 0)
first_date = datetime(2019, 1, 1, 0, 0)
#next_year_first_date = datetime(int(exec_year) + 1, 1, 1, 0, 0)

date_hyphen = first_date
#while date_hyphen < next_year_first_date:
while date_hyphen <= datetime.combine(exec_date_hyphen, datetime.min.time()):
    day_list.append(datetime.strftime(date_hyphen, "%Y%m%d")[0:8])
    date_hyphen += timedelta(days=1)

#実行日以前の分析ファイルにアクセスして特定の単語の件数を取得
#実行日より後の件数は0
#category_list = [ 'app', 'beauty', 'business', 'entertainment', 'fashion', 'gourmet', 'lifestyle', 'mobile', 'sports', 'technology']

keyword_dir = "src/rakuten/FGS214-cookpad/targets.csv"
os.chmod(keyword_dir, 0o755)

keyword_list = []

keyword_data = open(keyword_dir, "r")
for line in keyword_data:
    keyword_list.append(line.replace("\n", ""))
keyword_data.close()

for keyword in keyword_list:

    count_list = []

    for i in range(len(day_list)):

        count = 0
        data_list = []
        text = []

        #for category in category_list:
            
        # 実行日の単語取得
        source_dir = "src/mecab/FGS214-cookpad/{0}/{1}".format(day_list[i][0:6], day_list[i])
        source_file = "src/mecab/FGS214-cookpad/{0}/{1}/fgs214-cookpad-noun-result.txt".format(day_list[i][0:6], day_list[i])

        #if os.path.isdir(source_dir)==False:
        #    os.mkdir(source_dir)

        #print(source_file)
        
        if os.path.isfile(source_file)==True:
            f = open(source_file, "r")
            #text.extend(f.readlines()[3:])
            text.extend(f.readlines())

        # 当日のデータ
        for elem in text:
            data_list.append(elem.split(","))

        for elem in range(len(data_list)):
            
            if data_list[elem][0] == keyword:
                count = count + int(data_list[elem][2].replace("\r\n", ""))

        count_list.append(count)

    #print(count_list)

    date_index = pd.date_range('2019-01-01', freq='D', periods=(len(count_list)))
    
    fig, ax = plt.subplots()
    fig = plt.figure(figsize=(5.8, 4.2))
    ax = fig.add_subplot(111)

    #pandas type
    count = pd.Series(count_list)
    avg_list = count.rolling(window=28).mean()
    #avg_list = ["0" if x is None else x for x in avg_list]
    
    #list type
    avg_list = ["0" if x is None else x for x in avg_list]
    avg_list = [0 if str(x) == "nan" else x for x in avg_list]

    max_val = max(avg_list)
    rel_list = []
    relval = ''
    for rel_val in avg_list:
        if (rel_val != 0):
            rel_val = round(rel_val * 100 / max_val, 3)
            rel_list.append(rel_val)
        else:
            rel_list.append(rel_val)
    
    count = pd.Series(rel_list)

    multi_reg_x = []
    multi_reg_y = []
    multi_val_pre = ""
    #for multi_val in count_list:
    for multi_val in avg_list:
        if (multi_val == 0):
            multi_val = 0
            multi_reg_x.append([multi_val_pre ,multi_val])
            multi_reg_y.append([multi_val])
            multi_val_pre = multi_val
        else:
            multi_val = round(multi_val * 100 / max_val)
            multi_reg_x.append([multi_val_pre ,multi_val])
            multi_reg_y.append([multi_val])
            multi_val_pre = multi_val

    model = LinearRegression()
    model.fit(multi_reg_x[1:-1], multi_reg_y[2:])

    x_test = ([*multi_reg_y[-3],*multi_reg_y[-2]], [*multi_reg_y[-2],*multi_reg_y[-1]])

    preds = model.predict(x_test)

    pred_list = []
    for i, pred in enumerate(preds):
        #print('Predicted:%s, Target:%s'%(price,y_test[i]))
        pred_list.append(round(int(pred), 0))
        #print('Predicted:%s'%(pred))
    
    score = model.score(multi_reg_x[1:], multi_reg_y[1:])
    print("r-squared:",score)

    m = 100/len(date_index)

    #print(m)
    y_list = []
    for i in range (len(date_index)):
        y_list.append(round(m * i))

    #print(y_list)

    a = np.array(y_list)
    b = np.array(rel_list)

    print(round(int(np.linalg.norm(a - b))))

    avg_list = []
    flg = False
    for item in rel_list:
        if(flg == False and item == 0):
            item = float("nan")
            avg_list.append(item)
        else:
            flg = True
            avg_list.append(item)

    #res = sm.tsa.seasonal_decompose(count, freq=133)
    res = sm.tsa.seasonal_decompose(count, period=133)
    trend = res.trend
    trend = ["0" if x is None else x for x in trend]
    #seasonal = res.seasonal
    #seasonal = ["0" if x is None else x for x in seasonal]

    #df = pd.DataFrame(chart_list, date_index)
    df = pd.DataFrame(avg_list, date_index)
    dt = pd.DataFrame(trend, date_index)

    #avg_list = np.where(np.isnan(avg_list), 0, avg_list)
    #avg_list = [x for x in avg_list if str(x) != "nan"]
    
    try:
        #if(avg_list[-365] != 0):
        #    YoY = round((avg_list[-1]/avg_list[-365] * 100) - 100, 2)
        #else:
        #    YoY = 0
        if(rel_list[-365] == 0 or math.isnan(rel_list[-365])):
            YoY = 0
        else:
            #YoY = round((avg_list[-1]/avg_list[-365] * 100) - 100, 2)
            YoY = round(((rel_list[-1]/rel_list[-365]) - 1) * 100  , 2)
            print(rel_list[-1])
            print(rel_list[-365])
    except:
        YoY = 0

    aaa_list = [0 if str(x) == "nan" else x for x in avg_list]
    
    x = np.array(list(range(len(avg_list))))
    y = np.array(aaa_list)

    #回帰線を求める
    x_reg = x - x.mean()
    y_reg = y - y.mean()

    #傾きを求める
    denom = x_reg * x_reg
    numer = x_reg * y_reg

    answer = round(numer.sum()/denom.sum(),3)

    #直近28日
    avg_list_28 = avg_list[-28:]
    x_28 = np.array(list(range(len(avg_list_28))))
    y_28 = np.array(avg_list_28)

    #回帰線を求める
    x_reg_28 = x_28 - x_28.mean()
    y_reg_28 = y_28 - y_28.mean()

    #傾きを求める
    denom_28 = x_reg_28 * x_reg_28
    numer_28 = x_reg_28 * y_reg_28

    answer_28 = round(numer_28.sum()/denom_28.sum(),3)

    #直近7日
    avg_list_7 = avg_list[-7:]
    x_7 = np.array(list(range(len(avg_list_7))))
    y_7 = np.array(avg_list_7)

    #回帰線を求める
    x_reg_7 = x_7 - x_7.mean()
    y_reg_7 = y_7 - y_7.mean()

    #傾きを求める
    denom_7 = x_reg_7 * x_reg_7
    numer_7 = x_reg_7 * y_reg_7

    answer_7 = round(numer_7.sum()/denom_7.sum(),3)

    print(keyword + ":" + str(answer) + ":" + str(answer_28) + ":" + str(answer_7))

    #if (((answer > 0 and answer_28 > 0 and answer_7 > 0) or YoY > 0) and max(avg_list) >= 1):
    #if ((answer_28 >= 0.001 or answer_7 >= 0.001) and answer >= 0 and max(avg_list) >= 0.1):
    #if (answer_28 >= 0.1 and answer_7 >= 0.5):
    if (1 >= 0.1):

        #print(keyword + ":" + str(answer) + ":" + str(answer_28) + ":" + str(answer_7))

        if(YoY==0):
            YoY = "－"
        else:
            YoY = str(YoY) + "％"

        #if (answer_28 > 0 or answer_7 > 0):
        df.plot(ax = ax, x_compat=True, color="turquoise", alpha=0.7, legend=False)
        dt.plot(ax = ax, x_compat=True, color="tomato", alpha=0.7, legend=False)
        ax.xaxis.set_minor_locator(mdates.MonthLocator())
        
        plt.setp(ax.get_xticklabels(), rotation=60, ha="center")
        plt.title(keyword, fontsize = 13)
        plt.ylabel("", fontsize = 10)

        prediction = pred_list[1] - (round(rel_list[-1]))

        if (prediction > 0 ):
            prediction =  "+" + str(prediction)
        elif (prediction == 0 ):
            prediction =  "±" + str(prediction)
        else:
            prediction = str(prediction)
        
        trend_value = round(int(np.linalg.norm(a - b)))
        if (trend_value <= 100):
            trend_str = "◎"
        elif (trend_value <= 500):
            trend_str = "〇"
        else:
            trend_str = "△"

        #plt.text(0, 1.03, "YoY : " + YoY, size = 12, color = "black", alpha=0.7, transform=ax.transAxes)
        plt.text(0, 1.03, "\nYoY : " + YoY, size = 12, color = "black", alpha=0.7, transform=ax.transAxes)
        plt.text(0, 1.03, "                                                                     Trend  :  " + trend_str
        + "\n                                                               Prediction  :  " + prediction
                , size = 12, color = "black", alpha=0.7, transform=ax.transAxes, horizontalalignment="left")
        plt.ylim(0)
        #plt.show()

        filename = output_dir + keyword + "_" + exec_date + ".png"
        plt.savefig(filename)
import sys
import math
import os
import glob
import numpy as np
import pandas as pd
import img2pdf
import PyPDF2
import matplotlib as mpl
import matplotlib.dates as mdates
import statsmodels.api as sm
from datetime import datetime, timedelta
from matplotlib import pyplot as plt
from matplotlib.font_manager import FontProperties
from matplotlib.backends.backend_pdf import PdfPages
from pathlib import Path
from PIL import Image

font_path = '/usr/share/fonts/truetype/fonts-japanese-gothic.ttf'
font_prop = FontProperties(fname=font_path)
mpl.rcParams['font.family'] = font_prop.get_name()

# today
today = datetime.strptime(datetime.strftime(datetime.now(), "%Y%m%d"), "%Y%m%d")

# command line argument
args = sys.argv
#category = args[1]
#arg_date = datetime.strptime(args[2], "%Y%m%d")
arg_date = datetime.strptime(args[1], "%Y%m%d")
bef_day = str((arg_date - today).days)

# 日付配列作成
exec_date_hyphen = (datetime.now() + timedelta(days=int(bef_day))).date()
exec_date = datetime.strftime(exec_date_hyphen, "%Y%m%d")
exec_year = exec_date[0:4]
exec_month = exec_date[0:6]

#画像保存先ディレクトリ作成
output_dir = "src/rakuten/FGS214-cookpad/cookpad_pdf_out_{0}/".format(exec_date)
os.makedirs(output_dir, exist_ok=True)

# 実行年の日付リスト作成
day_list = []
#first_date = datetime(int(exec_year), 1, 1, 0, 0)
first_date = datetime(2019, 1, 1, 0, 0)
#next_year_first_date = datetime(int(exec_year) + 1, 1, 1, 0, 0)

date_hyphen = first_date
#while date_hyphen < next_year_first_date:
while date_hyphen <= datetime.combine(exec_date_hyphen, datetime.min.time()):
    day_list.append(datetime.strftime(date_hyphen, "%Y%m%d")[0:8])
    date_hyphen += timedelta(days=1)

#実行日以前の分析ファイルにアクセスして特定の単語の件数を取得
#実行日より後の件数は0
#category_list = [ 'app', 'beauty', 'business', 'entertainment', 'fashion', 'gourmet', 'lifestyle', 'mobile', 'sports', 'technology']

keyword_list = ['apple','peanuts','あおさ','あおさのり','あきたこまち','アマランサス','アレルギー','いくら','イクラ','イザメシ','いちご','イチゴ',
'いちごバター','いちじく','インスタントラーメン','インスタント食品','うどん','うなぎ','うに','ウニ','えいようかん','エシレバター',
'エビ','エビ豆','オーガニック','オーサワ','オートミール','オールスパイス','おかゆ','おせち','おつまみセット','オホーツクの塩',
'オホーツクの塩ラーメン','オホーツクラーメン','オホーツク塩ラーメン','オリーブオイル','オレンジ','お取り寄せグルメ','お茶漬け','お肉',
'お米','カップヌードル','カップラーメン','カップ麺','カニ','かに','かぼちゃ','カリフラワーライス','カルピスバター','カレー',
'カンジャンケジャン','かんずめ','きくらげ','キムチ','キャラ生春巻き','キャロブ','キャンディス','牛乳寒天','クノール','グラスフェッドバター',
'グラノーラ','くりきんとん','グルテンフリー','くるみ','クルミ','グルメ','グルメカタログギフト','ケイジャンシーズニング','けいちゃん',
'コーンスープ','ココナッツ','コシヒカリ','ごはん','ごま','こめ','コメ','ご飯','ご飯のお供','サーモン','サーモン塩辛','さくらんぼ','さつまいも',
'サトウのごはん','サニマル','サバ缶','さば缶詰','サムゲタン','シークヮーサー','シークワーサー','シャインマスカット','じゃばら',
'ジャンツォンジャン','シリアル','ジンギスカン','スープ','すじこ','すだち','ステーキ','スパイス','スパゲッティ','スパゲティ','ズワイガニ',
'せとか','そうめん','ソース','ソーセージ','そば','そばハチミツ','そばはちみつ','そば蜂蜜','ソフト麺','タコ','タコス','たこ焼き','だだちゃ豆',
'タラバガニ','タルタル醤油','だんご','タンミョン','断面テロ','断面萌え','チーズ','チーズボール','チャーハン','チャパグリ','チャパゲティ',
'ツナ缶','つや姫','ディチェコ','デーツ','とうもろこし','トーストアレンジ','トマト','とまとラーメン','トマトラーメン','トマト缶','ドレッシング',
'どん兵衛','長芋','にんじん','ネスカフェ','パスタ','パスタソース','パスタラザニア','バター','はちみつ','ハチミツ','パックご飯','ハトムギ',
'バナナ','ハム','ハモンセラーノ','ハリッサ','バリラ','パン','ハンバーグ','ビーガン','ピーナッツバター','ピクルス','ピザ','ピスタチオ',
'フィンガーライム','ふぐ','ぶどう','フラワーフルーツサンド','フリーズドライ','フリーズドライ食品','プルダックポックン','ブンモジャ','ペヤング',
'ほうとう','ホタテ','ほたて','ぼたん鍋','ホットサンド','ポポー','ホメオパシージャパン','ほりにし','ホルモン','マイサイズ','マキシマム','マグロ',
'マスカット','マヌカハニー','マヨネーズ','マヨらーめん','マヨラーメン','マンゴー','ミートソース','みうら食品','みかん','ミキ','みりん',
'ミルキークイーン','蒸しパン','メープルシロップ','メロン','めんつゆ','もつ鍋','モツ鍋','もつ鍋セット','もも','ヤーコン','ヤンニョムケジャン',
'ゆず','ゆめぴりか','ヨーグルト','よつ葉バター','ラーメン','ラーメン仮面','ライムバタークリーム','ラカンカ','りんご','レトルト','レトルトカレー',
'レトルト食品','ロカボ','ロングライフ','わかめ','ワンプレート','安納芋','伊勢うどん','伊勢海老','一平ちゃん','飲めるハンバーグ','塩','塩麹','翁霞',
'沖縄お土産','牡蠣','加工食品・惣菜','海鮮','海鮮セット','海苔','蟹','柿','学校給食','乾燥野菜','乾麺','干物','缶','缶つま','缶詰','缶詰・瓶詰',
'缶詰め','韓国ラーメン','韓国食品','丸上食品','岩国蓮根麺','菊芋','給食','牛タン','牛肉','牛丼','魚','魚介','強力粉','蕎麦','蕎麦蜂蜜','筋子',
'栗きんとん','鶏ちゃん','鶏中華','鶏肉','減塩','玄米','紅まどんな','穀物・豆・麺類','黒にんにく','黒糖','黒米','黒毛和牛','昆布','佐藤錦','砂糖',
'鮭','笹かまぼこ','雑穀','鯖缶','鯖寿司','三輪そうめん','参鶏湯','刺身','枝豆','七面鳥','小麦粉','松坂牛','焼きそば','焼き鳥','焼肉','醤油',
'常温保存','食べ物','食品','食品ロス','新之助','新米','酢','生こしょう','生ハム','生牡蠣','生胡椒','西京漬け','青切りシークワーサー','赤龍',
'赤龍ラーメン','全粒粉','惣菜','袋ラーメン','袋麺','大豆','大豆ミート','大豆粉','地球グミ','中国タンミョン','調味料','漬けダレ','低糖質','島中華',
'桃','糖質オフ','糖質制限','豆','豆乳','豚肉','鍋つゆ','軟骨','肉','納豆','馬刺し','梅干し','白茶','白米','発芽玄米','備蓄米','福岡トマトラーメン',
'福岡とまとラーメン','文旦','米','米10kg','米沢牛','米粉','米油','保存食','蜂蜜','北斎ラーメン','味噌','味噌汁','味付け極しょうが','無洗米','明太子',
'鳴門金時','麺','毛ガニ','毛蟹','餅','野菜','野菜チーズナッツ','油','羅漢果','落花生','卵','梨','料理酒','冷凍','冷凍パン','冷凍食品','冷凍餃子',
'和牛','姜葱醤','餃子']

for keyword in keyword_list:

    count_list = []

    for i in range(len(day_list)):

        count = 0
        data_list = []
        text = []

        #for category in category_list:
            
        # 実行日の単語取得
        source_dir = "src/mecab/FGS214-cookpad/{0}/{1}".format(day_list[i][0:6], day_list[i])
        source_file = "src/mecab/FGS214-cookpad/{0}/{1}/fgs214-cookpad-noun-result.txt".format(day_list[i][0:6], day_list[i])

        #if os.path.isdir(source_dir)==False:
        #    os.mkdir(source_dir)

        #print(source_file)
        
        if os.path.isfile(source_file)==True:
            f = open(source_file, "r")
            #text.extend(f.readlines()[3:])
            text.extend(f.readlines())

        # 当日のデータ
        for elem in text:
            data_list.append(elem.split(","))

        for elem in range(len(data_list)):
            
            if data_list[elem][0] == keyword:
                count = count + int(data_list[elem][2].replace("\r\n", ""))

        count_list.append(count)

    #print(count_list)

    date_index = pd.date_range('2019-01-01', freq='D', periods=(len(count_list)))
    
    fig, ax = plt.subplots()
    fig = plt.figure(figsize=(5.8, 4.2))
    ax = fig.add_subplot(111)

    count = pd.Series(count_list)
    avg_list = count.rolling(window=28).mean()
    avg_list = ["0" if x is None else x for x in avg_list]

    #res = sm.tsa.seasonal_decompose(count, freq=133)
    res = sm.tsa.seasonal_decompose(count, period=133)
    trend = res.trend
    trend = ["0" if x is None else x for x in trend]
    #seasonal = res.seasonal
    #seasonal = ["0" if x is None else x for x in seasonal]


    #df = pd.DataFrame(chart_list, date_index)
    df = pd.DataFrame(avg_list, date_index)
    dt = pd.DataFrame(trend, date_index)

    #avg_list = np.where(np.isnan(avg_list), 0, avg_list)
    avg_list = [x for x in avg_list if str(x) != "nan"]
    #print(avg_list)
    
    if(avg_list[-365] != 0):
        #YoY = str(round((avg_list[-1]/avg_list[-365] * 100) - 100, 2)) + "％"
        YoY = round((avg_list[-1]/avg_list[-365] * 100) - 100, 2)
    else:
        YoY = 0
    
    x = np.array(list(range(len(avg_list))))
    y = np.array(avg_list)

    #回帰線を求める
    x_reg = x - x.mean()
    y_reg = y - y.mean()

    #傾きを求める
    denom = x_reg * x_reg
    numer = x_reg * y_reg

    answer = round(numer.sum()/denom.sum(),3)

    #直近28日
    avg_list_28 = avg_list[-28:]
    x_28 = np.array(list(range(len(avg_list_28))))
    y_28 = np.array(avg_list_28)

    #回帰線を求める
    x_reg_28 = x_28 - x_28.mean()
    y_reg_28 = y_28 - y_28.mean()

    #傾きを求める
    denom_28 = x_reg_28 * x_reg_28
    numer_28 = x_reg_28 * y_reg_28

    answer_28 = round(numer_28.sum()/denom_28.sum(),3)

    #直近7日
    avg_list_7 = avg_list[-7:]
    x_7 = np.array(list(range(len(avg_list_7))))
    y_7 = np.array(avg_list_7)

    #回帰線を求める
    x_reg_7 = x_7 - x_7.mean()
    y_reg_7 = y_7 - y_7.mean()

    #傾きを求める
    denom_7 = x_reg_7 * x_reg_7
    numer_7 = x_reg_7 * y_reg_7

    answer_7 = round(numer_7.sum()/denom_7.sum(),3)

    #print(keyword + ":" + str(answer) + ":" + str(answer_28) + ":" + str(answer_7))

    #print(keyword + ":" + str(answer) + ":" + str(answer_28) + ":" + str(answer_7))

    #if (((answer > 0 and answer_28 > 0 and answer_7 > 0) or YoY > 0) and max(avg_list) >= 1):
    if ((answer_28 >= 0.001 or answer_7 >= 0.001) and answer >= 0 and max(avg_list) >= 0.1):
    #if (1 >= 0.1):

        print(keyword + ":" + str(answer) + ":" + str(answer_28) + ":" + str(answer_7))

        if(YoY==0):
            YoY = "－"
        else:
            YoY = str(YoY) + "％"

        #if (answer_28 > 0 or answer_7 > 0):
        df.plot(ax = ax, x_compat=True, color="turquoise", alpha=0.7, legend=False)
        dt.plot(ax = ax, x_compat=True, color="tomato", alpha=0.7, legend=False)
        ax.xaxis.set_minor_locator(mdates.MonthLocator())
        
        plt.setp(ax.get_xticklabels(), rotation=60, ha="center")
        plt.title(keyword, fontsize = 13)
        plt.ylabel("件数", fontsize = 10)
        plt.text(0, 1.03, "YoY : " + YoY, size = 12, color = "black", alpha=0.7, transform=ax.transAxes)
        plt.ylim(0)
        #plt.show()

        filename = output_dir + keyword + "_" + exec_date + ".png"
        plt.savefig(filename)
'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');
const moment = require('moment');
const _ = require('underscore');

// new modules
const conf = new(require('../../conf.js'));
const db = new(require('../../modules/db.js'));
const util = new(require('../../modules/util.js'));
const cheerio = new(require('../../modules/cheerio-httpcli.js'));
const selenium = new(require('../../modules/selenium.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
    { name: 'addDays', alias: 'a', type: Number },
    { name: 'skip', alias: 's', type: Boolean },
]);

const addDays = cli['addDays'] || 0;
const today = moment().add(addDays, 'days').format('YYYYMMDD');
const filePath_list = `${conf.dirPath_share_vb}/targets/targets_food.txt`;
const filePath_db1 = `${conf.dirPath_db}/cron0_1.db`;
const filePath_db2 = `${conf.dirPath_db}/cron0_2.db`;
const skip = cli['skip'] ? cli['skip']: false;

let driver = null;
let browser = 'chromium';

// init action
const init = function (isDriver) {

    return new Promise((resolve, reject) => {
        co(function* () {

            // cheerio timeout setting
            yield cheerio.init(10000);

            // run driver
            if (isDriver) {
                driver = yield selenium.init(browser, { inVisible:true });
            }

            // create table "proxy_list"
            yield db.connect(filePath_db);

            resolve();
        });
    });
}

// end action
const end = function (isDriver) {

    return new Promise((resolve, reject) => {
        co(function* () {
            
            yield db.close();

            // run driver
            if (isDriver) {
                driver.quit();
            }

            resolve();
        });
    });
}

// reset drivera
const reset = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            driver.quit();
            yield util.sleep(1500);
            driver = yield selenium.init(browser, { inVisible:true });

            resolve();
        });
    });
}

// get cookpad total amount with selenium
const getCookpadSelenium = function (keyword) {

    return new Promise((resolve, reject) => {
        co(function* () {

            let cookpad = -1;
            let keyword_enc = util.encodeUrl(keyword)
            let url = `https://cookpad.com/search/${keyword_enc}`

            let isopen = yield selenium.open(driver, url);
            if (!isopen) {
                process.exit();
            }

            let $ = yield selenium.parseBody(driver);
            if ($) {
                $('.count').each(function (i, e) {
                    cookpad = util.replace($(e).text().replace(/ /g,''), { price:true, match: '品', removeComma: true, removeMatch: true })
                });
            }
            resolve(cookpad);
        }).catch((e) => {
            // console.log(e);
            driver.quit();
            resolve(-2);
        });
    });
}

// get cookpad total amount with cheerio-httpclient
const getCookpadCheerio = function (keyword) {

    return new Promise((resolve, reject) => {
        co(function* () {

            let keyword_enc = util.encodeUrl(keyword)
            let url_cookpad = `https://cookpad.com/search/${keyword_enc}?page=`
            let page = 1;
            let total = -1;
            let cntRegToday = -1;
            let cntRegPast = 0;
            let cntTotal = 0;
            let isEnd = false;

            // scrape each page if it is not end
            while (!isEnd) {

                let $ = yield cheerio.get(url_cookpad + page, 4, 500);
                if ($) {

                    page++

                    // check total
                    $('.count').each(function (i, e) {
                        total = util.replace($(e).text().replace(/ /g,''), { price:true, match: '品', removeComma: true, removeMatch: true })
                    });

                    let detailUrl = []
                    $('.main_layout .recipe-title').each(function (i, e) {
                        detailUrl.push('https://cookpad.com' + $(e).attr('href'))
                    });

                    console.log(detailUrl, total)

                    // no data > finish scraping
                    if (detailUrl.length < 10) {
                        isEnd = true;
                        // continue;
                    }

                    // with data > count today's reg_date
                    for (let j = 0; j < detailUrl.length; j++) {

                        // cnt total
                        cntTotal++;

                        // set cntRegToday
                        if (cntRegToday == -1) {
                            cntRegToday = 0;
                        }

                        // set total
                        if (cntTotal > total) {
                            isEnd = true;
                            continue;
                        }

                        // reset check detailUrl
                        if (cntRegPast > 2) {
                            isEnd = true;
                            continue;
                        }

                        // detail page
                        let $2 = yield cheerio.get(detailUrl[j], 4, 500);
                        if ($2) {

                            // published_date
                            let published_date = '20' + util.replace($2('#published_date').text().replace(/[ 公開日:\/]/g, ''))
                            // console.log(published_date)

                            // same date > count 
                            if (today == published_date) {
                                // console.log(published_date, detailUrl[j])
                                // cntRegToday = cntRegToday == -1 ? 1: cntRegToday + 1;
                                cntRegToday++
                                cntRegPast = 0

                            } else {
                                cntRegPast++

                            }
                        }
                    }

                    // console.log(cntRegPast, cntRegToday, '<<<')
                }

            }

            // return count
            resolve({total, cntToday:cntRegToday});

        }).catch((e) => {

            driver.quit();
            resolve(-2);
        });
    });
}

// get rakuten total amount with cheerio-httpclient
const getRakutenCheerio = function (keyword) {

    return new Promise((resolve, reject) => {
        co(function* () {

            let keyword_enc = util.encodeUrl(keyword)
            let url_rakuten = `https://recipe.rakuten.co.jp/search/${keyword_enc}/{0}/?s=0&v=0&t=2`
            let page = 1;
            let total = -1;
            let cntRegToday = -1;
            let cntRegPast = 0;
            let cntTotal = 0;
            let isEnd = false;

            // scrape each page if it is not end
            while (!isEnd) {

                let url = url_rakuten.replace(/\{0\}/g, page);
                let $ = yield cheerio.get(url, 4, 500);
                if ($) {

                    page++

                    // check total
                    $('.cateResultTit').each(function (i, e) {
                        total = util.replace($(e).text().replace(/ /g,''), { price:true, match: '品', removeComma: true, removeMatch: true })
                    });

                    let detailUrl = []
                    $('.contentsBox li a').each(function (i, e) {
                        let href = $(e).attr('href')
                        if (href && href.match(/recipe_list_detail_recipe/i)) {
                            detailUrl.push('https://recipe.rakuten.co.jp' + href)
                        }
                    });

                    // no data > finish scraping
                    if (detailUrl.length < 10) {
                        isEnd = true;
                        // continue;
                    }

                    // with data > count today's reg_date
                    for (let j = 0; j < detailUrl.length; j++) {

                        // cnt total
                        cntTotal++;

                        // set cntRegToday
                        if (cntRegToday == -1) {
                            cntRegToday = 0;
                        }

                        // set total
                        if (cntTotal > total) {
                            isEnd = true;
                            continue;
                        }

                        // reset check detailUrl
                        if (cntRegPast > 2) {
                            isEnd = true;
                            continue;
                        }

                        // detail page
                        let $2 = yield cheerio.get(detailUrl[j], 4, 500);
                        if ($2) {

                            // published_date
                            let published_date = util.replace($2('#publish_day_itemprop').text().replace(/[\.]/g, ''))

                            // same date > count 
                            if (today == published_date) {
                                // cntRegToday = cntRegToday == -1 ? 1: cntRegToday + 1;
                                cntRegToday++
                                cntRegPast = 0

                            } else {
                                cntRegPast++

                            }
                        }
                    }

                    // console.log(cntRegPast, cntRegToday, '<<<')
                }

            }

            // return count
            resolve({total, cntToday:cntRegToday});

        }).catch((e) => {

            // driver.quit();
            resolve(-2);
        });
    });
}







// get cookpad total amount with cheerio-httpclient
const getCookpadCheerioTotal = function (keyword) {

    return new Promise((resolve, reject) => {
        co(function* () {

            let keyword_enc = util.encodeUrl(keyword)
            let url_cookpad = `https://cookpad.com/search/${keyword_enc}`
            let total = -1;

            let $ = yield cheerio.get(url_cookpad, 3, 500);
            if ($) {

                // check total
                $('.count').each(function (i, e) {
                    total = util.replace($(e).text().replace(/ /g,''), { price:true, match: '品', removeComma: true, removeMatch: true })
                });
            }

            // return count
            resolve(total);

        }).catch((e) => {

            resolve(-2);
        });
    });
}

// get rakuten total amount with cheerio-httpclient
const getRakutenCheerioTotal = function (keyword) {

    return new Promise((resolve, reject) => {
        co(function* () {

            let keyword_enc = util.encodeUrl(keyword)
            let url_rakuten = `https://recipe.rakuten.co.jp/search/${keyword_enc}/1/`
            let total = -1;

            let $ = yield cheerio.get(url_rakuten, 3, 500);
            if ($) {

                // check total
                $('.cateResultTit').each(function (i, e) {
                    total = util.replace($(e).text().replace(/ /g,''), { price:true, match: '品', removeComma: true, removeMatch: true })
                });
            }

            // return count
            resolve(total);

        }).catch((e) => {

            resolve(-2);
        });
    });
}

// reset database
const resetDB = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // drop table proxy_list
            yield db.connect(filePath_db);

            const sql1a_base = `DROP TABLE IF EXISTS FGS283_recipe_rakuten_base;`;
            yield db.do(`run`, sql1a_base, {});
            const sql2a_base = `CREATE TABLE IF NOT EXISTS FGS283_recipe_rakuten_base(keyword TEXT, total INTEGER, cntToday INTEGER, reg_date TEXT, PRIMARY KEY (keyword, reg_date));`;
            yield db.do(`run`, sql2a_base, {});
            // const sql1a_ids = `DROP TABLE IF EXISTS FGS283_recipe_rakuten_ids;`;
            // yield db.do(`run`, sql1a_ids, {});
            // const sql2a_ids = `CREATE TABLE IF NOT EXISTS FGS283_recipe_rakuten_ids(keyword TEXT primary key, id TEXT, reg_date TEXT);`;
            // yield db.do(`run`, sql2a_ids, {});

            const sql1b_base = `DROP TABLE IF EXISTS FGS283_recipe_cookpad_base;`;
            yield db.do(`run`, sql1b_base, {});
            const sql2b_base = `CREATE TABLE IF NOT EXISTS FGS283_recipe_cookpad_base(keyword TEXT, total INTEGER, cntToday INTEGER, reg_date TEXT, PRIMARY KEY (keyword, reg_date));`;
            yield db.do(`run`, sql2b_base, {});
            // const sql1b_ids = `DROP TABLE IF EXISTS FGS283_recipe_cookpad_ids;`;
            // yield db.do(`run`, sql1b_ids, {});
            // const sql2b_ids = `CREATE TABLE IF NOT EXISTS FGS283_recipe_cookpad_ids(keyword TEXT primary key, id TEXT, reg_date TEXT);`;
            // yield db.do(`run`, sql2b_ids, {});

            yield db.close();

            resolve();
        });
    });
}

// delete database
const deleteDB = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // drop table proxy_list
            yield db.connect(filePath_db);

            const beforeday = moment().add(-3, 'days').format('YYYYMMDD');
            const sql1 = `delete from FGS283_recipe_rakuten_base where reg_date < '${beforeday}';`;
            yield db.do(`run`, sql1, {});
            const sql2 = `delete from FGS283_recipe_cookpad_base where reg_date < '${beforeday}';`;
            yield db.do(`run`, sql2, {});
            // const sql3 = `delete from FGS283_recipe_rakuten_ids where reg_date < '${beforeday}';`;
            // yield db.do(`run`, sql3, {});
            // const sql4 = `delete from FGS283_recipe_cookpad_ids where reg_date < '${beforeday}';`;
            // yield db.do(`run`, sql4, {});

            yield db.close();

            resolve();
        });
    });
}

// set url list cookpad
const setListCookpad = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // cheerio timeout setting
            yield cheerio.init(10000);
            let filePath_list = `${conf.dirPath_share_vb}/targets/FGS283_recipe_posts_cookpad_total_20181211.txt`;
            let listPre = util.getRecords(filePath_list, { head:null, delimiter:null });
            // let AllList = _.map(listPre.records, function(arr){ return arr[0]; });
            // console.log(listPre.records);

            let records = listPre.records;
            for (let i = 0; i < records.length; i++) {
                const sql = `INSERT OR REPLACE INTO FGS283_recipe_cookpad_base(keyword, total, cntToday, reg_date) VALUES (?, ?, ?, ?);`;
                let opt = [records[i][0], records[i][1], -1, records[i][2]];
                yield db.do(`run`, sql, opt);
                // yield util.sleep(200)
            }

            resolve();
        });
    });
}

// set url list rakuten
const setListRakuten = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // cheerio timeout setting
            yield cheerio.init(10000);
            let filePath_list = `${conf.dirPath_share_vb}/targets/FGS283_recipe_posts_rakuten_total_20181211.txt`;
            let listPre = util.getRecords(filePath_list, { head:null, delimiter:null });
            // let AllList = _.map(listPre.records, function(arr){ return arr[0]; });
            // console.log(listPre.records);

            let records = listPre.records;
            for (let i = 0; i < records.length; i++) {
                const sql = `INSERT OR REPLACE INTO FGS283_recipe_rakuten_base(keyword, total, cntToday, reg_date) VALUES (?, ?, ?, ?);`;
                let opt = [records[i][0], records[i][1], -1, records[i][2]];
                yield db.do(`run`, sql, opt);
                // yield util.sleep(200)
            }

            resolve();
        });
    });
}









// get url list cookpad
const getListCookpad = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // cheerio timeout setting
            yield cheerio.init(10000);
            let listPre = util.getRecords(filePath_list, { head:null, delimiter:null });
            let allList = _.map(listPre.records, function(arr){ return arr[0]; });
            let yesterday = moment().add(-1, 'days').format('YYYYMMDD');

            // finished data
            // 昨日と今日で、どちらもデータなしとなっているものはfinishとみなす。
            // 昨日 データなし
            let sql3 = `SELECT keyword FROM FGS283_recipe_cookpad_base where reg_date = '${yesterday}' and total <= 0 and cntToday = -1 ;`;
            let res3 = yield db.do(`all`, sql3, {});
            let finishedList1a = _.map(res3, function(obj){ return obj.keyword; });
            // 今日 データなし
            sql3 = `SELECT keyword FROM FGS283_recipe_cookpad_base where reg_date = '${today}' and total = -1 and cntToday = -1 ;`;
            res3 = yield db.do(`all`, sql3, {});
            let finishedList1b = _.map(res3, function(obj){ return obj.keyword; });
            // 昨日と今日 データなし
            let finishedList1 = _.intersection(finishedList1a, finishedList1b);

            // 本日、データありの結果がtotalか本日の結果に存在するならfinishとみなす。
            let sql2 = `SELECT keyword FROM FGS283_recipe_cookpad_base where reg_date = '${today}' and total > -1 and cntToday > -1 ;`;
            let res2 = yield db.do(`all`, sql2, {});
            let finishedList2 = _.map(res2, function(obj){ return obj.keyword; });
            let finishedList = _.union(finishedList1, finishedList2);

            // 昨日か今日のtotalにデータが存在し、本日finishしていないなら、1件以上のデータが存在する未調査のリストとみなす。
            let sql1 = `SELECT keyword FROM FGS283_recipe_cookpad_base where reg_date in ('${yesterday}', '${today}') and total > 0 ;`;
            let res1 = yield db.do(`all`, sql1, {});
            let existList = _.map(res1, function(obj){ return obj.keyword; });
            existList = _.uniq(existList)
            existList = _.difference(existList, finishedList);

            // 未調査の0件のデータなしリスト
            let nodataList = _.difference(allList, existList);
            nodataList = _.difference(nodataList, finishedList);

            console.log(allList.length, existList.length, nodataList.length, finishedList.length)

            resolve({existList, nodataList});
        });
    });
}

// get url list rakuten
const getListRakuten = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // cheerio timeout setting
            yield cheerio.init(10000);
            let listPre = util.getRecords(filePath_list, { head:null, delimiter:null });
            let allList = _.map(listPre.records, function(arr){ return arr[0]; });
            let yesterday = moment().add(-1, 'days').format('YYYYMMDD');

            // finished data
            // 昨日と今日で、どちらもデータなしとなっているものはfinishとみなす。
            // 昨日 データなし
            let sql3 = `SELECT keyword FROM FGS283_recipe_rakuten_base where reg_date = '${yesterday}' and total <= 0 and cntToday = -1 ;`;
            let res3 = yield db.do(`all`, sql3, {});
            let finishedList1a = _.map(res3, function(obj){ return obj.keyword; });
            // 今日 データなし
            sql3 = `SELECT keyword FROM FGS283_recipe_rakuten_base where reg_date = '${today}' and total = -1 and cntToday = -1 ;`;
            res3 = yield db.do(`all`, sql3, {});
            let finishedList1b = _.map(res3, function(obj){ return obj.keyword; });
            // 昨日と今日 データなし
            let finishedList1 = _.intersection(finishedList1a, finishedList1b);

            // 本日、データありの結果がtotalか本日の結果に存在するならfinishとみなす。
            let sql2 = `SELECT keyword FROM FGS283_recipe_rakuten_base where reg_date = '${today}' and total > -1 and cntToday > -1 ;`;
            let res2 = yield db.do(`all`, sql2, {});
            let finishedList2 = _.map(res2, function(obj){ return obj.keyword; });
            let finishedList = _.union(finishedList1, finishedList2);

            // 昨日か今日のtotalにデータが存在し、本日finishしていないなら、1件以上のデータが存在する未調査のリストとみなす。
            let sql1 = `SELECT keyword FROM FGS283_recipe_rakuten_base where reg_date in ('${yesterday}', '${today}') and total > 0 ;`;
            let res1 = yield db.do(`all`, sql1, {});
            let existList = _.map(res1, function(obj){ return obj.keyword; });
            existList = _.uniq(existList)
            existList = _.difference(existList, finishedList);

            // 未調査の0件のデータなしリスト
            let nodataList = _.difference(allList, existList);
            nodataList = _.difference(nodataList, finishedList);

            console.log(allList.length, existList.length, nodataList.length, finishedList.length)

            resolve({existList, nodataList});
        });
    });
}

// scrape proxy data, and set db as proxy_list
const scrapeRakuten = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let reg_date = moment().format('YYYYMMDD');
            let res = yield getListRakuten();

            // nodata > get data with selenium
            let list = res.nodataList;

            if (!skip) {
                for (let i = 0; i < list.length; i++) {

                    let total = yield getRakutenCheerioTotal(list[i]);
                    if (total < 0) {

                        // insert
                        const sql = `INSERT OR REPLACE INTO FGS283_recipe_rakuten_base(keyword, total, cntToday, reg_date) VALUES (?, ?, ?, ?);`;
                        let opt = [list[i], -1, -1, reg_date];
                        console.log(opt, 700)
                        yield db.do(`run`, sql, opt);

                    } else if (total > 0) {

                        // scrape usuall
                        let res2 = yield getRakutenCheerio(list[i])

                        // insert
                        const sql = `INSERT OR REPLACE INTO FGS283_recipe_rakuten_base(keyword, total, cntToday, reg_date) VALUES (?, ?, ?, ?);`;
                        let opt = [list[i], res2.total, res2.cntToday, reg_date];
                        console.log(opt, 701)
                        yield db.do(`run`, sql, opt);
                    }
                }
            }

            // scrape rakuten exist data
            list = res.existList;
            for (let i = 0; i < list.length; i++) {

                // scrape usuall
                let res2 = yield getRakutenCheerio(list[i])

                // insert
                const sql = `INSERT OR REPLACE INTO FGS283_recipe_rakuten_base(keyword, total, cntToday, reg_date) VALUES (?, ?, ?, ?);`;
                let opt = [list[i], res2.total, res2.cntToday, reg_date];
                console.log(opt, 702)
                yield db.do(`run`, sql, opt);
            }

            resolve();
        });
    });
}

// scrape proxy data, and set db as proxy_list
const scrapeCookpad = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // get list of share/targets_food
            let reg_date = moment().format('YYYYMMDD');
            let res = yield getListCookpad();

            // nodata > get data with selenium
            let list = res.nodataList;

            if (!skip) {
                for (let i = 0; i < list.length; i++) {

                    let total = yield getCookpadCheerioTotal(list[i]);
                    if (total < 0) {

                        // insert
                        const sql = `INSERT OR REPLACE INTO FGS283_recipe_cookpad_base(keyword, total, cntToday, reg_date) VALUES (?, ?, ?, ?);`;
                        let opt = [list[i], -1, -1, reg_date];
                        console.log(opt, 700)
                        yield db.do(`run`, sql, opt);

                    } else if (total > 0) {

                        // scrape usuall
                        let res2 = yield getCookpadCheerio(list[i])

                        // insert
                        const sql = `INSERT OR REPLACE INTO FGS283_recipe_cookpad_base(keyword, total, cntToday, reg_date) VALUES (?, ?, ?, ?);`;
                        let opt = [list[i], res2.total, res2.cntToday, reg_date];
                        console.log(opt, 701)
                        yield db.do(`run`, sql, opt);
                    }
                }
            }

            // reset res
            res = yield getListCookpad();

            // nodata > get data with selenium
            list = res.existList;
            for (let i = 0; i < list.length; i++) {

                let {total, cntToday} = yield getCookpadCheerio(list[i])

                // insert
                const sql = `INSERT OR REPLACE INTO FGS283_recipe_cookpad_base(keyword, total, cntToday, reg_date) VALUES (?, ?, ?, ?);`;
                let opt = [list[i], total, cntToday, reg_date];
                console.log(opt, 702)
                yield db.do(`run`, sql, opt);
            }

            resolve();
        });
    });
}

// set out file to share folder
const setOutFileRakuten = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // get scraped data
            yield db.connect(filePath_db);

            let sql = `SELECT keyword, total, cntToday, reg_date FROM FGS283_recipe_rakuten_base where reg_date = '${today}' and total > -1`;
            let res = yield db.do(`all`, sql, {});

            // write header
            let filepath2 = `${conf.dirPath_share_vb}/FGS283_recipe_posts_rakuten_total_${today}.txt`;
            fs.writeFileSync(filepath2, '', 'utf-8');
            util.sleep(1500);

            // write body
            for (let i = 0; i < res.length; i++) {
                let total = res[i].total >= 0 ? res[i].total: '';
                let cntToday = res[i].cntToday >= 0 ? res[i].cntToday: '';
                fs.appendFileSync(filepath2, [res[i].keyword, total, cntToday, res[i].reg_date].join('\t') + '\n', 'utf-8');
            }

            yield db.close();
            resolve(res);
        });
    });
}

// set out file to share folder
const setOutFileCookpad = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // get scraped data
            yield db.connect(filePath_db);

            let sql = `SELECT keyword, total, cntToday, reg_date FROM FGS283_recipe_cookpad_base where reg_date = '${today}' and total > -1`;
            let res = yield db.do(`all`, sql, {});

            // write header
            let filepath1 = `${conf.dirPath_share_vb}/FGS283_recipe_posts_cookpad_total_${today}.txt`;
            fs.writeFileSync(filepath1, '', 'utf-8');
            util.sleep(1500);

            // write body
            for (let i = 0; i < res.length; i++) {
                let total = res[i].total >= 0 ? res[i].total: '';
                let cntToday = res[i].cntToday >= 0 ? res[i].cntToday: '';
                fs.appendFileSync(filepath1, [res[i].keyword, total, cntToday, res[i].reg_date].join('\t') + '\n', 'utf-8');
            }

            yield db.close();
            resolve(res);
        });
    });
}


// run action
const run = function () {

    co(function* () {
        // 
        switch (cli['run']) {
            case 99: // reset database
                yield resetDB();
                break;

            case 98: // delete database
                yield deleteDB();
                break;

            case 5: // out data to share folder
                yield init();
                yield setListCookpad();
                yield setListRakuten();
                yield end();
                break;

            case 4: // out data to share folder
                yield setOutFileRakuten();
                break;

            case 3: // out data to share folder
                yield setOutFileCookpad();
                break;

            case 2: // scrape data
                yield init();
                yield scrapeRakuten();
                yield end();
                break;

            case 1: // scrape data
            default:
                // yield init(true);
                yield init();
                yield scrapeCookpad();
                // yield end(true);
                yield end();
                break;
        }
    });
}

run();

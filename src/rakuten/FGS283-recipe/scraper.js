'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');
const moment = require('moment');
//const _ = require('underscore');

// new modules
const conf = new(require('../../conf.js'));
const db = new(require('../../modules/db.js'));
const util = new(require('../../modules/util.js'));
const cheerio = new(require('../../modules/cheerio-httpcli.js'));
const selenium = new(require('../../modules/selenium.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number }, // 1:new 2:continue 9:output
    { name: 'site', alias: 's', type: Number }, // 1:cookpad 2:rakuten
    { name: 'cron', alias: 'n', type: String },
    { name: 'fname', alias: 'f', type: String },// target file name
//    { name: 'addDays', alias: 'a', type: Number },
//    { name: 'DB', alias: 'b', type: String }, //
]);

let mode;
if(cli['run'] == 1){
    mode = 'new';
} else if (cli['run'] == 2){
    mode = 'continue';
} else if (cli['run'] == 9){
    mode = 'output';
}

const site = cli['site'] || 0;
if(site == 0){
    console.log(` -r cli['site'] null error`);
    return;
}else if (site != 1 && site != 2){
    console.log(` -r cli['site'] error : ${cli['site']}`);
    return;  
}
let category;
if(site == 1) {
    category = 'cookpad'
} else {
    category = 'rakuten'
}
const result_table = `FGS283_recipe_${category}_posts`;
const targets_table = `FGS283_targets_food`;
const max_table = `FGS283_recipe_${category}_max`;
const logtable = 'FGS283_log';

const cron = cli['cron'] || 'cron14';
const filePath_db = `${conf.dirPath_db}/${cron}.db`;
const getdate = moment().day(0).format('YYYYMMDD');
const getdate_hyphen = moment().day(0).format('YYYY-MM-DD');

const infile = `${conf.dirPath_share_vb}/targets/${cli['fname']}` || `${conf.dirPath_share_vb}/targets/targets_food.txt`;
const outfile = `${conf.dirPath_share_vb}/FGS283_recipe_posts_${category}_total_${getdate}.txt`;
const outlogfile = `${conf.dirPath_share_vb}/log/FGS283_recipe_${category}.log`;
const maxfile = `${conf.dirPath_share_vb}/targets/FGS283_recipe_${category}_max.txt`;
const ngfile = `${conf.dirPath_share_vb}/targets/FGS283_recipe_${category}_ngchk.txt`;

let getProc;// = 1;
const sttime = moment().format('YYYY/MM/DD HH:mm:ss');
console.log(`/// FGS283 recipe scraper.js start  ${sttime} ///`);
console.log(`  mode      : ${mode}`);
console.log(`  site      : ${category}`);
console.log(`  DB cron   : ${cron}`);
console.log(`  lastsunday: ${getdate}`);
console.log(`  infile    : ${infile}`);
console.log(`  outfile   : ${outfile}`);

const maxloop = 50;
const minnum = 0;

// variable
let keywords = [];

let tgtnum; // targets table word number
//let getkwd = [];
let getkwds = 0;
let lcnt = 1;
let zcnt = 0;
let starttime;
let posts = 0;
let keyword;

let driver = null;
let browser = 'chromium';
let lcomp = 0;
let stopflg = false;
let ngflg = 0;

// end action
const end = function (isDriver) {

    return new Promise((resolve, reject) => {
        co(function* () {
            
            yield db.close();

            resolve();
        });
    });
}

// insert Log
const insertLog = function () {

    return new Promise((resolve, reject) => {
        co(function* () {
            starttime  = moment().format('YYYY/MM/DD HH:mm:ss');
            const sql = `INSERT OR REPLACE INTO ${logtable} ` +
                `(tbname, getdate, loop_count, remainkwd, starttime, getProc) ` +
                `VALUES (?, ?, ?, ?, ?, ?);`;
            let opt = [result_table, getdate, lcnt, keywords.length, starttime, getProc];
            console.log(` insert log : tname:${result_table} getdate:${getdate} lcnt:${lcnt} kwds:${keywords.length} cron:${cron}  ${starttime}`);
            yield db.do(`run`, sql, opt);
            resolve();
        });
    });
}

// update Log
const updateLog = function () {

    return new Promise((resolve, reject) => {
        co(function* () {
            const curtime  = moment().format('YYYY/MM/DD HH:mm:ss');
            const sql = `UPDATE ${logtable} SET getkwd = ${getkwds}, endtime = '${curtime}', complete = ${lcomp} ` +
            `WHERE tbname = '${result_table}' and getdate = '${getdate}' and loop_count = ${lcnt} and starttime = '${starttime}';`;
            //console.log(sql);
            console.log(` update log : tname:${result_table} getdate:${getdate} lcnt:${lcnt} getkwds:${getkwds} lcomp:${lcomp} cron:${cron}  ${starttime}-${curtime}`);
            yield db.do(`run`, sql, {});
            resolve();
        });
    });
}

// output Log
const outLog = function () {

    return new Promise((resolve, reject) => {
        co(function* () {
            let sql = `select tbname, getdate, loop_count, remainkwd, getkwd, starttime, endtime, complete, getProc from ${logtable} ` +
                      `where tbname = '${result_table}' and getdate = '${getdate}' order by starttime;`;
            //console.log(sql);
            let res = yield db.do(`all`, sql, {});            

            console.log(`output log: ${outlogfile}`);

            // append
            for (let i = 0; i < res.length; i++) {
                fs.appendFileSync(outlogfile, ([res[i].tbname, res[i].getdate, res[i].loop_count, res[i].remainkwd, res[i].getkwd, res[i].starttime, res[i].endtime, res[i].complete, res[i].getProc].join('\t') + '\n'), 'utf8');
            }

            resolve();

        });
    });
}

// output NG file
const outNGfile = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let sql = `select a.keyword keyword, '${getdate}' getdate, a.posts posts, a.maxposts maxposts, a.ngflg ngflg, a.regtime regtime
            from ${result_table} a
            where not exists (select * from ${result_table} c where c.getdate = '${getdate}' and c.ngflg = 0 and c.keyword = a.keyword ) 
            and a.getdate = '${getdate}' order by 1,6;`;

            //console.log(sql);
            let res = yield db.do(`all`, sql, {});            

            if(res.length > 0){
                console.log(` output NG file: ${ngfile}`);

                // append
                for (let i = 0; i < res.length; i++) {
                    fs.appendFileSync(ngfile, ([res[i].keyword, res[i].getdate, res[i].posts, res[i].maxposts, res[i].ngflg, res[i].regtime].join('\t') + '\n'), 'utf8');
                }

            }else{
                console.log(` not exists NG words`);

            }

            resolve();

        });
    });
}

// init action
const init_table = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let sqlstr;

            // targets table drop
            sqlstr = `DROP TABLE IF EXISTS ${targets_table};`;
            //console.log(sqlstr);
            yield db.do(`run`, sqlstr, {});

            // targets table create
            sqlstr = `CREATE TABLE IF NOT EXISTS ${targets_table} ` +
                `(keyword TEXT PRIMARY KEY);`;
            //console.log(sqlstr);
            yield db.do(`run`, sqlstr, {});

            
            // max table drop
            sqlstr = `DROP TABLE IF EXISTS ${max_table};`;
            //console.log(sqlstr);
            yield db.do(`run`, sqlstr, {});

            // max table create
            sqlstr = `CREATE TABLE IF NOT EXISTS ${max_table} ` +
                `(keyword TEXT PRIMARY KEY, maxposts bigint);`;
            //console.log(sqlstr);
            yield db.do(`run`, sqlstr, {});


            // result table create
            sqlstr = `CREATE TABLE IF NOT EXISTS ${result_table} ` +
                `(keyword TEXT, getdate TEXT, posts bigint, maxposts bigint, ngflg integer, regtime TEXT);`;
            //console.log(sqlstr);
            yield db.do(`run`, sqlstr, {});

            sqlstr = `CREATE INDEX IF NOT EXISTS i01_${result_table} on ${result_table} (keyword, getdate, ngflg);`;
            //console.log(sqlstr);
            yield db.do(`run`, sqlstr, {});

            // old result delete
            const deldate = moment(getdate_hyphen).subtract(35, 'days').format('YYYYMMDD');
            console.log(` result deldate: ${deldate}`);
            sqlstr = `DELETE FROM ${result_table} WHERE getdate < '${deldate}' or getdate = '${getdate}';`;
            //console.log(sqlstr);
            yield db.do(`run`, sqlstr, {});

            // log table create
            sqlstr = `CREATE TABLE IF NOT EXISTS ${logtable} ` +
                `(tbname TEXT, getdate TEXT, loop_count integer, remainkwd bigint, getkwd bigint, starttime TEXT, endtime TEXT, complete integer, getProc integer );`;
            //console.log(sqlstr);
            yield db.do(`run`, sqlstr, {});

            sqlstr = `CREATE INDEX IF NOT EXISTS i_${logtable} on ${logtable} (tbname, getdate);`;
            //console.log(sqlstr);
            yield db.do(`run`, sqlstr, {});

            // old log delete
            const deldate2  = moment(getdate_hyphen).subtract(35, 'days').format('YYYY/MM/DD');
            console.log(` log deldate: ${deldate2}`);
            sqlstr = `DELETE FROM ${logtable} WHERE tbname = '${result_table}' and getdate < '${deldate2}' or getdate = '${getdate}';`;
            //console.log(sqlstr);
            yield db.do(`run`, sqlstr, {});

            resolve();
        });
    });
}

// store targets
const store_targets = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let result = false;
            //infile = `${conf.dirPath_share_vb}/targets_${category}.txt`;
            //console.log('infile',infile);
            if (fs.existsSync(infile)) {
                console.log(` infile: ${infile} read start`);
                let kywds = fs.readFileSync(infile).toString().split("\n");
                //console.log(kywds.length);
                if(kywds[kywds.length -1].length == 0) kywds.pop();
                console.log(` targets file: ${kywds.length}`);
                
                let sql = `insert into ${targets_table} values `;
                if (kywds.length > 0) {
                    const n = 3000;
                    let times = Math.floor(kywds.length / n);
                    //console.log('n: ' + n + '  times: ' + times);
                    if (times * n == kywds.length) times--;
                    let i = 0;
                    let end = 0;
                    do {
                        if (times <= i) {
                            end = kywds.length;
                        } else {
                            end = (i + 1) * n;
                        }
                        let st = i * n;
                        //console.log('st = [' + st + '] end = [' + end + ']');
                        let exc = kywds.slice(st, end);
                        //console.log(exc[0],exc[exc.length -1]);
                        //console.log(sql + '("' + exc.join('"),("') + '");');
                        yield db.do(`run`, sql + '("' + exc.join('"),("') + '");', {});
                        i++;
                    } while (times >= i);
                }
                result = true;

            }else{
                console.log('  not exists in file');
            
            }

            //maxfile
            if (fs.existsSync(maxfile) && result) {
                result = false;

                console.log(` maxfile: ${maxfile} read start`);
                let tgtsmaxfile = fs.readFileSync(maxfile).toString().split("\n");
                //console.log(kywds.length);
                if(tgtsmaxfile[tgtsmaxfile.length -1].length == 0) tgtsmaxfile.pop();
                //console.log(` targets max file: ${tgtsmaxfile.length}`);

                let mxkywds = tgtsmaxfile;
                console.log(` targets max keyword: ${mxkywds.length}`);

                let sql = `insert into ${max_table} values `;
                if (mxkywds.length > 0) {
                    const n = 3000;
                    let times = Math.floor(mxkywds.length / n);
                    //console.log('n: ' + n + '  times: ' + times);
                    if (times * n == mxkywds.length) times--;
                    let i = 0;
                    let end = 0;
                    do {
                        if (times <= i) {
                            end = mxkywds.length;
                        } else {
                            end = (i + 1) * n;
                        }
                        let st = i * n;
                        //console.log('st = [' + st + '] end = [' + end + ']');
                        let exc = mxkywds.slice(st, end);
                        //console.log(exc[0],exc[exc.length -1]);
                        let arr = [];
                        for ( let iarr = 0, len = exc.length; iarr < len; ++iarr) {
                            //console.log( exc[iarr].replace('///','",'));
                            arr.push(exc[iarr].replace('///','",'));
                        }
                        //console.log(sql + '("' + arr.join('),("') + ');');
                        yield db.do(`run`, sql + '("' + arr.join('),("') + ');', {});
                        i++;
                    } while (times >= i);
                }                
                result = true;

            }else{
                console.log('  not exists maxfile');
                result = false;

            }
            resolve(result);
        });
    });
}

// get targets list
const get_targets = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //tgtnum            
            const sql1 = `select count(*) cnt from ${targets_table};`;
            //console.log(sql1);
            let num = yield db.do(`all`, sql1, {});
            //console.log('num: ' + num);
            tgtnum = num[0].cnt;
            console.log(' targets table: ' + tgtnum);

            const sql = `select a.keyword keyword, ifnull(m.maxposts, 0) maxposts, 0 ngflg
            from ${targets_table} a 
            left join ${max_table} m on a.keyword = m.keyword
            where not exists ( select * from ${result_table} b where b.getdate = '${getdate}' and a.keyword = b.keyword )
            order by 1;`;
            //console.log(sql);
            keywords = yield db.do(`all`, sql, {});
            console.log('  get_targets  remain keywords: ' + keywords.length);

            if ( keywords.length == 0 ) {
                console.log(`    end once scraping`);
                yield get_targets2();
                if ( keywords.length == 0 ) {
                    console.log(`    end scraping`);
                }else{
                    getProc = 2;
                    console.log(`    getProc : 2`);
                }
            }else{
                getProc = 1;
                console.log(`    getProc : 1`);
            }

            resolve();

        });
    });
}

// get targets 2
const get_targets2 = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            const sql = `select a.keyword keyword, a.maxposts maxposts, a.ngflg ngflg
             from ( select keyword, max(maxposts) maxposts, max(ngflg) ngflg from ${result_table}
             where getdate = '${getdate}' and ngflg != 0 group by 1 ) a
            where not exists (select * from ${result_table} c where c.getdate = '${getdate}' 
                and c.ngflg = 0 and a.keyword = c.keyword )
              and not exists (select * from (
                  select keyword,count(*) from ${result_table} 
                   where getdate = '${getdate}' and ngflg != 0  
                   group by 1 having count(*) > 1
                ) tt where a.keyword = tt.keyword )
            order by 3,2,1;`;
            //console.log(`get2 sql:${sql}`);
            //console.log(`${moment().format('YYYY/MM/DD HH:mm:ss.SSS')}`);
            keywords = yield db.do(`all`, sql, {});
            //console.log(`${moment().format('YYYY/MM/DD HH:mm:ss.SSS')}`);
            console.log('  get_targets2 remain keywords: ' + keywords.length);

            resolve();
        });
    });
}

// get loop count
const get_loop_count = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //loop count
            
            const sql = `select max(loop_count) lct from ${logtable} where getdate = '${getdate}' and tbname = '${result_table}' and complete = 1;`;
            //console.log(sql);
            let num = yield db.do(`all`, sql, {});
            //console.log(' max loop_count: ' + num[0].lct);
            if ( num[0].lct > 0 ) {
                lcnt = num[0].lct + 1;
            }
            console.log(' loop_count: ' + lcnt);

            resolve();
        });
    });
}

// scrape cookpad posts
const get_cookpad = function (idx, kwd) {

    return new Promise((resolve, reject) => {
        co(function* () {

            if(!process.env.NODE_TLS_REJECT_UNAUTHORIZED){
                yield cheerio.init();
            }
            let result = 'false';
            const maxcnt = 3;
            let retry = false;
            let rcnt = 0;       //retryカウンタ
            
            do {
                if(retry) {
                    rcnt = rcnt + 1;
                } else {
                    rcnt = 0;
                } 
                        
                if(rcnt >= maxcnt){
                    console.log(`      get_cookpad retrymax  ${idx}:${kwd}  ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
                    result = 'false';
                    break;

                } else {

                    let url = `https://cookpad.com/search/${keyword}`;
                    //console.log(url);

                    let $ = yield cheerio.get2(url);
                    if(!$) {
                        yield util.sleep(1000);
                        if(!$) {
                            //console.log(`       get_cookpad get2(url) err ${idx}:${kwd}`);
                            retry = true;
                            continue;
                        }
                    }
                    let h = $('body').html();
                    if(!h || h.length == 0) {
                        yield util.sleep(1000);
                        if(!h || h.length == 0) {
                            console.log(`       get_cookpad get2(html) err ${idx}:${kwd}`);
                            retry = true;
                            continue;
                        }
                    }
                    //console.log(h);
                    if ( h.match(/recipes_section/i) ) {
                        let tmpstr = $('.count').text();
                        //console.log(`tmpstr:${tmpstr}`);

                        if (tmpstr.search('品') > 0) {
                            posts = tmpstr.replace(/[^0-9]/g, '');
                            //console.log(`posts:${posts}`);
                            result = 'true';
                            break;

                        }else{
                            console.log(`       get_cookpad get count err ${idx}:${kwd}`);
                            yield util.sleep(1000);
                            retry = true;
                            continue;
                        }

                    } else {
                        console.log(`       get_cookpad get posts err ${idx}:${kwd}`);
                        yield util.sleep(1000);
                        retry = true;
                        continue;
                    }
                }
            } while (true)

            resolve(result);
        }).catch((e) => {
           console.log('    catch get_cookpad');
           console.log(e);
           resolve('catch');
        });
    });
}


// scrape rakuten posts
const get_rakuten = function (idx, kwd) {

    return new Promise((resolve, reject) => {
        co(function* () {

            if(!process.env.NODE_TLS_REJECT_UNAUTHORIZED){
                yield cheerio.init();
            }
            let result = 'false';
            const maxcnt = 3;
            let retry = false;
            let rcnt = 0;       //retryカウンタ
            
            do {
                if(retry) {
                    rcnt = rcnt + 1;
                } else {
                    rcnt = 0;
                } 
                        
                if(rcnt >= maxcnt){
                    console.log(`      get_rakuten retrymax  ${idx}:${kwd}  ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
                    result = 'false';
                    break;

                } else {

                    let url = `https://recipe.rakuten.co.jp/search/${keyword}`;
                    //console.log(url);

                    let $ = yield cheerio.get2(url);
                    if(!$) {
                        yield util.sleep(1000);
                        if(!$) {
                            //console.log(`       get_rakuten get2(url) err ${idx}:${kwd}`);
                            retry = true;
                            continue;
                        }
                    }
                    let h = $('body').html();
                    if(!h || h.length == 0) {
                        yield util.sleep(1000);
                        if(!h || h.length == 0) {
                            console.log(`       get_rakuten get2(html) err ${idx}:${kwd}`);
                            retry = true;
                            continue;
                        }
                    }

                    if ( h.match(/cateResultTit/i) ) {
                        let tmpstr = $('.cateResultTit').text();
                        //console.log(`tmpstr:${tmpstr}`);

                        if (tmpstr.search('の人気レシピ') > 0) {
                            tmpstr = tmpstr.slice(tmpstr.search('の人気レシピ') + 6);
                            //console.log(`tmpstr:${tmpstr}`);
                            posts = tmpstr.replace(/[^0-9]/g, '');
                            //console.log(`posts:${posts}`);
                            result = 'true';
                            break;

                        }else{
                            console.log(`       get_cookpad get posts > 0 err ${idx}:${kwd}`);
                            yield util.sleep(1000);
                            retry = true;
                            continue;
                        }

                    } else if ( h.match(/noResultText/i) ) {
                        //console.log(` no recipe  ${idx}:${kwd}`);
                        result = 'true';
                        break;

                    } else if ( h.match(/notfoundTit/i) ) {
                        //console.log(` no recipe page  ${idx}:${kwd}`);
                        result = 'true';
                        break;

                    } else {
                        //console.log(`       get_rakuten get posts err ${idx}:${kwd}`);
                        yield util.sleep(1000);
                        retry = true;
                        continue;
                    }
                }
            } while (true)

            resolve(result);
        }).catch((e) => {
           console.log('    catch get_rakuten');
           console.log(e);
           resolve('catch');
        });
    });
}

// scrape by process Cheerio
const scrape_recipe_posts = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            const maxcnt = 2;
            let rcnt = 0; //kIDXごとのカウンタ
            let kIDX = 0;
            posts = 0;
            //let keyword = '#' + keywords[kIDX].keyword;
            let next = false;
            let result = false;
            let rt = true;
            let ccnt = 0; //continuous count Restart browser

            while(kIDX < keywords.length) {
                posts = 0;
                next = false;
                result = false;

                keyword = util.encodeUrl(keywords[kIDX].keyword);

                do {
                    let gprt;
                    if(site == 1) {
                        gprt = yield get_cookpad(kIDX, keywords[kIDX].keyword);
                    } else {
                        gprt = yield get_rakuten(kIDX, keywords[kIDX].keyword);
                    }
                    ngflg = 0;
                    if(gprt == 'false') {
                        ngflg = 9;
                        next = true;
                        result = true;
                        ccnt = ccnt + 1;
                    }else if(gprt == 'true'){
                        result = true;
                        next = true;
                        ccnt = 0;
                    }else{
                        lcomp = 30;
                        result = false;
                        rt = false;
                        break;
                    }

                }while (!next)               
                
                //log
                if((kIDX)%1000 == 0){
                    console.log(`  ${kIDX} : ${keywords[kIDX].keyword} ng:${keywords[kIDX].ngflg} posts:${posts} maxposts:${keywords[kIDX].maxposts}  ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
                }

                if(result){
                    const sql = `INSERT OR REPLACE INTO ${result_table} ` +
                        `(keyword, getdate, posts, maxposts, ngflg, regtime) ` +
                        `VALUES (?, ?, ?, ?, ?, ?);`;

                    //let ngflg = 0;
                    if(keywords[kIDX].maxposts >= 10 && posts == 0) {
                        ngflg = 2;
                        console.log(`     ngflg:${ngflg} ${kIDX}: ${keywords[kIDX].keyword}  posts:${posts} avr:${keywords[kIDX].maxposts}`);
                    }

                    let regtime = moment().format('YYYY/MM/DD HH:mm:ss');
                    let opt = [keywords[kIDX].keyword, getdate, posts, keywords[kIDX].maxposts, ngflg, regtime];
                    yield db.do(`run`, sql, opt);

                }

                if(ccnt >= 3){
                    rt = false;
                    lcomp = 20;
                    if( driver ){
                        driver.quit();
                        driver = null;
                    }
                    break;
                }

                if( stopflg ){
                    rt = true;
                    lcomp = 100;
                    if( driver ){
                        driver.quit();
                        driver = null;
                    }
                    break;
                }

                kIDX = kIDX + 1;

                }
    
            resolve(rt);

        }).catch((e) => {
            console.log('    catch scrape_recipe_posts');
            console.log(e);
            resolve(true);
        });
    });
}


// scrape main
const scrape_main = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let result = false;
            let lflg = true; // loop continue?

            if (mode == 'new') {
                yield init_table();
                let st = yield store_targets();
                if(!st){
                    console.log(`   store targets err`);
                    lflg = false;
                }
            }else{
                yield get_loop_count();
            }

            yield get_targets();
            if ( keywords.length == 0 ) {
                lflg = false;
                result = true;
            }

            while(lflg){

                if (lcnt > maxloop) {
                    console.log(`   end  maxloop`);
                    lflg = false;
                    break;
                }

                lcomp = 1;
                getkwds = 0;
                yield insertLog();
                let rt = yield scrape_recipe_posts();

                let bflen = keywords.length;
                yield get_targets();

                if ( minnum >= keywords.length ) {
                    console.log(`   end  min keywords`);
                    lflg = false;
                    result = true;
                }

                getkwds = bflen - keywords.length;
                if (getkwds == 0){
                    zcnt++;
                    console.log(` get keywords: zero count: ${zcnt}`);
                    if(zcnt >= 3) {
                        console.log(`   end scraping  file output`);
                        lflg = false;
                        result = true;
                    }

                }else{
                    zcnt = 0;
                }

                yield updateLog();
                lcnt++;

                if(!rt) {
                    console.log(`   exit scraping`);
                    lflg = false;
                    result = false;
                }

                if(stopflg) {
                    console.log(`   !! stop scraping  file output`);
                    lflg = false;
                    result = true;
                }

            }

            if( driver ){
                driver.quit();
                driver = null;
            }

            if(result) yield setOutFile();

            resolve();
        }).catch((e) => {
            console.log('    catch scrape_main');
            console.log(e);
            resolve('');
        //    return null;
        });
    });
}

// set out file to share folder
const setOutFile = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            if(!stopflg){
                let sql = `select keyword, max(posts) posts
                from ${result_table} where getdate = '${getdate}' and ngflg = 0 
                group by 1
                union all
                select keyword, max(posts) posts
                from (
                select keyword, posts, count(*) cnt
                from ${result_table} t
                where getdate = '${getdate}' and ngflg != 0 and ngflg != 9
                and not exists (select * from ${result_table} x where x.ngflg = 0 and x.keyword = t.keyword)
                group by 1, 2
                having cnt > 1
                ) a
                group by 1
                order by 1;`;
                let res = yield db.do(`all`, sql, {});

                for (let i = 0; i < res.length; i++) {
                    fs.appendFileSync(outfile, ([res[i].keyword, res[i].posts, getdate].join('\t') + '\n'), 'utf8');
                }

                yield outNGfile();
                yield outLog();
            
            }else{
                fs.writeFileSync(outfile, "" );
                let stat = fs.statSync(outfile);
                console.log(` stopflg! output filesize:${stat.size}`);
            }

            yield end();

            resolve();
        //}).catch((e) => {
        //    console.log(e);
        });
    });
}

// run action
const run = function () {

    co(function* () {
        
        yield db.connect(filePath_db);

        switch (mode) {

            case 'new':
            case 'continue':
                yield scrape_main();
                break;
            
            case 'output':
                yield setOutFile();
                break;

            default:
                break;
        }
        console.log(`/// FGS283-recipe scraper.js end /// ${sttime} - ${moment().format('YYYY/MM/DD HH:mm:ss')}`);

    });
}

run();

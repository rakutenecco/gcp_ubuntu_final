import sys
import math
import os
import glob
import numpy as np
import pandas as pd
import img2pdf
import PyPDF2
import matplotlib as mpl
import matplotlib.dates as mdates
import statsmodels.api as sm
import psutil
import gc
from datetime import datetime, timedelta
from time import sleep
from matplotlib import pyplot as plt
from matplotlib.font_manager import FontProperties
from matplotlib.backends.backend_pdf import PdfPages
from pathlib import Path
from PIL import Image

font_path = '/usr/share/fonts/truetype/fonts-japanese-gothic.ttf'
font_prop = FontProperties(fname=font_path)
mpl.rcParams['font.family'] = font_prop.get_name()

# today
today = datetime.strptime(datetime.strftime(datetime.now(), "%Y%m%d"), "%Y%m%d")

# command line argument
args = sys.argv
arg_date = datetime.strptime(args[1], "%Y%m%d")
bef_day = str((arg_date - today).days)

# 日付配列作成
exec_date_hyphen = (datetime.now() + timedelta(days=int(bef_day))).date()
exec_date = datetime.strftime(exec_date_hyphen, "%Y%m%d")
#exec_year = exec_date[0:4]
#exec_month = exec_date[0:6]

#画像保存先ディレクトリ作成
output_dir = "src/rakuten/FGS283-recipe/cookpad_pdf_out_{0}/".format(exec_date)
os.makedirs(output_dir, exist_ok=True)

input_dir = "src/rakuten/FGS283-recipe/download_data/"
os.makedirs(input_dir, exist_ok=True)

# 実行年の日付リスト作成
#day_list = []
#first_date = datetime(2019, 1, 1, 0, 0)

#date_hyphen = first_date
#while date_hyphen <= datetime.combine(exec_date_hyphen, datetime.min.time()):
#    day_list.append(datetime.strftime(date_hyphen, "%Y%m%d")[0:8])
#    date_hyphen += timedelta(days=1)

#data_list = [["20190130","〆に",2],["20200220","〆に",2],["20190129","〆は雑炊",3],
#          ["20200209","〆パフェ",50],["20200205","〆パフェ",80],["20200213","〆パフェ",100]]

input_filename = input_dir + "fgs283-cookpad_download.txt"

data_list = []

f = open(input_filename, "r")
text = f.readlines()[2:]
for elem in text:
    # 実行日までのデータをリストにアペンドする処理を追加したい
    if(elem[0:8] <= exec_date):
        data_list.append(elem.split(","))

day_list = []

strdt = datetime.strptime("20200105", '%Y%m%d')  # 開始日
#strdt = datetime.strptime("20190106", '%Y%m%d')  # 開始日
enddt = datetime.strptime(exec_date, '%Y%m%d')  # 終了日

# 日付差の日数を算出（リストに最終日も含めたいので、＋１しています）
days_num = int((enddt - strdt).days  / 7) + 1

datelist = []
d = 0
for i in range(days_num):
    datelist.append(strdt + timedelta(days=d))
    d = d + 7

def make_date_list(day_list):

    for d in datelist:
        day_list.append([d.strftime("%Y%m%d"),"",0])
        
    return day_list

def make_list(source_list):

    day_list=[]
    count_list = []
    
    make_date_list(day_list)
    
    for j in range(len(source_list)):
        for i in range(len(day_list)):
            if(day_list[i][0] == source_list[j][0]):
                day_list[i][1] = source_list[j][1]
                day_list[i][2] = source_list[j][2]
    #print(day_list)

    for k in range(len(day_list)):
        count_list.append(day_list[k][2])
    #print(count_list)

    date_index = pd.date_range('2020-01-01', freq='W-SUN', periods=(len(day_list)))
    #date_index = pd.date_range('2019-01-01', freq='W-SUN', periods=(len(day_list)))
    fig, ax = plt.subplots()
    fig = plt.figure(figsize=(5.8, 4.2))
    ax = fig.add_subplot(111)

    #print(count_list)

    # リストを直近一ヶ月のみに加工する
    count_list = count_list[-13:]
    date_index = date_index[-13:]

    #print(count_list)
    #print(date_index)

    date_index2 = []
    for date in range(len(date_index)):
        date_index2.append(str(date_index[date])[0:10])

    #print(date_index2)

    #print(type(date_index))
    #for k in range(len(day_list)):
    #    count_list.append(day_list[k][2])

    #count = pd.Series(count_list[-7:])
    #avg_list = count.rolling(window=1).mean()
    #avg_list = ["0" if x is None else x for x in avg_list]
    avg_list = [int(s) for s in count_list]
    #avg_list = count_list

    #print(avg_list)

    max_val = max(avg_list)
    #rel_val = ''
    avg_list2 = []
    flg = 0
    for i in avg_list:
        if flg == 0:
            ini_val = i
            avg_list2.append(1.0)
            flg = 1

        else:
            if ini_val == 0:
                avg_list2.append(0)
            else:
                i = round(i * 100 / max_val, 3)
                #i = round(i / ini_val, 3)
                avg_list2.append(i)
        
    avg_list = avg_list2

    #print(avg_list)

    #res = sm.tsa.seasonal_decompose(count, freq=133)
    #res = sm.tsa.seasonal_decompose(count, period=2)
    #trend = res.trend
    #trend = ["0" if x is None else x for x in trend]

    #print(avg_list[-4:])
    #print(date_index[-4:])

    # 折れ線グラフ
    df = pd.DataFrame(avg_list[-12:], date_index[-12:])
    #dt = pd.DataFrame(trend[-4:], date_index[-4:])

    #avg_list = [x for x in avg_list if str(x) != "nan"]

    if(avg_list[-5] != 0):
        #YoY = str(round((avg_list[-1]/avg_list[-365] * 100) - 100, 2)) + "％"
        #YoY = round((avg_list[-1]/avg_list[-52] * 100) - 100, 2)
        YoY = round((avg_list[-1]/avg_list[-5] * 100) - 100, 2)
    else:
        YoY = 0
    
    x = np.array(list(range(len(avg_list))))
    y = np.array(avg_list)

    #回帰線を求める
    x_reg = x - x.mean()
    y_reg = y - y.mean()

    #傾きを求める
    denom = x_reg * x_reg
    numer = x_reg * y_reg

    answer = round(numer.sum()/denom.sum(),3)

    #直近28日
    avg_list_28 = avg_list[-2:]
    #avg_list_28 = avg_list
    x_28 = np.array(list(range(len(avg_list_28))))
    y_28 = np.array(avg_list_28)

    #回帰線を求める
    x_reg_28 = x_28 - x_28.mean()
    y_reg_28 = y_28 - y_28.mean()

    #傾きを求める
    denom_28 = x_reg_28 * x_reg_28
    numer_28 = x_reg_28 * y_reg_28

    answer_28 = round(numer_28.sum()/denom_28.sum(),3)

    #直近7日
    avg_list_7 = avg_list[-1:]
    x_7 = np.array(list(range(len(avg_list_7))))
    y_7 = np.array(avg_list_7)

    #回帰線を求める
    x_reg_7 = x_7 - x_7.mean()
    y_reg_7 = y_7 - y_7.mean()

    #傾きを求める
    denom_7 = x_reg_7 * x_reg_7
    numer_7 = x_reg_7 * y_reg_7

    answer_7 = round(numer_7.sum()/denom_7.sum(),3)

    print(keyword + ":" + str(answer) + ":" + str(answer_28))

    #if (answer > 0 or answer < 0):
    #if ((answer > 0.001 and answer_28 > 0) or (answer > 0.001 and answer_7 > 0)):
    #if (answer >= 5.0 and answer_28 >= 10.0 and answer_7 >= 15.0):
    #if (answer >= 0.01 and answer_28 >= 0.01 and answer_7 >= 0.1): 
    if (answer >= 3.5 and answer_28 >= 1.0): 
    #if(1==1):

        if(YoY==0):
            YoY = "－"
        else:
            YoY = str(YoY) + "％"

        #df.plot(ax = ax, x_compat=True, legend=False)
        df.plot(ax = ax, x_compat=True, color="turquoise", alpha=0.7, legend=False)
        #dt.plot(ax = ax, x_compat=True, color="tomato", alpha=0.7, legend=False)
        ax.xaxis.set_minor_locator(mdates.MonthLocator())
        #print(ax.get_xlim())
        #plt.ylim(0, 2)
        #ax.set_xticklabels(date_index2)
        #ax.set_yticks([0, 0.5, 1, 1.5, 2])
        plt.setp(ax.get_xticklabels(), rotation=60, ha="center")
        plt.title(keyword, fontsize = 13)
        plt.ylabel("件数", fontsize = 10)
        #plt.text(0, 1.03, "YoY : " + str(YoY) + "％", size = 12, color = "black", transform=ax.transAxes)
        plt.text(0, 1.03, "MoM : " + YoY, size = 12, color = "black", transform=ax.transAxes)
        plt.ylim(0)
        #plt.show()

        filename = output_dir + keyword + "_" + exec_date + ".png"
        plt.savefig(filename)
        plt.close('all')

        del filename
        gc.collect()
    
    del ax, df, fig, answer, answer_28, answer_7, avg_list2, avg_list, YoY, date_index, count_list, denom_7, denom_28, numer_7, numer_28, x_reg_7, y_reg_7, x_reg_28, y_reg_28
    gc.collect()

    #print("プロット処理終了")
    #mem = psutil.virtual_memory() 
    #print(mem.percent)
    #sleep(3)

keyword=""
source_list=[]
count=0

for i in range(len(data_list)):
    count+=1

    if(count > 1000):
        count=0
        print("スリープ処理です")
        mem = psutil.virtual_memory() 
        print(mem.percent)
        sleep(10)
        gc.collect()

    if(keyword==""):
        keyword=data_list[i][1]
        
    if(keyword==data_list[i][1]):
        source_list.append([data_list[i][0],data_list[i][1],data_list[i][2].replace("\n", "")])
        keyword=data_list[i][1]
    else:
        make_list(source_list)
        del source_list
        gc.collect()
        source_list=[]
        source_list.append([data_list[i][0],data_list[i][1],data_list[i][2].replace("\n", "")])
        keyword=data_list[i][1]
make_list(source_list)
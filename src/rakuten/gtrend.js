
'use strict';

const shell = require('shelljs');
const cronJob = require('cron').CronJob;
const moment = require('moment');
const fs = require('fs');
const date = moment().format('YYYYMMDD');
const path_log = `../log/${date}.txt`;
let now = '';

/*
cronJob(`00 00 00 * * 1-5`)
Seconds: 0-59
Minutes: 0-59
Hours: 0-23
Day of Month: 1-31
Months: 0-11
Day of Week: 0-6
*/

//monthly skirt
new cronJob(`00 00 13 * * 2`, function () {
        shell.exec('node src/rakuten/FGS41-google-trends/scraper.js -r 100');
        shell.exec('node src/rakuten/FGS41-google-trends/scraper.js -c skirt -r 1 -B chromium -t month -o tor'); // scrape
        shell.exec('node src/rakuten/FGS41-google-trends/scraper.js -c skirt -r 5 -B chromium -t month -o tor'); // outfile
        shell.exec('node src/rakuten/FGS41-google-trends/shell.js -r 100');

    }, function () {
        // error
        now = moment().format('YYYYMMDD HH:mm:ss');
        fs.appendFileSync(path_log, `Error ${now} : stop google-trend weekly cron \n`, 'utf8');
    },
    true, 'Asia/Tokyo');

//monthly watch
new cronJob(`00 00 16 * * 2`, function () {
	    shell.exec('node src/rakuten/FGS41-google-trends/scraper.js -r 100');
        for (let i = 0; i < 30; i++) {
            shell.exec('node src/rakuten/FGS41-google-trends/scraper.js -c watch -r 1 -B chromium -t month -o tor'); // scrape
        }
        shell.exec('node src/rakuten/FGS41-google-trends/scraper.js -c watch -r 5 -B chromium -t month -o tor'); // outfile
        shell.exec('node src/rakuten/FGS41-google-trends/shell.js -r 100');
    }, function () {
        // error
        now = moment().format('YYYYMMDD HH:mm:ss');
        fs.appendFileSync(path_log, `Error ${now} : stop google-trend monthly cron \n`, 'utf8');
    },
    true, 'Asia/Tokyo');


console.log('clone setting');
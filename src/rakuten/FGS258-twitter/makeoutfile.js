'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');
const moment = require('moment');
const shell = require('shelljs');


// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: String },
    { name: 'filename', alias: 'f', type: String },
]);

let runtype = cli['run'] ? cli['run'] : 0; 


let infname;
let outfname;
let tmpfnm = cli['filename'];
//FGS258_twitter_posts_allposts_week_all_20200126_05_01_01.txt

if(tmpfnm){
    infname = `${tmpfnm.substr(0,tmpfnm.length-6)}[0-9][0-9].txt`;
    outfname = `${tmpfnm.substr(0,tmpfnm.length-7)}.txt`;
    
}


// output file
const output = function (n,f) {

    return new Promise((resolve, reject) => {
        co(function* () {

            let lines = fs.readFileSync(f).toString().split("\n");
            if(lines[lines.length -1].length == 0) lines.pop();
            console.log(`    infile ${n}: ${lines.length}`);

            for(let i = 0; i < lines.length; i++ ){ 
                fs.appendFileSync(outfname, (lines[i] + '\n'), 'utf8');
            }

            resolve();
        });
    });
}
    
// Concatenate files
const make_outfile = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //outfname check
            if (fs.existsSync(outfname)) {
                console.log('  outfile exists :',outfname);
            } else {
                let in_ls = shell.ls(infname);
                console.log(`   infname ls.length: ${in_ls.length}`);

                if(in_ls.length){                    
                    for(let j = 0; j < in_ls.length; j++ ){
                        yield output(j+1, in_ls[j]);
                    }                    

                }else{
                    console.log(`  Required file does not exist`);
                }
            }
            resolve();
        });
    });
}
 
// run action
// Eg: node src/rakuten/FGS258-twitter/makeoutfile.js -f /mnt/share/FGS258_twitter_allposts_week_all_20200126_05_02_02.txt
const run = function () {

    co(function* () {

        let st = moment().format('YYYY/MM/DD HH:mm:ss');
        console.log(` /// FGS256 makeoutfile.js start /// runtype:${runtype} ${st}`); 

        yield make_outfile()
        
        console.log(` /// FGS239 make_outfile.js end   ///  ${st} - ${moment().format('YYYY/MM/DD HH:mm:ss')}`);

        
    });
}

run();
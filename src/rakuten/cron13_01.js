'use strict';

const shell = require('shelljs');
const cronJob = require('cron').CronJob;
const moment = require('moment');
const fs = require('fs');
const conf = new (require('/var/app/total/src/conf.js'));

/*
cronJob(`00 00 00 * * 1-5`)
Seconds: 0-59
Minutes: 0-59
Hours: 0-23
Day of Month: 1-31
Months: 0-11
Day of Week: 0-6
*/

//// 01 ////
// FGS239 WEAR, FGS286 yahoo all weekly prod
new cronJob(`00 07 00 * * 0`, function () {
//new cronJob(`00 14 14 * * 0-2`, function () {

    let st = moment().format('YYYY/MM/DD HH:mm:ss');
    console.log(` yahoo cron13_01 start ${st}`);

    shell.exec(`node src/rakuten/FGS239-wear-posts/shell.js -r 1 -n cron10 -f targets_fashion.txt`);

    let startdate = moment().day(0).add(-7, "days").format('YYYYMMDD');
    shell.exec(`node src/rakuten/FGS286-yahoo/shell.js -r 1 -n cron13_01 -f targets_all_06_01.txt -s ${startdate}`);
    shell.exec(`node src/rakuten/FGS286-yahoo/shell.js -r 1 -n cron13_02 -f targets_all_06_02.txt -s ${startdate}`);
    shell.exec(`node src/rakuten/FGS286-yahoo/shell.js -r 1 -n cron13_03 -f targets_all_06_03.txt -s ${startdate}`);

    console.log(` WEAR yahoo cron13_01 end   ${st} - ${moment().format('YYYY/MM/DD HH:mm:ss')}`);

}, function () {
    // error
    console.log(`Error ${today} : FGS239 WEAR, FGS286 yahoo scrape cron13_01`);
},
true, 'Asia/Tokyo');

console.log('cron13_01 setting');

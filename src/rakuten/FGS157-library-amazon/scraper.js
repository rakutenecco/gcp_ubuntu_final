'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');
const he = require('he');
const moment = require('moment');

const conf = new(require('../../conf.js'));
const db = new(require('../../modules/db.js'));
const util = new(require('../../modules/util.js'));
const cheerio = new(require('../../modules/cheerio-httpcli.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
]);

let task_name = 'fgs157';
let cron = 'cron2';
let filePath_db = `${conf.dirPath_db}/cron2.db`;
let today = moment().format('YYYYMMDD');
let today_haifun = moment().add(0, 'days').format('YYYY-MM-DD');
const alphabet = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];

// init
const init = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // cheerio timeout setting
            yield cheerio.init(10000);

            // create table "proxy_list"
            yield db.connect(filePath_db);
            const sql = `CREATE TABLE IF NOT EXISTS fashion_${task_name}_library_amazon_list(FAT_id TEXT primary key, brand TEXT, brand_kana TEXT, product_num INTEGER, FATU_id TEXT, reg_date TEXT);`;
            yield db.do(`run`, sql, {});

            resolve();
        });
    });
}

// end
const end = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield db.close();

            resolve();
        });
    });
}

// drop table for concern tables
const drop = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // drop table proxy_list
            yield db.connect(filePath_db);

            let sql = `DROP TABLE IF EXISTS fashion_${task_name}_library_amazon_list;`;
            yield db.do(`run`, sql, {});

            sql = `DROP TABLE IF EXISTS fashion_${task_name}_library_amazon_result;`;
            yield db.do(`run`, sql, {});
            yield db.close();

            resolve();
        });
    });
}

// set url list
const setList = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // create table
            yield db.connect(filePath_db);
            const sql1 = `CREATE TABLE IF NOT EXISTS fashion_${task_name}_library_amazon_list(FATU_id TEXT primary key, url TEXT, site TEXT, page TEXT, genre TEXT, category TEXT, category2 TEXT);`;
            yield db.do(`run`, sql1, {});

            // create table
            yield db.connect(filePath_db);
            const sql2 = `CREATE TABLE IF NOT EXISTS fashion_${task_name}_library_amazon_result(FAT_id TEXT primary key, brand TEXT, brand_kana TEXT, product_num TEXT, FATU_id TEXT, reg_date TEXT);`;
            yield db.do(`run`, sql2, {});

            /////////////////
            // shoes
            /////////////////

            // amazon.co.jp shoes none
            let sql_1 = `INSERT OR REPLACE INTO fashion_${task_name}_library_amazon_list(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.co.jp-Top-shoes_none', 'https://www.amazon.co.jp/gp/search/other/ref=sr_sa_p_89?rh=n%3A2016926051%2Ck%3A%E9%9D%B4&keywords=%E9%9D%B4&pickerToList=lbr_brands_browse-bin&ie=UTF8&qid=1508214837', 'amazon.co.jp', 'Top Brands', 'shoes', 'none', '');`;
            yield db.do(`run`, sql_1, {});

            // amazon.co.uk shoes none
            sql_1 = `INSERT OR REPLACE INTO fashion_${task_name}_library_amazon_list(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.co.uk-Top-shoes_none', 'https://www.amazon.co.uk/gp/search/other/ref=sr_sa_p_89?rh=i%3Aaps%2Ck%3Ashoes%2Cp_89%3AClassyDude&keywords=shoes&pickerToList=lbr_brands_browse-bin&ie=UTF8&qid=1508139588', 'amazon.co.uk', 'Top Brands', 'shoes', 'none', '');`;
            yield db.do(`run`, sql_1, {});
            let sql_2 = `INSERT OR REPLACE INTO fashion_${task_name}_library_amazon_list(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.co.uk-#-shoes_none', 'https://www.amazon.co.uk/gp/search/other/ref=sr_in_1_-2?rh=i%3Aaps%2Ck%3Ashoes%2Cp_89%3AClassyDude&keywords=shoes&pickerToList=lbr_brands_browse-bin&indexField=%23&ie=UTF8&qid=1508139592', 'amazon.co.uk', '#', 'shoes', 'none', '');`;
            yield db.do(`run`, sql_2, {});
            let sql_3 = `INSERT OR REPLACE INTO fashion_${task_name}_library_amazon_list(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.co.uk-{0}-shoes_none', 'https://www.amazon.co.uk/gp/search/other/ref=sr_in_{0}_A?rh=i%3Aaps%2Ck%3Ashoes%2Cp_89%3AClassyDude&keywords=shoes&pickerToList=lbr_brands_browse-bin&indexField={0}&ie=UTF8&qid=1508139930', 'amazon.co.uk', '{0}', 'shoes', 'none', '');`;
            for (let i = 0; i < alphabet.length; i++) {
                let sql_this = sql_3.replace(/\{0\}/g, alphabet[i]);
                yield db.do(`run`, sql_this, {});
            }

            // amazon.com shoes none
            sql_1 = `INSERT OR REPLACE INTO fashion_${task_name}_library_amazon_list(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.com-Top-shoes_none', 'https://www.amazon.com/gp/search/other/ref=sr_sa_p_89?rh=n%3A7141123011%2Ck%3Ashoes&bbn=7141123011&keywords=shoes&pickerToList=lbr_brands_browse-bin&ie=UTF8&qid=1508139558', 'amazon.com', 'Top Brands', 'shoes', 'none', '');`;
            yield db.do(`run`, sql_1, {});
            sql_2 = `INSERT OR REPLACE INTO fashion_${task_name}_library_amazon_list(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.com-#-shoes_none', 'https://www.amazon.com/gp/search/other/ref=sr_in_1_-2?rh=i%3Afashion%2Cn%3A7141123011%2Ck%3Ashoes&bbn=7141123011&keywords=shoes&pickerToList=lbr_brands_browse-bin&indexField=%23&ie=UTF8&qid=1508139607', 'amazon.com', '#', 'shoes', 'none', '');`;
            yield db.do(`run`, sql_2, {});
            sql_3 = `INSERT OR REPLACE INTO fashion_${task_name}_library_amazon_list(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.com-{0}-shoes_none', 'https://www.amazon.com/gp/search/other/ref=sr_in_{0}_A?rh=i%3Afashion%2Cn%3A7141123011%2Ck%3Ashoes&bbn=7141123011&keywords=shoes&pickerToList=lbr_brands_browse-bin&indexField={0}&ie=UTF8&qid=1508139918', 'amazon.com', '{0}', 'shoes', 'none', '');`;
            for (let i = 0; i < alphabet.length; i++) {
                let sql_this = sql_3.replace(/\{0\}/g, alphabet[i]);
                yield db.do(`run`, sql_this, {});
            }

            /////////////////
            // bags
            /////////////////

            // amazon.co.jp bags none
            sql_1 = `INSERT OR REPLACE INTO fashion_${task_name}_library_amazon_list(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.co.jp-Top-bags_none', 'https://www.amazon.co.jp/gp/search/other/ref=sr_sa_p_89?rh=i%3Aaps%2Ck%3A%E3%83%90%E3%83%83%E3%82%B0%2Cp_89%3A%E3%82%B5%E3%83%B3%E3%83%AA%E3%82%AA&keywords=%E3%83%90%E3%83%83%E3%82%B0&pickerToList=lbr_brands_browse-bin&ie=UTF8&qid=1508139615', 'amazon.co.jp', 'Top Brands', 'bags', 'none', '');`;
            yield db.do(`run`, sql_1, {});

            // amazon.co.uk bags none
            sql_1 = `INSERT OR REPLACE INTO fashion_${task_name}_library_amazon_list(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.co.uk-Top-bags_none', 'https://www.amazon.co.uk/gp/search/other/ref=sr_sa_p_89?rh=i%3Aaps%2Ck%3Abags%2Cp_89%3AEastpak&keywords=bags&pickerToList=lbr_brands_browse-bin&ie=UTF8&qid=1508139580', 'amazon.co.uk', 'Top Brands', 'bags', 'none', '');`;
            yield db.do(`run`, sql_1, {});
            sql_2 = `INSERT OR REPLACE INTO fashion_${task_name}_library_amazon_list(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.co.uk-#-bags_none', 'https://www.amazon.co.uk/gp/search/other/ref=sr_in_1_-2?rh=i%3Aaps%2Ck%3Abags%2Cp_89%3AEastpak&keywords=bags&pickerToList=lbr_brands_browse-bin&indexField=%23&ie=UTF8&qid=1508139584', 'amazon.co.uk', '#', 'bags', 'none', '');`;
            yield db.do(`run`, sql_2, {});
            sql_3 = `INSERT OR REPLACE INTO fashion_${task_name}_library_amazon_list(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.co.uk-{0}-bags_none', 'https://www.amazon.co.uk/gp/search/other/ref=sr_in_{0}_A?rh=i%3Aaps%2Ck%3Abags%2Cp_89%3AEastpak&keywords=bags&pickerToList=lbr_brands_browse-bin&indexField={0}&ie=UTF8&qid=1508139940', 'amazon.co.uk', '{0}', 'bags', 'none', '');`;
            for (let i = 0; i < alphabet.length; i++) {
                let sql_this = sql_3.replace(/\{0\}/g, alphabet[i]);
                yield db.do(`run`, sql_this, {});
            }

            // amazon.com bags none
            sql_1 = `INSERT OR REPLACE INTO fashion_${task_name}_library_amazon_list(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.com-Top-bags_none', 'https://www.amazon.com/gp/search/other/ref=sr_sa_p_89?rh=i%3Aaps%2Ck%3Abags%2Cp_89%3AKate+Spade+New+York&keywords=bags&pickerToList=lbr_brands_browse-bin&ie=UTF8&qid=1508139598', 'amazon.com', 'Top Brands', 'bags', 'none', '');`;
            yield db.do(`run`, sql_1, {});
            sql_2 = `INSERT OR REPLACE INTO fashion_${task_name}_library_amazon_list(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.com-#-bags_none', 'https://www.amazon.com/gp/search/other/ref=sr_in_1_-2?rh=i%3Aaps%2Ck%3Abags%2Cp_89%3AKate+Spade+New+York&keywords=bags&pickerToList=lbr_brands_browse-bin&indexField=%23&ie=UTF8&qid=1508139601', 'amazon.com', '#', 'bags', 'none', '');`;
            yield db.do(`run`, sql_2, {});
            sql_3 = `INSERT OR REPLACE INTO fashion_${task_name}_library_amazon_list(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.com-{0}-bags_none', 'https://www.amazon.com/gp/search/other/ref=sr_in_{0}_A?rh=i%3Aaps%2Ck%3Abags%2Cp_89%3AKate+Spade+New+York&keywords=bags&pickerToList=lbr_brands_browse-bin&indexField={0}&ie=UTF8&qid=1508139931', 'amazon.com', '{0}', 'bags', 'none', '');`;
            for (let i = 0; i < alphabet.length; i++) {
                let sql_this = sql_3.replace(/\{0\}/g, alphabet[i]);
                yield db.do(`run`, sql_this, {});
            }

            /////////////////
            // watch
            /////////////////

            // amazon.co.jp watch ladies
            sql_1 = `INSERT OR REPLACE INTO fashion_${task_name}_library_amazon_list(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.co.jp-Top-watch_ladies', 'https://www.amazon.co.jp/gp/search/other/ref=lp_333010011_sa_p_89?rh=n%3A2229202051%2Cn%3A%212229203051%2Cn%3A2230006051%2Cn%3A333010011&bbn=333010011&pickerToList=lbr_brands_browse-bin&ie=UTF8&qid=1513751952', 'amazon.co.jp', 'Top Brands', 'watch', 'ladies', '');`;
            yield db.do(`run`, sql_1, {});

            // amazon.co.jp watch mens
            sql_1 = `INSERT OR REPLACE INTO fashion_${task_name}_library_amazon_list(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.co.jp-Top-watch_mens', 'https://www.amazon.co.jp/gp/search/other/ref=lp_333009011_sa_p_89?rh=n%3A2229202051%2Cn%3A%212229203051%2Cn%3A2230005051%2Cn%3A333009011&bbn=333009011&pickerToList=lbr_brands_browse-bin&ie=UTF8&qid=1513752004', 'amazon.co.jp', 'Top Brands', 'watch', 'mens', '');`;
            yield db.do(`run`, sql_1, {});

            // amazon.co.uk watch ladies
            sql_1 = `INSERT OR REPLACE INTO fashion_${task_name}_library_amazon_list(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.co.uk-Top-watch_ladies', 'https://www.amazon.co.uk/gp/search/other/ref=lp_10103527031_sa_p_89?rh=n%3A328228011%2Cn%3A%21328229011%2Cn%3A10103527031&bbn=10103527031&pickerToList=lbr_brands_browse-bin&ie=UTF8&qid=1513751647', 'amazon.co.uk', 'Top Brands', 'watch', 'ladies', '');`;
            yield db.do(`run`, sql_1, {});
            sql_2 = `INSERT OR REPLACE INTO fashion_${task_name}_library_amazon_list(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.co.uk-#-watch_ladies', 'https://www.amazon.co.uk/gp/search/other/ref=sr_in_1_-2?rh=i%3Awatches%2Cn%3A328228011%2Cn%3A%21328229011%2Cn%3A10103527031&bbn=10103527031&pickerToList=lbr_brands_browse-bin&indexField=%23&ie=UTF8&qid=1533270983', 'amazon.co.uk', '#', 'watch', 'ladies', '');`;
            yield db.do(`run`, sql_2, {});
            sql_3 = `INSERT OR REPLACE INTO fashion_${task_name}_library_amazon_list(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.co.uk-{0}-watch_ladies', 'https://www.amazon.co.uk/gp/search/other/ref=sr_in_{0}_1?rh=i%3Awatches%2Cn%3A328228011%2Cn%3A%21328229011%2Cn%3A10103527031&bbn=10103527031&pickerToList=lbr_brands_browse-bin&indexField={0}&ie=UTF8&qid=1533271032', 'amazon.co.uk', '{0}', 'watch', 'ladies', '');`;
            for (let i = 0; i < alphabet.length; i++) {
                let sql_this = sql_3.replace(/\{0\}/g, alphabet[i]);
                yield db.do(`run`, sql_this, {});
            }

            // amazon.co.uk watch mens
            sql_1 = `INSERT OR REPLACE INTO fashion_${task_name}_library_amazon_list(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.co.uk-Top-watch_mens', 'https://www.amazon.co.uk/gp/search/other/ref=lp_10103528031_sa_p_89?rh=n%3A328228011%2Cn%3A%21328229011%2Cn%3A10103528031&bbn=10103528031&pickerToList=lbr_brands_browse-bin&ie=UTF8&qid=1513751715', 'amazon.co.uk', 'Top Brands', 'watch', 'mens', '');`;
            yield db.do(`run`, sql_1, {});
            sql_2 = `INSERT OR REPLACE INTO fashion_${task_name}_library_amazon_list(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.co.uk-#-watch_mens', 'https://www.amazon.co.uk/gp/search/other/ref=sr_in_1_-2?rh=i%3Awatches%2Cn%3A328228011%2Cn%3A%21328229011%2Cn%3A10103528031&bbn=10103528031&pickerToList=lbr_brands_browse-bin&indexField=%23&ie=UTF8&qid=1533270784', 'amazon.co.uk', '#', 'watch', 'mens', '');`;
            yield db.do(`run`, sql_2, {});
            sql_3 = `INSERT OR REPLACE INTO fashion_${task_name}_library_amazon_list(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.co.uk-{0}-watch_mens', 'https://www.amazon.co.uk/gp/search/other/ref=sr_in_{0}_1?rh=i%3Awatches%2Cn%3A328228011%2Cn%3A%21328229011%2Cn%3A10103528031&bbn=10103528031&pickerToList=lbr_brands_browse-bin&indexField={0}&ie=UTF8&qid=1533270909', 'amazon.co.uk', '{0}', 'watch', 'mens', '');`;
            for (let i = 0; i < alphabet.length; i++) {
                let sql_this = sql_3.replace(/\{0\}/g, alphabet[i]);
                yield db.do(`run`, sql_this, {});
            }

            // amazon.com watch ladies
            sql_1 = `INSERT OR REPLACE INTO fashion_${task_name}_library_amazon_list(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.com-Top-watch_ladies', 'https://www.amazon.com/gp/search/other/ref=lp_6358544011_sa_p_89?rh=n%3A7141123011%2Cn%3A7147440011%2Cn%3A6358543011%2Cn%3A6358544011&bbn=6358544011&pickerToList=lbr_brands_browse-bin&ie=UTF8&qid=1513150068', 'amazon.com', 'Top Brands', 'watch', 'ladies', '');`;
            yield db.do(`run`, sql_1, {});
            sql_2 = `INSERT OR REPLACE INTO fashion_${task_name}_library_amazon_list(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.com-#-watch_ladies', 'https://www.amazon.com/gp/search/other/ref=sr_in_1_-2?rh=i%3Afashion-womens-watches%2Cn%3A7141123011%2Cn%3A7147440011%2Cn%3A6358543011%2Cn%3A6358544011&bbn=6358544011&pickerToList=lbr_brands_browse-bin&indexField=%23&ie=UTF8&qid=1533271157', 'amazon.com', '#', 'watch', 'ladies', '');`;
            yield db.do(`run`, sql_2, {});
            sql_3 = `INSERT OR REPLACE INTO fashion_${task_name}_library_amazon_list(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.com-{0}-watch_ladies', 'https://www.amazon.com/gp/search/other/ref=sr_in_{0}_1?rh=i%3Afashion-womens-watches%2Cn%3A7141123011%2Cn%3A7147440011%2Cn%3A6358543011%2Cn%3A6358544011&bbn=6358544011&pickerToList=lbr_brands_browse-bin&indexField={0}&ie=UTF8&qid=1533271174', 'amazon.com', '{0}', 'watch', 'ladies', '');`;
            for (let i = 0; i < alphabet.length; i++) {
                let sql_this = sql_3.replace(/\{0\}/g, alphabet[i]);
                yield db.do(`run`, sql_this, {});
            }

            // amazon.com watch mens
            sql_1 = `INSERT OR REPLACE INTO fashion_${task_name}_library_amazon_list(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.com-Top-watch_mens', 'https://www.amazon.com/gp/search/other/ref=lp_6358540011_sa_p_89?rh=n%3A7141123011%2Cn%3A7147441011%2Cn%3A6358539011%2Cn%3A6358540011&bbn=6358540011&pickerToList=lbr_brands_browse-bin&ie=UTF8&qid=1513150477', 'amazon.com', 'Top Brands', 'watch', 'mens', '');`;
            yield db.do(`run`, sql_1, {});
            sql_2 = `INSERT OR REPLACE INTO fashion_${task_name}_library_amazon_list(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.com-#-watch_mens', 'https://www.amazon.com/gp/search/other/ref=sr_in_1_-2?rh=i%3Afashion-mens-watches%2Cn%3A7141123011%2Cn%3A7147441011%2Cn%3A6358539011%2Cn%3A6358540011&bbn=6358540011&pickerToList=lbr_brands_browse-bin&indexField=%23&ie=UTF8&qid=1533271572', 'amazon.com', '#', 'watch', 'mens', '');`;
            yield db.do(`run`, sql_2, {});
            sql_3 = `INSERT OR REPLACE INTO fashion_${task_name}_library_amazon_list(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.com-{0}-watch_mens', 'https://www.amazon.com/gp/search/other/ref=sr_in_{0}_1?rh=i%3Afashion-mens-watches%2Cn%3A7141123011%2Cn%3A7147441011%2Cn%3A6358539011%2Cn%3A6358540011&bbn=6358540011&pickerToList=lbr_brands_browse-bin&indexField={0}&ie=UTF8&qid=1533271603', 'amazon.com', '{0}', 'watch', 'mens', '');`;
            for (let i = 0; i < alphabet.length; i++) {
                let sql_this = sql_3.replace(/\{0\}/g, alphabet[i]);
                yield db.do(`run`, sql_this, {});
            }

            yield db.close();

            resolve();
        });
    });
}

// get url list
const getList = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // create table "proxy_list"
            let sql = `SELECT * FROM fashion_${task_name}_library_amazon_list`;
            let res = yield db.do(`all`, sql, {});

            resolve(res);
        });
    });
}

// set amazon data
const setData = function (url, FATU_id) {

    return new Promise((resolve, reject) => {
        co(function* () {

            let datas = [];
            let flag = false;
            let $ = yield cheerio.get(url);
            if ($ && $('body').html().match(/a-list-item/i)) {
                $('.a-list-item').each(function (i, e) {

                    let reg_date = today;
                    let brand_pre = $(e).find('.a-link-normal').attr('title').replace(/[\n\r\t"]/g, '');
                    brand_pre = brand_pre.replace('（', '(');
                    let brand_splits = brand_pre.replace(/[\)）]/g, '').split('(');
                    let brand = he.decode(brand_splits[0].trim());
                    let brand_kana = brand_splits[1] ? he.decode(brand_splits[1].trim()) : '';
                    let product_num = $(e).find('.narrowValue').text().replace(/[\n\r\t\(\) ,]/g, '');
                    let FAT_id = `${FATU_id}-${brand}-${reg_date}`;
                    datas.push({FAT_id, brand, brand_kana, product_num, FATU_id, reg_date});
                });
            }

            for (let i = 0; i < datas.length; i++) {
                let sql = `INSERT OR REPLACE INTO fashion_${task_name}_library_amazon_result(FAT_id, brand, brand_kana, product_num, FATU_id, reg_date) VALUES (?, ?, ?, ?, ?, ?);`;
                let opt = [datas[i].FAT_id, datas[i].brand, datas[i].brand_kana, datas[i].product_num, datas[i].FATU_id, datas[i].reg_date];
                yield db.do(`run`, sql, opt);

                flag = true;
            }
            util.sleep(1000);

            resolve(flag);
        });
    });
}

// scrape
const scrape = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            const urls = yield getList();

            for (let i = 0; i < urls.length; i++) {

                console.log(`search: ${urls[i].FATU_id}`);

                let retry = 0;
                let FATU_id = urls[i].FATU_id;
                let site = urls[i].site;
                let page = urls[i].page;
                let reg_date = today;
                let isFinished = yield setData(urls[i].url, FATU_id);

                while (!isFinished && retry < 5) {
                    isFinished = yield setData(urls[i].url, FATU_id);
                    retry++;
                    console.log(`retry: ${retry}, ${urls[i].FATU_id}`);
                }
            }

            resolve();
        });
    });
}

// set out fastload data
const outFastload = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield db.connect(filePath_db);

            // set filepath
            let filepath_fastload = `${conf.dirPath_share_vb}\/${task_name}-amazon-library.txt`;
            let filepath_fastload2 = `${conf.dirPath_share_vb}\/${task_name}-amazon-library_${today}.txt`;

            // header check
            fs.writeFileSync(filepath_fastload, '', 'utf-8');
            fs.appendFileSync(filepath_fastload, ['id_num', 'brand', 'brand2', 'item_num', 'ge_accessories', 'ge_bag', 'ge_fashiongoods', 'ge_food', 'ge_inner', 'ge_kids', 'ge_kitchen', 'ge_ladiesfashion', 'ge_liquor_japan', 'ge_liquor_western', 'ge_mensfashion', 'ge_shoes', 'ge_sundries', 'ge_sweet', 'ge_watch', 'ge_water', 'ref_url', 'reg_date'].join('\t') + '\n', 'utf-8');
            fs.appendFileSync(filepath_fastload, ['id_num', 'brand', 'brand2', 'item_num', 'ge_accessories', 'ge_bag', 'ge_fashiongoods', 'ge_food', 'ge_inner', 'ge_kids', 'ge_kitchen', 'ge_ladiesfashion', 'ge_liquor_japan', 'ge_liquor_western', 'ge_mensfashion', 'ge_shoes', 'ge_sundries', 'ge_sweet', 'ge_watch', 'ge_water', 'ref_url', 'reg_date'].join('\t') + '\n', 'utf-8');
            fs.appendFileSync(filepath_fastload, ['VARCHAR(100)', 'VARCHAR(1000)', 'VARCHAR(1000)', 'INTEGER', 'INTEGER', 'INTEGER', 'INTEGER', 'INTEGER', 'INTEGER', 'INTEGER', 'INTEGER', 'INTEGER', 'INTEGER', 'INTEGER', 'INTEGER', 'INTEGER', 'INTEGER', 'INTEGER', 'INTEGER', 'INTEGER', 'VARCHAR(1000)', 'VARCHAR(8)'].join('\t') + '\n', 'utf-8');

            // get page data
            let sql = `SELECT b.FAT_id, b.FATU_id, b.brand, b.brand_kana, b.product_num, u.site, u.page, u.genre, u.category, u.category2, reg_date FROM fashion_${task_name}_library_amazon_result b inner join fashion_${task_name}_library_amazon_list u where b.FATU_id = u.FATU_id and reg_date = '${today}'`;
            let res = yield db.do(`all`, sql, {});

            // each target pages
            for (let i = 0; i < res.length; i++) {

                let accessories = res[i].FATU_id.match(/accessories/g) ? 1: 0;
                let bag = res[i].FATU_id.match(/bag/g) ? 1: 0;
                let fashiongoods = res[i].FATU_id.match(/fashiongoods/g) ? 1: 0;
                let food = res[i].FATU_id.match(/food/g) ? 1: 0;
                let inner = res[i].FATU_id.match(/inner/g) ? 1: 0;
                let kids = res[i].FATU_id.match(/kids/g) ? 1: 0;
                let kitchen = res[i].FATU_id.match(/kitchen/g) ? 1: 0;
                let ladiesfashion = res[i].FATU_id.match(/ladiesfashion/g) ? 1: 0;
                let liquor_japan = res[i].FATU_id.match(/liquor_japan/g) ? 1: 0;
                let liquor_western = res[i].FATU_id.match(/liquor_western/g) ? 1: 0;
                let mensfashion = res[i].FATU_id.match(/mensfashion/g) ? 1: 0;
                let shoes = res[i].FATU_id.match(/shoes/g) ? 1: 0;
                let sundries = res[i].FATU_id.match(/sundries/g) ? 1: 0;
                let sweet = res[i].FATU_id.match(/sweet/g) ? 1: 0;
                let watch = res[i].FATU_id.match(/watch/g) ? 1: 0;
                let water = res[i].FATU_id.match(/water/g) ? 1: 0;

                // append data
                fs.appendFileSync(filepath_fastload, [res[i].FAT_id, res[i].brand, res[i].brand_kana, res[i].product_num, accessories, bag, fashiongoods, food, inner, kids, kitchen, ladiesfashion, liquor_japan, liquor_western, mensfashion, shoes, sundries, sweet, watch, water, res[i].FATU_id, res[i].reg_date].join('\t') + '\n', 'utf-8');

                if (res[i].brand_kana && res[i].brand_kana != '') {
                    fs.appendFileSync(filepath_fastload, [res[i].FAT_id, res[i].brand_kana, res[i].brand, res[i].product_num, accessories, bag, fashiongoods, food, inner, kids, kitchen, ladiesfashion, liquor_japan, liquor_western, mensfashion, shoes, sundries, sweet, watch, water, res[i].FATU_id, res[i].reg_date].join('\t') + '\n', 'utf-8');
                }
            }

            // copy data
            let data = fs.readFileSync(filepath_fastload, 'utf-8');
            fs.writeFileSync(filepath_fastload2, data, 'utf-8');

            yield db.close();
            resolve();
        });
    });
}

// run action
const run = function () {

    co(function* () {

        switch (cli['run']) {

            // node src/rakuten/FGS157-library-amazon/scraper.js -r 99
            case 99: // reset list
                yield drop();
                yield setList();
                break;

                // node src/rakuten/fgs157-library-instagram/scraper.js -r 2
            case 2: // set out fastload data
                yield init();
                yield outFastload();
                yield end();
                break;

                // node src/rakuten/fgs157-library-instagram/scraper.js -r 1
                // -B chrome, -B phantom, -B firefox
            case 1: // get post from instagram targets
            default:
                yield init();
                yield scrape();
                yield end();
                break;
        }
    });
}

run();
'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const shell = require('shelljs');
const moment = require('moment');

const util = new(require('../../modules/util.js'));

// parse args
const cli = commandArgs([
    { name: 'startday', alias: 's', type: String },
]);

const yesterday = moment().subtract(1, 'days').format('YYYYMMDD');
const startday = cli['startday'] ? cli['startday'] : yesterday;

// run action
const run = function () {

    co(function* () {

        shell.exec('node src/rakuten/FGS252-FashionNetwork/scraper.js -r 1 -s ' + startday);
    });
}

run();
'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');
const moment = require('moment');

// new modules
const conf = new(require('../../conf.js'));
const db = new(require('../../modules/db.js'));
const tsv = new(require('../../modules/tsv.js'));
const util = new(require('../../modules/util.js'));
const cheerio = new(require('../../modules/cheerio-httpcli.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },//1:scrape/2:output
    { name: 'startdate', alias: 's', type: String },//YYYYMMDD
    { name: 'enddate', alias: 'e', type: String },//YYYYMMDD
    { name: 'category', alias: 'c', type: String },//1:book/2:music/3:movie/4:hotel/5:area/6:event/7:knowledge
    { name: 'cron', alias: 'n', type: String },//cron0,cron3
]);

const today = moment().format('YYYYMMDD');
const yesterday = moment().add(-1, 'd').format('YYYYMMDD');
//const yesterday_hiphen = moment().add(-1, 'd').format('YYYY-MM-DD');
const cron = cli['cron'] || 'cron0';
const filePath_db = `${conf.dirPath_db}/${cron}.db`;

let startdate;// = cli['startdate'] || yesterday;
let enddate;// = cli['enddate'] || yesterday;

if(cli['startdate'] && cli['startdate'] < today){
    startdate = cli['startdate'];
}else{
    startdate = yesterday;
}
if(cli['enddate'] && cli['enddate'] >= startdate){
    enddate = cli['enddate'];
}else{
    enddate = yesterday;
}

const startdate_hiphen = startdate.substr(0, 4) + '-' + startdate.substr(4, 2) + '-' + startdate.substr(6, 2);
//const enddate_hiphen = enddate.substr(0, 4) + '-' + enddate.substr(4, 2) + '-' + enddate.substr(6, 2);

let category = '';//1:book/2:music/3:movie/4:hotel/5:area/6:event/7:knowledge
if(cli['category'] == 1){
    category = 'book';
}else if (cli['category'] == 2){
    category = 'music';
}else if (cli['category'] == 3){
    category = 'movie';
}else if (cli['category'] == 4){
    category = 'hotel';
}else if (cli['category'] == 5){
    category = 'area';
}else if (cli['category'] == 6){
    category = 'area';
}else if (cli['category'] == 7){
    category = 'area';
}

let outfile = `${conf.dirPath_share_vb}/FGS284_kakakuTV_new_${today}.txt`;

const sttime = moment().format('YYYY/MM/DD HH:mm:ss');
console.log(`/// FGS284-kakakuTV scraper.js start  ${sttime} ///`);
console.log(`  run: ${cli['run']}`);
console.log(`  startdate : ${startdate}`);
console.log(`  enddate   : ${enddate}`);
console.log(`  cron      : ${cron}`);
console.log(`  arg category: ${category}`);
console.log(`  outfile: ${outfile}`);

// drop table for concern tables
const init_table = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let sql = `DROP TABLE IF EXISTS FGS284_kakakuTV;`;
            yield db.do(`run`, sql, {});

            sql = `CREATE TABLE IF NOT EXISTS FGS284_kakakuTV ` +
            `(date TEXT, category TEXT, page bigint, num TEXT, id TEXT PRIMARY KEY, infotype TEXT, ` +
            `program TEXT, episode TEXT, channel TEXT, title TEXT, article TEXT, ` +
            `URL TEXT, AREA TEXT, TEL TEXT, ADDRESS TEXT);`;
            yield db.do(`run`, sql, {});

            resolve();
        });
    });
}
// scrape article
const scrape_article = function (ctgry, date) {

    return new Promise((resolve, reject) => {
        co(function* () {

            //console.log('   日付:', date, 'カテゴリ:', ctgry);
            let artcnt = 0;
            let maxpage = 1;

            let url = `http://kakaku.com/tv/category=${ctgry}/date=${date}-${date}/page=1/`;
            //console.log(url);
            yield util.sleep(100);
            let $ = yield cheerio.get2(url);
            yield util.sleep(5000);
            if ($){ // && $('body').html().match(/maininfo/i)) 
                let main = $('#maininfo').html();
                
                //maxpage
                let tmp = $(main).find('.page').text();
                //console.log('tmp:',tmp);
                
                if(tmp.length > 0){
                    let tmpp = tmp.split('/');
                    //console.log('tmpp:',tmpp);  
                    maxpage = tmpp[1].replace(/[^0-9]/g, '');
                }
                //console.log('     maxpage:',maxpage);

                //page roop
                for (let p = 1; p <= maxpage; p++) {

                    if(p > 1){
                        url = `http://kakaku.com/tv/category=${ctgry}/date=${date}-${date}/page=${p}/`;
                        //console.log(url);
                        yield util.sleep(10);
                        $ = yield cheerio.get2(url);
                        if ($){ // && $('body').html().match(/maininfo/i)) 
                            main = $('#maininfo').html();
                        }
                    }
                    // id
                    let ids = $(main).find('.w680').find('a');//.attr('id');
                    //console.log('ids.length:',ids.length);
                    let idxs = [];
                    for (let i = 0; i < ids.length; i++) {
                        if($(ids).eq(i).attr('id')){
                            let strid = $(ids).eq(i).attr('id');
                            //console.log('strid:',strid);
                            idxs.push(strid); 
                        }
                    }
                    //console.log(idxs);
                    //console.log('idxs.length:',idxs.length);

                    // contents
                    let cts = $(main).find('.pdBtm20');//.html();
                    //console.log('cts.length:',cts.length);

                    if(idxs.length > 0 && idxs.length == cts.length ){
                        artcnt = artcnt + idxs.length;
                        for (let c = 0; c < idxs.length; c++) {

                            //let date;
                            //let category
                            let page = p;
                            let num = c + 1;
                            let id = idxs[c];
                            let title = '';
                            let article = '';
                            let infotype = '';
                            let program = '';
                            let episode = '';
                            let channel = '';
                            let URL = '';
                            let AREA = '';
                            let ADDRESS = '';
                            let TEL = '';

                            // title
                            let ttl = $(cts).eq(c).find('.tvnamebk');
                            title = $(ttl).find('p').text();
                            //console.log('title:',title);

                            // title tabelogURL
                            let tmp_t = $(ttl).attr('href')
                            if(tmp_t){
                                // title URL
                                let t_url = tmp_t;
                                //console.log('t_url:',t_url);
                                t_url = t_url.replace('https://kakaku.com/jump/?url=','')
                                //console.log('t_url:',t_url);
                                t_url = decodeURIComponent(t_url);
                                //console.log('t_url:',t_url);
                                URL = t_url;
                            }

                            // article
                            let atcl = $(cts).eq(c).find('.iteminfo.mTop10');
                            article = $(atcl).find('p').text();
                            //console.log('article:',article);

                            // ul
                            let lis = $(atcl).find('ul').find('li')
                            //console.log('lis.length:',lis.length);
                            if(lis.length > 2){
                                console.log('lis.length !!:',lis.length);
                            }
                            //console.log('lis:',lis);

                            // info1
                            let info1 = $(lis).eq(0).text();
                            //console.log('info1:',info1);

                            // info1 URL
                            let info1URL = $(lis).eq(0).find('a');
                            if(info1URL){
                                let t_url = info1URL.text();
                                if(t_url){
                                    //console.log('t_url:',t_url);
                                    URL = t_url;
                                }
                            }

                            let info1tmp = info1.split('　');
                            for (let i = 0; i < info1tmp.length; i++) {

                                //console.log('info1tmp['+i+']:',info1tmp[i]);

                                if (info1tmp[i].search('情報タイプ：') >= 0) {
                                    infotype = info1tmp[i].slice(6);
                                }
                                if (info1tmp[i].search('最寄り駅（エリア）：') >= 0) {
                                    AREA = info1tmp[i].slice(10);
                                }
                                if (info1tmp[i].search('電話：') >= 0) {
                                    TEL = info1tmp[i].slice(3);
                                }
                                if (info1tmp[i].search('住所：') >= 0) {
                                    ADDRESS = info1tmp[i].slice(3);
                                }
                            }

                            // info2
                            let tmp_p1 = $(lis).eq(1).find('a');
                            //console.log('tmp_p1.length:',tmp_p1.length);
                            if(tmp_p1.length == 3){
                                program = tmp_p1.eq(0).text();
                                //console.log('program:',program);
                                episode = tmp_p1.eq(1).text();
                                //console.log('episode:',episode);
                                channel = tmp_p1.eq(2).text();
                                //console.log('channel:',channel);
                            }

                            // insert
                            const sql = `INSERT OR REPLACE INTO FGS284_kakakuTV ` +
                                `(date,category,page,num,id,infotype,program,episode,channel,title,article,URL,AREA,TEL,ADDRESS) ` +
                                `VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);`;
                            //console.log('sql = ' + sql);
                            //let snum = ('000' + num).slice(-4);
                            //console.log('num = ' + num);
                            let opt = [date,ctgry,page,num,id,infotype,program,episode,channel,title,article,URL,AREA,TEL,ADDRESS];
                            //console.log('opt', opt);

                            yield db.do(`run`, sql, opt);
                        }
                    }
                }
            }
            console.log(`  ${date} : ${ctgry}    ページ数 : ${maxpage}  記事件数 : ${artcnt}   ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
            resolve();
        });
    });
}

// scrape main
const scrape_main = function () {

    return new Promise((resolve, reject) => {
        co(function* () {
            
            yield init_table();
            yield cheerio.init(10000);
            let date = startdate;
            let date_hiphen = startdate_hiphen;

            while (date <= enddate) {
                
                if(category){
                    yield scrape_article(category, date);
                }else{
                    yield scrape_article('book', date);
                    yield scrape_article('music', date);
                    yield scrape_article('movie', date);
                    yield scrape_article('hotel', date);
                    yield scrape_article('area', date);
                    yield scrape_article('event', date);
                    yield scrape_article('knowledge', date);
                }
                date_hiphen = moment(date_hiphen).add(1, 'd').format('YYYY-MM-DD');
                date = date_hiphen.replace(/[^0-9]/g, '');
            }
            yield util.sleep(3000);
            
            resolve();
        }).catch((e) => {
            console.log(e);
            resolve();
        });
    });
}

// set out file to share folder
const setOutFile = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // get scraped data
            let sql = `SELECT * FROM FGS284_kakakuTV Where date >= '${startdate}' and date <= '${enddate}'`;
            if(category){
                sql = sql + ` and category = '${category}'`;
            }
            sql = sql + ';';
            //console.log('sql = ' + sql);
            let res = yield db.do(`all`, sql, {});

            for (let i = 0; i < res.length; i++) {
                fs.appendFileSync(outfile, [res[i].date, res[i].category, res[i].page, res[i].num, res[i].id, res[i].title, res[i].article, res[i].infotype, res[i].program, res[i].episode, res[i].channel, res[i].URL, res[i].AREA, res[i].ADDRESS, res[i].TEL].join('\t') + '\n', 'utf-8');
            }

            resolve();
        });
    });
}

// run action
const run = function () {

    co(function* () {
        // 
        yield db.connect(filePath_db);

        if (!cli['run'] || cli['run'] == 1 ) {
            // scrape
            yield scrape_main();
        }

        if (!cli['run'] || cli['run'] == 2 ) {
            // out data to share folder
            yield setOutFile();
        }

        yield db.close();

        console.log(`/// FGS284-kakakuTV scraper.js end /// ${sttime} - ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
    });
}

run();
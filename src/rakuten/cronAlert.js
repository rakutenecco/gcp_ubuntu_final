'use strict';

const shell = require('shelljs');
const cronJob = require('cron').CronJob;
const moment = require('moment');
const fs = require('fs');
const date = moment().format('YYYYMMDD');
const path_log = `../log/${date}.txt`;
let now = '';

/*
cronJob(`00 00 00 * * 1-5`)
Seconds: 0-59
Minutes: 0-59
Hours: 0-23
Day of Month: 1-31
Months: 0-11
Day of Week: 0-6
*/


// daily
new cronJob(`00 30 00 * * 0-6`, function () {
    
        shell.exec('npm run googleAlert');

    }, function () {
        // error
        console.log(`Error Google Alerts Cron: ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
    },
    true, 'Asia/Tokyo');


console.log('cronGoogleArticle setting');
'use strict';

// const
const co = require('co');
const fs = require('fs');
const moment = require('moment');
const _ = require('underscore');

const conf = new(require('../../conf.js'));
const db = new(require('../../modules/db.js'));
const match = new(require('../../modules/match.js'));
const util = new(require('../../modules/util.js'));
const selenium = new(require('../../modules/selenium.js'));

let browser = '';
let driver = null;
const filePath_db = `${conf.dirPath_db}/cron6.db`;

// Michelklein 関数生成
let Michelklein = function () {

    this.title = 'Michelklein';
};

// init
const init = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // connect db
            yield db.connect(filePath_db);

            // set selenium with browser
            driver = yield selenium.init(browser, { inVisible: true });

            resolve();
        });
    });
}

// end
const end = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield util.sleep(2000);

            // quit browser
            driver.quit();

            // close db
            yield db.close();

            resolve();
        });
    });
}

// init
Michelklein.prototype.init = function (_browser) {

    return new Promise((resolve, reject) => {
        co(function* () {

            browser = _browser || 'chromium';

            resolve();
        });
    });
}

// delete
Michelklein.prototype.delete = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // delete
            yield db.connect(filePath_db);
            const sql1 = `delete from fashion_fgs44_seiko_category where page_id in ('5932-16');`;
            const sql2 = `delete from fashion_fgs44_seiko_product where page_id in ('5932-16');`;
            yield db.do(`run`, sql1, {});
            yield db.do(`run`, sql2, {});
            yield db.close();

            resolve();
        });
    });
}

// scrape category
Michelklein.prototype.scrapeCategory = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield init();

            let $ = null;
            let shop_category1 = 'ミッシェル クラン';
            let sql = `SELECT * FROM fashion_fgs44_seiko_list where shop_category1 = '${shop_category1}'`;
            let res = yield db.do(`all`, sql, {});

            for (let i = 0; i < res.length; i++) {

                let isEndPage = false;
                let id = res[i].id;
                let cat_url = res[i].cat_url;
                let shop_category1 = res[i].shop_category1;
                let shop_category2 = res[i].shop_category2;
                let page_id = res[i].page_id;
                let manufacturer_name = res[i].name;
                let baseUrl = 'http://license.seiko-watch.co.jp/p_search/';

                // open url
                driver.get(cat_url);
                yield util.sleep(2000);

                // scrape list
                let page_urls = [];
                $ = yield selenium.parseBody(driver);
                $('.number a').each(function (i, e) {
                    co(function* () {
                        page_urls.push(baseUrl + $(e).attr('href'));
                    });
                });
                page_urls = _.union(page_urls);
                page_urls = _.difference(page_urls, [cat_url]);

                // repete every page
                for (let j = 0; j < page_urls.length; j++) {

                    // open url
                    driver.get(page_urls[j]);
                    yield util.sleep(2000);

                    // scrape list
                    $ = yield selenium.parseBody(driver);
                    $('#lineup .price').each(function (i, e) {
                        co(function* () {
                            let text = $(e).find('a').text();
                            let url = baseUrl + $(e).find('a').attr('href');
                            // insert data
                            let sql = `INSERT OR REPLACE INTO fashion_fgs44_seiko_category(category_name, category_url, page_id) VALUES (?, ?, ?);`;
                            let opt = [text, url, page_id];
                            yield db.do(`run`, sql, opt);
                        });
                    });

                    yield util.sleep(200);
                }
            }

            yield end();

            resolve();
        });
    });
}

// scrape product
Michelklein.prototype.scrapeProduct = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield init();

            let $ = null;
            let sql1 = `SELECT * FROM fashion_fgs44_seiko_category a INNER JOIN fashion_fgs44_seiko_list b ON a.page_id = b.page_id and a.page_id in ('5932-16') WHERE NOT EXISTS (SELECT * FROM fashion_fgs44_seiko_product c WHERE c.url = a.category_url);`;
            let res = yield db.do(`all`, sql1, {});

            // each product
            for (let i = 0; i < res.length; i++) {

                let isEndPage = false;
                let category_name = res[i].category_name;
                let category_url = res[i].category_url;
                let id = res[i].id;
                let page_id = res[i].page_id;
                let shop_category1 = res[i].shop_category1;
                let shop_category2 = res[i].shop_category2;
                let manufacturer_name = res[i].name;
                let brand_name = '';
                let insert_date = moment().format('YYYY-MM-DD');
                console.log(category_url, shop_category1);

                // open url
                driver.get(category_url);
                yield util.sleep(2000);

                // set product lists
                $ = yield selenium.parseBody(driver);
                $('.watch_detail').each(function (i, e) {
                    co(function* () {

                        let url = category_url;
                        let name = util.replace($(e).find('.detail_ref').text());
                        let mpn_1 = name;
                        let mpn_2 = mpn_1;
                        let price = util.replace($(e).find('p').eq(0).html(), { price: true, match: '円', removeMatch: true, removeComma: true });
                        let price_tax = util.getPriceTax(price);
                        let price_unit = '';
                        let color = '';
                        let product_name = name;
                        // size
                        let size = '';
                        let size_value = yield match.getSizeUnit(size);

                        // insert data
                        let sql = `INSERT OR REPLACE INTO fashion_fgs44_seiko_product(id, page_id, mpn_1, mpn_2, manufacturer_name, brand_name, product_name, color, size, size_value, exc_price, inc_price, unit_price, category_name, insert_date, url) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);`;
                        let opt = [id, page_id, mpn_1, mpn_2, manufacturer_name, brand_name, product_name, color, size, size_value, price_tax, price, price_unit, category_name, insert_date, url];
                        yield db.do(`run`, sql, opt);
                    });
                });
            }

            yield end();

            resolve();
        });
    });
}

module.exports = Michelklein;
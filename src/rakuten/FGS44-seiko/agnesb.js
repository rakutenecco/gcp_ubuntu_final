'use strict';

// const
const co = require('co');
const fs = require('fs');
const moment = require('moment');

const conf = new(require('../../conf.js'));
const db = new(require('../../modules/db.js'));
const match = new(require('../../modules/match.js'));
const util = new(require('../../modules/util.js'));
const selenium = new(require('../../modules/selenium.js'));

let browser = '';
let driver = null;
const filePath_db = `${conf.dirPath_db}/cron6.db`;

// Agnesb 関数生成
let Agnesb = function () {

    this.title = 'Agnesb';
};

// init
const init = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // connect db
            yield db.connect(filePath_db);

            // set selenium with browser
            driver = yield selenium.init(browser, { inVisible: true });

            resolve();
        });
    });
}

// end
const end = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield util.sleep(2000);

            // quit browser
            driver.quit();

            // close db
            yield db.close();

            resolve();
        });
    });
}

// init
Agnesb.prototype.init = function (_browser) {

    return new Promise((resolve, reject) => {
        co(function* () {

            browser = _browser || 'chromium';

            resolve();
        });
    });
}

// delete
Agnesb.prototype.delete = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // delete
            yield db.connect(filePath_db);
            const sql1 = `delete from fashion_fgs44_seiko_category where page_id in ('5932-13');`;
            const sql2 = `delete from fashion_fgs44_seiko_product where page_id in ('5932-13');`;
            yield db.do(`run`, sql1, {});
            yield db.do(`run`, sql2, {});
            yield db.close();

            resolve();
        });
    });
}

// scrape category
Agnesb.prototype.scrapeCategory = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield init();

            let $ = null;
            let shop_category1 = 'アニエスベー';
            let sql = `SELECT * FROM fashion_fgs44_seiko_list where shop_category1 = '${shop_category1}'`;
            let res = yield db.do(`all`, sql, {});

            for (let i = 0; i < res.length; i++) {

                let isEndPage = false;
                let id = res[i].id;
                let cat_url = res[i].cat_url;
                let shop_category1 = res[i].shop_category1;
                let shop_category2 = res[i].shop_category2;
                let page_id = res[i].page_id;
                let manufacturer_name = res[i].name;
                let baseUrl = util.getBaseUrl(cat_url);

                // open url
                driver.get(cat_url);
                yield util.sleep(2000);

                // scrape list
                $ = yield selenium.parseBody(driver);
                $('.itemList_wrap li').each(function (i, e) {
                    co(function* () {
                        let text = $(e).find('img').attr('alt');
                        let url = baseUrl + '/' + $(e).find('a').attr('href');
                        // insert data
                        let sql = `INSERT OR REPLACE INTO fashion_fgs44_seiko_category(category_name, category_url, page_id) VALUES (?, ?, ?);`;
                        let opt = [text, url, page_id];
                        yield db.do(`run`, sql, opt);
                    });
                });

                yield util.sleep(200);
            }

            yield end();

            resolve();
        });
    });
}

// scrape product
Agnesb.prototype.scrapeProduct = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield init();

            let $ = null;
            let sql1 = `SELECT * FROM fashion_fgs44_seiko_category a INNER JOIN fashion_fgs44_seiko_list b ON a.page_id = b.page_id and a.page_id in ('5932-13') WHERE NOT EXISTS (SELECT * FROM fashion_fgs44_seiko_product c WHERE c.url = a.category_url);`;
            let res = yield db.do(`all`, sql1, {});

            // each product
            for (let i = 0; i < res.length; i++) {

                let isEndPage = false;
                let category_name = res[i].category_name;
                let category_url = res[i].category_url;
                let id = res[i].id;
                let page_id = res[i].page_id;
                let shop_category1 = res[i].shop_category1;
                let shop_category2 = res[i].shop_category2;
                let manufacturer_name = res[i].name;
                let brand_name = '';
                let insert_date = moment().format('YYYY-MM-DD');
                console.log(category_url, shop_category1);

                // open url
                driver.get(category_url);
                yield util.sleep(2000);

                // set product lists
                $ = yield selenium.parseBody(driver);

                let mpn_1 = '';
                let price = '';
                $('#txtBox tr').each(function (i, e) {
                    co(function* () {

                        let ths = [];
                        let tds = [];

                        $(e).find('th').each(function (i, e) {
                            ths.push(util.replace($(e).text()));
                        });
                        $(e).find('td').map(function (i, e) {
                            if (i % 2 === 1) tds.push(util.replace($(e).text()));
                        });

                        for (let i = 0; i < ths.length; i++) {
                            if (ths[i] === 'REF番号') mpn_1 = tds[i];
                            if (ths[i] === '価格') price = util.replace(tds[i], { price: true, match: '円', removeMatch: true, removeComma: true });
                        }
                    });
                });

                let mpn_2 = mpn_1;
                let color = '';
                let product_name = mpn_1;
                let price_tax = util.getPriceTax(price);
                let price_unit = '円';
                let size = '';
                let size_value = yield match.getSizeUnit(size);
                let url = category_url;

                // insert data
                let sql = `INSERT OR REPLACE INTO fashion_fgs44_seiko_product(id, page_id, mpn_1, mpn_2, manufacturer_name, brand_name, product_name, color, size, size_value, exc_price, inc_price, unit_price, category_name, insert_date, url) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);`;
                let opt = [id, page_id, mpn_1, mpn_2, manufacturer_name, brand_name, product_name, color, size, size_value, price_tax, price, price_unit, category_name, insert_date, url];
                yield db.do(`run`, sql, opt);
            }

            yield end();

            resolve();
        });
    });
}

module.exports = Agnesb;
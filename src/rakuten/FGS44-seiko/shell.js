'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const shell = require('shelljs');

const util = new(require('../../modules/util.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
]);

// run action
const run = function () {

    co(function* () {
        // 
        switch (cli['run']) {
            case 100: // total-data git
                shell.exec(`cd total-data && git add .`);
                shell.exec(`cd total-data && git add -u .`);
                shell.exec(`cd total-data && git commit -m 'up fgs44'`);
                shell.exec(`cd total-data && git pull origin master`);
                shell.exec(`cd total-data && git push origin master`);
                break;

            case 1: // usual action
            default:
                shell.exec('node src/rakuten/FGS44-seiko/scraper.js -r 100');
                shell.exec('node src/rakuten/FGS44-seiko/scraper.js -r 99');

                // shell.exec('node src/rakuten/FGS44-seiko/scraper.js -B chromium -r 9919');
                // shell.exec('node src/rakuten/FGS44-seiko/scraper.js -B chromium -r 19');

                shell.exec('node src/rakuten/FGS44-seiko/scraper.js -B chromium -r 9918');
                shell.exec('node src/rakuten/FGS44-seiko/scraper.js -B chromium -r 18');

                // shell.exec('node src/rakuten/FGS44-seiko/scraper.js -B chromium -r 9917');
                // shell.exec('node src/rakuten/FGS44-seiko/scraper.js -B chromium -r 17');

                shell.exec('node src/rakuten/FGS44-seiko/scraper.js -B chromium -r 9916');
                shell.exec('node src/rakuten/FGS44-seiko/scraper.js -B chromium -r 16');

                shell.exec('node src/rakuten/FGS44-seiko/scraper.js -B chromium -r 9915');
                shell.exec('node src/rakuten/FGS44-seiko/scraper.js -B chromium -r 15');

                // shell.exec('node src/rakuten/FGS44-seiko/scraper.js -B chromium -r 9914');
                // shell.exec('node src/rakuten/FGS44-seiko/scraper.js -B chromium -r 14');

                shell.exec('node src/rakuten/FGS44-seiko/scraper.js -B chromium -r 9913');
                shell.exec('node src/rakuten/FGS44-seiko/scraper.js -B chromium -r 13');

                shell.exec('node src/rakuten/FGS44-seiko/scraper.js -B chromium -r 9912');
                shell.exec('node src/rakuten/FGS44-seiko/scraper.js -B chromium -r 12');

                shell.exec('node src/rakuten/FGS44-seiko/scraper.js -B chromium -r 9911');
                shell.exec('node src/rakuten/FGS44-seiko/scraper.js -B chromium -r 11');

                shell.exec('node src/rakuten/FGS44-seiko/scraper.js -B chromium -r 9910');
                shell.exec('node src/rakuten/FGS44-seiko/scraper.js -B chromium -r 10');

                shell.exec('node src/rakuten/FGS44-seiko/scraper.js -B chromium -r 9909');
                shell.exec('node src/rakuten/FGS44-seiko/scraper.js -B chromium -r 9');

                shell.exec('node src/rakuten/FGS44-seiko/scraper.js -B chromium -r 9908');
                shell.exec('node src/rakuten/FGS44-seiko/scraper.js -B chromium -r 8');

                shell.exec('node src/rakuten/FGS44-seiko/scraper.js -B chromium -r 9907');
                shell.exec('node src/rakuten/FGS44-seiko/scraper.js -B chromium -r 7');

                shell.exec('node src/rakuten/FGS44-seiko/scraper.js -B chromium -r 9906');
                shell.exec('node src/rakuten/FGS44-seiko/scraper.js -B chromium -r 6');

                shell.exec('node src/rakuten/FGS44-seiko/scraper.js -r 5 -B chromium');
                shell.exec('node src/rakuten/FGS44-seiko/shell.js -r 100');
                break;
        }
    });
}

run();
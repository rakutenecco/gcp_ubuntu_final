'use strict';

// const
const co = require('co');
const fs = require('fs');
const moment = require('moment');

const conf = new(require('../../conf.js'));
const db = new(require('../../modules/db.js'));
const match = new(require('../../modules/match.js'));
const util = new(require('../../modules/util.js'));
const selenium = new(require('../../modules/selenium.js'));

let browser = '';
let driver = null;
const filePath_db = `${conf.dirPath_db}/cron6.db`;

// Galante 関数生成
let Galante = function () {

    this.title = 'Galante';
};

// init
const init = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // connect db
            yield db.connect(filePath_db);

            // set selenium with browser
            driver = yield selenium.init(browser, { inVisible: true });

            resolve();
        });
    });
}

// end
const end = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield util.sleep(2000);

            // quit browser
            driver.quit();

            // close db
            yield db.close();

            resolve();
        });
    });
}

// init
Galante.prototype.init = function (_browser) {

    return new Promise((resolve, reject) => {
        co(function* () {

            browser = _browser || 'chromium';

            resolve();
        });
    });
}

// delete
Galante.prototype.delete = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // delete
            yield db.connect(filePath_db);
            const sql1 = `delete from fashion_fgs44_seiko_category where page_id in ('5932-7');`;
            const sql2 = `delete from fashion_fgs44_seiko_product where page_id in ('5932-7');`;
            yield db.do(`run`, sql1, {});
            yield db.do(`run`, sql2, {});
            yield db.close();

            resolve();
        });
    });
}

// scrape product
Galante.prototype.scrapeProduct = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield init();

            let $ = null;
            let shop_category1 = 'ガランテ';
            let sql = `SELECT * FROM fashion_fgs44_seiko_list where shop_category1 = '${shop_category1}'`;
            let res = yield db.do(`all`, sql, {});

            for (let i = 0; i < res.length; i++) {

                let isEndPage = false;
                let id = res[i].id;
                let cat_url = res[i].cat_url;
                let shop_category1 = res[i].shop_category1;
                let shop_category2 = res[i].shop_category2;
                let page_id = res[i].page_id;
                let manufacturer_name = res[i].name;
                let baseUrl = util.getBaseUrl(cat_url);

                // open url
                driver.get(cat_url);
                yield util.sleep(2000);

                let ele1 = yield selenium.getElements(driver, '.collections_nav_top');
                ele1[0].click();
                yield util.sleep(2000);

                // set product lists
                $ = yield selenium.parseBody(driver);
                $('.productbox').each(function (i, e) {
                    co(function* () {
                        let mpn_pre = $(e).find('td').eq(0).html();
                        let mpn_pres = mpn_pre.replace('<br />', '<br>').split('<br>');
                        let mpn_1 = mpn_pres[0] ? mpn_pres[0].replace('＜', '<').split('<')[0] : ''; // [＜7月発売予定＞]から<より左を取得
                        mpn_1 = mpn_1.replace(/[　 ]/g, '@').split('@')[0]; // [SBLA107　数量限定32本]からスペースより左を取得
                        let mpn_2 = mpn_pres[1] ? mpn_pres[1].replace(/[＞]/g, '>').replace(/[＜]/g, '<') : '';
                        let brand_name = '';
                        let product_name = `${mpn_1} ${mpn_2}`;
                        let color = '';
                        let size = '';
                        $('tr').each(function (i2, e2) {
                            let th = $(e2).find('th').text();
                            let td = $(e2).find('td').text();
                            if (th === 'サイズ') {
                                size = td;
                            }
                        });
                        let size_value = yield match.getSizeUnit(size);
                        let price = util.replace($(e).find('td').eq(1).html(), { price: true, match: '円', removeMatch: true, removeComma: true });
                        let price_tax = util.getPriceTax(price);
                        let price_unit = '円';
                        let category_name = shop_category1;
                        let insert_date = moment().format('YYYY-MM-DD');
                        let url = cat_url;

                        // insert data
                        let sql = `INSERT OR REPLACE INTO fashion_fgs44_seiko_product(id, page_id, mpn_1, mpn_2, manufacturer_name, brand_name, product_name, color, size, size_value, exc_price, inc_price, unit_price, category_name, insert_date, url) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);`;
                        let opt = [id, page_id, mpn_1, mpn_2, manufacturer_name, brand_name, product_name, color, size, size_value, price_tax, price, price_unit, category_name, insert_date, url];
                        yield db.do(`run`, sql, opt);
                    });
                });
            }

            yield end();

            resolve();
        });
    });
}

module.exports = Galante;
'use strict';

// const
const co = require('co');
const fs = require('fs');
const moment = require('moment');

const conf = new(require('../../conf.js'));
const db = new(require('../../modules/db.js'));
const match = new(require('../../modules/match.js'));
const util = new(require('../../modules/util.js'));
const selenium = new(require('../../modules/selenium.js'));

let browser = '';
let driver = null;
const filePath_db = `${conf.dirPath_db}/cron6.db`;

// SeikoWatch 関数生成
let SeikoWatch = function () {

    this.title = 'SeikoWatch';
};

// init
const init = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // connect db
            yield db.connect(filePath_db);

            // set selenium with browser
            driver = yield selenium.init(browser, { inVisible: true });

            resolve();
        });
    });
}

// end
const end = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield util.sleep(2000);

            // quit browser
            driver.quit();

            // close db
            yield db.close();

            resolve();
        });
    });
}

// init
SeikoWatch.prototype.init = function (_browser) {

    return new Promise((resolve, reject) => {
        co(function* () {

            browser = _browser || 'chromium';

            resolve();
        });
    });
}

// delete
SeikoWatch.prototype.delete = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // delete
            yield db.connect(filePath_db);
            const sql1 = `delete from fashion_fgs44_seiko_category where page_id in ('5932-2','5932-3','5932-4','5932-5','5932-6');`;
            const sql2 = `delete from fashion_fgs44_seiko_product where page_id in ('5932-2','5932-3','5932-4','5932-5','5932-6');`;
            yield db.do(`run`, sql1, {});
            yield db.do(`run`, sql2, {});
            yield db.close();

            resolve();
        });
    });
}

// scrape category
SeikoWatch.prototype.scrapeCategory = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield init();

            let $ = null;
            let shop_category1 = 'クレドール';
            let sql = `SELECT * FROM fashion_fgs44_seiko_list where shop_category1 = '${shop_category1}'`;
            let res = yield db.do(`all`, sql, {});

            for (let i = 0; i < res.length; i++) {

                let isEndPage = false;
                let id = res[i].id;
                let cat_url = res[i].cat_url;
                let shop_category1 = res[i].shop_category1;
                let shop_category2 = res[i].shop_category2;
                let page_id = res[i].page_id;
                let manufacturer_name = res[i].name;
                let baseUrl = util.getBaseUrl(cat_url);

                // open url
                driver.get(cat_url);
                yield util.sleep(2000);

                yield selenium.scroll(driver, '/html/body/footer');

                // scrape list
                $ = yield selenium.parseBody(driver);
                $('.watchList li').each(function (i, e) {
                    co(function* () {
                        let text = $(e).find('p strong').text();
                        let url = baseUrl + $(e).find('a').attr('href');
                        // insert data
                        let sql = `INSERT OR REPLACE INTO fashion_fgs44_seiko_category(category_name, category_url, page_id) VALUES (?, ?, ?);`;
                        let opt = [text, url, page_id];
                        yield db.do(`run`, sql, opt);
                    });
                });
            }

            yield end();

            resolve();
        });
    });
}

// scrape product
SeikoWatch.prototype.scrapeProduct = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield init();

            let $ = null;
            let sql1 = `SELECT * FROM fashion_fgs44_seiko_category a INNER JOIN fashion_fgs44_seiko_list b ON a.page_id = b.page_id and a.page_id in ('5932-2','5932-3','5932-4','5932-5','5932-6') WHERE NOT EXISTS (SELECT * FROM fashion_fgs44_seiko_product c WHERE c.url = a.category_url);`;
            let res = yield db.do(`all`, sql1, {});

            // each product
            for (let i = 0; i < res.length; i++) {

                let isEndPage = false;
                let category_name = res[i].category_name;
                let category_url = res[i].category_url;
                let id = res[i].id;
                let page_id = res[i].page_id;
                let shop_category1 = res[i].shop_category1;
                let shop_category2 = res[i].shop_category2;
                let manufacturer_name = res[i].name;
                let brand_name = '';
                let insert_date = moment().format('YYYY-MM-DD');

                console.log(category_url, shop_category1);

                // open url
                driver.get(category_url);
                yield util.sleep(2000);

                // set product lists
                $ = yield selenium.parseBody(driver);
                $('.watchDetail').each(function (i, e) {
                    co(function* () {

                        let url = category_url;
                        let name_pre = $(e).find('h1').text().match(/[A-Z0-9]{7,}/i);
                        let name = name_pre ? util.replace(name_pre[0]) : '';
                        let mpn_1 = name;
                        let mpn_2 = mpn_1;
                        let price = util.replace($(e).find('.price').text(), { price: true, match: '円', removeMatch: true, removeComma: true });
                        let price_tax = util.getPriceTax(price);
                        let price_unit = '円';
                        let size = '';
                        let color = '';
                        let category_name = name;
                        let product_name = '';

                        $('.watchInfo dt').each(function (i, e) {
                            let dt = util.replace($(e).text());
                            let dd = util.replace($(e).next('dd').text());
                            if (dt === '製品名') {
                                product_name = dd ? util.replace(dd) : name;
                            }
                            if (dt === 'サイズ') {
                                size = util.replace(dd);
                            }
                        });

                        let size_value = yield match.getSizeUnit(size);

                        // insert data
                        let sql = `INSERT OR REPLACE INTO fashion_fgs44_seiko_product(id, page_id, mpn_1, mpn_2, manufacturer_name, brand_name, product_name, color, size, size_value, exc_price, inc_price, unit_price, category_name, insert_date, url) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);`;
                        let opt = [id, page_id, mpn_1, mpn_2, manufacturer_name, brand_name, product_name, color, size, size_value, price_tax, price, price_unit, category_name, insert_date, url];
                        yield db.do(`run`, sql, opt);

                    });
                });
            }

            yield end();

            resolve();
        });
    });
}

module.exports = SeikoWatch;
'use strict';

const shell = require('shelljs');
const cronJob = require('cron').CronJob;
const moment = require('moment');
const fs = require('fs');
const date = moment().format('YYYYMMDD');
const today = moment().format('YYYY-MM-DD hh:mm:ss');
const path_log = `../log/${date}.txt`;
let now = '';
const conf = new (require('/var/app/total/src/conf.js'));

/*
cronJob(`00 00 00 * * 1-5`)
Seconds: 0-59
Minutes: 0-59
Hours: 0-23
Day of Month: 1-31
Months: 0-11
Day of Week: 0-6
*/

//// 05 ////
// FGS258 twitter posts daily 0:19
new cronJob(`00 40 15 * * 4`, function () {

    let filepath = `${conf.dirPath_share_vb}/targets/targets_new_20190517.txt`;
    if (fs.existsSync(filepath)) {
        shell.exec(`node src/rakuten/FGS258-twitter/shell.js -r 1 -n cron11_05 -f ${filepath} -t 20190510`);
        shell.exec(`node src/rakuten/FGS258-twitter/shell.js -r 1 -n cron11_05 -f ${filepath} -t 20190509`);
    }
    
}, function () {
    // error
    console.log(`Error ${today} : FGS258 twitter posts scrape cron11_05`);
},
true, 'Asia/Tokyo');

console.log('cron11_05 setting');

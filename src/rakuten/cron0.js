'use strict';

const shell = require('shelljs');
const cronJob = require('cron').CronJob;
const moment = require('moment');
const fs = require('fs');
const date = moment().format('YYYYMMDD');
const path_log = `../log/${date}.txt`;
let now = '';

/*
cronJob(`00 00 00 * * 1-5`)
Seconds: 0-59
Minutes: 0-59
Hours: 0-23
Day of Month: 1-31
Months: 0-11
Day of Week: 0-6
*/

// articles daily 0:02
new cronJob(`00 02 00 * * 0-6`, function () {
//new cronJob(`00 33 09 * * 1`, function () {
    
    console.log(`cron0 articles scrape <start> ${moment().format('YYYY/MM/DD HH:mm:ss')}`);

    shell.exec('node src/rakuten/FGS247-insta-official/shell.js');
    shell.exec('npm run fgs45');

    console.log(`cron0 articles scrape <end> ${moment().format('YYYY/MM/DD HH:mm:ss')}`);

}, function () {
    // error
    console.log(`Error cron0 articles scrape ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
},
true, 'Asia/Tokyo');

console.log('cron0 setting');
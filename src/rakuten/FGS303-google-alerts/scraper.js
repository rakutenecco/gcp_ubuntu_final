'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');
const moment = require('moment');
const { Builder, By, Key, Capabilities, until, promise } = require('selenium-webdriver');
const selenium = new (require('../../modules/selenium.js'));


// new modules
const conf = new (require('../../conf.js'));
const db = new (require('../../modules/db.js'));
const util = new (require('../../modules/util.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
    { name: 'cron', alias: 'n', type: String },
    { name: 'fname', alias: 'f', type: String },
    { name: 'startday', alias: 's', type: String },
    { name: 'getdate', alias: 'g', type: String },
]);

let mode;
if(cli['run'] == 1){
    mode = 'new';
} else if (cli['run'] == 2){
    mode = 'continue';
} else if (cli['run'] == 9){
    mode = 'output';
}

const cron = cli['cron'] ;//cron13,cron13_01,cron13_02,cron13_03
const filePath_db = `${conf.dirPath_db}/${cron}.db`;

const infile = `${conf.dirPath_share_vb}/targets/${cli['fname']}`;//`${conf.dirPath_share_vb}/targets/targets_${category}.txt`;
const logtable = 'FGS303_log';

const tname = `FGS303_google_alerts`;
let getdate; //output filename
let getdate_slash; //result data, log data
let driver1 = null;

if(cli['getdate']){
    getdate = cli['getdate'];
    getdate_slash = cli['getdate'].substr(0, 4) + '/' + cli['getdate'].substr(4, 2) + '/' + cli['getdate'].substr(6, 2);
}else{
    getdate = moment().format('YYYYMMDD');
    getdate_slash = moment().format('YYYY/MM/DD');
}

let outfile = `${conf.dirPath_share_vb}/FGS303_google_alerts_${getdate}.txt`;

console.log('/// FGS286 scraper.js start ///');
console.log(`  mode: ${mode}`);
console.log(`  args  cron: ${cron}`);
console.log(`  args  fname: ${cli['fname']}`);
console.log(`  outfile: ${outfile}`);

// variable
let keywords = [];
let kwdRD = [];

let tgtnum; // targets table word number
let getkwd = [];
let starttime;

let driver;
let keyword;

// end action
const end = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield db.close();

            resolve();
        });
    });
}

// insert Log
const insertLog = function () {

    return new Promise((resolve, reject) => {
        co(function* () {
            starttime  = moment().format('YYYY/MM/DD HH:mm:ss');
            const sql = `INSERT OR REPLACE INTO FGS303_log ` +
                `(tbname, getdate, remainkwd, starttime) ` +
                `VALUES (?, ?, ?, ?);`;
            let opt = [tname, getdate_slash, keywords.length, starttime];
            console.log('', tname, getdate_slash, keywords.length, '', starttime, '', cron);
            yield db.do(`run`, sql, opt);
            resolve();
        });
    });
}

// update Log
const updateLog = function () {

    return new Promise((resolve, reject) => {
        co(function* () {
            const curtime  = moment().format('YYYY/MM/DD HH:mm:ss');
            const sql = `UPDATE FGS303_log SET getkwd = ${getkwd.length}, endtime = '${curtime}' ` +
            `WHERE tbname = '${tname}' and getdate = '${getdate_slash}' and starttime = '${starttime}';`;
            //console.log(sql);
            console.log(tname, getdate_slash, keywords.length, getkwd.length, starttime, curtime, cron);
            yield db.do(`run`, sql, {});
            resolve();
        });
    });
}

// output Log
const outLog = function () {

    return new Promise((resolve, reject) => {
        co(function* () {
            let sql = `select tbname, getdate, remainkwd, getkwd, starttime, endtime from FGS303_log ` +
                      `where tbname = '${tname}' and getdate = '${getdate_slash}' order by starttime;`;
            //console.log(sql);
            let res = yield db.do(`all`, sql, {});            

            let filepath = `${conf.dirPath_share_vb}/log/${tname}.log`;
            console.log(`output log: ${filepath}`);

            // append
            for (let i = 0; i < res.length; i++) {
                fs.appendFileSync(filepath, ([res[i].tbname, res[i].getdate, res[i].remainkwd, res[i].getkwd, res[i].starttime, res[i].endtime].join('\t') + '\n'), 'utf8');
            }

            resolve();

        });
    });
}

// getText
const getText = function (ele) {

    return new Promise((resolve, reject) => {
        co(function* () {

            if (!ele) {
                resolve('');
                return;
            }

            ele.getText().then(function (text) {
                resolve(text);
            }, function (err) {
                console.log('      getText err');
                resolve('error');
            });
        //}).catch((e) => {
        //    console.log(e);
        //    resolve('');
        });
    });
}

// Selenium 関数生成
let byElement = function (selector) {

    let tag = '';
    switch (selector.substr(0, 1)) {
        case '#':
            return By.id(selector.substr(1));
        case '.':
            return By.className(selector.substr(1));
        case '/':
            return By.xpath(selector);
        default:
            return By.css(selector);
    }
};

// getElements
const getElements = function (driver_ele, selector) {

    return new Promise((resolve, reject) => {
        co(function* () {

            if (!driver_ele || !selector) {
                resolve('');
                return;
            }
            driver_ele.findElements(byElement(selector)).then(function (ele) {
                resolve(ele);
            }, function (err) {
                resolve('');
            });
            
        });
    });
}

// getAttribute
const get_attribute = function (ele, attr) {

    return new Promise((resolve, reject) => {
        co(function* () {

            if (!ele) {
                resolve('');
                return;
            }

            ele.getAttribute(attr).then(function (val) {
                resolve(val);
            }, function (err) {
                console.log('      getText err');
                resolve('');
            });
        //}).catch((e) => {
        //    console.log(e);
        //    resolve('');
        });
    });
}

// init action
const init_table = function (result_table,targets_table) {

    return new Promise((resolve, reject) => {
        co(function* () {

            let sqlstr;

            // targets table drop
            sqlstr = `DROP TABLE IF EXISTS ${targets_table};`;
            //console.log('sql3 = ' + sqlstr);
            yield db.do(`run`, sqlstr, {});

            // targets table create
            sqlstr = `CREATE TABLE IF NOT EXISTS ${targets_table} ` +
                `(keyword TEXT PRIMARY KEY);`;
            //console.log('sql4 = ' + sql4);
            yield db.do(`run`, sqlstr, {});


            // create table
            sqlstr = `CREATE TABLE IF NOT EXISTS ${result_table} ` +
                `(keyword TEXT, post_by TEXT, getdate TEXT, article_title TEXT, article_url TEXT, article_body TEXT);`;
            //console.log('sql2 = ' + sql2);
            yield db.do(`run`, sqlstr, {});

            sqlstr = `CREATE INDEX IF NOT EXISTS i_${result_table} on ${result_table} (getdate, keyword);`;
            //console.log('sql2 = ' + sql2);
            yield db.do(`run`, sqlstr, {});

            // old result delete
            const deldate = moment(getdate.replace(/\//g, '-')).subtract(17, 'days').format('YYYY/MM/DD');
            //console.log(` result deldate: ${deldate}`);
            sqlstr = `DELETE FROM ${result_table} WHERE getdate < '${deldate}' or getdate = '${getdate_slash}';`;
            //const sql5 = `DELETE FROM ${result_table} WHERE getdate < '${deldate}';`;
            //console.log('sql5 = ' + sql5);
            yield db.do(`run`, sqlstr, {});

            // log table create
            sqlstr = `CREATE TABLE IF NOT EXISTS ${logtable} ` +
                `(tbname TEXT, getdate TEXT, target_date TEXT, remainkwd bigint, getkwd bigint, starttime TEXT, endtime TEXT );`;
            //console.log('sql6 = ' + sql6);
            yield db.do(`run`, sqlstr, {});

            sqlstr = `CREATE INDEX IF NOT EXISTS i_${logtable} on ${logtable} (tbname, getdate);`;
            //console.log('sql2 = ' + sql2);
            yield db.do(`run`, sqlstr, {});

            // old log delete
            const deldate2 = moment(getdate.replace(/\//g, '-')).subtract(16, 'days').format('YYYY/MM/DD');
            //console.log(` log deldate: ${deldate2}`);
            //const sql7 = `DELETE FROM FGS239_log WHERE tbname = '${result_table}' and (getdate < '${deldate2}' or getdate = '${getdate}');`;
            sqlstr = `DELETE FROM ${logtable} WHERE tbname = '${result_table}' and getdate < '${deldate2}';`;
            //console.log('sql7 = ' + sql7);
            yield db.do(`run`, sqlstr, {});


            resolve();
        });
    });

    
}

// store targets
const store_targets = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let result = false;
            //infile = `${conf.dirPath_share_vb}/targets_${category}.txt`;
            //console.log('infile',infile);
            if (fs.existsSync(infile)) {
                console.log(` infile: ${infile} read start`);
                let arr = fs.readFileSync(infile).toString().split("\n");
                //console.log(kywds.length);
                if(arr[arr.length -1].length == 0) arr.pop();
                console.log(` targets file: ${arr.length}`);
                
                //google alerts search string check utf-8
                let kywds = [];
                let exkywds = [];
                for(let i = 0; i < arr.length; i++) {
                    
                    let str = arr[i];
                    let testResult = str.match(/[^'\-%&\.:=’×°ー・々〆ﾟ･αβφ﨑ゞ0-9_A-Za-zｦ-ﾝ０-９Ａ-Ｚａ-ｚぁ-んァ-ヶ一-龠]/g);
                    if(testResult){
                        exkywds.push(str);
                    }else{
                        kywds.push(str);
                    }

                    //kywds.push(str);
                    //console.log(str);
                }
                let google_alert_except_file = `${conf.dirPath_share_vb}/targets/targets_except/targets_google_alert_except_${getdate}.txt`;

                if(exkywds.length > 0){
                    for (let i = 0; i < exkywds.length; i++) {
                        fs.appendFileSync(google_alert_except_file, exkywds[i] + '\n', 'utf8');
                    }
                }
                console.log(` except  keyword: ${exkywds.length}`);
                console.log(` targets keyword: ${kywds.length}`);
                result = true;

                let sql = `insert into FGS303_targets_google_alerts values `;
                if (kywds.length > 0) {
                    const n = 3000;
                    let times = Math.floor(kywds.length / n);
                    //console.log('n: ' + n + '  times: ' + times);
                    if (times * n == kywds.length) times--;
                    let i = 0;
                    let end = 0;
                    do {
                        if (times <= i) {
                            end = kywds.length;
                        } else {
                            end = (i + 1) * n;
                        }
                        let st = i * n;
                        //console.log('st = [' + st + '] end = [' + end + ']');
                        let exc = kywds.slice(st, end);
                        console.log(exc[0],exc[exc.length -1]);
                        //console.log(sql + '("' + exc.join('"),("') + '");');
                        yield db.do(`run`, `${sql} ("${exc.join('"),("')}");`, {});
                        i++;
                    } while (times >= i);
                }

            }else{
                console.log('  not exists in file');

            }
            resolve(result);

        });
    });
}

// get targets list
const get_targets = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //tgtnum            
            const sql1 = `select count(*) cnt from FGS303_targets_google_alerts;`;
            //console.log(sql1);
            let num = yield db.do(`all`, sql1, {});
            //console.log('num: ' + num);
            tgtnum = num[0].cnt;
            console.log(' targets table: ' + tgtnum);

            const sql = `select a.keyword keyword
            from FGS303_targets_google_alerts a 
            where not exists (select * from ${tname} b where b.getdate = '${getdate_slash}' and 
            b.article_url is NOT NULL 
            and a.keyword = b.keyword ) order by 1;`;
            //console.log(sql);
            keywords = yield db.do(`all`, sql, {});
            console.log(' remain keywords: ' + keywords.length);
            
            resolve(keywords.length);

        });
    });
}

// open getlist
const get_initElement = function () {

    return new Promise((resolve, reject) => {
        co(function* () {
            
            const url = 'https://www.google.co.jp/alerts';
            const slctr = 'input'
            let rt;
            
            if(!driver) {
                //console.log('  driver get');
                driver = yield selenium.init('chromium', { inVisible:true });
                driver.get(url);
                yield util.sleep(200);
            }
            
            let ele_input = yield getElements(driver, slctr);

            if(!ele_input){
                resolve(null);
            }else{
                resolve(ele_input[0]);
            }
        });
    });
}

// open get_data
const get_data = function (url_id) {

    return new Promise((resolve, reject) => {
        co(function* () {

            let result = false;

            let url = url_id;
            driver1 = yield selenium.init('chromium', { inVisible:true });
            driver1.manage().window().setSize(700,250);
            //driver1.get(url);

            //driver.get(url);
            driver1.get(url).then(null, function (err) {
                //resolve(text);
            }, function (err) {
                console.log('      driver get err');
                resolve('Invalid url');
            });
 
            yield util.sleep(4000);
            if(driver1){
                let content = yield getElements(driver1, "body");
                result = yield getText(content[0]);
                driver1.sleep(30);
               
            }
            resolve(result);
        }).catch((e) => {
            console.log('    get article catch');
            resolve('no body tag to scrape data');
        });
    });
}

const scrape_each_article = function(ele, keyword) {
    return new Promise((resolve, reject) => {
        co(function* () {
            let ele_title = yield getElements(ele, '.result_title_link');
            let ele_source = yield getElements(ele, '.result_source');
            let ele_href = yield getElements(ele, 'a[href]');
           
            let href_txt = yield get_attribute(ele_href[0], 'href');       
            let title_txt = yield getText(ele_title[0]);
            let source_txt = yield getText(ele_source[0]);
            let article = yield get_data(href_txt);

            title_txt = title_txt.replace(/[\n\r\t\(\) ,]/g, '');
            source_txt = source_txt.replace(/[\n\r\t\(\) ,]/g, '');
            
            //let content = article.replace(/[\n\r\t\(\) ]/g, '');
            let content = article.replace(/(\r\n|\n|\r)/gm, "");
            content = content.replace(/[\n\r\t\(\) ,]/g, '');

            if(driver1) {
                driver1.quit();
                driver1 = null;
            }


            const sql = `INSERT OR REPLACE INTO ${tname} ` +
                            `(keyword, post_by, getdate, article_title, article_url, article_body) ` +
                            `VALUES (?, ?, ?, ?, ?, ?);`;
                        
            let opt = [keyword, source_txt, getdate_slash, title_txt, href_txt, content];
            yield db.do(`run`, sql, opt);
            resolve(true);
        });
    });
}

//scrape content
const get_google_alert_data = function () {
    return new Promise((resolve, reject) => {
        co(function* () {
            let kIDX = 0;
            let ele_input;
            let ele_res;

            while(kIDX < keywords.length) {
                // keyword = util.encodeUrl(keywords[kIDX].keyword);
                keyword = keywords[kIDX].keyword;
                if(!ele_input) ele_input = yield get_initElement();

                ele_input.clear('');
                console.log(keyword);
                ele_input.sendKeys(keyword, Key.ENTER);
                yield util.sleep(1500);

                let ele_list = yield getElements(driver, 'ol');
                
                if(ele_list.length > 0){
                    ele_res = yield getElements(ele_list[0], 'li');
                }
                if(ele_res.length > 0){
                    let len = 0;
                    while(len < ele_res.length) {
                        yield scrape_each_article(ele_res[len], keyword);
                        len++;
                    }
                } else {
                    console.log("No data to scrape");
                }
                kIDX++;
            }
            if(driver) {
                driver.quit();
                driver = null;
            }
            resolve(true);
        }).catch((e) => {
            //console.log(e);
            console.log('scrape_main catch');
            resolve(false);
        });
    });
}

// scrape_google_alert
const scrape_main = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let result = false;
            let lflg = true; // loop continue?

            if (mode == 'new') {
                yield init_table(tname,`FGS303_targets_google_alerts`);
                let st = yield store_targets();
                if(!st){
                    console.log(`   store targets err`);
                    lflg = false;
                }
            }
            
            yield get_targets();
            if ( lflg ){
                yield insertLog();

                let rt = yield get_google_alert_data();
                yield updateLog();
                
                if(rt){
                    result = true;
                }

            }

            
            if(driver) driver.quit();

            if(result) yield setOutFile();

            resolve();
        }).catch((e) => {
            //console.log(e);
            console.log('scrape_main catch');
            resolve(false);
        });
    });
}

// set out file to share folder
const setOutFile = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let sql = `select post_by, getdate, article_title, article_url, SUBSTR(article_body,1, 4000) as article_body
             from ${tname} where getdate = '${getdate_slash}' AND article_url not in (
                 select article_url from ${tname} where getdate <> '${getdate_slash}')`;
            let res = yield db.do(`all`, sql, {});
            
            
            for (let i = 0; i < res.length; i++) {
                let clean_body = res[i].article_body.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '');
                clean_body = clean_body.replace(/[\n\r\t\(\) ,]/g, '');
                clean_body = clean_body.replace(/[^'\-%&\.:=’×°ー・々〆ﾟ･αβφ﨑ゞ0-9_A-Za-zｦ-ﾝ０-９Ａ-Ｚａ-ｚぁ-んァ-ヶ一-龠]/g, '')
                // fs.appendFileSync(outfile,[res[i].post_by, res[i].article_title, res[i].article_url, res[i].article_body, res[i].getdate].join('\t') + '\n', 'utf-8');
                fs.appendFileSync(outfile,[getdate, res[i].post_by, res[i].article_title, res[i].article_url, clean_body].join('\t') + '\n', 'utf-8');
            }
            yield outLog();
            yield end();
            console.log("File is outputed");
            resolve();
        //}).catch((e) => {
        //    console.log(e);
        });
    });
}


// run action
const run = function () {

    co(function* () {

        let st = moment().format('YYYY/MM/DD HH:mm:ss');
        yield db.connect(filePath_db);

        switch (mode) {

            case 'new':
            case 'continue':
                yield scrape_main();
                break;

            case 'output':
                yield setOutFile();
                break;

            default:
                break;
        }
        console.log(`/// FGS303-Google-Alerts scraper.js end /// ${st} - ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
    });
}

run();

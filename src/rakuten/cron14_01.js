'use strict';

const shell = require('shelljs');
const cronJob = require('cron').CronJob;
const moment = require('moment');
const fs = require('fs');
const conf = new (require('/var/app/total/src/conf.js'));

/*
cronJob(`00 00 00 * * 1-5`)
Seconds: 0-59
Minutes: 0-59
Hours: 0-23
Day of Month: 1-31
Months: 0-11
Day of Week: 0-6
*/

//// 01 ////
// FGS283 recipe Cookpad Weekly Sunday 0:05
new cronJob(`00 16 00 * * 0`, function () {

    // Cookpad
    shell.exec(`node src/rakuten/FGS283-recipe/shell.js -r 1 -b cron14_01`);

}, function () {
    // error
    console.log(`Error ${today} : FGS283 recipe Cookpad scrape error cron14_01`);
},
true, 'Asia/Tokyo');

console.log('cron14_01 setting');

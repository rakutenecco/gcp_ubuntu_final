'use strict';

const shell = require('shelljs');
const cronJob = require('cron').CronJob;
const moment = require('moment');
const fs = require('fs');
const date = moment().format('YYYYMMDD');
const today = moment().format('YYYY-MM-DD hh:mm:ss');
const path_log = `../log/${date}.txt`;
let now = '';
const conf = new (require('/var/app/total/src/conf.js'));

/*
cronJob(`00 00 00 * * 1-5`)
Seconds: 0-59
Minutes: 0-59
Hours: 0-23
Day of Month: 1-31
Months: 0-11
Day of Week: 0-6
*/

//// 03 ////
// FGS239 insta posts
//new cronJob(`00 05 00 * * 0-6`, function () {
new cronJob(`00 07 00 * * 0`, function () {
//    new cronJob(`00 45 09 * * 1`, function () {

    //WEAR
    let filename = 'targets_fashion.txt';
    shell.exec(`node src/rakuten/FGS239-wear-posts/shell.js -r 1 -n cron12_03 -f ${filename}`);

    // Yahoo
    //let startdate = moment().add(-7, 'd').format('YYYYMMDD');
    let startdate = '20190604';

    shell.exec(`node src/rakuten/FGS286-yahoo/shell.js -r 1 -n cron12_03 -f targets_all_06_01.txt -s ${startdate}`);
    shell.exec(`node src/rakuten/FGS286-yahoo/shell.js -r 1 -n cron12_04 -f targets_all_06_02.txt -s ${startdate}`);
    shell.exec(`node src/rakuten/FGS286-yahoo/shell.js -r 1 -n cron12_05 -f targets_all_06_03.txt -s ${startdate}`);


}, function () {
    // error
    console.log(`Error ${today} : FGS239 insta-posts scrape cron12_03`);
},
true, 'Asia/Tokyo');

console.log('cron12_03 setting');

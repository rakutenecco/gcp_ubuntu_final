'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');
const moment = require('moment');

// new modules
const conf = new(require('../../conf.js'));
const db = new(require('../../modules/db.js'));
const tsv = new(require('../../modules/tsv.js'));
const util = new(require('../../modules/util.js'));
const cheerio = new(require('../../modules/cheerio-httpcli.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
    { name: 'startday', alias: 's', type: String },
]);

const today = moment().format('YYYYMMDD');
const filePath_db = `${conf.dirPath_db}/cron2.db`;
const startday = cli['startday'];

// drop table for concern tables
const drop = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // drop table proxy_list
            yield db.connect(filePath_db);

            let sql = `DROP TABLE IF EXISTS FGS201_url;`;
            yield db.do(`run`, sql, {});

            sql = `DROP TABLE IF EXISTS FGS201_article;`;
            yield db.do(`run`, sql, {});
            yield db.close();

            resolve();
        });
    });
}

// set url list
const setList = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // create table "proxy_list"
            yield db.connect(filePath_db);
            const sql = `CREATE TABLE IF NOT EXISTS FGS201_url` +
                `(c_num TEXT primary key, c_getdate TEXT, c_URL TEXT, c_title TEXT, c_publish TEXT, c_category TEXT);`;
            yield db.do(`run`, sql, {});

            let start_site = 'https://www.fashion-headline.com/tag/%E3%83%A1%E3%83%B3%E3%82%BA';
            let grp = 'M'
            //Men's
            yield setListScrape(start_site, grp);

            start_site = 'https://www.fashion-headline.com/tag/%E3%82%A6%E3%82%A3%E3%83%A1%E3%83%B3%E3%82%BA';
            grp = 'W'
            //Women's
            yield setListScrape(start_site, grp);

            start_site = 'https://www.fashion-headline.com/category/fashion';
            grp = 'F'
            //Fashion
            yield setListScrape(start_site, grp);

            yield db.close();

            resolve();
        });
    });
}

// scrape setList data
const setListScrape = function (start_site, grp) {

    return new Promise((resolve, reject) => {
        co(function* () {

            // cheerio timeout setting
            yield cheerio.init(10000);

            let up_date = today;
            let p = 1;
            let n = 0;
            let site;

            //while (up_date > '17') {
            while (up_date >= startday) {

                if (p == 1) {
                    site = start_site;
                } else {
                    site = start_site + '?page=' + p;
                }
                //console.log('p = ' + p);

                let $ = yield cheerio.get(site);
                if ($ && $('body').html().match(/section-category/i)) {
                    $('.section-category').find('div').eq(1).find('li').each(function (i, e) {
                        co(function* () {

                            n = n + 1;
                            //console.log('n = ' + n);

                            let url = 'https://www.fashion-headline.com' + $(e).find('a').attr('href');
                            //console.log('url = ' + url);

                            let category = $(e).find('.category').find('span').eq(0).text();
                            //console.log('category = ' + category);

                            let title = $(e).find('.title').eq(0).text();
                            //console.log('title = ' + title);

                            let publish = $(e).find('.published').eq(0).text();
                            //console.log('publish = ' + publish);

                            let arr_date = publish.split(".");
                            if (String(arr_date[0]).length == 2) {
                                up_date = '20' + arr_date[0] + arr_date[1] + arr_date[2];
                                publish = '20' + publish;
                            } else {
                                up_date = arr_date[0] + arr_date[1] + arr_date[2];
                            }
                            //console.log('up_date = ' + up_date);

                            const sql = `INSERT OR REPLACE INTO FGS201_url ` +
                                `(c_num,c_getdate,c_URL,c_title,c_publish,c_category) VALUES (?, ?, ?, ?, ?, ?);`;
                            let num = grp + ('000' + n).slice(-4)
                            //console.log('num = ' + num);
                            let opt = [num, today, url, title, publish, category];
                            yield db.do(`run`, sql, opt);

                        });
                    });
                }
                p = p + 1;

            }

            resolve();
        });
    });
}

// init action
const init = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // cheerio timeout setting
            //yield cheerio.init(10000);

            // create table "proxy_list"
            yield db.connect(filePath_db);
            const sql = `CREATE TABLE IF NOT EXISTS FGS201_article ` +
                `(c_num TEXT primary key, c_getdate TEXT, c_art_title TEXT, c_art_publish TEXT, c_article TEXT);`;
            yield db.do(`run`, sql, {});

            resolve();
        });
    });
}

// get url list
const getList = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // create table "proxy_list"
            let sql = `SELECT * FROM FGS201_url`;
            let res = yield db.do(`all`, sql, {});

            resolve(res);
        });
    });
}

// scrape proxy data, and set db as proxy_list
const scrape = function (urls) {

    return new Promise((resolve, reject) => {
        co(function* () {

            // cheerio timeout setting
            yield cheerio.init(10000);

            for (let i = 0; i < urls.length; i++) {

                let num = urls[i].c_num;
                let getdate = urls[i].c_getdate;
                let URL = urls[i].c_URL;
                //console.log('num = ' + num);
                //console.log('URL = ' + URL);

                let $ = yield cheerio.get(urls[i].c_URL);
                if ($ && $('body').html().match(/container/i)) {

                    let e = $('.container').eq(1);

                    let art_title = $(e).find('.title-text').text();
                    //console.log(art_title);

                    let arr_pub = $(e).find('.published').text().split(".");
                    let art_publish = arr_pub[0] + arr_pub[1] + arr_pub[2];
                    //console.log(art_publish);

                    let article = $(e).find('.content').text().replace(/[\n\r\t\(\) ,]/g, '');

                    e = $('.pager');
                    //console.log(`  num:${num} e.length:${e.length}`);
                    if(e.length > 0) {
                        let next = $(e).find('.next');
                        if(next.length > 0) {
                            let nexturl = 'https://www.fashion-headline.com' + $(e).find('a').attr('href');
                            //console.log(` page:1  ${article}`);
                            let atmp = yield scrape_artnexturl(nexturl);
                            article = article + atmp;
                        }
                    }

                    let tmp = '';
                    for (let j = 0; j < article.length; j++) {
                        if (article.substr(j, 1) != "\uFFFC") {
                            tmp = tmp + article.substr(j, 1);
                        }
                    }
                    article = tmp
                    //console.log(article);

                    const sql = `INSERT OR REPLACE INTO FGS201_article ` +
                        `(c_num, c_getdate, c_art_title, c_art_publish, c_article) VALUES (?, ?, ?, ?, ?);`;
                    let opt = [num, getdate, art_title, art_publish, article];
                    yield db.do(`run`, sql, opt);

                }
            }

            resolve();
        });
    });
}

// scrape article data
const scrape_artnexturl = function (URL) {

    return new Promise((resolve, reject) => {
        co(function* () {

            let article = '';
            let $ = yield cheerio.get(URL);
            if ($ && $('body').html().match(/container/i)) {

                let e = $('.container').eq(1);
                article = $(e).find('.content').text().replace(/[\n\r\t\(\) ,]/g, '');

                e = $('.pager');
                //console.log(`  num:${num} e.length:${e.length}`);
                if(e.length > 0) {
                    let next = $(e).find('.next');
                    let anum = $(e).find('a');
                    let nexturl;
                    if(anum.length == 1){
                        nexturl = 'https://www.fashion-headline.com' + $(e).find('a').attr('href');
                    } else if (anum.length  == 2) {
                        nexturl = 'https://www.fashion-headline.com' + $(e).find('a').eq(1).attr('href');
                    }
                    //console.log(` next.length: ${next.length}  nexturl: ${nexturl}`);
                    //console.log(`   ${article}`);

                    if(next.length > 0) {
                        let atmp = yield scrape_artnexturl(nexturl);
                        article = article + atmp;
                    }
                }
            }

            resolve(article);
        });
    });
}


// end action
const end = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield db.close();

            resolve();
        });
    });
}

// set out file to share folder
const setOutFile = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // get scraped data
            yield db.connect(filePath_db);
            let sql = `SELECT a.c_getdate, a.min_num, a.c_URL, a.c_title, b.c_art_publish, a.c_category, b.c_art_title, b.c_article ` +
                `FROM ( SELECT MAX(c_getdate) c_getdate, MIN(c_num) min_num, c_publish, c_URL, MAX(c_category) c_category, MAX(c_title) c_title FROM FGS201_url GROUP BY c_publish, c_URL ) a ,FGS201_article b ` +
                `WHERE a.c_getdate = b.c_getdate AND a.min_num = b.c_num ` +
                `AND b.c_art_publish >= '${startday}' AND b.c_art_publish < '${today}' ` +
                `ORDER BY a.min_num;`;
            //console.log('sql = ' + sql);
            let res = yield db.do(`all`, sql, {});

            // write header
            let filepath = `${conf.dirPath_share_vb}/FGS201-${today}.txt`;
            fs.appendFileSync(filepath, ['データ取得日', 'number', 'URL', 'タイトル', '記事内更新日', 'カテゴリー', '記事内タイトル', '記事'].join('\t') + '\n', 'utf-8');
            fs.appendFileSync(filepath, ['c_getdate', 'c_num', 'c_URL', 'c_title', 'c_art_publish', 'c_category', 'c_art_title', 'c_article'].join('\t') + '\n', 'utf-8');
            fs.appendFileSync(filepath, ['VARCHAR(8)', 'VARCHAR(6)', 'VARCHAR(200)', 'VARCHAR(1000)', 'VARCHAR(8)', 'VARCHAR(50)', 'VARCHAR(1000)', 'VARCHAR(20000)'].join('\t') + '\n', 'utf-8');

            // write body
            for (let i = 0; i < res.length; i++) {
                fs.appendFileSync(filepath, [res[i].c_getdate, res[i].min_num, res[i].c_URL, res[i].c_title, res[i].c_art_publish, res[i].c_category, res[i].c_art_title, res[i].c_article].join('\t') + '\n', 'utf-8');
            }

            yield db.close();
            resolve(res);
        });
    });
}



// run action
const run = function () {

    co(function* () {
        // 
        switch (cli['run']) {
            case 1: // drop table
                yield drop();
                break;
            case 2: // set url list
                yield setList();
                break;
            case 3: // scrape data
                yield init();
                const list = yield getList();
                yield scrape(list);
                yield end();
                break;
            case 4: // out data to share folder
                yield setOutFile();
                break;
            default:
                break;
        }
    });
}

run();
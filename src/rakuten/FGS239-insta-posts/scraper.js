'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');
const moment = require('moment');
const cheerio = new (require('../../modules/cheerio-httpcli.js'));
const { Builder, By, Key, Capabilities, until, promise } = require('selenium-webdriver');
const selenium = new (require('../../modules/selenium.js'));

// new modules
const conf = new (require('../../conf.js'));
const db = new (require('../../modules/db.js'));
const util = new (require('../../modules/util.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
    { name: 'cron', alias: 'n', type: String },
    { name: 'fname', alias: 'f', type: String },
    { name: 'getdate', alias: 'g', type: String },// yyyymmdd output only
]);

let mode;
if(cli['run'] == 1){
    mode = 'new';
} else if (cli['run'] == 2){
    mode = 'continue';
} else if (cli['run'] == 9){
    mode = 'output';
}
const cron = cli['cron'];
const filePath_db = `${conf.dirPath_db}/${cron}.db`;

const infile = `${conf.dirPath_share_vb}/targets/${cli['fname']}`;

const category = cli['fname'].replace('targets_','').replace('.txt','');// fashion_03_03

const tname = `FGS239_insta_posts_${category}`;
const logtable = 'FGS239_insta_log';

let getdate; //output filename
let getdate_slash; //result data, log data
if(cli['getdate']){
    getdate = cli['getdate'];
    getdate_slash = cli['getdate'].substr(0, 4) + '/' + cli['getdate'].substr(4, 2) + '/' + cli['getdate'].substr(6, 2);
}else{
    //getdate = moment().format('YYYYMMDD');
    //getdate_slash = moment().format('YYYY/MM/DD');

    getdate = moment().day(0).format('YYYYMMDD');
    getdate_slash = moment().day(0).format('YYYY/MM/DD');
}
//let postdate = moment(getdate_slash.replace(/\//g, '-')).subtract(1, 'days').format('YYYY/MM/DD');; 
let postdate = moment().subtract(1, 'days').format('YYYY/MM/DD'); 

let outfile;
let outlogfile;
// ex) fname:targets_fashion.txt -> FGS239_insta_posts_fashion_20190304.txt
// ex) fname:targets_fashion_02_01.txt -> FGS239_insta_posts_fashion_20190304_02_01.txt
// ex) fname:targets_new_20190301.txt -> FGS239_insta_posts_new_20190301_20190304.txt
let tfnm = category.split(/_/g);
if(tfnm.length == 1) {
    outfile = `${conf.dirPath_share_vb}/FGS239_insta_posts_${tfnm[0]}_${getdate}.txt`;
    outlogfile = `${conf.dirPath_share_vb}/log/FGS239_insta_posts_${tfnm[0]}.log`;

} else if ( tfnm.length == 3 && tfnm[1].length == 2 && isFinite(tfnm[1]) && tfnm[2].length == 2 && isFinite(tfnm[2]) ){
    outfile = `${conf.dirPath_share_vb}/FGS239_insta_posts_${tfnm[0]}_${getdate}_${tfnm[1]}_${tfnm[2]}.txt`;
    outlogfile = `${conf.dirPath_share_vb}/log/FGS239_insta_posts_${tfnm[0]}_${tfnm[1]}_${tfnm[2]}.log`;

} else if ( tfnm.length == 2 && tfnm[0] == 'new' && isFinite(tfnm[1]) ) {
    outfile = `${conf.dirPath_share_vb}/FGS239_insta_posts_${tfnm[0]}_${tfnm[1]}_${getdate}.txt`;
    outlogfile = `${conf.dirPath_share_vb}/log/FGS239_insta_posts_${tfnm[0]}_${tfnm[1]}.log`;

} else {
    console.log(` cli['fname']: ${cli['fname']} check the file name.`);
    return;
}
//max file
const maxfile = `${conf.dirPath_share_vb}/targets/FGS239_insta_targets_all_max.txt`;
const maxtable = `FGS239_insta_targets_all_max`;

//NG file
const ngfile = `${conf.dirPath_share_vb}/targets/FGS239_insta_targets_${category}_ngchk.txt`;

console.log('/// FGS239-insta-posts scraper.js start ///');
console.log(`  mode: ${mode}`);
console.log(`  args  cron: ${cron}`);
console.log(`  args  fname: ${cli['fname']}`);
console.log(`  args  category: ${category}`);
console.log(`  getdate: ${getdate_slash}`);
console.log(`  outfile: ${outfile}`);

const maxloop = 60;
const minnum = 0;

let keywords = [];
//const getdate = cli['time'] ? cli['time'] : moment().format('YYYY/MM/DD');
//const postdate = moment().subtract(1, 'days').format('YYYY/MM/DD');
//const today = cli['time'] ? cli['time'].replace(/[^0-9]/g, '') : moment().format('YYYYMMDD');

let tgtnum; // targets table word number
let getkwd = [];
let lcnt = 1;
let zcnt = 0;
let starttime;
let prcstype = 1;

let driver;

// end action
const end = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield db.close();

            resolve();
        });
    });
}

// insert Log
const insertLog = function () {

    return new Promise((resolve, reject) => {
        co(function* () {
            starttime  = moment().format('YYYY/MM/DD HH:mm:ss');
            const sql = `INSERT OR REPLACE INTO ${logtable} ` +
                //`(tbname, getdate, loop_count, remainkwd, getkwd, starttime, endtime) ` +
                //`VALUES (?, ?, ?, ?, ?, ?, ?);`;
                `(tbname, getdate, loop_count, remainkwd, starttime, prcstype) ` +
                `VALUES (?, ?, ?, ?, ?, ?);`;
            let opt = [tname, getdate_slash, lcnt, keywords.length, starttime, prcstype];
            console.log(` insert log: tname:${tname}  getdate:${getdate}  lcnt:${lcnt}  remain:${keywords.length}  st:${starttime}  prc:${prcstype}  cron:${cron}`);
            yield db.do(`run`, sql, opt);
            resolve();
        });
    });
}

// update Log
const updateLog = function () {

    return new Promise((resolve, reject) => {
        co(function* () {
            const curtime  = moment().format('YYYY/MM/DD HH:mm:ss');
            const sql = `UPDATE ${logtable} SET getkwd = ${getkwd.length}, endtime = '${curtime}' ` +
            `WHERE tbname = '${tname}' and getdate = '${getdate_slash}' and loop_count = ${lcnt} and starttime = '${starttime}';`;
            //console.log(sql);
            console.log(` update log : tname:${tname}  getdate:${getdate}  lcnt:${lcnt}  remain:${keywords.length}  get:${getkwd.length}  st:${starttime}  end;${curtime}  prc:${prcstype}  cron:${cron}`);

            yield db.do(`run`, sql, {});
            resolve();
        });
    });
}

// output Log
const outLog = function () {

    return new Promise((resolve, reject) => {
        co(function* () {
            let sql = `select tbname, getdate, loop_count, remainkwd, getkwd, starttime, endtime, prcstype from ${logtable} ` +
                      `where tbname = '${tname}' and getdate = '${getdate_slash}' order by starttime;`;
            //console.log(sql);
            let res = yield db.do(`all`, sql, {});            

            console.log(` output log: ${outlogfile}`);

            // append
            for (let i = 0; i < res.length; i++) {
                fs.appendFileSync(outlogfile, ([res[i].tbname, res[i].getdate, res[i].loop_count, res[i].remainkwd, res[i].getkwd, res[i].starttime, res[i].endtime, res[i].prcstype].join('\t') + '\n'), 'utf8');
            }

            resolve();

        });
    });
}

// output NG file
const outNGfile = function () {

    return new Promise((resolve, reject) => {
        co(function* () {
            let sql = `select a.keyword keyword, '${getdate_slash}' getdate, a.maxposts maxposts, d.posts posts from ${maxtable} a 
            inner join (select keyword from FGS239_targets_${category} b where not exists (select * from ${tname} c
                where c.getdate = '${getdate_slash}' and c.ngflg = 0 and c.keyword = b.keyword ) 
                ) t on t.keyword = a.keyword
            left join (select keyword, getdate, posts from ${tname} b where getdate = '${getdate_slash}' 
            ) d on d.keyword = t.keyword
            where a.maxposts > 0;`;
            //console.log(sql);
            let res = yield db.do(`all`, sql, {});            

            if(res.length > 0){
                console.log(` output NG file: ${ngfile}`);

                // append
                for (let i = 0; i < res.length; i++) {
                    fs.appendFileSync(ngfile, ([res[i].keyword, res[i].getdate, res[i].maxposts, res[i].posts].join('\t') + '\n'), 'utf8');
                }

            }else{
                console.log(` not exists NG words`);

            }

            resolve();

        });
    });
}

// init action
const init_table = function (result_table,targets_table, max_table) {

    return new Promise((resolve, reject) => {
        co(function* () {

            let sqlstr;

            // targets table drop
            sqlstr = `DROP TABLE IF EXISTS ${targets_table};`;
            //console.log('sql3 = ' + sql3);
            yield db.do(`run`, sqlstr, {});

            // targets table create
            sqlstr = `CREATE TABLE IF NOT EXISTS ${targets_table} ` +
                `(keyword TEXT PRIMARY KEY);`;
            //console.log('sql4 = ' + sql4);
            yield db.do(`run`, sqlstr, {});


            // max table drop
            sqlstr = `DROP TABLE IF EXISTS ${max_table};`;
            //console.log('sql3 = ' + sql3);
            yield db.do(`run`, sqlstr, {});

            // max table create
            sqlstr = `CREATE TABLE IF NOT EXISTS ${max_table} ` +
                `(keyword TEXT PRIMARY KEY, maxposts bigint);`;
            //console.log('sql4 = ' + sql4);
            yield db.do(`run`, sqlstr, {});


            // result table create
            sqlstr = `CREATE TABLE IF NOT EXISTS ${result_table} ` +
                `(keyword TEXT, category TEXT, getdate TEXT, postdate TEXT, posts bigint, ngflg integer);`;
            //console.log('sql2 = ' + sql2);
            yield db.do(`run`, sqlstr, {});
            
            sqlstr = `CREATE INDEX IF NOT EXISTS i_${result_table} on ${result_table} (getdate, keyword);`;
            //console.log('sql2 = ' + sql2);
            yield db.do(`run`, sqlstr, {});

            // old result delete
            const deldate = moment(getdate.replace(/\//g, '-')).subtract(25, 'days').format('YYYY/MM/DD');
            console.log(` result deldate: ${deldate}`);
            sqlstr = `DELETE FROM ${result_table} WHERE getdate < '${deldate}';`;
            //sqlstr = `DELETE FROM ${result_table} WHERE getdate < '${getdate_slash}' or getdate = '${getdate_slash}';`;

            //const sql5 = `DELETE FROM ${result_table} WHERE getdate < '${deldate}';`;
            //console.log('sql5 = ' + sql5);
            yield db.do(`run`, sqlstr, {});

            // log table create
            sqlstr = `CREATE TABLE IF NOT EXISTS ${logtable} ` +
                `(tbname TEXT, getdate TEXT, loop_count integer, remainkwd bigint, getkwd bigint, starttime TEXT, endtime TEXT, prcstype TEXT );`;
            //console.log('sql6 = ' + sql6);
            yield db.do(`run`, sqlstr, {});

            sqlstr = `CREATE INDEX IF NOT EXISTS i_${logtable} on ${logtable} (tbname, getdate);`;
            //console.log('sql2 = ' + sql2);
            yield db.do(`run`, sqlstr, {});

            // old log delete
            const deldate2 = moment(getdate.replace(/\//g, '-')).subtract(30, 'days').format('YYYY/MM/DD');
            console.log(` log deldate: ${deldate2}`);
            //const sql7 = `DELETE FROM FGS239_log WHERE tbname = '${result_table}' and (getdate < '${deldate2}' or getdate = '${getdate}');`;
            sqlstr = `DELETE FROM ${logtable} WHERE tbname = '${result_table}' and getdate < '${deldate2}';`;
            //console.log('sql7 = ' + sql7);
            yield db.do(`run`, sqlstr, {});

            resolve();
        });
    });
}

// store targets
const store_targets = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let result = false;
            //infile = `${conf.dirPath_share_vb}/targets_${category}.txt`;
            //console.log('infile',infile);
            if (fs.existsSync(infile)) {
                console.log(` infile: ${infile} read start`);
                let tgtsfile = fs.readFileSync(infile).toString().split("\n");
                //console.log(kywds.length);
                if(tgtsfile[tgtsfile.length -1].length == 0) tgtsfile.pop();
                console.log(` targets file: ${tgtsfile.length}`);

                //let kywds = tgtsfile;

                //Instagram hashtag check!! utf-8
                let kywds = [];
                let exkywds = [];
                for(let i = 0; i < tgtsfile.length; i++) {
                    
                    let str = tgtsfile[i];
                    let testResult = str.match(/[^ー・々〆ﾟ･αβφゞ﨑0-9_A-Za-zｦ-ﾝ０-９Ａ-Ｚａ-ｚぁ-んァ-ヶ一-龠]/g);
                    let numtest =  str.match(/[^0-9]/g);
                    if(testResult || !numtest){
                        exkywds.push(str);
                    }else{
                        kywds.push(str);
                    }
                }
                let except_file = `${conf.dirPath_share_vb}/targets/targets_except/targets_insta_except_${category}_${getdate}.txt`;

                if(exkywds.length > 0){
                    for (let i = 0; i < exkywds.length; i++) {
                        fs.appendFileSync(except_file, exkywds[i] + '\n', 'utf8');
                    }
                }
                console.log(` except  keyword: ${exkywds.length}`);
                console.log(` targets keyword: ${kywds.length}`);

                let sql = `insert into FGS239_targets_${category} values `;
                if (kywds.length > 0) {
                    const n = 3000;
                    let times = Math.floor(kywds.length / n);
                    //console.log('n: ' + n + '  times: ' + times);
                    if (times * n == kywds.length) times--;
                    let i = 0;
                    let end = 0;
                    do {
                        if (times <= i) {
                            end = kywds.length;
                        } else {
                            end = (i + 1) * n;
                        }
                        let st = i * n;
                        //console.log('st = [' + st + '] end = [' + end + ']');
                        let exc = kywds.slice(st, end);
                        //console.log(exc[0],exc[exc.length -1]);
                        //console.log(sql + '("' + exc.join('"),("') + '");');
                        yield db.do(`run`, sql + '("' + exc.join('"),("') + '");', {});
                        i++;
                    } while (times >= i);
                }
                result = true;

            }else{
                console.log('  not exists infile');

            }

            //maxfile
            if (fs.existsSync(maxfile) && result) {
                result = false;

                console.log(` maxfile: ${maxfile} read start`);
                let tgtsmaxfile = fs.readFileSync(maxfile).toString().split("\n");
                //console.log(kywds.length);
                if(tgtsmaxfile[tgtsmaxfile.length -1].length == 0) tgtsmaxfile.pop();
                //console.log(` targets max file: ${tgtsmaxfile.length}`);

                let mxkywds = tgtsmaxfile;
                console.log(` targets max keyword: ${mxkywds.length}`);

                let sql = `insert into ${maxtable} values `;
                if (mxkywds.length > 0) {
                    const n = 3000;
                    let times = Math.floor(mxkywds.length / n);
                    //console.log('n: ' + n + '  times: ' + times);
                    if (times * n == mxkywds.length) times--;
                    let i = 0;
                    let end = 0;
                    do {
                        if (times <= i) {
                            end = mxkywds.length;
                        } else {
                            end = (i + 1) * n;
                        }
                        let st = i * n;
                        //console.log('st = [' + st + '] end = [' + end + ']');
                        let exc = mxkywds.slice(st, end);
                        //console.log(exc[0],exc[exc.length -1]);
                        let arr = [];
                        for ( let iarr = 0, len = exc.length; iarr < len; ++iarr) {
                            //console.log( exc[iarr].replace('///','",'));
                            arr.push(exc[iarr].replace('///','",'));
                        }
                        //console.log(sql + '("' + arr.join('),("') + ');');
                        yield db.do(`run`, sql + '("' + arr.join('),("') + ');', {});
                        i++;
                    } while (times >= i);
                }                
                result = true;

            }else{
                console.log('  not exists maxfile');
                result = false;

            }
            resolve(result);
        });
    });
}

// get targets
const get_targets = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //tgtnum            
            const sql1 = `select count(*) cnt from FGS239_targets_${category};`;
            //console.log(sql1);
            let num = yield db.do(`all`, sql1, {});
            //console.log('num: ' + num);
            tgtnum = num[0].cnt;
            console.log(' targets table: ' + tgtnum);

            const sql = `select a.keyword keyword, ifnull(m.maxposts, 0) maxposts
            from FGS239_targets_${category} a 
            left join ${maxtable} m on a.keyword = m.keyword
            where not exists ( select * from ${tname} b where b.getdate = '${getdate_slash}' and b.ngflg = 0 
            and a.keyword = b.keyword ) order by 1;`;
            //console.log(sql);
            keywords = yield db.do(`all`, sql, {});
            console.log(' remain keywords: ' + keywords.length);

            resolve();
        });
    });
}

// get targets 2
const get_targets2 = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //tgtnum            
            const sql1 = `select count(*) cnt from FGS239_targets_${category};`;
            //console.log(sql1);
            let num = yield db.do(`all`, sql1, {});
            //console.log('num: ' + num);
            tgtnum = num[0].cnt;
            console.log(' targets table: ' + tgtnum);

            const sql = `select keyword, maxposts from ${maxtable} a where maxposts > 0
            and exists (select * from FGS239_targets_${category} t where t.keyword = a.keyword )
            and not exists (select * from ${tname} b where b.getdate = '${getdate_slash}' and b.ngflg = 0 
            and a.keyword = b.keyword ) order by 1;`;
            //console.log(sql);
            keywords = yield db.do(`all`, sql, {});
            console.log(' remain keywords: ' + keywords.length);

            resolve();
        });
    });
}

// get loop count
const get_loop_count = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //loop count
            
            const sql = `select max(loop_count) lct from ${logtable} where getdate = '${getdate_slash}' and tbname = '${tname}' and getkwd >= 0;`;
            //console.log(sql);
            let num = yield db.do(`all`, sql, {});
            //console.log(' max loop_count: ' + num[0].lct);
            if ( num[0].lct > 0 ) {
                lcnt = num[0].lct + 1;
            }
            console.log(' loop_count: ' + lcnt);

            resolve();
        });
    });
}

// getElements
const getElements = function (driver_ele, selector) {

    return new Promise((resolve, reject) => {
        co(function* () {
            
            if (!driver_ele || !selector) {
                //console.log('    getElements arg null err');
                resolve('');
                return;
            }
            driver_ele.findElements(byElement(selector)).then(function (ele) {
                resolve(ele);
            }, function (err) {
                //console.log('    getElements err');
                resolve('');
            });
        });
    });
}

// Selenium 関数生成
let byElement = function (selector) {

    let tag = '';
    switch (selector.substr(0, 1)) {
        case '#':
            return By.id(selector.substr(1));
        case '.':
            return By.className(selector.substr(1));
        case '/':
            return By.xpath(selector);
        default:
            return By.css(selector);
    }
};

// get data from url
const getText = function (ele) {

    return new Promise((resolve, reject) => {
        co(function* () {

            if (!ele) {
                resolve('');
                return;
            }

            ele.getText().then(function (text) {
                resolve(text);
            }, function (err) {
                //console.log(err);
                resolve('');
            });
        //}).catch((e) => {
        //    console.log(e);
        //    resolve('');
        });
    });
}

// get explore count
const getExploreCount = function (page_id, ms) {
    return new Promise((resolve, reject) => {
        co(function* () {

            if(!process.env.NODE_TLS_REJECT_UNAUTHORIZED){
                yield cheerio.init();
            }

            let pg_id = util.encodeUrl(page_id);
            let url = `https://www.instagram.com/explore/tags/${pg_id}/`;

            let $ = yield cheerio.get2(url);
            yield util.sleep(ms);
            
            if ($) {
                //console.log($('title').text());
                // bodyから、page_info手前の数値を参照
                let body = $('body').html();
                let data1 = body.match(/(...............)page_info/i);
                //console.log('   getExCnt2 date1: '+date1);
                if (data1) {
                    let data2 = data1[0].replace(/[page_infocount, ":\{]/g, '');
                    //console.log('   getExCnt2 date2: '+date2);
                    resolve(data2);
                }else{
                    resolve(null);
                }
            } else {
                resolve(null);
            }

        }).catch((e) => {
            //console.log(e);
            console.log('    catch getExploreCount');
            resolve(null);
        });
    });
}


// scrape by process Cheerio
const get_insta_posts = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            const maxcnt = 2;
            let rcnt = 0; //kIDXごとのカウンタ
            let kIDX = 0;
            let posts = 0;
            //let keyword = '#' + keywords[kIDX].keyword;
            let next = false;
            let result = false;

            let eol = false;
            do { //while (keywords.length >= kIDX) {
                rcnt++;
                posts = 0;
                next = false;
                result = false;

                posts = yield getExploreCount(keywords[kIDX].keyword, 400);
                if(!posts){
                    if(rcnt > maxcnt){
                        //console.log(`   posts null maxcnt err : ${kIDX}: ${keywords[kIDX].keyword} retry: ${rcnt} max`);
                        posts = '-';
                        next = true;
                    }else{
                        //console.log(`   posts null err : ${kIDX} : ${keywords[kIDX].keyword} retry:${rcnt}`);
                        yield util.sleep(200);
                        continue;
                    }
                }else{
                    //console.log(`  posts: ${posts}`);
                    //if(posts > 0) 
                    result = true;
                    next = true;
                }                    
                
                if(next){
                    //console.log(kIDX + ' : ' +  keywords[kIDX].keyword + ' : ' + posts+ ' retry: ' + rcnt);
                    //log
                    if((kIDX)%500 == 0){
                    //if((kIDX)%500 == 0 || !result){
                        console.log(`  ${kIDX} : ${keywords[kIDX].keyword} : ${result} : ${posts} : maxposts:${keywords[kIDX].maxposts}    ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
                    }

                    if(result){
                        const sql = `INSERT OR REPLACE INTO ${tname} ` +
                            `(keyword, category, getdate, postdate, posts, ngflg) ` +
                            `VALUES (?, ?, ?, ?, ?, ?);`;

                        let ngflg = 0;
                        let criteria = Math.floor(keywords[kIDX].maxposts / 2);
                        if(keywords[kIDX].maxposts >= 50 && posts < criteria) {
                            ngflg = 1;
                            console.log(`     ${kIDX} : ${keywords[kIDX].keyword} : ${posts} < ${criteria}  maxposts:${keywords[kIDX].maxposts}`)
                        
                        }else{
                            getkwd.push(keywords[kIDX].keyword);
                        }

                        let opt = [keywords[kIDX].keyword, '', getdate_slash, postdate, posts, ngflg];
                        yield db.do(`run`, sql, opt);

                    }

                    if(keywords.length -1 > kIDX){
                        kIDX++;
                        rcnt=0;
                        //posts = 0;
                        //result = false;
                        //next = false;
                    }else{
                        eol = true;
                    }
                }

            }while (!eol)
            resolve();
        }).catch((e) => {
        //    console.log(e);
            console.log('    catch get_insta_posts');
            resolve('');
        });
    });
}


// open getlist
const get_initElement = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            const url = 'https://www.instagram.com/explore/tags/instagram00000000/';
            const slctr = 'input'
            let rt = '';
            let result = false;
            const maxcnt = 3;
            let icnt = 0; //カウンタ
            do {                 
                icnt++;
                
                if(!driver) {
                    console.log('  driver get');
                    driver = yield selenium.init('chromium', { inVisible:true });
                    driver.manage().window().setSize(600,600);
                    driver.get(url);
                    yield util.sleep(200);
                }
                
                let ele_input = yield getElements(driver, slctr);

                if( ele_input ) {
                    rt = ele_input[0];
                    result = true;
                }else{
                    if( icnt < maxcnt ){
                        continue;
                    }else if( icnt == maxcnt ){
                        if( driver ){
                            driver.quit();
                            driver = null;
                        }
                        continue;
                    }else{
                        console.log('  get_initElement err');
                        result = true;
                    }
                }
            
            }while (!result)

            resolve(rt);

        }).catch((e) => {
            console.log('    catch get_initElement');
        //    console.log(e);
            resolve('');
        });
    });
}


// scrape by process Selenium
const get_insta_posts2 = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let ele_input;

            let kIDX = 0;
            let keyword = '#' + keywords[kIDX].keyword;

            const maxcnt = 2;
            let rcnt = 0; //kIDXごとのカウンタ
            let posts = 0;
            let next = false;
            let result = false;
            let eol = false;

            rpt: do {                 
                rcnt++;
                posts = 0
                next = false;
                result = false;   

                if(rcnt > maxcnt){
                    //console.log(`  retry: ${rcnt} max`);
                    next = true;
                    //result = false;   
                }else{
                    //if(rcnt > 1)console.log(`  retry: ${rcnt}`);
                    
                    //const url = `https://www.instagram.com/explore/tags/aaaaaaaaaaaaaa/`;
                    if(!ele_input) ele_input = yield get_initElement();//driver, url);
                    if(!ele_input) {
                        //console.log(`  ele_input null ${keyword} retry: ${rcnt}`);
                        continue rpt;
                    }else if(ele_input.length == 0){
                        console.log(`  ele_input length 0 ${keyword} retry: ${rcnt}`);
                        yield util.sleep(1000);
                        continue rpt;
                    }
                    //yield util.sleep(100);

                    ele_input.clear('');
                    //yield util.sleep(100);
                    ele_input.sendKeys(keyword, Key.ENTER);
                    //yield util.sleep(1600);
                    if(rcnt == 1){
                        yield util.sleep(2000);
                    }else if(rcnt < 5){
                        //console.log('    sendKeys sleep 2000',rcnt, keyword);
                        yield util.sleep(2500);
                    }else{
                        //console.log('    sendKeys sleep 3000',rcnt, keyword);
                        yield util.sleep(3000);
                    }

                    let ele_list = yield getElements(driver, '.fuqBx');
                    if(!ele_list) {
                        console.log(`   ele_list err ${keyword}`);
                        continue rpt;
                    }
                    //console.log(`   ele_list.length: ${ele_list.length}`);
                    
                    let ele_words = yield getElements(ele_list[0], '.Ap253');
                    //let ele_words = yield getElements(ele_list[0], '.yCE8d');
                    if(!ele_words) {
                        //console.log(`   ele_words err ${keyword}`);
                        continue rpt;

                    } else if (ele_words.length == 0){
                        let ele_notresult = yield getText(ele_list[0]);
                        if(ele_notresult == 'No results found.'){
                            //console.log(`   No results found. ${keyword}`);
                            posts = 0;
                            result = true;
                            next = true;
                        }else{
                            console.log(`   ele_notresult err ${keyword}`);
                            continue rpt;
                        }

                    }else{
                        //console.log(`   ele_words.length: ${ele_words.length}`);

                        let ele_posts = yield getElements(ele_list[0], '.Fy4o8');
                        if(!ele_posts) {
                            console.log(`   ele_posts err ${keyword} `);
                            continue rpt;
                        }

                        if(ele_posts.length == ele_words.length){
                            let j = 0;
                            do{//(let j = 0; j < ele_posts.length; j++ ){
                                let ele_txt = yield getText(ele_words[j]);                                    
                                if(!ele_txt) {
                                    console.log(`   ele_posts err ${keyword} `);
                                    continue rpt;
                                }
                                //console.log(`    ele_txt[${j}]:[${ele_txt.toLowerCase()}]:[${keyword.toLowerCase()}]`);

                                if (ele_txt.toLowerCase() == keyword) {
                                    let ele_num = yield getText(ele_posts[j]);
                                    //console.log(`     ele_num[${j}]:[${ele_num}]`);
                                    if(!ele_num) {
                                        console.log(`   ${keyword} ele_posts`);
                                        continue rpt;
                                    }                                        

                                    posts = ele_num.replace(/[^0-9]/g, '');
                                    //console.log(`      posts[${j}]:[${posts}]`);
                                    result = true;
                                    next = true;
                                }
                                //if ( !result && j+1 >= ele_words.length ){
                                if ( j+1 >= ele_words.length ){
                                    //console.log(`        kouho gaitounashi`);
                                    //posts = 0;
                                    //result = true;
                                    next = true;
                                }
                                j++;
                            } while (!next)
                        }else{
                            console.log(`   ele_posts.length != ele_words.length err ${keyword} `);
                            continue rpt;
                        }
                    }
                }

                if(next){
                    //if((kIDX)%500 == 0 || !result){
                    if((kIDX)%500 == 0){
                        console.log(`  2: ${kIDX}: ${keywords[kIDX].keyword} : ${result} : ${posts} : maxposts:${keywords[kIDX].maxposts}    ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
                    }

                    if(result){
                        const sql = `INSERT OR REPLACE INTO ${tname} ` +
                            `(keyword, category, getdate, postdate, posts, ngflg) ` +
                            `VALUES (?, ?, ?, ?, ?, ?);`;

                        let ngflg = 0;
                        let criteria = Math.floor(keywords[kIDX].maxposts / 2);
                        if(keywords[kIDX].maxposts >= 50 && posts < criteria) {
                            ngflg = 1;
                            console.log(`     ${kIDX} : ${keywords[kIDX].keyword} : ${posts} < ${criteria}  maxposts:${keywords[kIDX].maxposts}`)
                        
                        }else{
                            getkwd.push(keywords[kIDX].keyword);
                        }

                        let opt = [keywords[kIDX].keyword, '', getdate_slash, postdate, posts, ngflg];
                        yield db.do(`run`, sql, opt);

                    }

                    if(keywords.length -1 > kIDX){
                        //ele_input = null;

                        kIDX++;
                        keyword = '#' + keywords[kIDX].keyword;
                        rcnt=0;
                        // posts = 0;
                        // next = false;
                        // result = false;

                    }else{
                        eol = true;
                    }
                }

            }while (!eol)

            if( driver ){
                driver.quit();
                driver = null;
            }
            
            resolve();
        }).catch((e) => {
        //    console.log(e);
            console.log('    catch get_insta_posts2');
            resolve(false);
        });
    });
}

// scrape instagram search window
const scrape_main = function () {

    return new Promise((resolve, reject) => {
        co(function* () {
            
            let result = false;
            let lflg = true; // loop continue?

            if (mode == 'new') {
                yield init_table(tname,`FGS239_targets_${category}`, maxtable);
                let st = yield store_targets();
                
                if(!st){
                    console.log(`   store targets err`);
                    lflg = false;
                }

            }else{
                yield get_loop_count();
            }
            
            while(lflg){

                if (lcnt > maxloop) {
                    console.log(`   end  maxloop`);
                    lflg = false;
                    //result = true;

                }else{
                    if(zcnt < 1){
                        yield get_targets();
                    }else{
                        yield get_targets2();
                    }
                    
                    if ( minnum >= keywords.length ) {
                        console.log(`   end  min keywords`);
                        lflg = false;
                        result = true;
                    }

                }

                // loop continue?
                if ( lflg ){

                    getkwd.length = 0;
                    if( ( lcnt % 3 ) == 0 && keywords.length < 1000 && prcstype == '1' ){
                        prcstype = '2';
                        yield insertLog();
                        yield get_insta_posts2();
                    } else {
                        prcstype = '1';
                        yield insertLog();
                        yield get_insta_posts();                            
                    }
                    yield updateLog();
                    lcnt++;

                    if (getkwd.length == 0){
                        zcnt++;
                        console.log(` get keywords: zero count: ${zcnt}`);
                        if(zcnt >= 3) {
                            lflg = false;
                            result = true;
                        }
                    }else{
                        console.log(` get keywords: ${getkwd.length} / ${keywords.length}`);
                        zcnt = 0;
                    }

                    if(keywords.length == getkwd.length) {
                        console.log(`   end scraping`);
                        lflg = false;
                        result = true;
                    }
                }
            }

            if(result) yield setOutFile();
            
            resolve();

        }).catch((e) => {
        //     console.log(e);
            console.log('    catch scrape_main');
            resolve('');
        });
    });
}

// set out file to share folder
const setOutFile = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let sql = `select keyword, lower(category) category, getdate, postdate, max(posts) posts
             from ${tname} where getdate = '${getdate_slash}' and (ngflg = 0 or posts > 0) 
             group by keyword, lower(category), getdate, postdate order by keyword;`;
            //console.log(sql);
            let res = yield db.do(`all`, sql, {});            

            for (let i = 0; i < res.length; i++) {
                fs.appendFileSync(outfile, ([res[i].keyword, res[i].category, res[i].getdate, res[i].postdate, res[i].posts].join('\t') + '\n'), 'utf8');
            }

            yield outNGfile();
            yield outLog();
            yield end();

            resolve();
        // }).catch((e) => {
        //     console.log(e);
        });
    });
}

// run action
const run = function () {

    co(function* () {
    
        let st = moment().format('YYYY/MM/DD HH:mm:ss');
        yield db.connect(filePath_db);

        switch (mode) {

            case 'new':
            case 'continue':
                yield scrape_main();
                break;

            case 'output':                
                yield setOutFile();
                break;

            default:
                break;
        }
        console.log(`/// FGS239-insta-posts scraper.js end /// ${st} - ${moment().format('YYYY/MM/DD HH:mm:ss')}`);

    });
}

run();
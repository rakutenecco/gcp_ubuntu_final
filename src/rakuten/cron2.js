'use strict';

const shell = require('shelljs');
const cronJob = require('cron').CronJob;
const moment = require('moment');
const fs = require('fs');
const date = moment().format('YYYYMMDD');
const path_log = `../log/${date}.txt`;
let now = '';

/*
cronJob(`00 00 00 * * 1-5`)
Seconds: 0-59
Minutes: 0-59
Hours: 0-23
Day of Month: 1-31
Months: 0-11
Day of Week: 0-6
*/

// recipe scrape weekly
new cronJob(`00 03 00 * * 0`, function () {
//new cronJob(`00 01 09 * * 0-4`, function () {

    let st = moment().format('YYYY/MM/DD HH:mm:ss');
    
    console.log(`cron2 recipe scrape <start>  ${st}`);

    shell.exec(`node src/rakuten/FGS283-recipe/shell.js -r 1 -s 2 -n cron14_02 -f targets_food.txt`); //rakuten
    shell.exec(`node src/rakuten/FGS283-recipe/shell.js -r 1 -s 1 -n cron14_01 -f targets_food.txt`); //cookpad

    console.log(`cron2 recipe scrape < end >  ${st} - ${moment().format('YYYY/MM/DD HH:mm:ss')}`);

    }, function () {
        // error
        now = moment().format('YYYYMMDD HH:mm:ss');
        fs.appendFileSync(path_log, `Error ${now} : stop citizen, articles scrape cron2 \n`, 'utf8');
    },
    true, 'Asia/Tokyo');


// citizen, articles scrape weekly
//new cronJob(`00 19 16 * * 4`, function () {
new cronJob(`00 53 11 * * 4`, function () {

    let st = moment().format('YYYY/MM/DD HH:mm:ss');
    const startday = moment().add(-7, 'days').format('YYYYMMDD');

    
    console.log(`cron2 articles scrape <start> startday[${startday}]   ${st}`);

    console.log(`  FGS200-fashion-press  ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
        shell.exec(`node src/rakuten/FGS200-fashion-press/shell.js -s ${startday}`);
    console.log(`  FGS201-fashion-headline  ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
        shell.exec(`node src/rakuten/FGS201-fashion-headline/shell.js -s ${startday}`);
    //console.log(`  FGS203-gqjapan  ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
        //shell.exec(`node src/rakuten/FGS203-gqjapan/scraper.js -s ${startday} -n cron2`);
    console.log(`  FGS214-cookpad  ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
        shell.exec(`node src/rakuten/FGS214-cookpad/shell.js -s ${startday}`);
    console.log(`  FGS251-vogue  ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
        shell.exec(`node src/rakuten/FGS251-vogue/scraper.js -s ${startday}`);
    console.log(`  FGS252-FashionNetwork  ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
        shell.exec(`node src/rakuten/FGS252-FashionNetwork/shell.js -s ${startday}`);
    console.log(`  FGS254-senken  ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
        shell.exec(`node src/rakuten/FGS254-senken/scraper.js -s ${startday}`);
        
    console.log(`  FGS166-amazon-brands  ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
        shell.exec('node src/rakuten/amazon-brands-FGS166/shell.js -r 2');
        shell.exec('node src/rakuten/amazon-brands-FGS166/shell.js -r 1');

    console.log(`  FGS196-citizen  ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
        shell.exec('node src/rakuten/FGS196-citizen/scraper.js -B phantomjs');

    console.log(`  FGS284-kakakuTV  ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
        shell.exec(`node src/rakuten/FGS284-kakakuTV/scraper.js -s ${startday}`);
    
    console.log(`  FGS284-kakakuTV More category  ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
        shell.exec(`node src/rakuten/FGS284-kakakuTV/scraper1.js -s ${startday}`);

    
    console.log(`  FGS284-kakakuTV Category None  ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
        shell.exec(`node src/rakuten/FGS284-kakakuTV/scraper_none.js -s ${startday} -e ${st}`);

    console.log(`cron2 articles scrape < end > startday[${startday}]   ${st} - ${moment().format('YYYY/MM/DD HH:mm:ss')}`);

    }, function () {
        // error
        now = moment().format('YYYYMMDD HH:mm:ss');
        fs.appendFileSync(path_log, `Error ${now} : stop citizen, articles scrape cron2 \n`, 'utf8');
    },
    true, 'Asia/Tokyo');

console.log('cron2 setting');
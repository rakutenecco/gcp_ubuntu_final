'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');
const moment = require('moment');
const shell = require('shelljs');
const _ = require('underscore');

const conf = new(require('../../conf.js'));
const db = new(require('../../modules/db.js'));
const selenium = new(require('../../modules/selenium.js'));
const cheerio = new(require('../../modules/cheerio-httpcli.js'));
const util = new(require('../../modules/util.js'));
const instagram = new(require('../../modules/instagram.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
    { name: 'browser', alias: 'B', type: String },
    { name: 'post_target_day', alias: 't', type: String }, // instagram post day (ex. 2018-05-01)
    { name: 'update_target_day', alias: 'T', type: String }, // update day (ex.2018-05-19)
    { name: 'category', alias: 'c', type: String },
    { name: 'hash', alias: 'h', type: Boolean }, // scrape tag_hash as web page
]);

let driver = null;
let driver2 = null;
let task_name = 'fgs45';
let today = moment().format('YYYYMMDD');
let today_haifun = moment().add(0, 'days').format('YYYY-MM-DD');
let yesterday = moment().add(-1, 'days').format('YYYYMMDD');
let yesterday_haifun = moment().add(-1, 'days').format('YYYY-MM-DD');
let yesterday_slash = moment().add(-1, 'days').format('YYYY/MM/DD');
let update_target_day = cli['update_target_day'] ? moment(cli['update_target_day']).format('YYYY-MM-DD') : today_haifun;
let post_target_day = cli['post_target_day'] ? moment(cli['post_target_day']).format('YYYY-MM-DD') : yesterday_haifun;
let category = cli['category'] ? cli['category'] : 'basic';
let browser = cli['browser'] || 'chromium';
let checkHash = cli['hash'] || false;
const cron = 'cron1';
const filePath_db = `${conf.dirPath_db}/cron1.db`;
// const cron = 'cron0';
// const filePath_db = `${conf.dirPath_db}/cron0.db`;
console.log('post_target_day', post_target_day, ', update_target_day', update_target_day);

// init
const init = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield instagram.init(task_name, update_target_day, post_target_day, category, cron, browser, checkHash, true, filePath_db);
            resolve();
        });
    });
}

// end
const end = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            resolve();
        });
    });
}

// set out fastload data
const outFastload = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield db.connect(filePath_db);

            // set filepath
            let filepath_fastload = `${conf.dirPath_share_vb}\/${task_name}-instagram.txt`;
            let filepath_fastload2 = `${conf.dirPath_share_vb}\/${task_name}-instagram-${today}.txt`;

            // header check
            fs.writeFileSync(filepath_fastload, '', 'utf-8');
            fs.appendFileSync(filepath_fastload, ['id2', 'id', 'url', 'keyword', 'tag1', 'tag2', 'detailurl', 'linkurl', 'reg_date'].join('\t') + '\n', 'utf-8');
            fs.appendFileSync(filepath_fastload, ['id2', 'id', 'url', 'keyword', 'tag1', 'tag2', 'detailurl', 'linkurl', 'reg_date'].join('\t') + '\n', 'utf-8');
            fs.appendFileSync(filepath_fastload, ['VARCHAR(20)', 'VARCHAR(20)', 'VARCHAR(2000)', 'VARCHAR(100)', 'VARCHAR(100)', 'VARCHAR(100)', 'VARCHAR(2000)', 'VARCHAR(2000)', 'VARCHAR(20)'].join('\t') + '\n', 'utf-8');

            // get page data
            let sql = `SELECT a.page_id as page_id, a.cat as cat, a.tag_hash as tag_hash, a.tag_at as tag_at, a.url as url, a.post_date as post_date, c.web_site as web_site FROM fashion_${task_name}_library_instagram_targets_result a left join fashion_${task_name}_library_instagram_targets_page c on a.tag_key = c.page_id WHERE NOT EXISTS(SELECT * FROM fashion_${task_name}_library_instagram_targets_match b WHERE a.url = b.match and cat = '${category}' and del = 0 and up_date <> '${yesterday_haifun}') and tag_none = '';`;
            // let sql = `SELECT page_id, cat, tag_hash, tag_at, url FROM fashion_${task_name}_library_instagram_targets_result a WHERE NOT EXISTS(SELECT * FROM fashion_${task_name}_library_instagram_targets_match b WHERE a.url = b.match and cat = '${category}' and del = 0 and up_date <> '${yesterday_haifun}') and tag_none = '';`;
            let targets = yield db.do(`all`, sql, {});

            // each target pages
            for (let i = 0; i < targets.length; i++) {
                let id2 = `${yesterday}_${i}`;
                let id_pre = targets[i].url.split('/');
                let id = id_pre[2];
                let url = `https://www.instagram.com${targets[i].url}`;
                let tag1 = targets[i].tag_hash;
                let tag2 = targets[i].tag_at;
                let keyword = targets[i].tag_hash ? targets[i].tag_hash.substr(1) : (targets[i].tag_at ? targets[i].tag_at.substr(1) : '');
                let detailurl = targets[i].tag_at ? `https://www.instagram.com/${keyword}/` : '';
                let linkurl = targets[i].web_site ? targets[i].web_site : '';
                let reg_date = targets[i].post_date;

                // append data
                fs.appendFileSync(filepath_fastload, [id2, id, url, keyword, tag1, tag2, detailurl, linkurl, reg_date].join('\t') + '\n', 'utf-8');

                // set data
                const sql1 = `INSERT OR REPLACE INTO fashion_${task_name}_library_instagram_targets_match(match, page_id, page_id_ref, cat, up_date, del) VALUES (?, ?, ?, ?, ?, ?);`;
                let opt1 = [targets[i].url, targets[i].page_id, '', targets[i].cat, yesterday_haifun, 0]; // this data start 0:20 for yesterday
                yield db.do(`run`, sql1, opt1);
            }

            // copy data
            let data = fs.readFileSync(filepath_fastload, 'utf-8');
            fs.writeFileSync(filepath_fastload2, data, 'utf-8');

            yield db.close();
            resolve();
        });
    });
}

// run action
const run = function () {

    co(function* () {

        switch (cli['run']) {

            // node src/rakuten/FGS45-instagram-MF/scraper.js -r 99
            case 99: // reset list for target instagram web page
                yield init();
                let tables = { targets: true, targets_result: true, targets_page: true, targets_match: true };
                let targets = [
                    { page_id: 'mensfashionpost', cat: category }
                ];
                yield instagram.resetDB(tables, targets);
                yield end();
                break;

                // node src/rakuten/FGS45-instagram-MF/scraper.js -r 2 -t 2018-04-01 -T 2018-05-20 -h
                // -B chrome, -B phantom, -B firefox
                // -t post_target_day, -T update_target_day, -h update at_mark and hash as page_id
            case 12: // set list for target instagram web page
                for (let i = 0; i < 10; i++) {
                    shell.exec(`node src/rakuten/FGS45-instagram-MF/scraper.js -r 2 -h -T ${update_target_day}`);
                    // shell.exec('pkill chromium');
                }
                break;

                // node src/rakuten/FGS45-instagram-MF/scraper.js -r 3
            case 3: // set out fastload data
                yield init();
                yield outFastload();
                yield end();
                break;

                // node src/rakuten/FGS45-instagram-MF/scraper.js -r 2 -t 2018-04-01 -T 2018-05-20 -h
                // -B chrome, -B phantom, -B firefox
                // -t post_target_day, -T update_target_day, -h update at_mark and hash as page_id
            case 2: // set list for target instagram web page
                yield init();
                yield instagram.scrapePage();
                yield end();
                break;

                // node src/rakuten/FGS45-instagram-MF/scraper.js -r 1
                // -B chrome, -B phantom, -B firefox
            case 1: // get post from instagram targets
            default:
                yield init();
                let post_date_min = moment().add(-10, 'days').format('YYYY-MM-DD');
                // let post_date_min = moment().add(-30, 'months').format('YYYY-MM-DD');
                yield instagram.scrapePosts(post_date_min);
                yield end();
                break;
        }
    });
}

run();
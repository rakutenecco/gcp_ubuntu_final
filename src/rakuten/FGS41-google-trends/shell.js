'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const shell = require('shelljs');
const moment = require('moment');

const util = new(require('../../modules/util.js'));

let now = '';

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
]);

// run action
const run = function () {

    co(function* () {

        // info1
        now = moment().format('YYYYMMDD HH:mm:ss');
        console.log('google trend shell start ',now)

        console.log(`-- use command -- usual task ----------`);
        console.log(`npm run server`);
        console.log(`npm run local`);
        console.log(`npm run gtw`);
        console.log(`---------------------------------------
        `);

        // info2
        console.log(`-- use command -- reflesh data --------`);
        console.log(`npm run gtw-d`);
        console.log(`---------------------------------------
        `);

        // 
        switch (cli['run']) {
            case 100: // total-data git
                shell.exec(`cd total-data && git add .`);
                shell.exec(`cd total-data && git add -u .`);
                shell.exec(`cd total-data && git commit -m 'up fgs41'`);
                shell.exec(`cd total-data && git pull origin master`);
                shell.exec(`cd total-data && git push origin master`);
                break;
            case 99: // reset table
                shell.exec('node src/rakuten/FGS41-google-trends/scraper.js -c watch -r 2 -B chromium -t week -o tor');
                break;
            case 41: // skirt, month
                shell.exec('node src/rakuten/FGS41-google-trends/scraper.js -r 100');
                shell.exec('node src/rakuten/FGS41-google-trends/scraper.js -c skirt -r 1 -B chromium -t month'); // scrape
                shell.exec('node src/rakuten/FGS41-google-trends/scraper.js -c skirt -r 5 -B chromium -t month'); // outfile
                shell.exec('node src/rakuten/FGS41-google-trends/shell.js -r 100');
                break;
            case 40: // skirt, month
                shell.exec('node src/rakuten/FGS41-google-trends/scraper.js -r 100');
                shell.exec('node src/rakuten/FGS41-google-trends/scraper.js -c skirt -r 1 -B chromium -t month -o tor'); // scrape
                shell.exec('node src/rakuten/FGS41-google-trends/scraper.js -c skirt -r 5 -B chromium -t month -o tor'); // outfile
                shell.exec('node src/rakuten/FGS41-google-trends/shell.js -r 100');
                break;
            case 4: // skirt, month
                shell.exec('node src/rakuten/FGS41-google-trends/scraper.js -r 100');
                for (let i = 0; i < 30; i++) {
                    shell.exec('node src/rakuten/FGS41-google-trends/scraper.js -c skirt -r 1 -B chromium -t month -o tor'); // scrape
                }
                shell.exec('node src/rakuten/FGS41-google-trends/scraper.js -c skirt -r 5 -B chromium -t month -o tor'); // outfile
                shell.exec('node src/rakuten/FGS41-google-trends/shell.js -r 100');
                break;
            case 30: // skirt, week
                shell.exec('node src/rakuten/FGS41-google-trends/scraper.js -r 100');
                shell.exec('node src/rakuten/FGS41-google-trends/scraper.js -c skirt -r 1 -B chromium -t week'); // scrape  -o tor
                shell.exec('node src/rakuten/FGS41-google-trends/scraper.js -c skirt -r 5 -B chromium -t week'); // outfile  -o tor
                shell.exec('node src/rakuten/FGS41-google-trends/shell.js -r 100');
                break;
            case 3: // skirt, week
                shell.exec('node src/rakuten/FGS41-google-trends/scraper.js -r 100');
                for (let i = 0; i < 30; i++) {
                    shell.exec('node src/rakuten/FGS41-google-trends/scraper.js -c skirt -r 1 -B chromium -t week -o tor'); // scrape
                }
                shell.exec('node src/rakuten/FGS41-google-trends/scraper.js -c skirt -r 5 -B chromium -t week -o tor'); // outfile
                shell.exec('node src/rakuten/FGS41-google-trends/shell.js -r 100');
                break;
            case 20: // watch, month
                shell.exec('node src/rakuten/FGS41-google-trends/scraper.js -r 100');
                shell.exec('node src/rakuten/FGS41-google-trends/scraper.js -c watch -r 1 -B chromium -t month -o tor'); // scrape
                shell.exec('node src/rakuten/FGS41-google-trends/scraper.js -c watch -r 5 -B chromium -t month -o tor'); // outfile
                shell.exec('node src/rakuten/FGS41-google-trends/shell.js -r 100');
                break;
            case 2: // watch, month
                shell.exec('node src/rakuten/FGS41-google-trends/scraper.js -r 100');
                for (let i = 0; i < 30; i++) {
                    shell.exec('node src/rakuten/FGS41-google-trends/scraper.js -c watch -r 1 -B chromium -t month -o tor'); // scrape
                }
                shell.exec('node src/rakuten/FGS41-google-trends/scraper.js -c watch -r 5 -B chromium -t month -o tor'); // outfile
                shell.exec('node src/rakuten/FGS41-google-trends/shell.js -r 100');
                break;
            case 10: // watch, week
                shell.exec('node src/rakuten/FGS41-google-trends/scraper.js -r 100');
                shell.exec('node src/rakuten/FGS41-google-trends/scraper.js -c watch -r 1 -B chromium -t week -o tor'); // scrape
                shell.exec('node src/rakuten/FGS41-google-trends/scraper.js -c watch -r 5 -B chromium -t week -o tor'); // outfile
                shell.exec('node src/rakuten/FGS41-google-trends/shell.js -r 100');
                break;
            case 1: // watch, week
            default:
                shell.exec('node src/rakuten/FGS41-google-trends/scraper.js -r 100');
                for (let i = 0; i < 30; i++) {
                    shell.exec('node src/rakuten/FGS41-google-trends/scraper.js -c watch -r 1 -B chromium -t week -o tor'); // scrape
                }
                shell.exec('node src/rakuten/FGS41-google-trends/scraper.js -c watch -r 5 -B chromium -t week -o tor'); // outfile
                shell.exec('node src/rakuten/FGS41-google-trends/shell.js -r 100');
                break;
        }

        now = moment().format('YYYYMMDD HH:mm:ss');
        console.log('google trend shell end ',now)

    });
}

run();
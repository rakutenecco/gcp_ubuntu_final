'use strict';

const shell = require('shelljs');
const cronJob = require('cron').CronJob;
const moment = require('moment');
const fs = require('fs');
const date = moment().format('YYYYMMDD');
const today = moment().format('YYYY-MM-DD hh:mm:ss');
const path_log = `../log/${date}.txt`;
let now = '';
const conf = new (require('/var/app/total/src/conf.js'));

/*
cronJob(`00 00 00 * * 1-5`)
Seconds: 0-59
Minutes: 0-59
Hours: 0-23
Day of Month: 1-31
Months: 0-11
Day of Week: 0-6
*/


//// 02 ////
// FGS258 twitter posts weekly
new cronJob(`00 06 00 * * 0`, function () {
    
    let startdate = moment().day(0).add(-14, 'd').format('YYYYMMDD');
    let dd = startdate.slice(6);
    let tmp = Math.floor( (Number(dd) - 1) / 7) + 1;
    console.log(`startdate:${startdate} dd:${dd} week:${tmp} cron11_0${tmp}`);

    shell.exec(`node src/rakuten/FGS258-twitter/shell.js -r 1 -n cron11_0${tmp} -f targets_all_05_02.txt -s ${startdate}`);
    shell.exec(`node src/rakuten/FGS258-twitter/shell.js -r 1 -n cron11_0${tmp} -f targets_all_05_03.txt -s ${startdate}`);
    shell.exec(`node src/rakuten/FGS258-twitter/shell.js -r 1 -n cron11_0${tmp} -f targets_all_05_04.txt -s ${startdate}`);
    shell.exec(`node src/rakuten/FGS258-twitter/shell.js -r 1 -n cron11_0${tmp} -f targets_all_05_05.txt -s ${startdate}`);
    
}, function () {
    // error
    console.log(`Error ${today} : FGS258 twitter posts scrape cron11_04`);
},
true, 'Asia/Tokyo');


console.log('cron11_04 setting');

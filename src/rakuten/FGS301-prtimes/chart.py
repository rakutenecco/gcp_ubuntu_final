import sys
import math
import os
import glob
import numpy as np
import pandas as pd
import img2pdf
import PyPDF2
import matplotlib as mpl
import matplotlib.dates as mdates
import statsmodels.api as sm
import psutil
import gc
from datetime import datetime, timedelta
from time import sleep
from matplotlib import pyplot as plt
from matplotlib.font_manager import FontProperties
from matplotlib.backends.backend_pdf import PdfPages
from pathlib import Path
from PIL import Image

font_path = '/usr/share/fonts/truetype/fonts-japanese-gothic.ttf'
font_prop = FontProperties(fname=font_path)
mpl.rcParams['font.family'] = font_prop.get_name()

# today
today = datetime.strptime(datetime.strftime(datetime.now(), "%Y%m%d"), "%Y%m%d")

# command line argument
args = sys.argv
arg_date = datetime.strptime(args[1], "%Y%m%d")
bef_day = str((arg_date - today).days)

# 日付配列作成
exec_date_hyphen = (datetime.now() + timedelta(days=int(bef_day))).date()
exec_date = datetime.strftime(exec_date_hyphen, "%Y%m%d")
#exec_year = exec_date[0:4]
#exec_month = exec_date[0:6]

#画像保存先ディレクトリ作成
output_dir = "src/rakuten/FGS301-prtimes/pdf_out_{0}/".format(exec_date)
os.makedirs(output_dir, exist_ok=True)

input_dir = "src/rakuten/FGS301-prtimes/download_data/"
os.makedirs(input_dir, exist_ok=True)

# 実行年の日付リスト作成
#day_list = []
#first_date = datetime(2019, 1, 1, 0, 0)

#date_hyphen = first_date
#while date_hyphen <= datetime.combine(exec_date_hyphen, datetime.min.time()):
#    day_list.append(datetime.strftime(date_hyphen, "%Y%m%d")[0:8])
#    date_hyphen += timedelta(days=1)

#data_list = [["20190130","〆に",2],["20200220","〆に",2],["20190129","〆は雑炊",3],
#          ["20200209","〆パフェ",50],["20200205","〆パフェ",80],["20200213","〆パフェ",100]]

input_filename = input_dir + "fgs301-prtimes_download.txt"

data_list = []

f = open(input_filename, "r")
text = f.readlines()[2:]
for elem in text:
    # 実行日までのデータをリストにアペンドする処理を追加したい
    if(elem[0:8] <= exec_date):
        data_list.append(elem.split(","))

day_list = []

strdt = datetime.strptime("20190101", '%Y%m%d')  # 開始日
enddt = datetime.strptime(exec_date, '%Y%m%d')  # 終了日

# 日付差の日数を算出（リストに最終日も含めたいので、＋１しています）
days_num = (enddt - strdt).days + 1

datelist = []
for i in range(days_num):
    datelist.append(strdt + timedelta(days=i))

def make_date_list(day_list):

    for d in datelist:
        day_list.append([d.strftime("%Y%m%d"),"",0])
        
    return day_list

def make_list(source_list):

    day_list=[]
    count_list = []
    
    make_date_list(day_list)
    
    for j in range(len(source_list)):
        for i in range(len(day_list)):
            if(day_list[i][0] == source_list[j][0]):
                day_list[i][1] = source_list[j][1]
                day_list[i][2] = source_list[j][2]
    #print(day_list)

    for k in range(len(day_list)):
        count_list.append(day_list[k][2])
    #print(count_list)

    date_index = pd.date_range('2019-01-01', freq='D', periods=(len(day_list)))
    fig, ax = plt.subplots()
    fig = plt.figure(figsize=(5.8, 4.2))
    ax = fig.add_subplot(111)

    count = pd.Series(count_list)
    avg_list = count.rolling(window=28).mean()
    avg_list = ["0" if x is None else x for x in avg_list]

    #res = sm.tsa.seasonal_decompose(count, freq=133)
    res = sm.tsa.seasonal_decompose(count, period=133)
    trend = res.trend
    trend = ["0" if x is None else x for x in trend]

    df = pd.DataFrame(avg_list, date_index)
    dt = pd.DataFrame(trend, date_index)

    avg_list = [x for x in avg_list if str(x) != "nan"]

    if(avg_list[-365] != 0):
        #YoY = str(round((avg_list[-1]/avg_list[-365] * 100) - 100, 2)) + "％"
        YoY = round((avg_list[-1]/avg_list[-365] * 100) - 100, 2)
    else:
        YoY = 0
    
    x = np.array(list(range(len(avg_list))))
    y = np.array(avg_list)

    #回帰線を求める
    x_reg = x - x.mean()
    y_reg = y - y.mean()

    #傾きを求める
    denom = x_reg * x_reg
    numer = x_reg * y_reg

    answer = round(numer.sum()/denom.sum(),5)

    #直近84日(3month)
    avg_list_84 = avg_list[-84:]
    x_84 = np.array(list(range(len(avg_list_84))))
    y_84 = np.array(avg_list_84)

    #回帰線を求める
    x_reg_84 = x_84 - x_84.mean()
    y_reg_84 = y_84 - y_84.mean()

    #傾きを求める
    denom_84 = x_reg_84 * x_reg_84
    numer_84 = x_reg_84 * y_reg_84

    answer_84 = round(numer_84.sum()/denom_84.sum(),5)

    #直近28日
    avg_list_28 = avg_list[-28:]
    x_28 = np.array(list(range(len(avg_list_28))))
    y_28 = np.array(avg_list_28)

    #回帰線を求める
    x_reg_28 = x_28 - x_28.mean()
    y_reg_28 = y_28 - y_28.mean()

    #傾きを求める
    denom_28 = x_reg_28 * x_reg_28
    numer_28 = x_reg_28 * y_reg_28

    answer_28 = round(numer_28.sum()/denom_28.sum(),3)

    #直近7日
    avg_list_7 = avg_list[-7:]
    x_7 = np.array(list(range(len(avg_list_7))))
    y_7 = np.array(avg_list_7)

    #回帰線を求める
    x_reg_7 = x_7 - x_7.mean()
    y_reg_7 = y_7 - y_7.mean()

    #傾きを求める
    denom_7 = x_reg_7 * x_reg_7
    numer_7 = x_reg_7 * y_reg_7

    answer_7 = round(numer_7.sum()/denom_7.sum(),3)

    print(keyword + ":" + str(answer) + ":" + str(answer_28) + ":" + str(answer_7))

    #if (answer > 0 or answer < 0):
    #if ((answer > 0.001 and answer_28 > 0) or (answer > 0.001 and answer_7 > 0)):
    #if ((answer_28 > 0 or answer_7 > 0 or YoY > 0) and max(avg_list) >= 1):
    #if ((answer > 0.001 and answer_28 > 0.1 and answer_7 > 0.1) and max(avg_list) >= 1):
    if ((answer > 0.05 or (answer > 0.001 and answer_28 > 0.01) or (answer > 0.001 and answer_7 > 0.05)) and max(avg_list) >= 1):

        if(YoY==0):
            YoY = "－"
        else:
            YoY = str(YoY) + "％"

        #df.plot(ax = ax, x_compat=True, legend=False)
        df.plot(ax = ax, x_compat=True, color="turquoise", alpha=0.7, legend=False)
        dt.plot(ax = ax, x_compat=True, color="tomato", alpha=0.7, legend=False)
        ax.xaxis.set_minor_locator(mdates.MonthLocator())
        plt.setp(ax.get_xticklabels(), rotation=60, ha="center")
        plt.title(keyword, fontsize = 13)
        plt.ylabel("件数", fontsize = 10)
        #plt.text(0, 1.03, "YoY : " + str(YoY) + "％", size = 12, color = "black", transform=ax.transAxes)
        plt.text(0, 1.03, "YoY : " + YoY, size = 12, color = "black", transform=ax.transAxes)
        plt.ylim(0)
        #plt.show()

        filename = output_dir + keyword + "_" + exec_date + ".png"
        plt.savefig(filename)
        plt.close('all')

        del filename
        gc.collect()
    
    del ax, fig, df, dt, answer, answer_28, answer_7
    gc.collect()

    #print("プロット処理終了")
    #mem = psutil.virtual_memory() 
    #print(mem.percent)
    #sleep(3)

keyword=""
source_list=[]
count=0

for i in range(len(data_list)):
    count+=1

    if(count > 1000):
        count=0
        #print("スリープ処理です")
        mem = psutil.virtual_memory() 
        #print(mem.percent)
        #sleep(10)

    if(keyword==""):
        keyword=data_list[i][1]
        
    if(keyword==data_list[i][1]):
        source_list.append([data_list[i][0],data_list[i][1],data_list[i][2].replace("\n", "")])
        keyword=data_list[i][1]
    else:
        make_list(source_list)
        del source_list
        gc.collect()
        source_list=[]
        source_list.append([data_list[i][0],data_list[i][1],data_list[i][2].replace("\n", "")])
        keyword=data_list[i][1]
make_list(source_list)
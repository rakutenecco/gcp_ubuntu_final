# -*- coding: utf-8 -*-

import requests
import json
import os
import sys
import re
import emoji
from bs4 import BeautifulSoup
from datetime import datetime, timedelta
from time import sleep
from retrying import retry, RetryError

# limit of articles amount to get at once
article_limit = 50

# define today
today = datetime.strptime(datetime.strftime(datetime.now(), "%Y%m%d"), "%Y%m%d")

# header for request
headers = {"User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0"}

# command line argument
args = sys.argv
category = args[1]
arg_day = datetime.strptime(args[2], "%Y%m%d")
bef_day = str((arg_day - today).days) 

# define exect date
exec_date_hyphen = (datetime.now() + timedelta(days=int(bef_day))).date()
exec_date = datetime.strftime(exec_date_hyphen, "%Y%m%d")
print("■{0}の記事取得処理■".format(exec_date))

# grant work folder access rights
dir = "total-data/FGS301-prtimes"
os.chmod(dir, 0o755)

# create dir for output fastload result
create_dir = "{0}/{1}".format(dir, exec_date)
if os.path.isdir(create_dir)==False:
    os.mkdir(create_dir)

# remove emoji
def remove_emoji(article_str):
    return ''.join(c for c in article_str if c not in emoji.UNICODE_EMOJI)

# get article list with json
@retry(stop_max_attempt_number=1000, wait_fixed=30000)
def get_article(url):
    try:
        r = requests.get(url, headers=headers, timeout=60)
        text = (r.text)
        text = text.replace("addReleaseList(", "")[:-1]
        all_data_list = json.loads(text)
        return all_data_list
    except Exception as e:
        print(e)
        print("記事取得処理が異常終了しました。")
        sleep(10)

# get article header & article sub title
@retry(stop_max_attempt_number=1000, wait_fixed=30000)
def get_article_info(article_url):
    try:
        r = requests.get(article_url, headers=headers, timeout=60)
        bs = BeautifulSoup(r.text, "html.parser")

        article_header = bs.find(class_="r-head")
        if article_header is not None:
            article_header = article_header.text
        else:
            article_header = ""
        
        article_sub_title = bs.find(class_="release--sub_title")
        if article_sub_title is not None:
            article_sub_title = article_sub_title.text
        else:
            article_sub_title = ""

    except Exception as e:
        print(e)
        print("記事サブタイトル、見出し取得処理が異常終了しました。")
        sleep(10)

    article_header_lines = [line.strip() for line in article_header.splitlines()]
    article_header = "".join(line for line in article_header_lines if line)

    article_sub_title_lines = [line.strip() for line in article_sub_title.splitlines()]
    article_sub_title = "".join(line for line in article_sub_title_lines if line)

    return article_header, article_sub_title

now = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S")
print("******************* {0} ******************************".format(category))
print("{0} {1} start".format(now, category))

# counter of article API's page number
#page_num = 7330
page_num = 0

# variable of article release_date
release_date_hyphen = exec_date_hyphen

# output file of fastload result 
out_file = "{0}/fgs301-prtimes-{1}.txt".format(create_dir, category)

with open(out_file , 'w') as f:
    f.write("article_id" + "\t" + "company_id" + "\t" + "category" + "\t" + "release_date" + "\t" + "company_name" + "\t" + "article_title" + "\t" + "article_body" + "\t" + "article_url" + "\n")
    f.write("article_id" + "\t" + "company_id" + "\t" + "category" + "\t" + "release_date" + "\t" + "company_name" + "\t" + "article_title" + "\t" + "article_body" + "\t" + "article_url" + "\n")
    f.write("INTEGER" + "\t" + "INTEGER" + "\t" + "VARCHAR(50)" + "\t" + "VARCHAR(50)" + "\t" + "VARCHAR(200)"  + "\t" + "VARCHAR(1000)" + "\t" + "VARCHAR(30000)" + "\t" + "VARCHAR(100)" + "\n")

# list of articles data
article_list = []

# get articles that release date from latest to processing date
while release_date_hyphen >= exec_date_hyphen:
    
    # count up of article API's page number
    page_num+=1

    # URL
    url = "https://prtimes.jp/api/search_release.php?callback=addReleaseList&type=main_category&page={0}&v={1}&limit={2}".format(page_num, category, article_limit)

    # get article
    try:
        sleep(3)
        data = get_article(url)
    except Exception as e:
        print(e)
    
    for i in range(article_limit):

        # release date
        try:
            release_date_time = data["articles"][i]["updated_at"]["origin"][:-6].replace("T", " ")
        except Exception as e:
            print(e)
            print(data["articles"][i]["updated_at"]["origin"])
        
        release_date_hyphen = datetime.strptime(release_date_time, "%Y-%m-%d %H:%M:%S").date()
        
        # if release date and processing date are different, skip the process
        if release_date_hyphen != exec_date_hyphen:
            continue

        # article_id
        article_id = data["articles"][i]["id"]

        # company id
        company_id = data["articles"][i]["provider"]["id"]

        # company name
        company_name = data["articles"][i]["provider"]["name"]

        # article title
        article_title = data["articles"][i]["title"]

        # article text
        article_text = data["articles"][i]["text"]
        article_text = BeautifulSoup(article_text,"html.parser")
        article_text = article_text.text
        text_lines = [line.strip() for line in article_text.splitlines()]
        article_text ="".join(line for line in text_lines if line)

        # article URL
        article_url = data["articles"][i]["url"]
        article_url = ("https://prtimes.jp%s" % article_url)

        # get article header & article sub title
        try:
            article_header, article_sub_title = get_article_info(article_url)
        except Exception as e:
            print(e)
        
        # store the data in list by tab delimited format
        article_list.append(str(article_id)
        + "\t" + str(company_id)
        + "\t" + category
        + "\t" + release_date_time
        + "\t" + company_name.replace("\t","")
        + "\t" + re.sub(r"[\t\r\n🢭𓂃𓂅􁧉􁧊􁧋􀃜􏰄₿⮚�🄫🄬􏰆︎􀀀􏰁􏰂􏰿􏰀􏰃󠄀️󠄁]", "", remove_emoji(article_title + article_sub_title)).replace("𠮷", "吉").replace("𣘺", "橋").replace("𠮟", "叱").replace("𦚰", "脇").replace("𠅘", "亭").replace("𡈽", "土").replace("櫛", "櫛")
        + "\t" + re.sub(r"[\t\r\n🢭𓂃𓂅􁧉􁧊􁧋􀃜􏰄₿⮚�🄫🄬􏰆︎􀀀􏰁􏰂􏰿􏰀􏰃󠄀️󠄁]", "", remove_emoji(article_header + article_text)).replace("𠮷", "吉").replace("𣘺", "橋").replace("𠮟", "叱").replace("𦚰", "脇").replace("𠅘", "亭").replace("𡈽", "土").replace("櫛", "櫛")
        + "\t" + article_url
        )

        sleep(3)

# number of data
print("記事数(重複削除前)：{0}".format(len(article_list)))

# delete duplicate line
article_list = set(article_list)

# number of data after delete duplicate line
print("記事数(重複削除後)：{0}".format(len(article_list)))

# output file for fastload
print("出力先 --> {0}".format(out_file))
f = open(out_file, "a", encoding="utf-8")
article_list = [str(i) for i in list(article_list)]
f.write("\n".join(article_list))
f.close()

now = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S")
print("{0} {1} finish\n".format(now, category))


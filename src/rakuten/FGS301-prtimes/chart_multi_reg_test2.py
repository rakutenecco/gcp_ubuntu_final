import pandas as pd
import numpy as np
import statsmodels.api as sm 
#import statsmodels.formula.api as smf
import statsmodels.api as smf
from sklearn.linear_model import LinearRegression

#df = pd.read_csv('src/rakuten/FGS301-prtimes/trend.csv')

#x = pd.get_dummies(df[['prtimes','google']]) # 説明変数
#y = df['y'] # 目的変数

#x = [[12,2],[16,1],[20,0],[28,2],[36,0]]
#y = [[700],[900],[1300],[1750],[1800]]

#x = [[22,19],[19,0],[0,9],[9,29],[29,38],[38,34],[34,83],[83,46],[46,2],[2,0],[0,17]]
#y = [[0],[22],[19],[0],[9],[29],[38],[34],[83],[46],[2]]

x = [[31,44],[44,34],[34,57],[57,46],[46,65],[65,38],[38,21]]
y = [[34],[57],[46],[65],[38],[21],[35]]


model = LinearRegression()
model.fit(x,y)

#x_test = [[16,2],[18,0],[22,2],[32,2],[24,0]]
#y_test = [[1100],[850],[1500],[1800],[1100]]

#x_test = ([0,22],[22,0])
x_test = ([21,35],[21,35])


# prices = model.predict([[16, 2], [18, 0], [22, 2], [32, 2], [24, 0]])
prices = model.predict(x_test) # 上のコメントと同じ

#print('Predicted:%s'%(enumerate(prices)))

for i, price in enumerate(prices):
    #print('Predicted:%s, Target:%s'%(price,y_test[i]))
    print('Predicted:%s'%(price))

score = model.score(x, y)
#score = model.score(x_test,y_test)
print("r-squared:",score)


import os
import sys
from datetime import datetime, timedelta

# define today
today = datetime.strptime(datetime.strftime(datetime.now(), "%Y%m%d"), "%Y%m%d")

args = sys.argv
#category = args[1]
arg_day = datetime.strptime(args[1], "%Y%m%d")
bef_day = str((arg_day - today).days)

now = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S")
print("{0} start".format(now))

# define exect date
exec_date_hyphen = (datetime.now() + timedelta(days=int(bef_day))).date()
exec_date = datetime.strftime(exec_date_hyphen, "%Y%m%d")

category_list = [ 'app', 'beauty', 'business', 'entertainment', 'fashion', 'gourmet', 'lifestyle', 'mobile', 'sports', 'technology']

keyword_dir = "src/rakuten/FGS301-prtimes/targets.csv"
os.chmod(keyword_dir, 0o755)

keyword_list = []

keyword_data = open(keyword_dir, "r")
for line in keyword_data:
    keyword_list.append(line.replace("\n", ""))
keyword_data.close()

#print(keyword_list)

#keyword_list = ['本', '企業']

source_dir = "src/mecab/FGS301-prtimes/" + exec_date[0:6] + "/" + exec_date
os.chmod(source_dir, 0o755)

output_dir = "total-data/FGS301-prtimes/" + exec_date[0:6] + "/" + exec_date
os.chmod(output_dir, 0o755)

output_file = "{0}/fgs301-prtimes-result.txt".format(output_dir)

with open(output_file , 'w') as f:
    f.write("get_date" + "\t" + "category" + "\t" + "keyword" + "\t" + "keyword_count" + "\t" + "up_date" + "\n")
    f.write("get_date" + "\t" + "category" + "\t" + "keyword" + "\t" + "keyword_count" + "\t" + "up_date" + "\n")
    f.write("VARCHAR(50)" + "\t" + "VARCHAR(50)" + "\t" + "VARCHAR(200)" + "\t" + "VARCHAR(10)" + "\t" + "VARCHAR(50)"  + "\n")

output_list = []

for keyword in keyword_list:

    for category in category_list:

        count = 0
        text = []
        data_list = []

        source_file = "{0}/fgs301-prtimes-{1}-noun-result.txt".format(source_dir, category)
        f = open(source_file, "r")
        text.extend(f.readlines()[3:])

        # 当日のデータ
        for elem in text:
            data_list.append(elem.split(","))

        for elem in range(len(data_list)):
            
            if data_list[elem][0] == keyword:
                count = count + int(data_list[elem][2].replace("\r\n", ""))

        if(count != 0):
            output_list.append(exec_date + "\t" + category + "\t" + keyword + "\t" + str(count) + "\t" + (str(today)[0:10]))

# output file for fastload
print("出力先 --> {0}".format(output_file))
f = open(output_file, "a", encoding="utf-8")
output_list = [str(i) for i in list(output_list)]
f.write("\n".join(output_list))
f.close()

now = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S")
print("{0} end".format(now))

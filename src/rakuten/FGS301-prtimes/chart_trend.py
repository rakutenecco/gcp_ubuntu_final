import sys
import math
import os
import glob
import numpy as np
import pandas as pd
import img2pdf
import PyPDF2
import matplotlib as mpl
import matplotlib.dates as mdates
import statsmodels.api as sm
from datetime import datetime, timedelta
from matplotlib import pyplot as plt
from matplotlib.font_manager import FontProperties
from matplotlib.backends.backend_pdf import PdfPages
from pathlib import Path
from PIL import Image

font_path = '/usr/share/fonts/truetype/fonts-japanese-gothic.ttf'
font_prop = FontProperties(fname=font_path)
mpl.rcParams['font.family'] = font_prop.get_name()

# today
today = datetime.strptime(datetime.strftime(datetime.now(), "%Y%m%d"), "%Y%m%d")

# command line argument
args = sys.argv
#category = args[1]
#arg_date = datetime.strptime(args[2], "%Y%m%d")
arg_date = datetime.strptime(args[1], "%Y%m%d")
bef_day = str((arg_date - today).days)

# 日付配列作成
exec_date_hyphen = (datetime.now() + timedelta(days=int(bef_day))).date()
exec_date = datetime.strftime(exec_date_hyphen, "%Y%m%d")
exec_year = exec_date[0:4]
exec_month = exec_date[0:6]

#画像保存先ディレクトリ作成
output_dir = "src/rakuten/FGS301-prtimes/pdf_out_{0}/".format(exec_date)
os.makedirs(output_dir, exist_ok=True)

# 実行年の日付リスト作成
day_list = []
#first_date = datetime(int(exec_year), 1, 1, 0, 0)
first_date = datetime(2019, 1, 1, 0, 0)
#next_year_first_date = datetime(int(exec_year) + 1, 1, 1, 0, 0)

date_hyphen = first_date
#while date_hyphen < next_year_first_date:
while date_hyphen <= datetime.combine(exec_date_hyphen, datetime.min.time()):
    day_list.append(datetime.strftime(date_hyphen, "%Y%m%d")[0:8])
    date_hyphen += timedelta(days=1)

keyword = 'サステナビリティold'

#count_list = []

#print(count_list)

#average = sum(count_list) / len(count_list)

avg_list = []
#relval = ''
#for rel_val in count_list:
#    relval = rel_val / average
#    rel_list.append(relval)

avg_list = [34.615, 38.462, 39.423, 40.865, 45.192, 45.192, 45.192, 43.75, 41.827, 43.75, 40.865, 47.596, 47.596, 47.596, 45.673, 44.712, 
50.962, 50.481, 49.519, 49.519, 49.519, 48.558, 46.154, 50.962, 54.808, 51.923, 51.923, 51.923, 50.481, 50.481, 50.481, 50.0, 
47.596, 47.596, 47.596, 49.519, 57.212, 56.25, 56.731, 52.885, 53.846, 53.846, 53.846, 57.212, 53.365, 52.885, 50.962, 50.962, 
50.962, 55.288, 59.135, 57.212, 50.481, 49.519, 50.481, 50.481, 51.442, 50.481, 50.962, 52.885, 51.442, 51.442, 51.442, 50.481, 
45.192, 48.558, 47.115, 44.231, 43.269, 43.269, 43.269, 39.904, 37.5, 38.462, 34.135, 34.135, 34.135, 29.808, 28.846, 26.442, 
27.885, 32.212, 31.25, 31.25, 31.25, 29.327, 27.885, 26.923, 28.365, 28.365, 28.365, 27.404, 25.0, 20.673, 20.673, 19.712, 
19.712, 19.712, 19.712, 19.231, 18.75, 17.788, 18.269, 18.269, 18.269, 18.269, 17.308, 18.75, 20.192, 20.192, 20.192, 20.192, 
19.231, 25.0, 28.846, 29.808, 27.885, 27.885, 27.885, 27.885, 27.885, 29.327, 30.288, 30.769, 30.769, 30.769, 31.731, 35.577, 
37.019, 37.5, 37.981, 37.981, 37.981, 38.942, 36.058, 34.135, 30.288, 27.885, 27.885, 27.885, 28.365, 23.558, 19.712, 19.712, 
21.635, 21.635, 21.635, 28.365, 28.365, 30.288, 29.808, 33.654, 33.654, 33.654, 34.615, 30.769, 30.769, 30.288, 30.769, 30.769, 
30.769, 30.769, 31.25, 35.096, 35.096, 33.173, 33.173, 33.173, 32.692, 36.058, 37.981, 39.423, 37.5, 37.5, 37.5, 32.692, 34.135, 
31.25, 32.692, 31.731, 31.731, 31.731, 38.942, 40.865, 50.962, 59.135, 57.212, 57.212, 57.212, 56.731, 57.692, 54.327, 54.327,
 55.288, 55.288, 55.288, 55.288, 50.962, 49.038, 44.712, 47.115, 47.115, 47.115, 48.558, 47.115, 46.635, 52.885, 49.519, 49.519, 
 49.519, 42.788, 45.673, 34.615, 27.404, 35.096, 35.096, 35.096, 46.154, 45.673, 45.192, 50.0, 49.519, 49.519, 49.519, 50.481, 
 49.519, 55.288, 58.173, 56.731, 56.731, 56.731, 53.365, 54.327, 59.135, 54.808, 59.135, 59.135, 59.135, 56.731, 57.212, 62.019, 
 69.712, 63.462, 63.462, 63.462, 58.654, 62.981, 64.423, 64.423, 68.75, 68.75, 68.75, 69.231, 70.192, 64.423, 68.269, 69.231, 
 69.231, 69.231, 71.154, 72.596, 77.885, 75.962, 71.635, 71.635, 71.635, 74.519, 69.231, 64.423, 63.942, 63.942, 63.942, 63.942, 
 60.096, 54.808, 54.808, 51.442, 60.577, 60.577, 60.577, 59.135, 59.615, 69.231, 63.462, 63.462, 63.462, 63.462, 62.5, 62.5, 
 57.212, 69.712, 76.442, 76.442, 76.442, 76.923, 85.577, 85.096, 83.173, 87.019, 87.019, 87.019, 84.135, 87.5, 87.019, 87.981, 
 78.365, 78.365, 78.365, 80.288, 86.058, 77.404, 81.25, 80.288, 80.288, 80.288, 82.692, 81.25, 81.25, 69.231, 69.231, 69.231, 
 69.231, 65.865, 58.173, 65.385, 62.981, 59.615, 59.615, 59.615, 59.615, 64.423, 64.423, 72.596, 69.231, 69.231, 69.231, 67.308, 
 60.096, 59.135, 57.692, 56.731, 56.731, 56.731, 53.365, 55.769, 51.442, 52.885, 46.635, 46.635, 46.635, 46.635, 47.596, 42.788, 
 41.827, 43.75, 43.75, 43.75, 45.673, 44.231, 44.231, 36.058, 36.058, 36.058, 36.058, 47.115, 50.962]

date_index = pd.date_range('2019-01-01', freq='D', periods=(len(avg_list)))

fig, ax = plt.subplots()
fig = plt.figure(figsize=(5.8, 4.2))
ax = fig.add_subplot(111)

count = pd.Series(avg_list)
#avg_list = count.rolling(window=28).mean()
#avg_list = ["0" if x is None else x for x in avg_list]

#res = sm.tsa.seasonal_decompose(count, freq=133)
res = sm.tsa.seasonal_decompose(count, period=70)
trend = res.trend
trend = ["0" if x is None else x for x in trend]
#seasonal = res.seasonal
#seasonal = ["0" if x is None else x for x in seasonal]

#df = pd.DataFrame(chart_list, date_index)
df = pd.DataFrame(avg_list, date_index)
dt = pd.DataFrame(trend, date_index)

#avg_list = np.where(np.isnan(avg_list), 0, avg_list)
avg_list = [x for x in avg_list if str(x) != "nan"]

if(avg_list[-70] != 0):
    #YoY = str(round((avg_list[-1]/avg_list[-365] * 100) - 100, 2)) + "％"
    YoY = round((avg_list[-1] / avg_list[-70] * 100) - 100, 2)
else:
    YoY = 0

x = np.array(list(range(len(avg_list))))
y = np.array(avg_list)

#回帰線を求める
x_reg = x - x.mean()
y_reg = y - y.mean()

#傾きを求める
denom = x_reg * x_reg
numer = x_reg * y_reg

answer = round(numer.sum()/denom.sum(),5)

#直近84日(3month)
avg_list_84 = avg_list[-84:]
x_84 = np.array(list(range(len(avg_list_84))))
y_84 = np.array(avg_list_84)

#回帰線を求める
x_reg_84 = x_84 - x_84.mean()
y_reg_84 = y_84 - y_84.mean()

#傾きを求める
denom_84 = x_reg_84 * x_reg_84
numer_84 = x_reg_84 * y_reg_84

answer_84 = round(numer_84.sum()/denom_84.sum(),5)

#直近28日
avg_list_28 = avg_list[-28:]
x_28 = np.array(list(range(len(avg_list_28))))
y_28 = np.array(avg_list_28)

#回帰線を求める
x_reg_28 = x_28 - x_28.mean()
y_reg_28 = y_28 - y_28.mean()

#傾きを求める
denom_28 = x_reg_28 * x_reg_28
numer_28 = x_reg_28 * y_reg_28

answer_28 = round(numer_28.sum()/denom_28.sum(),5)

#直近7日
avg_list_7 = avg_list[-7:]
x_7 = np.array(list(range(len(avg_list_7))))
y_7 = np.array(avg_list_7)

#回帰線を求める
x_reg_7 = x_7 - x_7.mean()
y_reg_7 = y_7 - y_7.mean()

#傾きを求める
denom_7 = x_reg_7 * x_reg_7
numer_7 = x_reg_7 * y_reg_7

answer_7 = round(numer_7.sum()/denom_7.sum(),5)

#if (((answer > 0 and answer_28 > 0 and answer_7 > 0) or YoY > 0) and max(avg_list) >= 1):
#if (answer >= 0.001 and (answer_84 > 0.001 or answer_7 > 0.001)):
if (0==0):

    print(keyword + ":" + str(answer) + ":" + str(answer_84) + ":" + str(answer_28) + ":" + str(answer_7))

    if(YoY==0):
        YoY = "－"
    else:
        YoY = str(YoY) + "％"

    #if (answer_28 > 0 or answer_7 > 0):
    df.plot(ax = ax, x_compat=True, color="turquoise", alpha=0.7, legend=False)
    dt.plot(ax = ax, x_compat=True, color="tomato", alpha=0.7, legend=False)
    ax.xaxis.set_minor_locator(mdates.MonthLocator())
    
    plt.setp(ax.get_xticklabels(), rotation=60, ha="center")
    plt.title(keyword, fontsize = 13)
    plt.ylabel("件数", fontsize = 10)
    plt.text(0, 1.03, "YoY : " + YoY, size = 12, color = "black", alpha=0.7, transform=ax.transAxes)
    plt.ylim(0)
    #plt.show()

    filename = output_dir + keyword + "_" + exec_date + ".png"
    plt.savefig(filename)
import sys
import os
import glob
import PyPDF2
from datetime import datetime, timedelta
from PIL import Image, ImageDraw, ImageFont

# today
today = datetime.strptime(datetime.strftime(datetime.now(), "%Y%m%d"), "%Y%m%d")

# command line argument
args = sys.argv
#category = args[1]
#arg_date = datetime.strptime(args[2], "%Y%m%d")
arg_date = datetime.strptime(args[1], "%Y%m%d")
bef_day = str((arg_date - today).days)

# 日付配列作成
exec_date_hyphen = (datetime.now() + timedelta(days=int(bef_day))).date()
exec_date = datetime.strftime(exec_date_hyphen, "%Y%m%d")
exec_year = exec_date[0:4]
exec_month = exec_date[0:6]

# 画像保存先ディレクトリ作成
output_dir = "src/rakuten/FGS301-prtimes/pdf_out_{0}/".format(exec_date)
font_path = "src/rakuten/FGS301-prtimes/ipaexm.ttf"

png_files = sorted(glob.glob("{0}/{1}".format(output_dir, "*.png")))

#im1 = Image.open('src/rakuten/FGS301-prtimes/pdf_out_20200202/ASMR_20200202.png')

def get_concat(*args):
    if (png == png_files[-1]):
        mod = len(png_files) % 10
        if(mod >= 1 or mod == 0):
            
            im1 = Image.open(args[0])
            dst = Image.new("RGB", (im1.width + im1.width + 240, (im1.height + 35) * 5), (255,255,255))
            dst.paste(im1, (120, 35))

        if(mod >= 2 or mod == 0):
            im2 = Image.open(args[1])
            dst.paste(im2, (im1.width + 120, 35))
     
        if(mod >= 3 or mod == 0):
            im3 = Image.open(args[2])
            dst.paste(im3, (120, im1.height + 35))
        
        if(mod >= 4 or mod == 0):
            im4 = Image.open(args[3])
            dst.paste(im4, (im1.width + 120, im1.height + 35))
        
        if(mod >= 5 or mod == 0):
            im5 = Image.open(args[4])
            dst.paste(im5, (120, (im1.height + 35) * 2))
        
        if(mod >= 6 or mod == 0):
            im6 = Image.open(args[5])
            dst.paste(im6, (im1.width + 120, (im1.height + 35) * 2))
        
        if(mod >= 7 or mod == 0):
            im7 = Image.open(args[6])
            dst.paste(im7, (120, (im1.height + 35) * 3))
        
        if(mod >= 8 or mod == 0):
            im8 = Image.open(args[7])
            dst.paste(im8, (im1.width + 120, (im1.height + 35) * 3))
        
        if(mod >= 9 or mod == 0):
            im9 = Image.open(args[8])
            dst.paste(im9, (120, (im1.height + 35) * 4))

        if(mod == 0):
            im10 = Image.open(args[9])
            dst.paste(im10, (im1.width + 120, (im1.height + 35) * 4))
        
    else:
        im1 = Image.open(args[0])
        im2 = Image.open(args[1])
        im3 = Image.open(args[2])
        im4 = Image.open(args[3])
        im5 = Image.open(args[4])
        im6 = Image.open(args[5])
        im7 = Image.open(args[6])
        im8 = Image.open(args[7])
        im9 = Image.open(args[8])
        im10 = Image.open(args[9])

        dst = Image.new("RGB", (im1.width + im1.width + 240, (im1.height + 35) * 5), (255,255,255))
        dst.paste(im1, (120, 35))
        dst.paste(im2, (im1.width + 120, 35))
        dst.paste(im3, (120, im1.height + 35))
        dst.paste(im4, (im1.width + 120, im1.height + 35))
        dst.paste(im5, (120, (im1.height + 35) * 2))
        dst.paste(im6, (im1.width + 120, (im1.height + 35) * 2))
        dst.paste(im7, (120, (im1.height + 35) * 3))
        dst.paste(im8, (im1.width + 120, (im1.height + 35) * 3))
        dst.paste(im9, (120, (im1.height + 35) * 4))
        dst.paste(im10, (im1.width + 120, (im1.height + 35) * 4))
    
    return dst

cnt=0
page_num=0
path_list=[]
# １つのPDFファイルにまとめる
for png in png_files:
    cnt+=1
    
    if(cnt <= 10):
        #print(cnt)
        path_list.append(png)

        if (cnt == 10):
            page_num+=1
            get_concat(*path_list).save("src/rakuten/FGS301-prtimes/pdf_out_" +  exec_date + "/prtimes_" + exec_date + "_" + str(page_num) + ".pdf")
            cnt = 0
            path_list = []
        
        elif (cnt != 10 and png == png_files[-1]):
            page_num+=1
            get_concat(*path_list).save("src/rakuten/FGS301-prtimes/pdf_out_" +  exec_date + "/prtimes_" + exec_date + "_" + str(page_num) + ".pdf")

# PDFまとめる処理
pdf_files = sorted(glob.glob("{0}/{1}".format(output_dir, "*.pdf")), key=os.path.getmtime)
merger=PyPDF2.PdfFileMerger()

for pdf in pdf_files:
    merger.append(pdf)

merger.write("src/rakuten/FGS301-prtimes/pdf_out_" +  exec_date + "/prtimes_" + exec_date + ".pdf")
merger.close()
import sys
import math
import os
import glob
import numpy as np
import pandas as pd
import img2pdf
import PyPDF2
import matplotlib as mpl
import matplotlib.dates as mdates
import statsmodels.api as sm
from datetime import datetime, timedelta
from matplotlib import pyplot as plt
from matplotlib.font_manager import FontProperties
from matplotlib.backends.backend_pdf import PdfPages
from pathlib import Path
from PIL import Image

font_path = '/usr/share/fonts/truetype/fonts-japanese-gothic.ttf'
font_prop = FontProperties(fname=font_path)
mpl.rcParams['font.family'] = font_prop.get_name()

# today
today = datetime.strptime(datetime.strftime(datetime.now(), "%Y%m%d"), "%Y%m%d")

# command line argument
args = sys.argv
#category = args[1]
#arg_date = datetime.strptime(args[2], "%Y%m%d")
arg_date = datetime.strptime(args[1], "%Y%m%d")
bef_day = str((arg_date - today).days)

# 日付配列作成
exec_date_hyphen = (datetime.now() + timedelta(days=int(bef_day))).date()
exec_date = datetime.strftime(exec_date_hyphen, "%Y%m%d")
exec_year = exec_date[0:4]
exec_month = exec_date[0:6]

#画像保存先ディレクトリ作成
output_dir = "src/rakuten/FGS301-prtimes/pdf_out_{0}/".format(exec_date)
os.makedirs(output_dir, exist_ok=True)

# 実行年の日付リスト作成
day_list = []
#first_date = datetime(int(exec_year), 1, 1, 0, 0)
first_date = datetime(2019, 1, 1, 0, 0)
#next_year_first_date = datetime(int(exec_year) + 1, 1, 1, 0, 0)

date_hyphen = first_date
#while date_hyphen < next_year_first_date:
while date_hyphen <= datetime.combine(exec_date_hyphen, datetime.min.time()):
    day_list.append(datetime.strftime(date_hyphen, "%Y%m%d")[0:8])
    date_hyphen += timedelta(days=1)

#実行日以前の分析ファイルにアクセスして特定の単語の件数を取得
#実行日より後の件数は0
category_list = [ 'app', 'beauty', 'business', 'entertainment', 'fashion', 'gourmet', 'lifestyle', 'mobile', 'sports', 'technology']

#keyword_list = ['サステナビリティ','いちご飴']

#keyword_dir = "src/rakuten/FGS301-prtimes/targets.csv"
#os.chmod(keyword_dir, 0o755)

#keyword_list = []

#keyword_data = open(keyword_dir, "r")
#for line in keyword_data:
#    keyword_list.append(line.replace("\n", ""))
#keyword_data.close()

#keyword_list = ['タピオカ','SDGs','バナナジュース']
keyword_list = ['SDGs']

for keyword in keyword_list:

    count_list = []

    for i in range(len(day_list)):

        count = 0
        data_list = []
        text = []

        for category in category_list:
            
            # 実行日の単語取得
            source_file = "src/mecab/FGS301-prtimes/{0}/{1}/fgs301-prtimes-{2}-noun-result.txt".format(day_list[i][0:6], day_list[i], category)
            
            f = open(source_file, "r")
            #text.extend(f.readlines()[3:])
            text.extend(f.readlines())

        # 当日のデータ
        for elem in text:
            data_list.append(elem.split(","))

        for elem in range(len(data_list)):
            
            if data_list[elem][0] == keyword:
                count = count + int(data_list[elem][2].replace("\r\n", ""))

        count_list.append(count)

    print(count_list)

    date_index = pd.date_range('2019-01-01', freq='D', periods=(len(count_list)))
    
    fig, ax = plt.subplots()
    fig = plt.figure(figsize=(5.8, 4.2))
    ax = fig.add_subplot(111)

    count = pd.Series(count_list)
    avg_list = count.rolling(window=28).mean()
    avg_list = ["0" if x is None else x for x in avg_list]

    #res = sm.tsa.seasonal_decompose(count, freq=133)
    res = sm.tsa.seasonal_decompose(count, period=133)
    trend = res.trend
    trend = ["0" if x is None else x for x in trend]
    #seasonal = res.seasonal
    #seasonal = ["0" if x is None else x for x in seasonal]


    #df = pd.DataFrame(chart_list, date_index)
    df = pd.DataFrame(avg_list, date_index)
    dt = pd.DataFrame(trend, date_index)

    #avg_list = np.where(np.isnan(avg_list), 0, avg_list)
    avg_list = [x for x in avg_list if str(x) != "nan"]
    #print(avg_list)
    
    if(avg_list[-365] != 0):
        #YoY = str(round((avg_list[-1]/avg_list[-365] * 100) - 100, 2)) + "％"
        YoY = round((avg_list[-1]/avg_list[-365] * 100) - 100, 2)
    else:
        YoY = 0
    
    x = np.array(list(range(len(avg_list))))
    y = np.array(avg_list)

    #回帰線を求める
    x_reg = x - x.mean()
    y_reg = y - y.mean()

    #傾きを求める
    denom = x_reg * x_reg
    numer = x_reg * y_reg

    answer = round(numer.sum()/denom.sum(),5)

    #直近84日(3month)
    avg_list_84 = avg_list[-84:]
    x_84 = np.array(list(range(len(avg_list_84))))
    y_84 = np.array(avg_list_84)

    #回帰線を求める
    x_reg_84 = x_84 - x_84.mean()
    y_reg_84 = y_84 - y_84.mean()

    #傾きを求める
    denom_84 = x_reg_84 * x_reg_84
    numer_84 = x_reg_84 * y_reg_84

    answer_84 = round(numer_84.sum()/denom_84.sum(),5)

    #直近28日
    avg_list_28 = avg_list[-28:]
    x_28 = np.array(list(range(len(avg_list_28))))
    y_28 = np.array(avg_list_28)

    #回帰線を求める
    x_reg_28 = x_28 - x_28.mean()
    y_reg_28 = y_28 - y_28.mean()

    #傾きを求める
    denom_28 = x_reg_28 * x_reg_28
    numer_28 = x_reg_28 * y_reg_28

    answer_28 = round(numer_28.sum()/denom_28.sum(),5)

    #直近7日
    avg_list_7 = avg_list[-7:]
    x_7 = np.array(list(range(len(avg_list_7))))
    y_7 = np.array(avg_list_7)

    #回帰線を求める
    x_reg_7 = x_7 - x_7.mean()
    y_reg_7 = y_7 - y_7.mean()

    #傾きを求める
    denom_7 = x_reg_7 * x_reg_7
    numer_7 = x_reg_7 * y_reg_7

    answer_7 = round(numer_7.sum()/denom_7.sum(),5)

    #if (((answer > 0 and answer_28 > 0 and answer_7 > 0) or YoY > 0) and max(avg_list) >= 1):
    #if (answer >= 0.001 and (answer_84 > 0.001 or answer_7 > 0.001)):
    if (0==0):

        print(keyword + ":" + str(answer) + ":" + str(answer_84) + ":" + str(answer_28) + ":" + str(answer_7))

        if(YoY==0):
            YoY = "－"
        else:
            YoY = str(YoY) + "％"

        #if (answer_28 > 0 or answer_7 > 0):
        df.plot(ax = ax, x_compat=True, color="turquoise", alpha=0.7, legend=False)
        dt.plot(ax = ax, x_compat=True, color="tomato", alpha=0.7, legend=False)
        ax.xaxis.set_minor_locator(mdates.MonthLocator())
        
        plt.setp(ax.get_xticklabels(), rotation=60, ha="center")
        plt.title(keyword, fontsize = 13)
        plt.ylabel("件数", fontsize = 10)
        plt.text(0, 1.03, "YoY : " + YoY, size = 12, color = "black", alpha=0.7, transform=ax.transAxes)
        plt.ylim(0)
        #plt.show()

        filename = output_dir + keyword + "_" + exec_date + ".png"
        plt.savefig(filename)
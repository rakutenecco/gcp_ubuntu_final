import pandas as pd
import numpy as np
import statsmodels.api as sm 
#import statsmodels.formula.api as smf
import statsmodels.api as smf
from sklearn.linear_model import LinearRegression

#df = pd.read_csv('src/rakuten/FGS301-prtimes/trend.csv')

#x = pd.get_dummies(df[['prtimes','google']]) # 説明変数
#y = df['y'] # 目的変数

x = [[12,2],[16,1],[20,0],[28,2],[36,0]]
y = [[700],[900],[1300],[1750],[1800]]

model = LinearRegression()
model.fit(x,y)

x_test = [[16,2],[18,0],[22,2],[32,2],[24,0]]
y_test = [[1100],[850],[1500],[1800],[1100]]

# prices = model.predict([[16, 2], [18, 0], [22, 2], [32, 2], [24, 0]])
prices = model.predict(x_test) # 上のコメントと同じ

#price_list = []
for i, price in enumerate(prices):
    #price_list.append(price)
    print('Predicted:%s, Target:%s'%(price,y_test[i]))

#score = model.score(x, y)
#score = model.score(x_test,y_test)
#print("r-squared:",score)

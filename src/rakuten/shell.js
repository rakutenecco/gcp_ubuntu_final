'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const shell = require('shelljs');

// parse args
const cli = commandArgs([
    { name: 'cron', alias: 'c', type: String },
    { name: 'run', alias: 'r', type: Number },
]);

// run action
const run = function () {

    co(function* () {
        // 
        switch (cli['cron']) {
            case 100: // total-data git
                shell.exec(`cd total-data && git add .`);
                shell.exec(`cd total-data && git add -u .`);
                shell.exec(`cd total-data && git commit -m 'up fgs44'`);
                shell.exec(`cd total-data && git pull origin master`);
                shell.exec(`cd total-data && git push origin master`);
                break;

            case 'cron5': // usual action
                switch (cli['run']) {

                    case 1: // usual action
                    default:

                        shell.exec('node src/rakuten/FGS258-twitter-explore/shell.js -r 3');
                        shell.exec('node src/rakuten/FGS242-instagram-explore/shell.js -r 3');
                        break;
                }
                break;

            case 'cron4': // usual action
                switch (cli['run']) {

                    case 1: // usual action
                    default:

                        shell.exec('node src/rakuten/FGS258-twitter-explore/shell.js -r 2');
                        shell.exec('node src/rakuten/FGS242-instagram-explore/shell.js -r 2');
                        break;
                }
                break;

            case 'cron3': // usual action
            default:

                switch (cli['run']) {

                    case 1: // usual action
                    default:
            
                        shell.exec('node src/rakuten/FGS258-twitter-explore/shell.js -r 1');
                        shell.exec('node src/rakuten/FGS242-instagram-explore/shell.js -r 1');
                        break;
                }
                break;
        }
    });
}

run();
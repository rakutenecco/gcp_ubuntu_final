'use strict';

const shell = require('shelljs');
const cronJob = require('cron').CronJob;
const moment = require('moment');
const fs = require('fs');
const date = moment().format('YYYYMMDD');
const today = moment().format('YYYY-MM-DD hh:mm:ss');
const path_log = `../log/${date}.txt`;
let now = '';
const conf = new (require('/var/app/total/src/conf.js'));

/*
cronJob(`00 00 00 * * 1-5`)
Seconds: 0-59
Minutes: 0-59
Hours: 0-23
Day of Month: 1-31
Months: 0-11
Day of Week: 0-6
*/

//// 01 ////
// FGS239 insta posts
new cronJob(`00 05 00 * * 0`, function () {
//new cronJob(`00 32 09 * * 1`, function () {

    shell.exec(`node src/rakuten/FGS239-insta-posts/shell.js -r 1 -n cron12_01 -f targets_flower.txt`);
    shell.exec(`node src/rakuten/FGS239-insta-posts/shell.js -r 1 -n cron12_01 -f targets_interior.txt`);
    shell.exec(`node src/rakuten/FGS239-insta-posts/shell.js -r 1 -n cron12_01 -f targets_watch.txt`);
    shell.exec(`node src/rakuten/FGS239-insta-posts/shell.js -r 1 -n cron12_01 -f targets_life.txt`);
    shell.exec(`node src/rakuten/FGS239-insta-posts/shell.js -r 1 -n cron12_01 -f targets_kitchen.txt`);
    shell.exec(`node src/rakuten/FGS239-insta-posts/shell.js -r 1 -n cron12_01 -f targets_fashion.txt`);

    // let filepath4 = `${conf.dirPath_share_vb}/targets/targets_flower.txt`;
    // if (fs.existsSync(filepath4)) {
    //     shell.exec(`node src/rakuten/FGS239-insta-posts/shell.js -r 6 -n cron12_01 -f ${filepath4}`);
    // }

    // let filepath5 = `${conf.dirPath_share_vb}/targets/targets_interior.txt`;
    // if (fs.existsSync(filepath5)) {
    //     shell.exec(`node src/rakuten/FGS239-insta-posts/shell.js -r 8 -n cron12_01 -f ${filepath5}`);
    // }

    // let filepath2 = `${conf.dirPath_share_vb}/targets/targets_watch.txt`;
    // if (fs.existsSync(filepath2)) {
    //     shell.exec(`node src/rakuten/FGS239-insta-posts/shell.js -r 4 -n cron12_01 -f ${filepath2}`);
    // }

    // let filepath6 = `${conf.dirPath_share_vb}/targets/targets_kitchen.txt`;
    // if (fs.existsSync(filepath6)) {
    //     shell.exec(`node src/rakuten/FGS239-insta-posts/shell.js -r 7 -n cron12_01 -f ${filepath6}`);
    // }

}, function () {
    // error
    console.log(`Error ${today} : FGS239 insta-posts scrape cron12_01`);
},
true, 'Asia/Tokyo');

console.log('cron12_01 setting');

'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');
const moment = require('moment');
const { Builder, By, Key, Capabilities, until, promise } = require('selenium-webdriver');

// new modules
const conf = new(require('../../conf.js'));
const db = new(require('../../modules/db.js'));
const match = new(require('../../modules/match.js'));
const tsv = new(require('../../modules/tsv.js'));
const util = new(require('../../modules/util.js'));
const selenium = new(require('../../modules/selenium.js'));
const phantom = new(require('../../modules/phantom.js'));

// each scrape modules
const casioUsa = new(require('./casioUsa.js'));
const casioUsa_WSD = new(require('./casioUsa_WSD.js'));
const casioJP = new(require('./casioJP.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
    { name: 'browser', alias: 'B', type: String },
]);

// variable
let driver = null;
const today = moment().format('YYYYMMDD');
const filePath_db = `${conf.dirPath_db}/cron6.db`;

// init action
const init = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // set selenium with browser
            driver = yield selenium.init(cli['browser'], { inVisible: true });

            yield util.mkdir(`${conf.dirPath_data}\/FGS42-casio`);
            // create table "proxy_list"
            yield db.connect(filePath_db);

            resolve();
        });
    });
}

// end action
const end = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            driver.quit();
            yield db.close();

            resolve();
        });
    });
}

// drop table
const drop = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // drop table proxy_list
            yield db.connect(filePath_db);

            let sql1 = `DROP TABLE IF EXISTS fashion_fgs42_casio_product;`;
            yield db.do(`run`, sql1, {});

            let sql2 = `DROP TABLE IF EXISTS fashion_fgs42_casio_category;`;
            yield db.do(`run`, sql2, {});

            let sql3 = `DROP TABLE IF EXISTS fashion_fgs42_casio_list;`;
            yield db.do(`run`, sql3, {});

            yield db.close();
            resolve();
        });
    });
}

// set url list
const setList = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // create table "proxy_list"
            yield db.connect(filePath_db);
            const sqlA = `CREATE TABLE IF NOT EXISTS fashion_fgs42_casio_product(num INTEGER primary key, id INTEGER, page_id TEXT, mpn_1 TEXT, mpn_2 TEXT, manufacturer_name TEXT, brand_name TEXT, product_name TEXT, color TEXT, size TEXT, size_value TEXT, exc_price NUMERIC, inc_price NUMERIC, unit_price TEXT, category_name TEXT, insert_date TEXT, url TEXT);`;
            const sqlB = `CREATE TABLE IF NOT EXISTS fashion_fgs42_casio_category(Num INTEGER primary key, category_name TEXT, category_url TEXT, page_id TEXT);`;
            const sqlC = `CREATE TABLE IF NOT EXISTS fashion_fgs42_casio_list(page_id TEXT primary key, id TEXT, num INTEGER, category TEXT, name TEXT, delflag INTEGER, shop_category1 TEXT, shop_category2 TEXT, cat_url TEXT, cat_url2 TEXT, cat_url3 TEXT, stage TEXT);`;
            yield db.do(`run`, sqlA, {});
            yield db.do(`run`, sqlB, {});
            yield db.do(`run`, sqlC, {});

            const sql1 = `INSERT OR REPLACE INTO fashion_fgs42_casio_list(page_id, id, num, category, name, delflag, shop_category1, shop_category2, cat_url, cat_url2, cat_url3, stage) VALUES ("12-1", "12", 1, "totalScraper", "casio", 0, "casio USA", "G-SHOCK", "https://www.casio.com/products/watches/g-shock", "none", "none", "productList");`;
            const sql2 = `INSERT OR REPLACE INTO fashion_fgs42_casio_list(page_id, id, num, category, name, delflag, shop_category1, shop_category2, cat_url, cat_url2, cat_url3, stage) VALUES ("12-2", "12", 2, "totalScraper", "casio", 0, "casio USA", "BABY-G", "https://www.casio.com/products/watches/baby-g", "none", "none", "productList");`;
            const sql3 = `INSERT OR REPLACE INTO fashion_fgs42_casio_list(page_id, id, num, category, name, delflag, shop_category1, shop_category2, cat_url, cat_url2, cat_url3, stage) VALUES ("12-3", "12", 3, "totalScraper", "casio", 0, "casio USA", "G-Shock S Series", "https://www.casio.com/products/watches/g-shock-s-series", "none", "none", "productList");`;
            const sql4 = `INSERT OR REPLACE INTO fashion_fgs42_casio_list(page_id, id, num, category, name, delflag, shop_category1, shop_category2, cat_url, cat_url2, cat_url3, stage) VALUES ("12-4", "12", 4, "totalScraper", "casio", 0, "casio USA", "Edifice", "https://www.casio.com/products/watches/edifice", "none", "none", "productList");`;
            const sql5 = `INSERT OR REPLACE INTO fashion_fgs42_casio_list(page_id, id, num, category, name, delflag, shop_category1, shop_category2, cat_url, cat_url2, cat_url3, stage) VALUES ("12-5", "12", 5, "totalScraper", "casio", 0, "casio USA", "Pro Trek", "https://www.casio.com/products/watches/pro-trek", "none", "none", "productList");`;
            const sql6 = `INSERT OR REPLACE INTO fashion_fgs42_casio_list(page_id, id, num, category, name, delflag, shop_category1, shop_category2, cat_url, cat_url2, cat_url3, stage) VALUES ("12-6", "12", 6, "totalScraper", "casio", 0, "casio USA", "Bluetooth Sports Gear", "https://www.casio.com/products/watches/bluetooth-sports-gear", "none", "none", "productList");`;
            const sql7 = `INSERT OR REPLACE INTO fashion_fgs42_casio_list(page_id, id, num, category, name, delflag, shop_category1, shop_category2, cat_url, cat_url2, cat_url3, stage) VALUES ("12-7", "12", 7, "totalScraper", "casio", 0, "casio USA", "Wave Ceptor", "https://www.casio.com/products/watches/wave-ceptor", "none", "none", "productList");`;
            const sql8 = `INSERT OR REPLACE INTO fashion_fgs42_casio_list(page_id, id, num, category, name, delflag, shop_category1, shop_category2, cat_url, cat_url2, cat_url3, stage) VALUES ("12-8", "12", 8, "totalScraper", "casio", 0, "casio USA", "Dress", "https://www.casio.com/products/watches/dress", "none", "none", "productList");`;
            const sql9 = `INSERT OR REPLACE INTO fashion_fgs42_casio_list(page_id, id, num, category, name, delflag, shop_category1, shop_category2, cat_url, cat_url2, cat_url3, stage) VALUES ("12-9", "12", 9, "totalScraper", "casio", 0, "casio USA", "Classic", "https://www.casio.com/products/watches/classic", "none", "none", "productList");`;
            const sql10 = `INSERT OR REPLACE INTO fashion_fgs42_casio_list(page_id, id, num, category, name, delflag, shop_category1, shop_category2, cat_url, cat_url2, cat_url3, stage) VALUES ("12-10", "12", 10, "totalScraper", "casio", 0, "casio USA", "Databank", "https://www.casio.com/products/watches/databank", "none", "none", "productList");`;
            const sql11 = `INSERT OR REPLACE INTO fashion_fgs42_casio_list(page_id, id, num, category, name, delflag, shop_category1, shop_category2, cat_url, cat_url2, cat_url3, stage) VALUES ("12-11", "12", 11, "totalScraper", "casio", 0, "casio USA", "Sports", "https://www.casio.com/products/watches/sports", "none", "none", "productList");`;
            const sql12 = `INSERT OR REPLACE INTO fashion_fgs42_casio_list(page_id, id, num, category, name, delflag, shop_category1, shop_category2, cat_url, cat_url2, cat_url3, stage) VALUES ("12-12", "12", 12, "totalScraper", "casio", 0, "casio wsd-f20", "lineup", "https://wsd.casio.com/us/en/wsd-f20/products/", "none", "none", "productList");`;
            const sql13 = `INSERT OR REPLACE INTO fashion_fgs42_casio_list(page_id, id, num, category, name, delflag, shop_category1, shop_category2, cat_url, cat_url2, cat_url3, stage) VALUES ("12-13", "12", 13, "totalScraper", "casio", 0, "casio JP", "ウオッチ検索", "https://casio.jp/wat/search/watch/", "none", "none", "productList");`;
            yield db.do(`run`, sql1, {});
            yield db.do(`run`, sql2, {});
            yield db.do(`run`, sql3, {});
            yield db.do(`run`, sql4, {});
            yield db.do(`run`, sql5, {});
            yield db.do(`run`, sql6, {});
            yield db.do(`run`, sql7, {});
            yield db.do(`run`, sql8, {});
            yield db.do(`run`, sql9, {});
            yield db.do(`run`, sql10, {});
            yield db.do(`run`, sql11, {});
            yield db.do(`run`, sql12, {});
            yield db.do(`run`, sql13, {});
            yield db.close();

            resolve();
        });
    });
}

// set out file to share folder
const setOutFile = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // get scraped data
            let sql = `SELECT page_id, mpn_1, mpn_2, manufacturer_name, brand_name, product_name, color, size, size_value, exc_price, inc_price, category_name, min(insert_date) insert_date, url
            FROM fashion_fgs42_casio_product
            where page_id not in ('12-13')
            group by mpn_1, mpn_2, manufacturer_name, brand_name, product_name, color, size, size_value, exc_price, inc_price, category_name, url
            order by insert_date;`;

            let res = yield db.do(`all`, sql, {});

            // write header
            yield util.mkdir(`${conf.dirPath_data}\/FGS42-casio`);
            yield util.mkdir(`${conf.dirPath_data}\/FGS42-casio\/${today}-${conf.pc_id}`);
            let filepath = `${conf.dirPath_share_vb}/fgs42-casio-usa-${today}.txt`;
            let filepath2 = `${conf.dirPath_data}\/FGS42-casio\/${today}-${conf.pc_id}\/fgs42-casio-usa.txt`;

            fs.writeFileSync(filepath, '', 'utf-8');
            fs.appendFileSync(filepath, ['num', 'id', 'mpn_1', 'mpn_2', 'manufacturer_name', 'brand_name', 'product_name', 'color', 'size', 'size_value', 'exc_price', 'inc_price', 'category_name', 'insert_date', 'url'].join('\t') + '\n', 'utf-8');
            fs.appendFileSync(filepath, ['num', 'id', 'mpn_1', 'mpn_2', 'manufacturer_name', 'brand_name', 'product_name', 'color', 'size', 'size_value', 'exc_price', 'inc_price', 'category_name', 'insert_date', 'url'].join('\t') + '\n', 'utf-8');
            fs.appendFileSync(filepath, ['BIGINT', 'BIGINT', 'VARCHAR(100)', 'VARCHAR(100)', 'VARCHAR(200)', 'VARCHAR(200)', 'VARCHAR(300)', 'VARCHAR(100)', 'VARCHAR(300)', 'VARCHAR(100)', 'DECIMAL(15,2)', 'DECIMAL(15,2)', 'VARCHAR(200)', 'VARCHAR(8)', 'VARCHAR(300)'].join('\t') + '\n', 'utf-8');

            // write body
            for (let i = 0; i < res.length; i++) {
                fs.appendFileSync(filepath, [i + 1, '12', res[i].mpn_1, res[i].mpn_2, res[i].manufacturer_name, res[i].brand_name, res[i].product_name, res[i].color, res[i].size, res[i].size_value, res[i].exc_price, res[i].inc_price, res[i].category_name.substring(0, 100), res[i].insert_date.replace(/-/g, ''), res[i].url].join('\t') + '\n', 'utf-8');
            }

            // copy data
            let data = fs.readFileSync(filepath, 'utf-8');
            fs.writeFileSync(filepath2, data, 'utf-8');



            // get scraped data
            let sql_usa = `SELECT page_id, mpn_1, mpn_2, manufacturer_name, brand_name, product_name, color, size, size_value, exc_price, inc_price, category_name, min(insert_date) insert_date, url
            FROM fashion_fgs42_casio_product
            where page_id in ('12-13')
            group by mpn_1, mpn_2, manufacturer_name, brand_name, product_name, color, size, size_value, exc_price, inc_price, category_name, url
            order by insert_date;`;

            let res_usa = yield db.do(`all`, sql_usa, {});

            let filepath_usa = `${conf.dirPath_share_vb}/fgs42-casio-${today}.txt`;
            let filepath_usa2 = `${conf.dirPath_data}\/FGS42-casio\/${today}-${conf.pc_id}\/fgs42-casio.txt`;

            fs.writeFileSync(filepath_usa, '', 'utf-8');
            fs.appendFileSync(filepath_usa, ['num', 'id', 'mpn_1', 'mpn_2', 'manufacturer_name', 'brand_name', 'product_name', 'color', 'size', 'size_value', 'exc_price', 'inc_price', 'category_name', 'insert_date', 'url'].join('\t') + '\n', 'utf-8');
            fs.appendFileSync(filepath_usa, ['num', 'id', 'mpn_1', 'mpn_2', 'manufacturer_name', 'brand_name', 'product_name', 'color', 'size', 'size_value', 'exc_price', 'inc_price', 'category_name', 'insert_date', 'url'].join('\t') + '\n', 'utf-8');
            fs.appendFileSync(filepath_usa, ['BIGINT', 'BIGINT', 'VARCHAR(100)', 'VARCHAR(100)', 'VARCHAR(200)', 'VARCHAR(200)', 'VARCHAR(300)', 'VARCHAR(100)', 'VARCHAR(300)', 'VARCHAR(100)', 'DECIMAL(15,2)', 'DECIMAL(15,2)', 'VARCHAR(200)', 'VARCHAR(8)', 'VARCHAR(300)'].join('\t') + '\n', 'utf-8');

            // write body
            for (let i = 0; i < res_usa.length; i++) {
                fs.appendFileSync(filepath_usa, [i + 1, '12', res_usa[i].mpn_1, res_usa[i].mpn_2, res_usa[i].manufacturer_name, res_usa[i].brand_name, res_usa[i].product_name, res_usa[i].color, res_usa[i].size, res_usa[i].size_value, res_usa[i].exc_price, res_usa[i].inc_price, res_usa[i].category_name.substring(0, 50), res_usa[i].insert_date.replace(/-/g, ''), res_usa[i].url].join('\t') + '\n', 'utf-8');
            }

            let data_usa = fs.readFileSync(filepath_usa, 'utf-8');
            fs.writeFileSync(filepath_usa2, data_usa, 'utf-8');

            resolve();
        });
    });
}

// run action
const run = function () {

    co(function* () {
        // 
        switch (cli['run']) {
            case 100: // login
                yield phantom.loginNoneIntra();
                break;

            case 99: // reset table
                yield drop();
                yield setList();
                break;

            case 9908: // casio JP 削除
                yield casioJP.delete();
                break;

            case 8: // casio JP
                yield casioJP.init(cli['browser']);
                yield casioJP.scrapeCategory();
                yield casioJP.scrapeProduct();
                break;

            case 9907: // casio USA 削除
                yield casioUsa_WSD.delete();
                break;

            case 7: // casio USA
                yield casioUsa_WSD.init(cli['browser']);
                yield casioUsa_WSD.scrapeProduct();
                break;

            case 9906: // casio USA 削除
                yield casioUsa.delete();
                break;

            case 6: // casio USA
                yield casioUsa.init(cli['browser']);
                yield casioUsa.scrapeCategory();
                yield casioUsa.scrapeProduct();
                break;

            case 5: // scrape product only
                yield init();
                yield setOutFile();
                yield end();
                break;
            case 4: // scrape product only
                yield init();
                yield scrapeProduct();
                yield setOutFile();
                yield end();
                break;
                // case 3: // check each data
                //     yield showList();
                //     break;
            case 1: // scrape data
            default:
                yield init();
                yield scrapeCategory();
                yield scrapeProduct();
                yield scrapeProduct();
                yield scrapeProduct();
                yield setOutFile();
                yield end();
                break;
        }
    });
}

run();
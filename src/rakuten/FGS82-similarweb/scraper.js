'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');
const moment = require('moment');
const { Builder, By, Key, Capabilities, until, promise } = require('selenium-webdriver');

// new modules
const conf = new(require('../../conf.js'));
const db = new(require('../../modules/db.js'));
const match = new(require('../../modules/match.js'));
const tsv = new(require('../../modules/tsv.js'));
const util = new(require('../../modules/util.js'));
const selenium = new(require('../../modules/selenium.js'));

// each scrape modules
const scrapeSelenium = new(require('./scrapeSelenium.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
    { name: 'browser', alias: 'B', type: String },
]);

// variable
let driver = null;
const today = moment().format('YYYYMMDD');
const filePath_db = `${conf.dirPath_db}/cron6.db`;

// init action
const init = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // set selenium with browser
            driver = yield selenium.init(cli['browser'], { inVisible: false }); // true

            yield util.mkdir(`${conf.dirPath_data}\/FGS82-similarweb`);

            // create table "proxy_list"
            yield db.connect(filePath_db);

            resolve();
        });
    });
}

// end action
const end = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            driver.quit();
            yield db.close();

            resolve();
        });
    });
}

// drop table
const drop = function () {

    return new Promise((resolve, reject) => {
        co(function* () {
/*
            // drop table proxy_list
            yield db.connect(filePath_db);

            let sql1 = `DROP TABLE IF EXISTS fashion_fgs44_seiko_product;`;
            yield db.do(`run`, sql1, {});

            let sql2 = `DROP TABLE IF EXISTS fashion_fgs44_seiko_category;`;
            yield db.do(`run`, sql2, {});

            let sql3 = `DROP TABLE IF EXISTS fashion_fgs44_seiko_list;`;
            yield db.do(`run`, sql3, {});

            yield db.close();
*/
            resolve();
        });
    });
}

// set url list
const setList = function () {

    return new Promise((resolve, reject) => {
        co(function* () {
/*
            // create table "proxy_list"
            yield db.connect(filePath_db);
            const sqlA = `CREATE TABLE IF NOT EXISTS fashion_fgs44_seiko_product(num INTEGER primary key, id INTEGER, page_id TEXT, mpn_1 TEXT, mpn_2 TEXT, manufacturer_name TEXT, brand_name TEXT, product_name TEXT, color TEXT, size TEXT, size_value TEXT, exc_price NUMERIC, inc_price NUMERIC, unit_price TEXT, category_name TEXT, insert_date TEXT, url TEXT);`;
            const sqlB = `CREATE TABLE IF NOT EXISTS fashion_fgs44_seiko_category(Num INTEGER primary key, category_name TEXT, category_url TEXT, page_id TEXT);`;
            const sqlC = `CREATE TABLE IF NOT EXISTS fashion_fgs44_seiko_list(page_id TEXT primary key, id TEXT, num INTEGER, category TEXT, name TEXT, delflag INTEGER, shop_category1 TEXT, shop_category2 TEXT, cat_url TEXT, cat_url2 TEXT, cat_url3 TEXT, stage TEXT);`;
            yield db.do(`run`, sqlA, {});
            yield db.do(`run`, sqlB, {});
            yield db.do(`run`, sqlC, {});

            const sql1 = `INSERT OR REPLACE INTO fashion_fgs44_seiko_list(page_id, id, num, category, name, delflag, shop_category1, shop_category2, cat_url, cat_url2, cat_url3, stage) VALUES ("5932-1", "5932", 1, "totalScraper", "SEIKO", 0, "セイコーウオッチ株式会社 製品検索", "", "https://www.seiko-watch.co.jp/products/", "none", "none", "productList");`;
            yield db.do(`run`, sql1, {});

            yield db.close();
*/
            resolve();
        });
    });
}

// set out file to share folder
const setOutFile = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // get scraped data
            let sql = `SELECT mpn_1, mpn_2, manufacturer_name, brand_name, product_name, color, size, size_value, exc_price, inc_price, category_name, min(insert_date) insert_date, url
            FROM fashion_fgs44_seiko_product
            group by mpn_1, mpn_2, manufacturer_name, brand_name, product_name, color, size, size_value, exc_price, inc_price, category_name, url
            order by insert_date;`;
            //console.log(sql);
            let res = yield db.do(`all`, sql, {});

            yield util.mkdir(`${conf.dirPath_data}\/FGS44-seiko`);
            yield util.mkdir(`${conf.dirPath_data}\/FGS44-seiko\/${today}-${conf.pc_id}`);
            let filepath = `${conf.dirPath_share_vb}/FGS44-seiko-${today}.txt`;
            let filepath2 = `${conf.dirPath_data}\/FGS44-seiko\/${today}-${conf.pc_id}\/FGS44-seiko.txt`;

            // write header
            fs.writeFileSync(filepath, '', 'utf-8');
            fs.appendFileSync(filepath, ['num', 'id', 'mpn_1', 'mpn_2', 'manufacturer_name', 'brand_name', 'product_name', 'color', 'size', 'size_value', 'exc_price', 'inc_price', 'category_name', 'insert_date', 'url'].join('\t') + '\n', 'utf-8');
            fs.appendFileSync(filepath, ['num', 'id', 'mpn_1', 'mpn_2', 'manufacturer_name', 'brand_name', 'product_name', 'color', 'size', 'size_value', 'exc_price', 'inc_price', 'category_name', 'insert_date', 'url'].join('\t') + '\n', 'utf-8');
            fs.appendFileSync(filepath, ['BIGINT', 'BIGINT', 'VARCHAR(100)', 'VARCHAR(100)', 'VARCHAR(200)', 'VARCHAR(200)', 'VARCHAR(300)', 'VARCHAR(100)', 'VARCHAR(300)', 'VARCHAR(100)', 'DECIMAL(15,2)', 'DECIMAL(15,2)', 'VARCHAR(200)', 'VARCHAR(8)', 'VARCHAR(300)'].join('\t') + '\n', 'utf-8');

            // write body
            for (let i = 0; i < res.length; i++) {
                fs.appendFileSync(filepath, [i + 1, '5932', res[i].mpn_1, res[i].mpn_2, res[i].manufacturer_name, res[i].brand_name, res[i].product_name, res[i].color, res[i].size, res[i].size_value, res[i].exc_price, res[i].inc_price, (res[i].category_name ? res[i].category_name.substr(0, 50) : ''), res[i].insert_date.replace(/-/g, ''), res[i].url].join('\t') + '\n', 'utf-8');
            }

            // copy data
            let data = fs.readFileSync(filepath, 'utf-8');
            fs.writeFileSync(filepath2, data, 'utf-8');

            resolve(res);
        });
    });
}

// run action
const run = function () {

    co(function* () {
        // 
        switch (cli['run']) {
            case 100: // out file
                yield init();
                yield setOutFile();
                yield end();
                break;
            case 99: // reset table
                yield drop();
                yield setList();
                break;

            case 2: // scrape by selenium
                yield scrapeSelenium.init(cli['browser']);
                yield scrapeSelenium.showSimilarDirect();
                // message of "Unable To Identify Your Browser"
                // never login
                break;

            case 1: // scrape by selenium
            default:
                yield scrapeSelenium.init(cli['browser']);
                yield scrapeSelenium.showSimilar();
                // robot check
                break;
        }
    });
}

run();
'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');
const moment = require('moment');
const { Builder, By, Key, Capabilities, until, promise } = require('selenium-webdriver');

// new modules
const conf = new(require('../../conf.js'));
const db = new(require('../../modules/db.js'));
const tsv = new(require('../../modules/tsv.js'));
const util = new(require('../../modules/util.js'));
const selenium = new(require('../../modules/selenium.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
    { name: 'browser', alias: 'B', type: String }
]);

// variable
let driver = null;
const today = moment().format('YYYYMMDD');


// init action
const init = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            driver = yield selenium.init();

            resolve();
        });
    });
}


// open browser
const showSimilar = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // show similar on google search
            driver.get('https://www.google.com/search?q=similarweb');
            driver.findElement(By.name('q')).click();
            driver.findElement(By.name('q')).sendKeys('similarweb', Key.RETURN);
            driver.wait(until.titleIs('similarweb - Google 検索'), 1000);

            // select similar web
            driver.findElements(By.className('r')).then(function (elements) {
                let texts = [];
                elements.map(function (e, i) {
                    co(function* () {
                        let text = yield selenium.getText(e);
                        let url = yield selenium.getUrl(e);

                        if (url === 'https://www.similarweb.com/') {
                            console.log(8888);
                            e.click();
                            resolve();
                            return;
                        }
                    });
                });

                resolve();
            });
        });
    });
}

// run action
const run = function () {

    co(function* () {
        // 
        switch (cli['run']) {
            case 1: // scrape data
            default:
                yield init();
                yield showSimilar();

                break;
        }
    });
}

run();
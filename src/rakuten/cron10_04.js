'use strict';

const shell = require('shelljs');
const cronJob = require('cron').CronJob;
const moment = require('moment');
const fs = require('fs');
const date = moment().format('YYYYMMDD');
const today = moment().format('YYYY-MM-DD hh:mm:ss');
let yesterday;
const path_log = `../log/${date}.txt`;
let now = '';
const conf = new (require('/var/app/total/src/conf.js'));

/*
cronJob(`00 00 00 * * 1-5`)
Seconds: 0-59
Minutes: 0-59
Hours: 0-23
Day of Month: 1-31
Months: 0-11
Day of Week: 0-6
*/

//// 04 ////
// // FGS239 insta posts daily 0:16
// new cronJob(`00 16 00 * * 0-6`, function () {
//     let filepath = `${conf.dirPath_share_vb}/targets/targets_food_04.txt`;
//     if (fs.existsSync(filepath)) {
//         shell.exec(`node src/rakuten/FGS239-insta-posts/shell.js -r 1 -n cron10_04 -f ${filepath}`);
//         shell.exec(`node src/rakuten/FGS239-insta-posts/make_outfile.js`);
//     }

// }, function () {
//     // error
//     console.log(`Error ${today} : FGS239 insta-posts scrape cron10_04`);
// },
// true, 'Asia/Tokyo');

// FGS239 insta posts
//// fashion //// by targets file
new cronJob(`00 10 00 * * 0`, function () {
//new cronJob(`00 11 09 * * 1`, function () {

    shell.exec(`node src/rakuten/FGS239-insta-posts/shell.js -r 1 -n cron10_04 -f targets_food_05_04.txt`);

    // let filepath = `${conf.dirPath_share_vb}/targets/targets_food_04_04.txt`;
    // if (fs.existsSync(filepath)) {
    //     shell.exec(`node src/rakuten/FGS239-insta-posts/shell.js -r 1 -n cron10_04 -f ${filepath}`);
    //     shell.exec(`node src/rakuten/FGS239-insta-posts/make_outfile.js`);
    // }

    // let filepath = `${conf.dirPath_share_vb}/targets/targets_food_05_04.txt`;
    // if (fs.existsSync(filepath)) {
    //     shell.exec(`node src/rakuten/FGS239-insta-posts/shell.js -r 1 -n cron10_04 -f ${filepath}`);
    //     //shell.exec(`node src/rakuten/FGS239-insta-posts/make_outfile.js -r 1`);
    //     //yesterday = moment().add(-1, 'days').format('YYYYMMDD');
    //     //shell.exec(`node src/rakuten/FGS239-insta-posts/make_outfile.js -r 1 -t ${yesterday}`);
    // }
    
    // let filepath = `${conf.dirPath_share_vb}/targets/targets_fashion.txt`;
    // if (fs.existsSync(filepath)) {
    //     shell.exec(`node src/rakuten/FGS239-insta-posts/shell.js -r 2 -n cron10_04 -f ${filepath}`);
    // }

}, function () {
    // error
    console.log(`Error ${today} : FGS239 insta-posts scrape cron10_04`);
},
true, 'Asia/Tokyo');

console.log('cron10_04 setting');

'use strict';

const shell = require('shelljs');
const cronJob = require('cron').CronJob;
const moment = require('moment');
const fs = require('fs');
const date = moment().format('YYYYMMDD');
const today = moment().format('YYYY-MM-DD hh:mm:ss');
const path_log = `../log/${date}.txt`;
let now = '';
const conf = new (require('/var/app/total/src/conf.js'));

/*
cronJob(`00 00 00 * * 1-5`)
Seconds: 0-59
Minutes: 0-59
Hours: 0-23
Day of Month: 1-31
Months: 0-11
Day of Week: 0-6
*/

// FGS258 twitter posts daily
//new cronJob(`00 05 00 * * 0-6`, function () {
new cronJob(`00 15 19 * * 2`, function () {
    
        //let targetday = '20190419';
        let filepath;
        
        filepath = `${conf.dirPath_share_vb}/targets/targets_kitchen.txt`;
        if (fs.existsSync(filepath)) {
            shell.exec(`node src/rakuten/FGS258-twitter/shell.js -r 5 -n cron11_03 -f ${filepath} -d continue`);
        }

        filepath = `${conf.dirPath_share_vb}/targets/targets_fashion.txt`;
        if (fs.existsSync(filepath)) {
            shell.exec(`node src/rakuten/FGS258-twitter/shell.js -r 2 -n cron11_03 -f ${filepath} -t 20190603`);
        }

        filepath = `${conf.dirPath_share_vb}/targets/targets_watch.txt`;
        if (fs.existsSync(filepath)) {
            shell.exec(`node src/rakuten/FGS258-twitter/shell.js -r 3 -n cron11_03 -f ${filepath} -t 20190603`);
        }

        filepath = `${conf.dirPath_share_vb}/targets/targets_life.txt`;
        if (fs.existsSync(filepath)) {
            shell.exec(`node src/rakuten/FGS258-twitter/shell.js -r 7 -n cron11_03 -f ${filepath} -t 20190603`);
        }

        filepath = `${conf.dirPath_share_vb}/targets/targets_flower.txt`;
        if (fs.existsSync(filepath)) {
            shell.exec(`node src/rakuten/FGS258-twitter/shell.js -r 4 -n cron11_03 -f ${filepath} -t 20190604`);
        }

        filepath = `${conf.dirPath_share_vb}/targets/targets_interior.txt`;
        if (fs.existsSync(filepath)) {
            shell.exec(`node src/rakuten/FGS258-twitter/shell.js -r 6 -n cron11_03 -f ${filepath} -t 20190604`);
        }

        filepath = `${conf.dirPath_share_vb}/targets/targets_kitchen.txt`;
        if (fs.existsSync(filepath)) {
            shell.exec(`node src/rakuten/FGS258-twitter/shell.js -r 5 -n cron11_03 -f ${filepath} -t 20190604`);
        }

        filepath = `${conf.dirPath_share_vb}/targets/targets_fashion.txt`;
        if (fs.existsSync(filepath)) {
            shell.exec(`node src/rakuten/FGS258-twitter/shell.js -r 2 -n cron11_03 -f ${filepath} -t 20190604`);
        }

        filepath = `${conf.dirPath_share_vb}/targets/targets_watch.txt`;
        if (fs.existsSync(filepath)) {
            shell.exec(`node src/rakuten/FGS258-twitter/shell.js -r 3 -n cron11_03 -f ${filepath} -t 20190604`);
        }

        filepath = `${conf.dirPath_share_vb}/targets/targets_life.txt`;
        if (fs.existsSync(filepath)) {
            shell.exec(`node src/rakuten/FGS258-twitter/shell.js -r 7 -n cron11_03 -f ${filepath} -t 20190604`);
        }

        filepath = `${conf.dirPath_share_vb}/targets/targets_flower.txt`;
        if (fs.existsSync(filepath)) {
            shell.exec(`node src/rakuten/FGS258-twitter/shell.js -r 4 -n cron11_03 -f ${filepath} -t 20190605`);
        }

        filepath = `${conf.dirPath_share_vb}/targets/targets_interior.txt`;
        if (fs.existsSync(filepath)) {
            shell.exec(`node src/rakuten/FGS258-twitter/shell.js -r 6 -n cron11_03 -f ${filepath} -t 20190605`);
        }

        filepath = `${conf.dirPath_share_vb}/targets/targets_kitchen.txt`;
        if (fs.existsSync(filepath)) {
            shell.exec(`node src/rakuten/FGS258-twitter/shell.js -r 5 -n cron11_03 -f ${filepath} -t 20190605`);
        }

        filepath = `${conf.dirPath_share_vb}/targets/targets_fashion.txt`;
        if (fs.existsSync(filepath)) {
            shell.exec(`node src/rakuten/FGS258-twitter/shell.js -r 2 -n cron11_03 -f ${filepath} -t 20190605`);
        }

        filepath = `${conf.dirPath_share_vb}/targets/targets_watch.txt`;
        if (fs.existsSync(filepath)) {
            shell.exec(`node src/rakuten/FGS258-twitter/shell.js -r 3 -n cron11_03 -f ${filepath} -t 20190605`);
        }

        filepath = `${conf.dirPath_share_vb}/targets/targets_life.txt`;
        if (fs.existsSync(filepath)) {
            shell.exec(`node src/rakuten/FGS258-twitter/shell.js -r 7 -n cron11_03 -f ${filepath} -t 20190605`);
        }
        
    }, function () {
        // error
        console.log(`Error ${today} : FGS258 twitter posts scrape cron21`);
    },
    true, 'Asia/Tokyo');
    

console.log('cron21(cron11_03) setting');

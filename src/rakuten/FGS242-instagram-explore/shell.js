'use strict';

const co = require('co');
const shell = require('shelljs');
const commandArgs = require('command-line-args');
const moment = require('moment');
const today = moment().format('YYYYMMDD');

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
]);

// run action
const run = function () {

    co(function* () {

        // 
        switch (cli['run']) {

            case 90: // total-data git
                shell.exec(`cd total-data && git add .`);
                shell.exec(`cd total-data && git add -u .`);
                shell.exec(`cd total-data && git commit -m 'up fgs242'`);
                shell.exec(`cd total-data && git pull origin master`);
                shell.exec(`cd total-data && git push origin master`);
                break;

            case 13: // life cron5
                shell.exec(`node src/rakuten/FGS242-instagram-explore/scraper.js -r 98 -C cron5`);
                shell.exec(`node src/rakuten/FGS242-instagram-explore/scraper.js -r 2 -C cron5 -o tor`);
                // shell.exec(`node src/rakuten/FGS242-instagram-explore/scraper.js -r 90 -C cron5`);
                // shell.exec(`node src/rakuten/FGS242-instagram-explore/shell.js -r 90`);
                break;

            case 12: // fashion cron4
                shell.exec(`node src/rakuten/FGS242-instagram-explore/scraper.js -r 98 -C cron4`);
                shell.exec(`node src/rakuten/FGS242-instagram-explore/scraper.js -r 2 -C cron4 -o tor`);
                // shell.exec(`node src/rakuten/FGS242-instagram-explore/scraper.js -r 90 -C cron4`);
                // shell.exec(`node src/rakuten/FGS242-instagram-explore/shell.js -r 90`);
                break;

            case 11: // food cron3
                shell.exec(`node src/rakuten/FGS242-instagram-explore/scraper.js -r 98 -C cron3`);
                shell.exec(`node src/rakuten/FGS242-instagram-explore/scraper.js -r 2 -C cron3 -o tor`);
                // shell.exec(`node src/rakuten/FGS242-instagram-explore/scraper.js -r 90 -C cron3`);
                // shell.exec(`node src/rakuten/FGS242-instagram-explore/shell.js -r 90`);
                break;

            case 3: // life cron5
                shell.exec(`node src/rakuten/FGS242-instagram-explore/scraper.js -r 98 -C cron5`);
                shell.exec(`node src/rakuten/FGS242-instagram-explore/scraper.js -r 2 -C cron5 -o tor`);
                shell.exec(`node src/rakuten/FGS242-instagram-explore/scraper.js -r 90 -C cron5`);
                shell.exec(`node src/rakuten/FGS242-instagram-explore/shell.js -r 90`);
                break;

            case 2: // fashion cron4
                shell.exec(`node src/rakuten/FGS242-instagram-explore/scraper.js -r 98 -C cron4`);
                shell.exec(`node src/rakuten/FGS242-instagram-explore/scraper.js -r 2 -C cron4 -o tor`);
                shell.exec(`node src/rakuten/FGS242-instagram-explore/scraper.js -r 90 -C cron4`);
                shell.exec(`node src/rakuten/FGS242-instagram-explore/shell.js -r 90`);
                break;

            case 1: // food cron3
            default:
                shell.exec(`node src/rakuten/FGS242-instagram-explore/scraper.js -r 98 -C cron3`);
                shell.exec(`node src/rakuten/FGS242-instagram-explore/scraper.js -r 2 -C cron3 -o tor`);
                shell.exec(`node src/rakuten/FGS242-instagram-explore/scraper.js -r 90 -C cron3`);
                shell.exec(`node src/rakuten/FGS242-instagram-explore/shell.js -r 90`);
                break;
        }
    });
}

run();
'use strict';

const shell = require('shelljs');
const cronJob = require('cron').CronJob;
const moment = require('moment');
const fs = require('fs');
const date = moment().format('YYYYMMDD');
const path_log = `../log/${date}.txt`;
let now = '';

/*
cronJob(`00 00 00 * * 1-5`)
Seconds: 0-59
Minutes: 0-59
Hours: 0-23
Day of Month: 1-31
Months: 0-11
Day of Week: 0-6
*/


// daily
new cronJob(`00 05 00 * * 0-6`, function () {
    
        shell.exec('npm run fgs45');

    }, function () {
        // error
        console.log(`Error fgs45 ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
    },
    true, 'Asia/Tokyo');

// recipe weekly
// new cronJob(`00 10 00 * * 0`, function () {

//         shell.exec('node src/rakuten/FGS283-recipe/shell.js -r 1');

//     }, function () {
//         // error
//         now = moment().format('YYYYMMDD HH:mm:ss');
//         fs.appendFileSync(path_log, `Error ${now} : stop recipe cron \n`, 'utf8');
//     },
//     true, 'Asia/Tokyo');

console.log('cron1 setting');
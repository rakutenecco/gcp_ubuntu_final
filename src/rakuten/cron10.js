'use strict';

const shell = require('shelljs');
const cronJob = require('cron').CronJob;
const moment = require('moment');
const fs = require('fs');
const date = moment().format('YYYYMMDD');
const today = moment().format('YYYY-MM-DD hh:mm:ss');
const path_log = `../log/${date}.txt`;
let now = '';
const conf = new (require('/var/app/total/src/conf.js'));

/*
cronJob(`00 00 00 * * 1-5`)
Seconds: 0-59
Minutes: 0-59
Hours: 0-23
Day of Month: 1-31
Months: 0-11
Day of Week: 0-6
*/

//// targets create ////
// FGS239 insta posts daily 0:05
// new cronJob(`00 05 00 * * 0-6`, function () {
//     shell.exec(`node src/rakuten/FGS239-insta-posts/targets_create.js`);

// }, function () {
//     // error
//     console.log(`Error ${today} : FGS239 insta-posts scrape cron10`);
// },
// true, 'Asia/Tokyo');


// FGS239 insta posts daily
//new cronJob(`00 06 00 * * 0-6`, function () {
new cronJob(`00 25 00 * * 0`, function () {
//new cronJob(`00 27 10 * * 5`, function () {    

    //WEAR
    let filename = 'targets_fashion.txt';
    shell.exec(`node src/rakuten/FGS239-wear-posts/shell.js -r 1 -n cron10 -f ${filename}`);

    // let filepath = `${conf.dirPath_share_vb}/targets/targets_fashion.txt`;
    // if (fs.existsSync(filepath)) {
    //     shell.exec(`node src/rakuten/FGS239-insta-posts/shell.js -r 3 -n cron10 -f ${filepath}`);
    // }


}, function () {
    // error
    console.log(`Error ${today} : FGS239 wear posts scrape cron10`);
},
true, 'Asia/Tokyo');

console.log('cron10 setting');

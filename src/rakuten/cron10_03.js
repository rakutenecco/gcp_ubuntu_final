'use strict';

const shell = require('shelljs');
const cronJob = require('cron').CronJob;
const moment = require('moment');
const fs = require('fs');
const date = moment().format('YYYYMMDD');
const today = moment().format('YYYY-MM-DD hh:mm:ss');
let yesterday;
const path_log = `../log/${date}.txt`;
let now = '';
const conf = new (require('/var/app/total/src/conf.js'));

/*
cronJob(`00 00 00 * * 1-5`)
Seconds: 0-59
Minutes: 0-59
Hours: 0-23
Day of Month: 1-31
Months: 0-11
Day of Week: 0-6
*/

//// 03 ////
// FGS239 insta posts
new cronJob(`00 09 00 * * 0`, function () {
//new cronJob(`00 10 09 * * 1`, function () {

    shell.exec(`node src/rakuten/FGS239-insta-posts/shell.js -r 1 -n cron10_03 -f targets_food_05_03.txt`);

    // let filepath5 = `${conf.dirPath_share_vb}/targets/targets_kitchen.txt`;
    // if (fs.existsSync(filepath5)) {
    //     shell.exec(`node src/rakuten/FGS239-insta-posts/shell.js -r 7 -n cron10_03 -f ${filepath5}`);
    // }

    // let filepath2 = `${conf.dirPath_share_vb}/targets/targets_watch.txt`;
    // if (fs.existsSync(filepath2)) {
    //     shell.exec(`node src/rakuten/FGS239-insta-posts/shell.js -r 4 -n cron10_03 -f ${filepath2}`);
    // }

    // let filepath3 = `${conf.dirPath_share_vb}/targets/targets_life.txt`;
    // if (fs.existsSync(filepath3)) {
    //     shell.exec(`node src/rakuten/FGS239-insta-posts/shell.js -r 5 -n cron10_03 -f ${filepath3}`);
    // }

    // let filepath = `${conf.dirPath_share_vb}/targets/targets_food_04_03.txt`;
    // if (fs.existsSync(filepath)) {
    //     shell.exec(`node src/rakuten/FGS239-insta-posts/shell.js -r 1 -n cron10_03 -f ${filepath}`);
    //     shell.exec(`node src/rakuten/FGS239-insta-posts/make_outfile.js`);
    // }

    // let filepath = `${conf.dirPath_share_vb}/targets/targets_food_05_03.txt`;
    // if (fs.existsSync(filepath)) {
    //     shell.exec(`node src/rakuten/FGS239-insta-posts/shell.js -r 1 -n cron10_03 -f ${filepath}`);
    //     //shell.exec(`node src/rakuten/FGS239-insta-posts/make_outfile.js -r 1`);
    //     //yesterday = moment().add(-1, 'days').format('YYYYMMDD');
    //     //shell.exec(`node src/rakuten/FGS239-insta-posts/make_outfile.js -r 1 -t ${yesterday}`);
    // }

}, function () {
    // error
    console.log(`Error ${today} : FGS239 insta-posts scrape cron10_03`);
},
true, 'Asia/Tokyo');

console.log('cron10_03 setting');

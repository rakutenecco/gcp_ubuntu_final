'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');
const moment = require('moment');

//config
const conf = new(require('../../conf.js'));
const db = new(require('../../modules/db.js'));
const util = new(require('../../modules/util.js'));
const cheerio = new(require('../../modules/cheerio-httpcli.js'));

//parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number},
    { name: 'type', alias: 't', type: String},
    { name: 'cron', alias: 'n', type: String}
]);

const today = moment().format('YYYYMMDD');
const yesterday =  moment().add(-1,'d').format('YYYYMMDD');

const cron =  cli['cron'] || 'cron0';
const filePath_db = `${conf.dirPath_db}/${cron}.db`;

let type = '';

if(cli['type'] == 1){
    type = 'category';
}else {
    type = 'product';
}

let outfile = `${conf.dirPath_share_vb}/daraz_${today}.txt`;
const sttime = moment().format("YYYY/MM/DD HH:mm:ss");

console.log(`/// Daraz scra[er.js start ${sttime} ///`);
console.log(`args type: ${type}`);

const init_table = function () {
    return new Promise((resolve, reject ) => {
        co(function* () {

            let sql = `DROP TABLE IF EXISTS daraz;`;
            yield db.do(`run`, sql, {});

            resolve();
        });
    });
}



const scrape_product = function(key,val){
   return new Promise((resolve, reject) => {
       co(function* () {
           let $ = yield cheerio.get(val);

           if($) {
               let product_main = $('body').html();
               console.log(product_main);
               
           }
       })
   })
    
}

const scrape_category = function () {

    return new Promise((resolve, reject) => {
        co(function* () {
           
            let url = `https://www.daraz.com.np/`;

            let $ = yield cheerio.get2(url);
            yield util.sleep(20);
            if ($){
                let main = $('body').html();

                let cat = $(main).find('.lzd-site-menu-grand > li > a');

                let catList = [];

                for(let i=0; i < cat.length; i++){
                    if($(cat).eq(i).attr('href')){
                        let url = $(cat).eq(i).attr('href');
                        catList.push(url);
                    }
                }

                if(catList.length > 0){
                    for(let c = 0; c < catList.length; c++){
                       let data = catList[c];
                       scrape_product(c,data.slice(16,data.length-1));
                    }
                }
            }
       
        
        });
    });
}

const run = function  () {
    co(function* () {
        //
        yield db.connect(filePath_db);

        if(!cli[`run`] || cli[`run`] == 1) {
            //scrape
            if(!cli[`type`] || cli[`type`] == 1) {
                //scrape
               scrape_category();
               //console.log('category');
    
            }
            else scrape_product(c, url);

        }


        if(!cli[`run`] || cli[`run`] == 2) {
            //output
           // yield setOutFile();

        }

        yield db.close();

        console.log(`//// Daraz scrapper.js end`)

    })
}

run();
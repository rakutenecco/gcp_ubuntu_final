'use strict';

const shell = require('shelljs');
const cronJob = require('cron').CronJob;
const moment = require('moment');
const fs = require('fs');
const date = moment().format('YYYYMMDD');
const today = moment().format('YYYY-MM-DD hh:mm:ss');
const path_log = `../log/${date}.txt`;
let now = '';
const conf = new (require('/var/app/total/src/conf.js'));

/*
cronJob(`00 00 00 * * 1-5`)
Seconds: 0-59
Minutes: 0-59
Hours: 0-23
Day of Month: 1-31
Months: 0-11
Day of Week: 0-6
*/

//// 02 ////
// FGS239 insta posts
//new cronJob(`00 05 00 * * 0-6`, function () {
new cronJob(`00 06 00 * * 0`, function () {
//new cronJob(`00 33 09 * * 1`, function () {

    shell.exec(`node src/rakuten/FGS239-insta-posts/shell.js -r 1 -n cron12_02 -f targets_wine.txt`);
    shell.exec(`node src/rakuten/FGS239-insta-posts/shell.js -r 1 -n cron12_02 -f targets_cosme.txt`);

    // let filepath3 = `${conf.dirPath_share_vb}/targets/targets_life.txt`;
    // if (fs.existsSync(filepath3)) {
    //     shell.exec(`node src/rakuten/FGS239-insta-posts/shell.js -r 5 -n cron12_02 -f ${filepath3}`);
    // }

    // let filepath = `${conf.dirPath_share_vb}/targets/targets_fashion.txt`;
    // if (fs.existsSync(filepath)) {
    //     shell.exec(`node src/rakuten/FGS239-insta-posts/shell.js -r 2 -n cron12_02 -f ${filepath}`);
    // }

}, function () {
    // error
    console.log(`Error ${today} : FGS239 insta-posts scrape cron12_02`);
},
true, 'Asia/Tokyo');


console.log('cron12_02 setting');

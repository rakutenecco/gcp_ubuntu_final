'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');

const conf = new(require('../conf.js'));
const db = new(require('../modules/db.js'));
const util = new(require('../modules/util.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
]);

let driver = null;


// init
const init = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield db.connect(conf.filePath_db);
            resolve();
        });
    });
}

// end
const end = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield db.close();
            resolve();
        });
    });
}

// end
const drop = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            const sql1 = `DROP TABLE IF EXISTS sample_test;`;
            yield db.do(`run`, sql1, {});
            resolve();
        });
    });
}

// do sample
const doSample = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            const sql1 = `CREATE TABLE IF NOT EXISTS sample_test(Num INTEGER primary key, id INTEGER, name TEXT, float NUMERIC);`;
            yield db.do(`run`, sql1, {});

            const sql2 = `INSERT OR REPLACE INTO sample_test(id, name, float) VALUES (?, ?, ?);`;
            const opt2 = [12, 'This is sample', 24.00];
            yield db.do(`run`, sql2, opt2);

            const sql3 = `SELECT * FROM sample_test`;
            const res = yield db.do(`all`, sql3, {});
            console.log(res);

            resolve();
        });
    });
}

// run action
const run = function () {

    co(function* () {

        switch (cli['run']) {
            case 2: // drop
                yield init();
                yield drop();
                yield end();
                break;
                // node src/sample/selenium.js -r 1 -B firefox
                // -B chrome, -B phantom, -B firefox
            case 1: // doSample
            default:
                yield init();
                yield doSample();
                yield end();
        }
    });
}

run();
'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');
const shell = require('shelljs');

const conf = new(require('../conf.js'));
const util = new(require('../modules/util.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
]);


// init
const init = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            resolve();
        });
    });
}

// end
const end = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            resolve();
        });
    });
}

// reflesh Total Data
const refleshTotalData = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            shell.exec(`rm -f -r total-data-bk`);
            shell.exec(`mv total-data total-data-bk`);
            shell.exec(`git clone https://RakuTaro:filebox1234@bitbucket.org/RakuTaro/total-data.git`);
            // shell.exec(`cd total-data && git remote set-url origin https://RakuTaro:filebox1234@bitbucket.org/RakuTaro/total-data.git`);
            shell.exec(`cd total-data && mkdir FGS41-google-trends`);
            shell.exec(`touch total-data/FGS41-google-trends/git.txt`);
            shell.exec(`cd total-data && mkdir FGS42-casio`);
            shell.exec(`touch total-data/FGS42-casio/git.txt`);
            shell.exec(`cd total-data && mkdir FGS44-seiko`);
            shell.exec(`touch total-data/FGS44-seiko/git.txt`);
            shell.exec(`cd total-data && mkdir FGS242-instagram-explore`);
            shell.exec(`touch total-data/FGS242-instagram-explore/git.txt`);
            shell.exec(`cd total-data && mkdir FGS258-twitter-explore`);
            shell.exec(`touch total-data/FGS258-twitter-explore/git.txt`);
            shell.exec(`cd total-data && git add .`);
            shell.exec(`cd total-data && git commit -m 'init'`);
            shell.exec(`cd total-data && git pull origin master`);
            shell.exec(`cd total-data && git push origin master`);

            resolve();
        });
    });
}


// run action
const run = function () {

    co(function* () {

        switch (cli['run']) {
            case 1: // do sample
            default:
                yield init();
                yield refleshTotalData();
                yield end();
        }
    });
}

run();
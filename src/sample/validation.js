'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');

const conf = new(require('../conf.js'));
const util = new(require('../modules/util.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
]);


// init
const init = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            resolve();
        });
    });
}

// end
const end = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            resolve();
        });
    });
}

// doSample
const doSample = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // check isText
            let val = 'This いず　テスト=1234567890';
            console.log('isText (1-20 length) | string > ', val, ' result > ', util.isText(val, 10, 20));
            console.log('isText (100-1000 length) | string > ', val, ' result > ', util.isText(val, 100, 1000));
            console.log('isText (1-100 length) | string > ', val, ' result > ', util.isText(val, 1, 100));

            val = 50000;
            console.log('isText (1-100 length) | number > ', val, ' result > ', util.isText(val, 1, 100));
            val = true;
            console.log('isText (1-100 length) | boolean > ', val, ' result > ', util.isText(val, 1, 100));
            val = { a: 1 };
            console.log('isText (1-100 length) | object > ', val, ' result > ', util.isText(val, 1, 100));
            val = [1, 'test'];
            console.log('isText (1-100 length) | object > ', val, ' result > ', util.isText(val, 1, 100));
            console.log('---');

            // check isAlphameric
            val = 'ThisIsTest1234567890ABCDE';
            console.log('isAlphameric (1-20 length) | string > ', val, ' result > ', util.isAlphameric(val, 10, 20));
            console.log('isAlphameric (100-1000 length) | string > ', val, ' result > ', util.isAlphameric(val, 100, 1000));
            console.log('isAlphameric (1-100 length) | string > ', val, ' result > ', util.isAlphameric(val, 1, 100));

            val = 'ABC***';
            console.log('isAlphameric (1-100 length) | string > ', val, ' result > ', util.isAlphameric(val, 1, 100));
            val = '日本語123';
            console.log('isAlphameric (1-100 length) | string > ', val, ' result > ', util.isAlphameric(val, 1, 100));
            val = 50000;
            console.log('isAlphameric (1-100 length) | number > ', val, ' result > ', util.isAlphameric(val, 1, 100));
            val = true;
            console.log('isAlphameric (1-100 length) | boolean > ', val, ' result > ', util.isAlphameric(val, 1, 100));
            val = { a: 1 };
            console.log('isAlphameric (1-100 length) | object > ', val, ' result > ', util.isAlphameric(val, 1, 100));
            val = [1, 'test'];
            console.log('isAlphameric (1-100 length) | object > ', val, ' result > ', util.isAlphameric(val, 1, 100));
            console.log('---');

            resolve();
        });
    });
}


// run action
const run = function () {

    co(function* () {

        switch (cli['run']) {
            case 1: // do sample
            default:
                yield init();
                yield doSample();
                yield end();
        }
    });
}

run();
'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');
const shell = require('shelljs');

const conf = new(require('../conf.js'));
const selenium = new(require('../modules/selenium.js'));
const util = new(require('../modules/util.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
    { name: 'browser', alias: 'B', type: String },
]);

let driver = null;
let driver2 = null;

// init
const init = function (opt) {

    return new Promise((resolve, reject) => {
        co(function* () {

            opt = opt || {};
            if (opt.inVisible) {
                driver = yield selenium.init(cli['browser'], { inVisible: true });
            } else {
                driver = yield selenium.init(cli['browser']);
            }

            resolve();
        });
    });
}

// end
const end = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            driver.quit();

            resolve();
        });
    });
}

// do sample
const doSendkey = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            driver.get('http://www.google.com/ncr');
            yield util.sleep(3000);
            let ele1 = yield selenium.getElements(driver, '.gsfi');
            ele1[0].sendKeys('webdriver'); // , Key.RETURN
            yield util.sleep(150000);
            driver.quit();

            resolve(res);
        });
    });
}

// do sample
const doFullImage = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            driver.get('https://www.instagram.com/mensfashionpost/');

            // scroll
            for (let i = 0; i < 5; i++) {
                yield selenium.scroll(driver, 'footer');
                yield util.sleep(1500);
            }

            let $ = yield selenium.parseBody(driver);

            let res = [];
            $('a').each(function (i, e) {
                res.push($(e).attr('href'));
            });
            console.log(res);

            resolve(res);
        });
    });
}

// do double
const doDouble = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // get url
            let res = yield doSingle();

            // open 2nd window
            driver2 = yield selenium.init(cli['browser']);
            let url = res[0] && res[0].url ? res[0].url : 'https://ejje.weblio.jp/';
            driver2.get(url);
            yield util.sleep(3000);

            let $ = yield selenium.parseBody(driver2);
            $('a').each(function (i, e) {
                console.log('double > ', $(e).attr('href'));
            });

            // close 2nd window
            driver2.quit();
            yield util.sleep(1500);

            resolve();
        });
    });
}


// do sample
const doSingle = function (isStop) {

    return new Promise((resolve, reject) => {
        co(function* () {

            isStop = isStop || false;
            driver.get('https://www.google.co.jp/search?q=sample');

            yield selenium.scroll(driver, '#xfootw');
            yield util.sleep(3000);

            let $ = yield selenium.parseBody(driver);

            let res = [];
            $('.r').each(function (i, e) {
                res.push({
                    txt: $(e).text(),
                    url: $(e).find('a').attr('href')
                });
            });
            console.log(res);

            // stop throw error
            if (isStop) {
                throw new Error('this is Intentional Error !');
            }

            resolve(res);
        });
    });
}

// run action
const run = function () {

    co(function* () {

        switch (cli['run']) {

            // node src/sample/selenium.js -r 5 -B chromium
            case 6: // sendkey
                yield init({ inVisible: true });
                yield doSendkey();
                yield end();
                break;

                // node src/sample/selenium.js -r 5 -B chromium
            case 5: // image no display
                yield init({ inVisible: true });
                yield doFullImage();
                yield end();
                break;

                // node src/sample/selenium.js -r 4
                // -B chrome, -B phantom, -B firefox
            case 4: // error stoping chrome is closed (all chrome is closed)
                shell.exec('node src/sample/selenium.js -r 3 -B chromium');
                yield util.sleep(4000);
                shell.exec('pkill chromium');
                break;

                // node src/sample/selenium.js -r 3 -B chromium
                // -B chrome, -B phantom, -B firefox
            case 3: // throw error when it was a single windows
                yield init();
                yield doSingle(true);
                yield end();
                break;

                // node src/sample/selenium.js -r 2 -B firefox
                // -B chrome, -B phantom, -B firefox
            case 2: // open double windows
                yield init();
                yield doDouble();
                yield end();
                break;

                // node src/sample/selenium.js -r 1 -B firefox
                // -B chrome, -B phantom, -B firefox
            case 1: // open and scroll single window and get data from google.com
            default:
                yield init();
                yield doSingle();
                yield end();
                break;
        }
    });
}

run();
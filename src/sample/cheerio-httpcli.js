'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');

const conf = new(require('../conf.js'));
const cheerio = new(require('../modules/cheerio-httpcli.js'));
const facebook = new(require('../modules/facebook.js'));
const util = new(require('../modules/util.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
    { name: 'browser', alias: 'B', type: String },
]);

let driver = null;


// init
const init = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield cheerio.init(10000);
            resolve();
        });
    });
}

// cheerio
const getData = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let ip = '';
            let port = '';
            let url = 'http://www.stat.go.jp/';

            // yield cheerio.setProxy(ip, port);
            let $ = yield cheerio.get(url);
            console.log($('title').text());
            console.log($('h2').text());
            resolve();
        });
    });
}

// cheerio with tor
const getDataWithTor = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let url = 'https://browserleaks.com/ip';
            let $ = yield cheerio.get(url, 5, null, true);
            if ($) {
                console.log($('#geoip-country-flag').text());
                console.log($('#client-ip').text());
            }

            resolve();
        });
    });
}

// run action
const run = function () {

    co(function* () {

        switch (cli['run']) {

            // 
            case 2: // get browserleaks ip changed by tor
                yield getDataWithTor();
                break;

            case 1: // get facebook page
            default:
                yield init();
                yield getData();
        }
    });
}

run();
'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');

const conf = new(require('../conf.js'));
const phantom = new(require('../modules/phantom.js'));
const util = new(require('../modules/util.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
]);


// init
const init = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield phantom.init();
            resolve();
        });
    });
}

// end
const end = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield phantom.end();
            resolve();
        });
    });
}

// doSample
const doSample = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let url = 'https://www.npmjs.com/package/phantom';
            let isOpen = yield phantom.open(url);
            if (isOpen) {
                let contents = yield phantom.getContents();
                let $ = contents.body;
                console.log('contents.title >> ', contents.title);
                console.log('contents.url >> ', contents.url);
                console.log('contents.body h1 >> ', $('h1.package-name').text().replace(/[\t\n\r\s ]/g, ''));
                console.log('contents.head title >> ', contents.head.match(/<title>(.*?)<\/title>/i)[0]);
            }
            resolve();
        });
    });
}

// loginNoneIntra
const loginNoneIntra = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let url = 'http://guest-wifi-portal.rakuten-it.com/fs/customwebauth/login.html?switch_url=http://guest-wifi-portal.rakuten-it.com/login.html';
            let isOpen = yield phantom.open(url);
            if (isOpen) {
                yield util.sleep(500)
                yield phantom.getScreenShot('/var/app/sample.jpg',500)
                yield phantom.click('#button_1')
                yield phantom.getScreenShot('/var/app/sample2.jpg',500)
            }
            resolve();
        });
    });
}


// run action
const run = function () {

    co(function* () {

        switch (cli['run']) {
            case 3: // login none intra pc network
                yield phantom.loginNoneIntra();
                break;

            case 2: // login none intra pc network
                yield init();
                yield loginNoneIntra();
                yield end();
                break;

            case 1: // do sample
            default:
                yield init();
                yield doSample();
                yield end();
        }
    });
}

run();
'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');

const conf = new(require('../conf.js'));
const facebook = new(require('../modules/facebook.js'));
const util = new(require('../modules/util.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
    { name: 'browser', alias: 'B', type: String },
]);

let driver = null;


// init
const init = function () {

    return new Promise((resolve, reject) => {
        co(function* () {


            resolve();
        });
    });
}

// getToken
const getToken = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let token = yield facebook.getToken();
            console.log(token);
            resolve();
        });
    });
}

// get facebook page
const getFBpage = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let fbdata = yield facebook.getFBpage();
            console.log(fbdata);
            resolve();
        });
    });
}

// run action
const run = function () {

    co(function* () {

        switch (cli['run']) {
            case 2: // get access_token
                yield getToken();
                break;
            case 1: // get facebook page
            default:
                yield init();
                yield getFBpage();
        }
    });
}

run();
'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');

const conf = new(require('../conf.js'));
const util = new(require('../modules/util.js'));
const textGoogle = new(require('../modules/text_google.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
]);

// init
const init = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            textGoogle.init();
            resolve();
        });
    });
}

// end
const end = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            resolve();
        });
    });
}

// doSample
const doSample1 = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let text = 'Michelangelo Caravaggio, Italian painter, is known for The Calling of Saint Matthew.';

            // plain text
            let res = yield textGoogle.getAnalyzeSentiment(text);
            console.log(res);

            resolve();
        });
    });
}

// doSample
const doSample2 = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let text = 'セイコー 社員が出演する企業イメージ動画を公開しました。';

            // plain text
            let res = yield textGoogle.getAnalyzeEntities(text);

            console.log(res);

            resolve();
        });
    });
}

// doSample
const doSample3 = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let text = '彼はgoogleの2番手だ！';

            // plain text
            let res = yield textGoogle.getAnalyzeSyntax(text);

            console.log(res);

            resolve();
        });
    });
}





// run action
const run = function () {

    co(function* () {

        switch (cli['run']) {

            case 3: // do sample 3
                yield init();
                yield doSample3();
                yield end();
                break;

            case 2: // do sample 2
                yield init();
                yield doSample2();
                yield end();
                break;

            case 1: // do sample 1
            default:
                yield init();
                yield doSample1();
                yield end();
                break;
        }
    });
}

run();
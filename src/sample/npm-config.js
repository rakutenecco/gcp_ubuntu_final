'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');
const shell = require('shelljs');

const conf = new(require('../conf.js'));
const util = new(require('../modules/util.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
    { name: 'browser', alias: 'B', type: String },
]);

let driver = null;


// init
const init = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            resolve();
        });
    });
}

// end
const end = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            resolve();
        });
    });
}

// do sample
const doSample = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // set config to npm gloval key:foo val:bar
            shell.exec(`npm config set foo bar`);

            // show list
            shell.exec(`npm config list`);

            // show direct for via npm package.json
            console.log('show direct >> ', process.env.npm_config_foo);
            shell.exec(`cd src/sample; npm run sample-npm-config;`);

            // set config to npm local(total) key:foo2 val:bar2
            shell.exec(`npm config set total:foo2 bar2`);

            // show list
            shell.exec(`npm config list`);

            // show direct for via npm package.json
            console.log('show direct local(total) >> ', process.env.npm_package_config_foo2)
            shell.exec(`cd src/sample; npm run sample-npm-config2;`);

            resolve();
        });
    });
}

// get conf
const getConf = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            console.log('show via npm package.json >> ', process.env.npm_config_foo);
            resolve();
        });
    });
}

// get conf 2
const getConf2 = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            console.log('show via npm package.json local(total) >> ', process.env.npm_package_config_foo2);
            resolve();
        });
    });
}

// run action
const run = function () {

    co(function* () {

        switch (cli['run']) {
            case 3: // npm run > get conf
                yield getConf2();
                break;
            case 2: // npm run > get conf
                yield getConf();
                break;
            case 1: // doSample
            default:
                yield init();
                yield doSample();
        }
    });
}

run();
use strict';

const shell = require('shelljs');
const cronJob = require('cron').CronJob;
const moment = require('moment');
const fs = require('fs');
const date = moment().format('YYYYMMDD');
const path_log = `../log/${date}.txt`;
let now = '';

/*
cronJob(`00 00 00 * * 1-5`)
Seconds: 0-59
Minutes: 0-59
Hours: 0-23
Day of Month: 1-31
Months: 0-11
Day of Week: 0-6
*/


//cron3
// instagram-explore daily
new cronJob(`00 03 16 * * 1`, function () {
        //twitter
        shell.exec(`node src/rakuten/FGS258-twitter-explore/scraper.js -r 99 -C cron3 -t 20190803`);
        for (let i = 0; i < 2; i++) {
            shell.exec(`node src/rakuten/FGS258-twitter-explore/scraper.js -r 2 -C cron3 -t 20190803`);
            }
        shell.exec(`node src/rakuten/FGS258-twitter-explore/scraper.js -r 90 -C cron3 -t 20190803`);

        //instagram
        shell.exec(`node src/rakuten/FGS242-instagram-explore/scraper.js -r 98 -C cron3 -t 20190803`);
        shell.exec(`node src/rakuten/FGS242-instagram-explore/scraper.js -r 2 -C cron3 -o tor -t 20190803`);


        //twitter
        for (let i = 0; i < 2; i++) {
            shell.exec(`node src/rakuten/FGS258-twitter-explore/scraper.js -r 2 -C cron3 -t 20190803`);
        }
        shell.exec(`node src/rakuten/FGS258-twitter-explore/scraper.js -r 90 -C cron3 -t 20190803`);
        shell.exec(`node src/rakuten/FGS258-twitter-explore/shell.js -r 90`);

        //instagram
        shell.exec(`node src/rakuten/FGS242-instagram-explore/scraper.js -r 98 -C cron3 -t 20190803`);
        shell.exec(`node src/rakuten/FGS242-instagram-explore/scraper.js -r 2 -C cron3 -o tor -t 20190803`);

        shell.exec(`node src/rakuten/FGS242-instagram-explore/scraper.js -r 98 -C cron3 -t 20190803`);
        shell.exec(`node src/rakuten/FGS242-instagram-explore/scraper.js -r 2 -C cron3 -o tor -t 20190803`);
        shell.exec(`node src/rakuten/FGS242-instagram-explore/scraper.js -r 90 -C cron3 -t 20190803`);
        shell.exec(`node src/rakuten/FGS242-instagram-explore/shell.js -r 90`);

    }, function () {
        // error
        now = moment().format('YYYYMMDD HH:mm:ss');
        fs.appendFileSync(path_log, `Error ${now} : stop google-trend weekly cron \n`, 'utf8');
    },
    true, 'Asia/Tokyo');

//cron4
// instagram-explore daily
new cronJob(`00 04 16 * * 1`, function () {
        //twitter
        shell.exec(`node src/rakuten/FGS258-twitter-explore/scraper.js -r 99 -C cron4 -t 20190803`);
        for (let i = 0; i < 2; i++) {
            shell.exec(`node src/rakuten/FGS258-twitter-explore/scraper.js -r 2 -C cron4 -t 20190803`);
        }
        shell.exec(`node src/rakuten/FGS258-twitter-explore/scraper.js -r 90 -C cron4 -t 20190803`);


        //instagram
        shell.exec(`node src/rakuten/FGS242-instagram-explore/scraper.js -r 98 -C cron4 -t 20190803`);
        shell.exec(`node src/rakuten/FGS242-instagram-explore/scraper.js -r 2 -C cron4 -o tor -t 20190803`);


        //twitter
        for (let i = 0; i < 2; i++) {
            shell.exec(`node src/rakuten/FGS258-twitter-explore/scraper.js -r 2 -C cron4 -t 20190803`);
        }
        shell.exec(`node src/rakuten/FGS258-twitter-explore/scraper.js -r 90 -C cron4 -t 20190803`);
        shell.exec(`node src/rakuten/FGS258-twitter-explore/shell.js -r 90`);

        //instagram
        shell.exec(`node src/rakuten/FGS242-instagram-explore/scraper.js -r 98 -C cron4 -t 20190803`);
        shell.exec(`node src/rakuten/FGS242-instagram-explore/scraper.js -r 2 -C cron4 -o tor -t 20190803`);

        shell.exec(`node src/rakuten/FGS242-instagram-explore/scraper.js -r 98 -C cron4 -t 20190803`);
        shell.exec(`node src/rakuten/FGS242-instagram-explore/scraper.js -r 2 -C cron4 -o tor -t 20190803`);
        shell.exec(`node src/rakuten/FGS242-instagram-explore/scraper.js -r 90 -C cron4 -t 20190803`);
        shell.exec(`node src/rakuten/FGS242-instagram-explore/shell.js -r 90`);



    }, function () {
        // error
        now = moment().format('YYYYMMDD HH:mm:ss');
        fs.appendFileSync(path_log, `Error ${now} : stop google-trend weekly cron \n`, 'utf8');
    },
    true, 'Asia/Tokyo');



//cron5
new cronJob(`00 05 16 * * 1`, function () {
        //twitter
        shell.exec(`node src/rakuten/FGS258-twitter-explore/scraper.js -r 99 -C cron5 -t 20190803`);
        for (let i = 0; i < 2; i++) {
            shell.exec(`node src/rakuten/FGS258-twitter-explore/scraper.js -r 2 -C cron5 -t 20190803`);
        }
        shell.exec(`node src/rakuten/FGS258-twitter-explore/scraper.js -r 90 -C cron5 -t 20190803`);

        //instagram
        shell.exec(`node src/rakuten/FGS242-instagram-explore/scraper.js -r 98 -C cron5 -t 20190803`);
        shell.exec(`node src/rakuten/FGS242-instagram-explore/scraper.js -r 2 -C cron5 -o tor -t 20190803`);

        //twitter
        for (let i = 0; i < 2; i++) {
            shell.exec(`node src/rakuten/FGS258-twitter-explore/scraper.js -r 2 -C cron5 -t 20190803`);
        }
        shell.exec(`node src/rakuten/FGS258-twitter-explore/scraper.js -r 90 -C cron5 -t 20190803`);
        shell.exec(`node src/rakuten/FGS258-twitter-explore/shell.js -r 90`);

        //instagram
        shell.exec(`node src/rakuten/FGS242-instagram-explore/scraper.js -r 98 -C cron5 -t 20190803`);
        shell.exec(`node src/rakuten/FGS242-instagram-explore/scraper.js -r 2 -C cron5 -o tor -t 20190803`);


        shell.exec(`node src/rakuten/FGS242-instagram-explore/scraper.js -r 98 -C cron5 -t 20190803`);
        shell.exec(`node src/rakuten/FGS242-instagram-explore/scraper.js -r 2 -C cron5 -o tor -t 20190803`);
        shell.exec(`node src/rakuten/FGS242-instagram-explore/scraper.js -r 90 -C cron5 -t 20190803`);
        shell.exec(`node src/rakuten/FGS242-instagram-explore/shell.js -r 90`);


    }, function () {
        // error
        now = moment().format('YYYYMMDD HH:mm:ss');
        fs.appendFileSync(path_log, `Error ${now} : stop google-trend weekly cron \n`, 'utf8');
    },
    true, 'Asia/Tokyo');

console.log('clone setting');
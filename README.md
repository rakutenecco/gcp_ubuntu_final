
## pre setting
Before clone total, you have to set this enviroment.

#### make app directory
Make /var/app for user account.
```
cd /var
sudo mkdir app
sudo chown <user>:<user> app
sudo chmod 777 app
ls -l
```
Check `ls -l`,  
then you can find `drwxrwxrwx`, `user` and `user`.
```
drwxrwxrwx  4 <user> <user> 4096 jul 15 02:04 app
drwxr-xr-x  2 root      root      4096 jul 14 12:18 backups
...
```

#### visual studio code
Download deb file from web site.  
<https://code.visualstudio.com/Download>  
Use this command
```
cd <where deb file>
sudo dpkg -i <code_..._amd64.deb>
code
```

#### node install
Set node version as 7.5.0.
```
sudo apt-get install -y nodejs npm
sudo npm cache clean
npm config set ca null
sudo npm install n -g
sudo n 7.5.0
```


## settings

#### install Java
Install Java as version 1.8.0
```
sudo add-apt-repository ppa:openjdk-r/ppa
sudo apt-get update
sudo apt-get install openjdk-8-jdk
java -version
```
> (reference)  
> https://qiita.com/ytkumasan/items/0a6b9e512e3dd5c08a31
> https://qiita.com/ayihis@github/items/01f95d5d465168043ae3


#### Canvas elements
Update canvas elements
```
sudo apt-get install libcairo2-dev libjpeg-dev libpango1.0-dev libgif-dev build-essential g++
```


#### Download total
Clone total under ./app .
```
cd /var/app
git clone https://ts-satohiro@bitbucket.org/rakutenecco/test20171201.git
mv test20171201/ total/
```


#### Npm install
Download node_modules
```
cd /var/app/total
npm install
```


#### Download total/src/modules
Clone modules under ./src .
```
cd /var/app/total/src
git clone https://ts-satohiro@bitbucket.org/rakutenecco/modules-20171206.git
mv modules-20171206/ modules/
```


#### init total
Run initialization for total
```
cd /var/app/total
npm run init
```


#### sqlite3 browser
Install sqlitebrowser
```
sudo apt-get install sqlitebrowser
sqlitebrowser
```


#### python
Install python
```
sudo apt-get -y install python3-pip
```


#### tor browser
Install tor browser  
Check gloval IP for usual `curl -sL ipinfo.io`  
Check gloval IP with tor `curl -sL ipinfo.io --socks5 127.0.0.1:9050`
```
sudo apt-get install -y tor curl
curl -sL ipinfo.io
curl -sL ipinfo.io --socks5 127.0.0.1:9050
```


#### chromium browser
Install chromium browser  
```
sudo dpkg -i chromium-browser_65.0.3325.181-0ubuntu0.14.04.1_amd64.deb
chromium-browser
```
>*if libnss3 and chromium-codecs-ffmpeg-extra are old version*  
> download new versions and install them
>><https://packages.ubuntu.com/trusty/amd64/libnspr4/download>  
>><https://packages.ubuntu.com/ja/trusty/amd64/libnss3/download>  
>><https://packages.ubuntu.com/trusty/amd64/chromium-codecs-ffmpeg-extra/download>  
>`sudo dpkg -i <libnspr4_..._amd64.deb>`  
>`sudo dpkg -i <libnss3_..._amd64.deb>`  
>`sudo dpkg -i <chromium-codecs-ffmpeg-extra_..._amd64.deb>`  

>*if chrome is already installed, use this command before install chromium*  
>`sudo apt-get remove google-chrome-stable`
>`sudo dpkg -l | grep chromi` # check related packages (unity-scope-chromiumbookmarks is not need to be removed)
>`sudo apt-get -r <package name>` # remove or purge all related packages
>`sudo apt-get --purge <package name>` # remove or purge all related packages
>`sudo dpkg -l | grep chromi` # recheck chromium related packages
> if all related packages are removed, install chromium packages.


#### silversearcher
Install silversearcher
```
sudo apt-get install silversearcher-ag
cd /var/app/total
ag test
```


#### Download total/total-data
Clone total-data under ./total .
The password is `filebox1234`
```
cd /var/app/total
git clone https://RakuTaro@bitbucket.org/RakuTaro/total-data.git
cd total-data/
git remote set-url origin https://RakuTaro:filebox1234@bitbucket.org/RakuTaro/total-data.git
```

#### MeCab
MeCab is a morphological analysis for Japanese text.  
There are 3 steps, install mecab, add mecab-ipadic-neologd library, and adapt user library.
##### Install Mecab and check
```
sudo apt-get install mecab libmecab-dev mecab-ipadic mecab-ipadic-utf8
mecab
    安倍晋三首相
```
Check the morphological analysis of "安倍晋三首相" is 4 proper noun.
>`安倍  名詞,固有名詞,人名,姓,*,*,安倍,アベ,アベ`  
>`晋 名詞,固有名詞,人名,名,*,*,晋,ススム,ススム`  
>`三 名詞,数,*,*,*,*,三,サン,サン`  
>`首相  名詞,一般,*,*,*,*,首相,シュショウ,シュショー`
##### Add mecab-ipadic-neologd library and check
```
cd /var/app/total/src/mecab
git clone --depth 1 https://github.com/neologd/mecab-ipadic-neologd.git
cd mecab-ipadic-neologd
./bin/install-mecab-ipadic-neologd -n -a
sudo vim /etc/mecabrc
    ;dicdir = /var/lib/mecab/dic/debian
    dicdir = /usr/lib/mecab/dic/mecab-ipadic-neologd
mecab
    安倍晋三首相
```
Check the morphological analysis of "安倍晋三首相" is 1 proper noun.
>`安倍晋三首相  名詞,固有名詞,一般,*,*,*,安倍晋三,アベシンゾウシュショウ,アベシンゾウシュショー`  

Then, check by `sample.py`  
```
sudo apt-get install mecab mecab-ipadic-utf8 libmecab-dev swig
sudo pip3 install mecab-python3
cd /var/app/total/src/mecab
python3 sample.py -c 1
```
Check the morphological analysis of "安倍晋三首相" is 1 proper noun.
>`安倍晋三首相	アベシンゾウシュショウ	安倍晋三	名詞-固有名詞-一般	`  
>`は	ハ	は	助詞-係助詞`  
>`、	、	、	記号-読点`  
>`国会	コッカイ	国会	名詞-一般	`  
>`で	デ	で	助詞-格助詞-一般`  
>`施政方針演説	シセイホウシンエンゼツ	施政方針演説	名詞-固有名詞-一般	`  
>`を	ヲ	を	助詞-格助詞-一般`  
>`行っ	オコナッ	行う	動詞-自立	五段・ワ行促音便	連用タ接続`  
>`た	タ	た	助動詞	特殊・タ	基本形`  
>`。	。	。	記号-句点`  
>`EOS`  

<https://qiita.com/elm200/items/2c2aa2093e670036bb30>  

##### Adapt user library
The user library is created by `/var/app/total/src/mecab/user_dic.txt`, and it is out to `/var/app/total/src/mecab/userdic/user_dic.dic`
```
cd /var/app/total/src/mecab
mkdir userdic
/usr/lib/mecab/mecab-dict-index -d /usr/lib/mecab/dic/mecab-ipadic-neologd/  -u userdic/user_dic.dic -f utf-8 -t utf-8 user_dic.txt
sudo vim /etc/mecabrc
    ;userdic = /home/foo/bar/user.dic
    userdic = /var/app/total/src/mecab/userdic/user_dic.dic
python3 sample.py -c 2
```
Check the morphological analysis of "安倍晋三首相" is 1 proper noun.  
Check the morphological analysis of "カシオ" contains separater "<__>".
>`安倍晋三首相	名詞,固有名詞,一般,*,*,*,安倍晋三,アベシンゾウシュショウ,アベシンゾウシュショー`  
>`が	助詞,格助詞,一般,*,*,*,が,ガ,ガ`  
>`、	記号,読点,*,*,*,*,、,、,、`  
>`カシオ	名詞,固有名詞,*,*,*,*,"カシオ",*,ブランド<__>ブランド名<__>カシオ<__>カシオ<__>0<__>0<__>0`  
>`の	助詞,連体化,*,*,*,*,の,ノ,ノ`  
>`時計	名詞,一般,*,*,*,*,時計,トケイ,トケイ`  
>`を	助詞,格助詞,一般,*,*,*,を,ヲ,ヲ`  
>`買っ	動詞,自立,*,*,五段・ワ行促音便,連用タ接続,買う,カッ,カッ`  
>`て	助詞,接続助詞,*,*,*,*,て,テ,テ`  
>`い	動詞,非自立,*,*,一段,連用形,いる,イ,イ`  
>`た	助動詞,*,*,*,特殊・タ,基本形,た,タ,タ`  
>`。	記号,句点,*,*,*,*,。,。,。`  
>`EOS`  








## Other setting
other setting for use total

#### jenkins
Run jenkins
```
npm run jenkins
```
Check url <http://localhost:8090/>

Then, please install "HTML Publisher" pulgin  
Manage Jenkins > Manage Plugins > Available > HTML Publisher

Set Nat Port Forwarding  
setting > network > Port Forwarding > host IP: 10.48.59.198(fixed host IP) 8085(port)   guest IP: 127.0.0.1(locahost IP) 8085(port)

#### Fix IP (virtualbox)
##### gest: ubuntuから host:windowsへの操作
root権限で、/etc/network/interfaces の末尾に次のように書き込む。  
```
auto eth1
iface eth1 inet static
address 192.168.56.99
netmask 255.255.255.0
```
ubuntu再起動
```
sudo /etc/init.d/networking restart
```

##### host: windowsから gest:ubuntuへの操作
virtualboxの環境設定 > ネットワーク > ホストオンリーネットワーク  
IPv4: 192.168.56.98   ネットマスク: 255.255.255.0


#### Bookmark
##### download
[visual studio code](https://code.visualstudio.com/Download)  

##### util
[ローマ字⇔ひらがな⇔漢字かな交じり文 を相互変換](http://anti.rosx.net/etc/tools/rome.php)  


'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const cronJob = require('cron').CronJob;
const fs = require('fs');
const moment = require('moment');
const shell = require('shelljs');

// def
const today = moment().format('YYYYMMDD');


// amazon-brands-FGS16 scrape > am 2:30, every day
new cronJob(`00 30 2 * * 0-6`, function(){
    shell.exec('node src/rakuten/amazon-brands-FGS166/shell.js -r 1');
}, function(){
    // error
    console.log(`Error ${today} : amazon-brands-FGS16 scrape`);
},
true, 'Asia/Tokyo');

// amazon-brands-FGS16 drop > pm 10:00, every Friday
new cronJob(`00 00 22 * * 5`, function(){
    shell.exec('node src/rakuten/amazon-brands-FGS166/shell.js -r 2');
}, function(){
    // error
    console.log(`Error ${today} : amazon-brands-FGS16 drop`);
},
true, 'Asia/Tokyo');

// FGS44-seiko scrape > pm 01:05, every Wednesday
new cronJob(`00 05 1 * * 3`, function(){
    shell.exec('node src/rakuten/FGS44-seiko/shell.js');
}, function(){
    // error
    console.log(`Error ${today} : FGS44-seiko scrape`);
},
true, 'Asia/Tokyo');


console.log('cron start');

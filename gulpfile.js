var gulp = require('gulp');
var prettify = require('gulp-jsbeautifier');

// beautify js, css, html files
gulp.task('prettify', function() {
    gulp.src(['./src/**/*.css', './src/**/*.html', './src/**/*.js'])
    .pipe(prettify({
        js: {
            // Rules added here apply only to JS files. They take precedence over all
            // of the settings in the 'all' category above.

            // You can add other .jsbeautifyrc rules in this section too.

            // [collapse|collapse-preserve-inline|expand|end-expand|none] Put braces on the same line as control statements (default), or put braces on own line (Allman / ANSI style), or put end braces on own line, or attempt to keep them where they are
            "brace_style": "collapse-preserve-inline",
        
            // Break chained method calls across subsequent lines
            "break_chained_methods": false,
    
            // Put commas at the beginning of new line instead of end
            "comma_first": false,
    
            // Pass E4X xml literals through untouched
            "e4x": false,
    
            // If true, then jslint-stricter mode is enforced
            "jslint_happy": false,
    
            // Preserve array indentation
            "keep_array_indentation": false,
    
            // Preserve function indentation
            "keep_function_indentation": false,
    
            // [before-newline|after-newline|preserve-newline] Set operator position
            "operator_position": "before-newline",
    
            // Should the space before an anonymous function's parens be added, "function()" vs "function ()"
            "space_after_anon_function": true,
    
            // Should the space before conditional statement be added, "if(true)" vs "if (true)"
            "space_before_conditional": true,
    
            // Add padding spaces within empty paren, "f()" vs "f( )"
            "space_in_empty_paren": false,
    
            // Add padding spaces within paren, ie. f( a, b )
            "space_in_paren": false,
    
            // Should printable characters in strings encoded in \xNN notation be unescaped, "example" vs "\x65\x78\x61\x6d\x70\x6c\x65"
            "unescape_strings": false,

            // Number of line-breaks to be preserved in one chunk
            "max_preserve_newlines": 4
        }
      }))
    .pipe(gulp.dest('./src'));
});

// run task
gulp.task('default', ['prettify']);